function images = loadKittiImages( path, first, last, format )
%LOADKITTIIMAGES Load Kitti images
%   Load a set of images from specified filespath
if ~exist('format', 'var')
    format = '/%010d.png';
end
if ~exist('first', 'var')
    d = dir([path, '/*.png']);
    last = length(d(not([d.isdir]))) - 1;
    first = 0;
end
if ~exist('last','var')
    last = first;
end
file_s = [path format];
% if exist(path, 'dir')
%     file_s = strcat(path, format);
%     if ~exist('first','var')
%         last_ = length(dir(path))-3; % consider . and .. in directory
%     end
% else
%     if exist(path, 'file')
%         images = im2double(imread(path));
%         return
%     else
%         file_s = path;
%     end
% end

images = cell(last-first+1, 1);
for i = 0 : last - first
    images{i+1} = im2double(imread(sprintf(file_s, i + first)));
end

if length(images) <= 1
    images = images{1};
end
end