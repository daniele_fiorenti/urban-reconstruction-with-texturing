function [ p_out ] = intelliProject( p_in, T, frame_id, origin, veh_speed)
%PROJECTTOIMAGE Projects points to image plane
% input points should be in homogeneous coordinate system
% get the principal plane normal
if ~exist('frame_id', 'var') || frame_id < 0
    f = -1;
else
    f  = frame_id;
end

if ~exist('origin', 'var')
    origin = 0;
end

if ~exist('veh_speed', 'var')
    veh_speed = 0;
end

if f < 50
    p_out = projectToImage(p_in, T);
elseif f < 80
    p_out = smartProjectToImage(p_in, T, origin, veh_speed, -12.0);
elseif f < 110
    p_out = smartProjectToImage(p_in, T, origin, veh_speed, -0.5);
elseif f < 133
    p_out = smartProjectToImage(p_in, T, origin, veh_speed, -20.0);
elseif f < 142
    p_out = smartProjectToImage(p_in, T, origin, veh_speed, -24.0);
elseif f < 180
    p_out = smartProjectToImage(p_in, T, origin, veh_speed, 1.0);
else
    p_out = smartProjectToImage(p_in, T, origin, veh_speed, 35.0);
end
end


