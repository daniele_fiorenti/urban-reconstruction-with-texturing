%% Experimenting with 2D only
% Loading section
close all;
clear all;
clc;

path_workspace = '/mnt/WinPartition/Users/danie/workspace';
path_dataset = strcat(path_workspace,'/dataset_0095');
path_output = strcat(path_dataset, '/output_0-267');

[velo_to_cams, K] = loadCalibration(strcat(path_workspace,'/calibration'));
velo2cam = loadCalibrationRigid(fullfile(strcat(path_workspace,'/calibration'),'calib_velo_to_cam.txt'));
cam_calib = loadCalibrationCamToCam(strcat(path_workspace,'/calibration/calib_cam_to_cam.txt'));

camera_id = 2;
imgs_s = 0;
imgs_f = 267;

switch camera_id
    case 0
        rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_00/data'), imgs_s, imgs_f); % left gray
    case 1
        rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_01/data'), imgs_s, imgs_f); % right gray
    case 2
        rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_02/data'), imgs_s, imgs_f); % left color
    case 3
        rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_03/data'), imgs_s, imgs_f); % right color
    otherwise
        rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_02/data'), 1); % left color
        camera_id = 2;
end

% rig_pack.depths = loadKittiImages('../data/depths/', imgs_s+1, imgs_f+1, 'depth-%04d.png');
% rig_pack.gt_mask = loadKittiImages('../data/masks/', imgs_s, imgs_f, 'mask_%03d.png');
rig_pack.size = imgs_f - imgs_s + 1;
if iscell(rig_pack.images) % è un set di immagini o è una sola immagine?
    rig_pack.img_h = size(rig_pack.images{1}, 1);
    rig_pack.img_w = size(rig_pack.images{1}, 2);
else
    rig_pack.img_h = size(rig_pack.images, 1);
    rig_pack.img_w = size(rig_pack.images, 2);
end

% Aggiungo 1 perché in matlab tutto parte da 1
camera_id = camera_id + 1; 

% Precompute matrix transformations
% Faccio la trasformazione
R_cam_to_rect = eye(4);
R_cam_to_rect(1:3,1:3) = cam_calib.R_rect{camera_id};
P_velo_to_img = cam_calib.P_rect{camera_id}*R_cam_to_rect*velo_to_cams{camera_id};%*I2V;
P = cam_calib.P_rect{camera_id};
P_velo2cam = R_cam_to_rect*velo_to_cams{camera_id};

R_y = [0 0 -1 0; 0 1 0 0; 1 0 0 0; 0 0 0 1];
R_z = [0 -1 0 0; 1 0 0 0; 0 0 1 0; 0 0 0 1];

R_pts = R_z*R_y;

rig_pack.K = K;
rig_pack.cam_matrix = P_velo_to_img;
rig_pack.poses = loadLidarPoses(strcat(path_output, '/all_lidar_poses.txt'));
% inv_poses sono le inverse di poses, sono qui per non ricalcolarle ogni
% volta
rig_pack.inv_poses = cellfun(@inv, rig_pack.poses, 'UniformOutput', false);


%% Here we will prepare the frames (CAUTION: Very slow)
% It is done here mainly to prepare data for the false negatives

% gt_frames è groundthruth frames, ovvero le nuvole del velodyne a cui è
% stato applicato il filtro di cropBox.
minf = 10;
gt_frames = loadVelodynePoints(-1, strcat(path_output, '/wdst/rem_points/'), '000%d.bin', 3, minf); 
frame_s = imgs_s + minf;
frame_f = imgs_f - imgs_s + 1;
for i = 1:length(gt_frames)
    gt_frames{i} = intelliPreparePoints(gt_frames{i}, ...
        rig_pack.cam_matrix*rig_pack.inv_poses{i+minf}, i + frame_s,...
        rig_pack.poses{i+minf}(1:3,4), vehicleSpeed(rig_pack.poses{i+minf-1}, rig_pack.poses{i+minf}));
end

%% Save the masks from 2D patch subtraction
changeDetect2D(rig_pack, ...                                               % rig_pack
               gt_frames, ...                                              % frames
               0.3, ...                                                    % threshold
               frame_s, ...                                                % start_frame
               8, ...                                                      % frame_diff
               strcat(path_dataset, '/sync/image_02/data'), ...            % load_folder
               strcat(path_output, '/exp_masks/mask_%04d.png'), ...                      % save_file
               'NCC', ...                                                  % err_function
               false);%, ...                                                  % intelliP
               %'../data/depths/', ...                                      % depths_folder
               %'simple-depth-%04d.png');                                   % depths_format



%% And now performing the cycle for all precision recall of both orig and ours
% Perform now for other values
% Recall        = TP/(TP + FN)
% Precision     = TP/(TP + FP)
% Specificity   = TN/(TP + FP)

% Commentato dato che non abbiamo le gt_masks
% Attraverso una maschera di groundtruth fatta a mano guardiamo se i punti 
% di movimento identificati dalla change detection sono corretti, questo 
% viene fatto proiettando i punti del velodyne (originali e croppati) 
% sulle camere. Quindi serve solo come valutazione dei risultati! 

% [TP, TN, FP, FN] = performMaskClassification('../data/exp_masks/', 'mask_%04d.png',...
%                           '../data/gt_masks/', 'mask-%03d.png',...
%                           43, 246, 20);
% computeROCdata(TP, TN, FP, FN, '../experiments/2Ddata_only_');
%                       
% display('---- iteration done');

%% 2D + 3D on various evaluation sets...
% Generate ground truth values
[GTP, GTN, GFP, GFN] = performMaskClassification('../data/gt_masks/', 'mask-%03d.png', ...
        '../data/gt_masks/', 'mask-%03d.png', ...
        43, 246, 20, false);

OrigPR = [0 0];
OrigSR = [0 0];
PR = [0 0];
SR = [0 0];
    
for i = 0:8
    display('---- iteration started....');
    
    wdst_frames = loadVelodynePoints(-1, ...
        sprintf('../data/cd_points/wdst/eval_%d/', i), '000%d.bin', 3, 8);
    
    folder_wdst = sprintf('../experiments/2d3d/wdst/eval_%d/', i);
    
    changeDetect2D(rig_pack, wdst_frames, 0.1, frame_s, 8, ...
        '../data/left_color/', ...
        [folder_wdst 'mask_%04d.png'], 'NCC', true, ...
        '../data/depths/', 'depth-%04d.png');
    
    display('---- wdst completed ----');
    
    orig_frames = loadVelodynePoints(-1, ...
        sprintf('../data/cd_points/orig/eval_%d/', i), '000%d.bin', 3, 8);
    
    folder_orig = sprintf('../experiments/2d3d/orig/eval_%d/', i);
    
%     changeDetect2D(rig_pack, orig_frames, 0.1, frame_s, 8, ...
%         '../data/left_color/', ...
%         [folder_orig 'mask_%04d.png'], 'NCC', true, ...
%         '../data/depths/', 'depth-%04d.png');
    
    [TP, TN, FP, FN] = performMaskClassification(folder_wdst, 'mask_%04d.png', ...
        '../data/gt_masks/', 'mask-%03d.png', ...
        43, 246, 20);
    
    [OTP, OTN, OFP, OFN] = performMaskClassification(folder_orig, 'mask_%04d.png',...
        '../data/gt_masks/', 'mask-%03d.png',...
        43, 246, 20);
    
    TP = TP(GTP>0);
    TN = TN(GTP>0);
    FP = FP(GTP>0);
    FN = FN(GTP>0);
    
    [iPR iSR] = computeROCdata(TP, TN, FP, FN,...
        sprintf('../experiments/wdst/eval_%02d_', i));
    
    OFN = GTP - OTP;
    OTN = GTN - OFP;
    
    OTP = OTP(GTP>0);
    OTN = OTN(GTP>0);
    OFP = OFP(GTP>0);
    OFN = OFN(GTP>0);
    
    [iOPR iOSR] = computeROCdata(OTP, OTN, OFP, OFN,...
        sprintf('../experiments/orig/eval_%02d_', i));
    
%     R = TP./(TP + FN);
%     P = TP./(TP + FP);
%     S = TN./(TN + FP);
%     
%     R = R(GTP>0);
%     P = P(GTP>0);
%     S = S(GTP>0);
%     
%     R = R(~isnan(R));
%     P = P(P>0);
%     S = S(~isnan(S));
%     
%     OFN = GTP - OTP;
%     OTN = GTN - OFP
%     OR = OTP./(OTP + OFN);
%     OP = OTP./(OTP + OFP);
%     OS = OTN./(OTN + OFP);
%     
%     OR = OR(GTP>0);
%     OP = OP(GTP>0);
%     OS = OS(GTP>0);
%     
%     OR = OR(~isnan(OR));
%     OP = OP(OP>0);
%     OS = OS(~isnan(OS));
    
    OrigPR = [OrigPR; mean(iOPR)];
    OrigSR = [OrigSR; mean(iOSR)];
    PR = [PR; mean(iPR)];
    SR = [SR; mean(iSR)];
    
    display('---- iteration complete ....');
end

% prepare and save the data
OrigPR(1,:) = [];
OrigSR(1,:) = [];
save('../experiments/orig_2d3d_avg_PR', 'OrigPR');
save('../experiments/orig_2d3davg_SR', 'OrigSR');

PR(1,:) = [];
SR(1,:) = [];
save('../experiments/wdst_2d3d_avg_PR', 'PR');
save('../experiments/wdst_2d3d_avg_SR', 'SR');
    

%% Changing the threshold
% OrigPR = [0 0];
% OrigSR = [0 0];
PR = [0 0];
SR = [0 0];
    
test_values = [3 9 14 20];
threshold_vals = [0.15 0.15 0.2];
progress = 0;
for t_i = 1:3
    for v_i = 1:4
        i = test_values(v_i);
        progress = progress + 1;
        display('---- iteration started....');
        
        wdst_frames = loadVelodynePoints(-1, ...
            sprintf('../data/cd_points/wdst/eval_%d/', i), '000%d.bin', 3, 8);
        
        folder_wdst = sprintf('../experiments/2d3d/wdst_only/test_%02d/', progress);
        
        changeDetect2D(rig_pack, wdst_frames, threshold_vals(t_i), frame_s, 8, ...
            '../data/left_color/', ...
            [folder_wdst 'mask_%04d.png'], 'NCC', true, ...
            '../data/depths/', 'depth-%04d.png');
        
        display('---- wdst completed ----');
        
%         orig_frames = loadVelodynePoints(-1, ...
%             sprintf('../data/cd_points/orig/eval_%d/', i), '000%d.bin', 3, 8);
%         
%         folder_orig = sprintf('../experiments/2d3d/orig/eval_%d/', i);
%         
%         changeDetect2D(rig_pack, orig_frames, 0.1, frame_s, 8, ...
%             '../data/left_color/', ...
%             [folder_orig 'mask_%04d.png'], 'NCC', true, ...
%             '../data/depths/', 'depth-%04d.png');
        
        [TP, TN, FP, FN] = performMaskClassification(folder_wdst, 'mask_%04d.png', ...
            '../data/gt_masks/', 'mask-%03d.png', ...
            43, 246, 20);
        
%         [OTP, OTN, OFP, OFN] = performMaskClassification(folder_orig, 'mask_%04d.png',...
%             '../data/gt_masks/', 'mask-%03d.png',...
%             43, 246, 20);
        
        TP = TP(GTP>0);
        TN = TN(GTP>0);
        FP = FP(GTP>0);
        FN = FN(GTP>0);
        
        [iPR iSR] = computeROCdata(TP, TN, FP, FN,...
            sprintf('../experiments/wdst/alone/test_%02d_', progress));
        
%         OFN = GTP - OTP;
%         OTN = GTN - OFP;
%         
%         OTP = OTP(GTP>0);
%         OTN = OTN(GTP>0);
%         OFP = OFP(GTP>0);
%         OFN = OFN(GTP>0);
%         
%         [iOPR iOSR] = computeROCdata(OTP, OTN, OFP, OFN,...
%             sprintf('../experiments/orig/eval_%02d_', i));
        
        %     R = TP./(TP + FN);
        %     P = TP./(TP + FP);
        %     S = TN./(TN + FP);
        %
        %     R = R(GTP>0);
        %     P = P(GTP>0);
        %     S = S(GTP>0);
        %
        %     R = R(~isnan(R));
        %     P = P(P>0);
        %     S = S(~isnan(S));
        %
        %     OFN = GTP - OTP;
        %     OTN = GTN - OFP
        %     OR = OTP./(OTP + OFN);
        %     OP = OTP./(OTP + OFP);
        %     OS = OTN./(OTN + OFP);
        %
        %     OR = OR(GTP>0);
        %     OP = OP(GTP>0);
        %     OS = OS(GTP>0);
        %
        %     OR = OR(~isnan(OR));
        %     OP = OP(OP>0);
        %     OS = OS(~isnan(OS));
        
%         OrigPR = [OrigPR; mean(iOPR)];
%         OrigSR = [OrigSR; mean(iOSR)];
        PR = [PR; mean(iPR)];
        SR = [SR; mean(iSR)];
        
        display('---- iteration complete ....');
    end
end

% prepare and save the data
% OrigPR(1,:) = [];
% OrigSR(1,:) = [];
% save('../experiments/orig_2d3d_avg_PR', 'OrigPR');
% save('../experiments/orig_2d3davg_SR', 'OrigSR');

PR(1,:) = [];
SR(1,:) = [];
save('../experiments/wdst_2d3d_alone_avg_PR', 'PR');
save('../experiments/wdst_2d3d_alone_avg_SR', 'SR');

%% only 3d...

%% Changing the threshold
% OrigPR = [0 0];
% OrigSR = [0 0];
PR = [0 0];
SR = [0 0];
    
test_values = [32 33 34];
threshold_vals = [0.05, 0.1, 0.1, 0.2];
progress = 29;
for t_i = 3:4
    for v_i = 2:3
        i = test_values(v_i);
        progress = progress + 1;
        display('---- iteration started....');
        
        wdst_frames = loadVelodynePoints(-1, ...
            sprintf('../data/cd_points/wdst/eval_%d/', i), '000%d.bin', 3, 8);
        
        folder_wdst = sprintf('../experiments/2d3d/wdst_simple/test_%02d/', progress);
        
        changeDetect2DVideo(rig_pack, wdst_frames, threshold_vals(t_i), frame_s, 8, ...
            '../data/left_color/', ...
            folder_wdst, 'NCC', true, ...
            '../data/depths/', 'simple-depth-%04d.png');
        
        display('---- wdst completed ----');
        
%         orig_frames = loadVelodynePoints(-1, ...
%             sprintf('../data/cd_points/orig/eval_%d/', i), '000%d.bin', 3, 8);
%         
%         folder_orig = sprintf('../experiments/2d3d/orig/eval_%d/', i);
%         
%         changeDetect2D(rig_pack, orig_frames, 0.1, frame_s, 8, ...
%             '../data/left_color/', ...
%             [folder_orig 'mask_%04d.png'], 'NCC', true, ...
%             '../data/depths/', 'depth-%04d.png');
        
%         [TP, TN, FP, FN] = performMaskClassification(folder_wdst, 'mask_%04d.png', ...
%             '../data/gt_masks/', 'mask-%03d.png', ...
%             43, 246, 20);
%         
%         [OTP, OTN, OFP, OFN] = performMaskClassification(folder_orig, 'mask_%04d.png',...
%             '../data/gt_masks/', 'mask-%03d.png',...
%             43, 246, 20);
        
%         TP = TP(GTP>0);
%         TN = TN(GTP>0);
%         FP = FP(GTP>0);
%         FN = FN(GTP>0);
%         
%         [iPR iSR] = computeROCdata(TP, TN, FP, FN,...
%             sprintf('../experiments/wdst/simple/test_%02d_', progress));
        
%         OFN = GTP - OTP;
%         OTN = GTN - OFP;
%         
%         OTP = OTP(GTP>0);
%         OTN = OTN(GTP>0);
%         OFP = OFP(GTP>0);
%         OFN = OFN(GTP>0);
%         
%         [iOPR iOSR] = computeROCdata(OTP, OTN, OFP, OFN,...
%             sprintf('../experiments/orig/eval_%02d_', i));
        
        %     R = TP./(TP + FN);
        %     P = TP./(TP + FP);
        %     S = TN./(TN + FP);
        %
        %     R = R(GTP>0);
        %     P = P(GTP>0);
        %     S = S(GTP>0);
        %
        %     R = R(~isnan(R));
        %     P = P(P>0);
        %     S = S(~isnan(S));
        %
        %     OFN = GTP - OTP;
        %     OTN = GTN - OFP
        %     OR = OTP./(OTP + OFN);
        %     OP = OTP./(OTP + OFP);
        %     OS = OTN./(OTN + OFP);
        %
        %     OR = OR(GTP>0);
        %     OP = OP(GTP>0);
        %     OS = OS(GTP>0);
        %
        %     OR = OR(~isnan(OR));
        %     OP = OP(OP>0);
        %     OS = OS(~isnan(OS));
        
%         OrigPR = [OrigPR; mean(iOPR)];
%         OrigSR = [OrigSR; mean(iOSR)];
%         PR = [PR; mean(iPR)];
%         SR = [SR; mean(iSR)];
        
        display('---- iteration complete ....');
    end
end

% prepare and save the data
% OrigPR(1,:) = [];
% OrigSR(1,:) = [];
% save('../experiments/orig_2d3d_avg_PR', 'OrigPR');
% save('../experiments/orig_2d3davg_SR', 'OrigSR');
% 
% PR(1,:) = [];
% SR(1,:) = [];
% save('../experiments/wdst_2d3d_simple_avg_PR', 'PR');
% save('../experiments/wdst_2d3d_simple_avg_SR', 'SR');