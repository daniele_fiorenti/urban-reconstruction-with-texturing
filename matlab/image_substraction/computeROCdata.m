function [PR, SR] = computeROCdata( TP, TN, FP, FN , save_file)
%SAVEDATA compute Precision-Recall and Specificity-Recall
if isrow(TP)
    TP = TP';
end
if isrow(TN)
    TN = TN';
end
if isrow(FP)
    FP = FP';
end
if isrow(FN)
    FN = FN';
end

TPFP = TP + FP;
TPFN = TP + FN;
TNFP = TN + FP;

TPFP(TPFP==0) = 1;
TPFN(TPFN==0) = 1;

PR = [TP./TPFP TP./TPFN];
SR = [TN./TNFP TP./TPFN];

if exist('save_file', 'var')
    save([save_file 'PR'], 'PR');
    save([save_file 'SR'], 'SR');
end
end

