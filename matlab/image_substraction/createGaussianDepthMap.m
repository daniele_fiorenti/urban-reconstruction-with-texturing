function [ depth_image, depth_points ] = createGaussianDepthMap( velo_points, T, f, w, h )
%CONVERTTODEPTHMAP Project velo points into depth map
%   Detailed explanation goes here
pts = velo_points(velo_points(:,1) > 0.5, :);
pts = pts(pts(:,1) < 50, :);

if ~exist('w', 'var')
    w = 1242;
end
if ~exist('h', 'var')
    h = 375;
end

[~,idx] = sort(pts(:,1));
pts = pts(idx,:);

depth_image = ones(h,w);

if ~isempty(pts)
    max_d = pts(end,1);
else
    max_d = 1;
end

p = T*pts';
p(1,:) = p(1,:)./p(3,:);
p(2,:) = p(2,:)./p(3,:);
p(3,:) = [];

depth_points = zeros(size(p,2), 3);

k = 0.05;

bounds = [w h]';

for i = 1:size(pts,1)
   if isempty(p(i)) || p(1,i) > w || p(2,i) > h || p(1,i) < 1  || p(2,i) < 1
       continue;
   end
   d = (max_d - pts(i,1))/max_d;
   depth_points(i,:) = [p(:,i)' d];
   
   b_size = k*f/pts(i,1);
   
   uv = int16(p(:,i));
   
   r1 = max(int16(uv(2) - b_size), 1);
   r2 = min(int16(uv(2) + b_size), h);
   c1 = max(int16(uv(1) - b_size), 1);
   c2 = min(int16(uv(1) + b_size), w);
   
   g_size = double(min(r2 - r1, c2 - c1) + 1);
   gm = 1 - gaussMask(g_size, g_size/3)*d;
   
   patch = depth_image(r1:r2, c1:c2);
   gm = imresize(gm, [r2 - r1 + 1, c2 - c1 + 1]);
%    gm = imresize(gm, [r2 - r1 + 1, c2 - c1 + 1]);
   
   depth_image(r1:r2, c1:c2) = min(patch,gm);
end

depth_points(depth_points(:,1) <= 0, :) = [];

end
