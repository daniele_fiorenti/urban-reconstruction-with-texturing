function [ frames, poses ] = loadLidarRemPoints( poses_file, rem_points_file, max_clouds )
%LOADLIDARREMPOINTS Loads the datastructure containing removed points
%   This datastructure includes: removed points, view rays and poses

if ~exist(poses_file, 'file') && ~exist(rem_points_file,'file')
    error('loadLidarRemPoints: unable to find specified files');
end

% Open files
poses_fin = fopen(poses_file, 'r');
rem_pts_fin = fopen(rem_points_file, 'r');

% Set the upper bound limit of poses and rigs
n_rigs = fscanf(poses_fin, '%d', 1);
if exist('max_clouds', 'var')
    n_rigs = min(n_rigs, max(0, max_clouds));
end

% Set variable lengths
frames = struct('scanned', 0, 'pose', zeros(4,4), 'views', {});

% Read poses
poses{n_rigs} = 0;
for i = 1:n_rigs
    poses{i} = fscanf(poses_fin, '%f %f %f %f', [4 4])';
    frames(i).pose = poses{i};
end

% Read points
scanned_rig = fscanf(rem_pts_fin, '%d', 1) + 1;
n_points = fscanf(rem_pts_fin, '%d', 1);
while ~feof(rem_pts_fin) && scanned_rig <= n_rigs 
    if isnan(n_points)
        break;
    end
    frames(scanned_rig).views{n_rigs} = []';
    frames(scanned_rig).scanned = 1;
    pts = ones(4,1);
    for i = 1:n_points
        pts(1:3) = fscanf(rem_pts_fin, '%f %f %f', 3)';
        n_views = fscanf(rem_pts_fin, '%d', 1);
        for v = 1:n_views
            view_id = fscanf(rem_pts_fin, '%d', 1) + 1;
            frames(scanned_rig).views{view_id} = ... 
                [frames(scanned_rig).views{view_id} pts];
        end
    end
    scanned_rig = fscanf(rem_pts_fin, '%d', 1) + 1;
    n_points = fscanf(rem_pts_fin, '%d', 1);
end

% Close files
fclose(poses_fin);
fclose(rem_pts_fin);
end

