function [ depth_image, depth_points, max_d ] = ...
    convertToDepthMap( velo_points, T_velo_act_to_img_plane, T_velo_from_first_to_act, img_w, img_h )

%CONVERTTODEPTHMAP Project velo points into depth map
%   Detailed explanation goes here
pts_velo_wrt_first = velo_points;
% pts_velo_wrt_first = velo_points(velo_points(:,1) > 0.4, :);
% pts_velo_wrt_first = pts_velo_wrt_first(pts_velo_wrt_first(:,1) < 30, :);

if ~exist('T_velo_from_first_to_act', 'var')
    T_velo_from_first_to_act = eye(4);
end
if ~exist('img_w', 'var')
    img_w = 1242;
end
if ~exist('img_h', 'var')
    img_h = 375;
end

[~,idx] = sort(pts_velo_wrt_first(:,1));
pts_velo_wrt_first = pts_velo_wrt_first(idx,:); % Ordina i punti in ordine crescente rispetto alla prima colonna

depth_image = ones(img_h, img_w);

p_velo_act = T_velo_from_first_to_act*pts_velo_wrt_first';
p_velo_act(:, p_velo_act(1,:) <= 0) = []; % we have to delete points behind (so with negative x);
p_velo_act = p_velo_act';

if ~isempty(p_velo_act)
    max_d = p_velo_act(end,1);
else
    max_d = 1;
end

p_img_plane = T_velo_act_to_img_plane*p_velo_act';
p_img_plane(1,:) = p_img_plane(1,:)./p_img_plane(3,:);
p_img_plane(2,:) = p_img_plane(2,:)./p_img_plane(3,:);
p_img_plane(3,:) = [];


depth_points = zeros(size(p_img_plane,2), 3);

for i = 1:size(p_velo_act,1)
    if isempty(p_img_plane(:,i)) || ...
            p_img_plane(1,i) > img_w || ...
            p_img_plane(2,i) > img_h || ...
            p_img_plane(1,i) < 1  || ...
            p_img_plane(2,i) < 1
        continue;
    end

    uv = int16(p_img_plane(:,i));
    depth_points(i,:) = [p_img_plane(:,i)' p_velo_act(i,1)];
    if p_velo_act(i,1) > 0
        depth_image(uv(2),uv(1)) = p_velo_act(i,1)/max_d;
    end
end

depth_points(depth_points(:,1) <= 0, :) = [];

% imshow(depth_image);
% pause;

% Let single points grow
depth_image = imgaussfilt(imdilate(1 - depth_image, strel('disk', 5)),1.2);

% imshow(1 - depth_image);
% pause;
end

