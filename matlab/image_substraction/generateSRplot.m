function generateSRplot( SR, GTP, tit )
%GENERATEPRPLOT 
if exist('GTP', 'var')
    SR = SR(GTP>0,:);
end

if ~exist('tit', 'var')
    tit = 'Specificity-Sensitivity'
end

[~, idx] = sort(SR(:,1));
SR = SR(idx,:);

plot(SR(:,1), SR(:,2))
title(tit)
xlabel('Specificity')
ylabel('Sensitivity')
end

