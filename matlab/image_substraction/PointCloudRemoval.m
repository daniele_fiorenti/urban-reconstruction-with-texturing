%% Removing or accepting the removed points from the cloud
K = [7.215377e+02 0.000000e+00 6.095593e+02; ...
     0.000000e+00 7.215377e+02 1.728540e+02; ...
     0.000000e+00 0.000000e+00 1.000000e+00];
W = [0 -1 0; 1 0 0; 0 0 1]; %Needed for essential svd

I2V = [ 9.999976e-01    7.553071e-04   -2.035826e-03    -8.086759e-01; ...
       -7.854027e-04    9.998898e-01   -1.482298e-02     3.195559e-01; ...
        2.024406e-03    1.482454e-02    9.998881e-01    -7.997231e-01; ...
                   0               0               0                1];

% img(1) = im2double(imread(sprintf('../data/right/%010d.png', 70)));
% img(2) = im2double(imread(sprintf('../data/right/%010d.png', 70)));
% img(3) = im2double(imread(sprintf('../data/right/%010d.png', 70)));
% img(4) = im2double(imread(sprintf('../data/right/%010d.png', 70)));
poses = loadLidarPoses('../data/all_lidar_poses.txt');
pts = loadLidarPoints('../data/rem_points_out.txt');
[velo_to_cams, Ks] = loadCalibration('../data/calibration');
imgs = loadKittiImages('../data/left/', 92, 144);
velo2cam = loadCalibrationRigid(fullfile('../data/calibration','calib_velo_to_cam.txt'));
cam_calib = loadCalibrationCamToCam('../data/calibration/calib_cam_to_cam.txt');

% %% Trying other new stuff
% cam_i = 4;
% frame_i = 4;
% 
% R_cam = eye(4);
% R_cam(1:3,1:3) = cam_calib.R_rect{1};
% P_cam = cam_calib.P_rect{1};
% L_pose = poses{cam_i};
% % P_cam = K * poses{cam_i}(1:3,:);
% 
% vel_to_cam = L_pose(1:3,:) * [P_cam; 0 0 0 1] * R_cam * velo2cam;
% 
% imshow(imgs{cam_i+8}); hold on;
% velo_img = project(pts{frame_i}(1:3,:)',vel_to_cam);
% 
% for i=1:size(velo_img,1)
% %   col_idx = round(64*5/pts(i,1));
%   plot(velo_img(i,1),velo_img(i,2),'o','LineWidth',3,'MarkerSize',1,'Color', [1 0 0]);
% end

%% calculating stuff...
% uv_pts = velo_to_cams{1}(1:3,:)*pts;
% uv_pts = [K, [0; 0; 0]]*velo2cam*pts;
% compute projection matrix velodyne->image plane

R_cam_to_rect = eye(4);
R_cam_to_rect(1:3,1:3) = cam_calib.R_rect{1};
P = [cam_calib.K{1}, [0 0 0]'];
P_velo_to_img = cam_calib.P_rect{1}*R_cam_to_rect*velo_to_cams{1};%*I2V;

% tform = cam_calib.P_rect{1} * [cam_calib.R_rect{1}; 0 0 1] * velo2cam(1:3,:);
% uv_pts = tform*pts;
% uv_pts(1,:) = uv_pts(1,:)./uv_pts(3,:);
% uv_pts(2,:) = uv_pts(2,:)./uv_pts(3,:);
% uv_pts(3,:) = [];
for i = 1:length(pts)
%     pts{i}(3,:) = -pts{i}(3,:);
end
% frame = 18;
cam = 8;
for frame = 1:29
% pts{:}(3,:) = -pts{:}(3,:);
% figure('Position',[20 100 size(imgs{1},2) size(imgs{1},1)]); axes('Position',[0 0 1 1]);
imshow(imgs{cam+8}); hold on;
% project to image plane (exclude luminance)
velo_img = project(pts{frame}(1:3,:)',P_velo_to_img*inv(poses{cam}));
% u_max = size(imgs{1},2);
% v_max = size(imgs{1},1);
% counter = 1;
% imgaa = imgs{frame+16};
% for i=1:size(velo_img,1)
%     uv = int16(velo_img(i,:));
%     if uv(1) > 0 && uv(1) < u_max ...
%             && uv(2) > 0 && uv(2) < v_max
%         imgaa(uv(2),uv(1)) = 1.0;
%     end
% end

% plot points
% cols = jet;
for i=1:size(velo_img,1)
%   col_idx = round(64*5/pts(i,1));
  plot(velo_img(i,1),velo_img(i,2),'o','LineWidth',3,'MarkerSize',1,'Color', [1 0 0]);
end
cam = cam + 1;
pause
end
% image_rem_points = imgs{1}(int16(uv_pts(:)));
% imshow(imgs{1});
% imshow(image_rem_points);
