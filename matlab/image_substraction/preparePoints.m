function [ p_out ] = preparePoints(p_in, T)
%PROJECTTOIMAGE Prepare points for smaller point cloud
% input points should be in homogeneous coordinate system
% get the principal plane normal
P_normal = T(3,:);

% get the points which are in front of camera
p_pos = P_normal*p_in;
p_out = p_in;
p_out(:,p_pos < 0) = [];

end

