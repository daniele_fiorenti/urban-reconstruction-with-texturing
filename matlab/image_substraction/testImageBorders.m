function [ isInImage ] = testImageBorders( pts, bottom, right, top, left, box_size )
%TESTIMAGEBORDERS checks whether or not points are in image boundaries

%============================================
% check for optionals
if ~exist('box_size', 'var')
    box_size = 0;
end

if ~exist('top', 'var')
    top = 1;
end

if ~exist('left', 'var')
    % here left needs to be equal to top to keep the aspect ratio
    left = top;
end

%============================================
% perform the check
box_size = int16(round(box_size / 2));
bottom = bottom - box_size;
right = right - box_size;
top = top + box_size;
left = left + box_size;



end

