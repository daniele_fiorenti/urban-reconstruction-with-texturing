function [ speed ] = vehicleSpeed( pose1, pose2, pose3, rotation_time )
%VEHICLESPEED Calculates the average speed of the vehicle between frames

if ~exist('rotation_time', 'var')
    rotation_time = 0.1;
end

speed = norm(pose2(1:3,4) - pose1(1:3,4))/rotation_time;

if exist('pose3', 'var')
    speed2 = norm(pose3(1:3,4) - pose2(1:3,4))/rotation_time;
    speed = (speed + speed2)*0.5;
end

end

