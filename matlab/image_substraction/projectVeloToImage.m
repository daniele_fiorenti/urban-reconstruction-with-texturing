function projected_image = projectVeloToImage( velo_points, T_velo_act_to_img_plane, T_velo_from_first_to_act, img_w, img_h )

%PROJECTVELOTOIMAGE Project velo points into camera image
% Detailed explanation goes here
pts_velo_wrt_first = velo_points;

if ~exist('T_velo_from_first_to_act', 'var')
    T_velo_from_first_to_act = eye(4);
end
if ~exist('img_w', 'var')
    img_w = 1242;
end
if ~exist('img_h', 'var')
    img_h = 375;
end

projected_image = ones(img_h, img_w);

p_velo_act = T_velo_from_first_to_act*pts_velo_wrt_first';
p_velo_act(:, p_velo_act(1,:) <= 0) = []; % we have to delete points behind (so with negative x);
p_velo_act = p_velo_act';

p_img_plane = T_velo_act_to_img_plane*p_velo_act';
p_img_plane(1,:) = p_img_plane(1,:)./p_img_plane(3,:);
p_img_plane(2,:) = p_img_plane(2,:)./p_img_plane(3,:);
p_img_plane(3,:) = [];

for i = 1:size(p_velo_act,1)
    if isempty(p_img_plane(:,i)) || ...
            p_img_plane(1,i) > img_w || ...
            p_img_plane(2,i) > img_h || ...
            p_img_plane(1,i) < 1  || ...
            p_img_plane(2,i) < 1
        continue;
    end
    uv = int16(p_img_plane(:,i));
    projected_image(uv(2),uv(1)) = 0; % Se il punto passa tutti i test lo faccio nero
end

% Let single points grow
% projected_image = imgaussfilt(imdilate(1 - projected_image, strel('disk', 5)),1.2);
% 
% imshow(projected_image);
% pause(0.1);
end

