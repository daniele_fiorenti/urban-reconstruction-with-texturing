function [ TP, FP, TN ] = getFast3Dmetrics( rig_pack, frames, first_frame, last_frame, ...
    start_mask, masks_dir, mask_format )
%GET3DMETRICS get the metrics for Precision and Recall
if ~exist('first_frame', 'var')
    first_frame = 1;
end

if ~exist('last_frame', 'var')
    last_frame = rig_pack.frames;
end

if ~exist('masks_dir', 'var')
    masks_dir = '../data/gt_masks/';
end

if ~exist('mask_format', 'var')
    mask_format = 'mask-%03d.png';
end

if ~exist('start_mask', 'var')
    start_mask = first_frame;
end

start_mask = start_mask - 1;

TP = zeros(last_frame - first_frame + 1, 1);
FP = zeros(last_frame - first_frame + 1, 1);

img_w = rig_pack.img_w;
img_h = rig_pack.img_h;

for frame = first_frame:last_frame;
    pts = frames{frame - first_frame + 1};
   
    % prepare the transformation for every point
    T = rig_pack.cam_matrix*rig_pack.inv_poses{frame};
    
    % and an empty mask
    img_mask = zeros(rig_pack.img_h, rig_pack.img_w);
    
    % Save sample picture removed points locations
    rem_img = projectToImage(pts, T);
    
    % remove unused points
    rem_img = int16(rem_img);
    rem_img(:, rem_img(1,:) > img_w) = [];
    rem_img(:, rem_img(2,:) > img_h) = [];
    
%     img_mask(rem_img(:,2), rem_img(:,1)) = 1;
    for i = 1:size(rem_img,2)
        img_mask(rem_img(2,i), rem_img(1,i)) = 1;
    end

    gt_mask = loadKittiImages(masks_dir, frame + start_mask, frame + start_mask, mask_format);
    
%     plotRemovedPoints(rem_img, gt_mask,1,'testing');
%     pause;
    
    % confront with ground_truth
    final_mask = img_mask.*gt_mask;
    
    TP(frame) = size(final_mask(final_mask > 0),1);
    FP(frame) = size(img_mask(img_mask > 0),1) - TP(frame);
    TN(frame) = size(final_mask(final_mask <= 0.1),1);
    
%     imshow(final_mask);
%     pause;

end

end