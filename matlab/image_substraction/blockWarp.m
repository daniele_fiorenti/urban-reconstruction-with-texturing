function [ warpedBlock ] = blockWarp( block1, block2, point, rel_pose, K, P)
%BLOCKWARP warps a block into another

R = rel_pose(1:3,1:3);
t = rel_pose(1:3,4);%/1000;
%     t = [0 0 0]';

cam_t = K\P(1:3,4);
n_plane = P(3,1:3)';
d_plane = norm(point(1:3));

Kx = K/1000;

Hx = R - (t*n_plane')/d_plane;
H = Kx*Hx/Kx;

% H=K*R/K - 1/d_plane * K*t*n_plane'*K;

Rfixed = imref2d(size(block2));
tform = projective2d(Hx);
warpedBlock = imwarp(block1, tform,'FillValues', 0,'OutputView',Rfixed);

% display(d_plane);
% display(n_plane);
% display(H);
end

