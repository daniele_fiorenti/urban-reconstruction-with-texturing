%% Check the removed points from point clouds in images
% Loading section
close all;
clear all;
clc;
[velo_to_cams, K] = loadCalibration('../data/calibration');
velo2cam = loadCalibrationRigid(fullfile('../data/calibration','calib_velo_to_cam.txt'));
cam_calib = loadCalibrationCamToCam('../data/calibration/calib_cam_to_cam.txt');
% [rem_frames, poses] = ...
%     loadLidarRemPoints('../data/all_lidar_poses.txt', '../data/rem_points_out.txt');

[rem_frames, poses] = ...
    loadFastRemFrames('../data/all_lidar_poses.txt', '../data/rem_points_out.txt');

% Done loading stuff

%% Precomputation section
camera_id = 2;
imgs_s = 80;
imgs_f = 159;

switch camera_id
    case 0
        imgs_orig = loadKittiImages('../data/left/', imgs_s, imgs_f);
    case 1
        imgs_orig = loadKittiImages('../data/right/', imgs_s, imgs_f);
    case 2
        imgs_orig = loadKittiImages('../data/left_color/', imgs_s, imgs_f);
    case 3
        imgs_orig = loadKittiImages('../data/right_color/', imgs_s, imgs_f);
    case 4
        imgs_orig = loadKittiImages('../data/depths/', imgs_s+1, imgs_f+1, 'depth-%04d.png');
        camera_id = 2;
end

depths = loadKittiImages('../data/depths/', imgs_s+1, imgs_f+1, 'depth-%04d.png');

camera_id = camera_id + 1;

% Precompute matrix inversions
inv_poses = cellfun(@inv, poses, 'UniformOutput', false);

% Precompute matrix transformations
R_cam_to_rect = eye(4);
R_cam_to_rect(1:3,1:3) = cam_calib.R_rect{camera_id};
P_velo_to_img = cam_calib.P_rect{camera_id}*R_cam_to_rect*velo_to_cams{camera_id};%*I2V;
P = cam_calib.P_rect{camera_id};
P_velo2cam = R_cam_to_rect*velo_to_cams{camera_id};

R_y = [0 0 -1 0; 0 1 0 0; 1 0 0 0; 0 0 0 1];
R_z = [0 -1 0 0; 1 0 0 0; 0 0 1 0; 0 0 0 1];

R_pts = R_z*R_y;

% =========================================================================
%% Smooth images
% =========================================================================
gauss_sigma = 1.2;
edge_f = fspecial('sobel');
for i = 1: length(imgs_orig)
    imgs{i} = imgs_orig{i};
%     imgs{i} = imgaussfilt(imgs_orig{i}, gauss_sigma);
%     images{i} = mat2gray(imfilter(imgs_orig{i}, edge_f) + ...
%                          imfilter(imgs_orig{i}, edge_f'));
%     images{i} = abs(images{i} - 0.5);
    images{i} = imgs_orig{i};
    images{i} = imgaussfilt(images{i}, gauss_sigma);
%     images{i} = imgaussfilt(mat2gray(edge(imgs_orig{i}, 'Canny')), gauss_sigma);
%     imgs{i} = edge(imgs_orig{i}, 'sobel', 0.5);
end

% Done precomputing stuff

% =========================================================================
%% Patch matching and removing                 WINDOW CORRELATION
% =========================================================================
close all;

% Parameters ============================
% Frame related -------------------------
frame       = 45;
frame_start = 9;
frame_end   = size(rem_frames,2);
frame_dist  = 1;
[img_h, img_w] = size(images{1});
img_cx = img_w/2;
img_cy = img_h/2;

% Patches related ------------------------
f_length    = K(1,1);   % Focal length in pixels
height_w    = 0.1;      % Height in meters
corr_th     = 0.25;      % Correlation threshold in percentage
window_mul  = 2;        % Window multiplier = Windows_size/Block_size
d_max       = 25;       % Maximum world distance from camera
dp_max      = sqrt((img_h/2)^2 + (img_w/2)^2); % Max pixel distance from center
sigma_mul   = 1/3.16;   % Sigma multiplier = sigma/Block_size
max_b_size  = int16(round(height_w/2*f_length/2)); % Max block size
sigma_c     = ceil(height_w/(d_max)*f_length)*sigma_mul; % sigma at image center

% Masking related -----------------------
e_shape     = strel('disk',7);
d_shape     = strel('disk',6);
gauss_mul   = 1.5;
gauss_var   = 1.5;

for frame = frame_start:frame_end
    if rem_frames(frame).scanned == 0
        continue;
    end
    
    % Initialization
    cam_i = frame - frame_dist;
    cam_i_valid = rem_frames(frame).views(:,(cam_i - rem_frames(frame).first_id));
    cam_s = rem_frames(frame).image_id;
    pts = rem_frames(frame).points;
   
    % prepare the transformation for every point
    T_1 = P_velo_to_img*inv_poses{cam_i};
    T_s = P_velo_to_img*inv_poses{cam_s};
    
    % and an empty mask
    img_mask = zeros(img_h, img_w);
    
    % Save sample picture removed points locations
    rem_img = projectToImage(rem_frames(frame).points, T_s);
    
    % remove unused points
    rem_img = int16(rem_img);
    rem_img(:, rem_img(1,:) > img_w) = [];
    rem_img(:, rem_img(2,:) > img_h) = [];
    
    % show the intermediate result
%     plotRemovedPoints(rem_img, imgs_orig{cam_s}, 1, 'Original Image');
    
    % Visualization =======================================================
    % create figures...
    
%     scrsz = get(groot,'ScreenSize');
%     h = scrsz(4);
%     w = scrsz(3);
%     fig_patch_i     = figure('Name', 'Patch i', ...
%                       'Position',[w/2 h/2 w/4 h/2]);
%                       axes('position', [0 0 1 1]);
%     fig_patch_s     = figure('Name', 'Patch s', ...
%                       'Position',[3*w/4 h/2 w/4 h/2]);
%                       axes('position', [0 0 1 1]);
%     fig_th_window   = figure('Name', 'Threshold Window', ...
%                       'Position',[1 h/2 w/4 h/2]);
%                       axes('position', [0 0 1 1]);
%     fig_corr_res    = figure('Name', 'Correlation Result', ...
%                       'Position',[1 1 w/4 h/2]);
%                       axes('position', [0 0 1 1]);
%     fig_window_m    = figure('Name', 'Window mask', ...
%                       'Position',[w/4 h/2 w/4 h/2]);
%                       axes('position', [0 0 1 1]);
%     fig_window_i    = figure('Name', 'Window block', ...
%                       'Position',[w/4 1 w/4 h/2]);
%                       axes('position', [0 0 1 1]);
%     fig_img_s       = figure('Name', 'Sample Image', ...
%                       'Position',[w/2 h/4 w/2 h/4]);
%                       axes('position', [0 0 1 1]), ...
%                       imshow(imgs_orig{cam_s});
%     hold on;
%     plot_h_s        = plot(300, 300, 'rs', 'MarkerSize', 2, 'MarkerEdgeColor', [1,0,0]);
%     hold off;
%     
%     fig_img_i       = figure('Name', 'Test Image', ...
%                       'Position',[w/2 1 w/2 h/4]);
%                       axes('position', [0 0 1 1]), ...
%                       imshow(imgs_orig{cam_i});
%     hold on;
%     plot_h_i        = plot(300, 300, 'rs', 'MarkerSize', 2, 'MarkerEdgeColor', [1,0,0]);
%     hold off;
    
    % =====================================================================
    
    % save metrics... and other statistics
    all_metrics = zeros(size(rem_img, 2), 1);
    total_pts_1 = 0;
    
    % other rem_img
    rem_img_1 = zeros(2, size(pts, 2));
     
    % image borders
    top     = max_b_size;
    left    = max_b_size;
    bottom  = img_h - max_b_size;
    right   = img_w - max_b_size;
    
    for i = 1:size(pts,2)
        % skip points not visible by sensor
        if ~cam_i_valid(i)
            continue;
        end
        
        % get the point
        point_i = pts(:,i);
        
        % and not visible by at least one camera
        p_cam_i = projectToImage(point_i, T_1);
        p_cam_s = projectToImage(point_i, T_s);
        if isempty(p_cam_i) || p_cam_i(1) > right || p_cam_i(2) > bottom ...
                            || p_cam_i(1) < left  || p_cam_i(2) < top || ...
           isempty(p_cam_s) || p_cam_s(1) > right || p_cam_s(2) > bottom ...
                            || p_cam_s(1) < left  || p_cam_s(2) < top
            continue;
        end
        
        % now we are ready to work
        total_pts_1 = total_pts_1 + 1;
        
        % setup some variables --------------------------------------------
        point_is = inv_poses{cam_s}*point_i;
        point_i  = inv_poses{cam_i}*point_i;
        b_size_s = round(height_w/sqrt(point_is(1))*f_length);
        b_size_i = round(height_w/sqrt(point_i(1))*f_length);
        w_size_i = window_mul*b_size_i;
        
        dp_ix    = sqrt((p_cam_i(1)-img_cx)^2 + (p_cam_i(2)-img_cy)^2);
        sigma_x  = sigma_c*(1+(window_mul-1)*(1-point_i(1)/d_max))*(0.75+1.25*dp_ix/dp_max);
        
        % get the patches -------------------------------------------------
        % first extract the blocks
        block_i   = extractImageBlock(images{cam_i}, p_cam_i, (b_size_i));
        block_s   = extractImageBlock(images{cam_s}, p_cam_s, (b_size_s));
        
        % and apply a gaussian mask
        patch_i   = block_i;%.*gaussMask(b_size_i);
        patch_s   = block_s;%.*gaussMask(b_size_s);
        
        % resize patch_s to match patch_i size
        patch_s = imresize(patch_s, size(patch_i,1)/size(patch_s,1));
        
        if std(patch_s(:)) == 0
            continue;
        end
        
        % get the window and threshold mask
        window_i = extractImageBlock(images{cam_i}, p_cam_i, window_mul*b_size_i);
        th_window = corr_th*gaussMask(window_mul*b_size_i, sigma_x);
%         th_window = imcrop(th_window

        % cross-correlation computation -----------------------------------
        % first get the upscaled threshold
        % with its maximum value
        max_c = max(max(xcorr2(patch_s, patch_s)));
%         max_c = max(max(conv2(patch_s, patch_s)));
        
        % now the threshold
%         th_window = th_window*max_c;
        
        
        % perform cross-correlation with other patch
%         method_res = xcorr2(window_i, patch_s);
        method_res = normxcorr2(patch_s, window_i);
        method_res = abs(1 - method_res/max(max(method_res))); % - max_c);
%         corr_res = abs(conv2(window_i, patch_s)); % - max_c);
        method_res = imresize(method_res, size(th_window,1)/size(method_res,1));
        method_res = imresize(method_res, size(th_window,1)/size(method_res,1));
        method_res = method_res - th_window;
        % check against threshold
        % get the max correlation value with its pixel coordinates
        [min1, rows] = min(method_res);
        [hit_v, hit_col] = min(min1);
        hit_row = rows(hit_col);
        max_v = 1;%max(max(method_res));
        
        % get the metric and save it
%         metric = hit_v - th_window(hit_row, hit_col);
        metric = hit_v;
        % -----------------------------------------------------------------

        % window-patch difference -----------------------------------------
%         method_res = patchDifferenceWindow(window_i, patch_s, 'SSD', true);
%         th_window = imresize(th_window, size(method_res,1)/size(th_window,1));
%         th_window = imresize(th_window, size(method_res,1)/size(th_window,1));
%         method_res = method_res/max(max(method_res)) - th_window;
%         [min1, rows] = min(method_res);
%         [hit_v, hit_col] = min(min1);
%         hit_row = rows(hit_col);
%         max_v = max(max(method_res));% - hit_v;
% %         metric = hit_v/max_v - th_window(hit_row, hit_col);
%         metric = hit_v;
        % -----------------------------------------------------------------
        
        all_metrics(i) = metric;
        
        m_color = [0 0 1];
        
        if metric > 0
            rem_img_1(:,i) = p_cam_i;
            uv = int16(p_cam_s);
            img_mask(uv(2), uv(1)) = 1;
            m_color = [1 0 0];
        end
        
        % Visualization ===================================================
        
%         figure(fig_patch_i), axes('position', [0 0 1 1]);
%         imshow(patch_i);
%         
%         figure(fig_patch_s), axes('position', [0 0 1 1]);
%         imshow(patch_s);
%         
%         figure(fig_th_window), axes('position', [0 0 1 1]);
%         imshow(th_window);
%         
%         figure(fig_corr_res), axes('position', [0 0 1 1]);
%         imshow(method_res/max_v);
%         hold on;
%         plot(hit_col, hit_row, 'r+', 'MarkerSize', 20, 'MarkerEdgeColor', m_color);
%         hold off;
%         
%         figure(fig_window_m), axes('position', [0 0 1 1]);
%         imshow(gaussMask(window_mul*b_size_i, sigma_x));
%         
%         figure(fig_window_i), axes('position', [0 0 1 1]);
%         imshow(window_i);
%         hold on;
%         plot(int16(size(window_i,2)/2), int16(size(window_i,1)/2), ...
%             'rs', 'MarkerSize', b_size_i, 'MarkerEdgeColor', m_color);
%         hold off;
%         
%         plot_h_s.XData = p_cam_s(1);
%         plot_h_s.YData = p_cam_s(2);
%         plot_h_s.MarkerSize = b_size_i*window_mul;
%         plot_h_s.MarkerEdgeColor = m_color;
%         
%         plot_h_i.XData = p_cam_i(1);
%         plot_h_i.YData = p_cam_i(2);
%         plot_h_i.MarkerSize = b_size_i*window_mul;
%         plot_h_i.MarkerEdgeColor = m_color;
%         
%         pause;
        
        % =================================================================
    end
    
%     all_metrics(all_metrics <= 0) = [];
    
    rem_img_1 = int16(rem_img_1);
    rem_img_1(:, rem_img_1(1,:) < 1) = [];
    rem_img_1(:, rem_img_1(2,:) < 1) = [];
    
%     plotRemovedPoints(rem_img_1, imgs_orig{cam_i}, 2, 'Other Image');
    
%     figure(); plot(all_metrics);
    display(['Total projected points for frame ' num2str(frame) ':']);
    display(['  ==> sample = ' num2str(size(rem_img,2))]);
    display(['  ==> cam_1  = ' num2str(total_pts_1)]);
    display(['  ==> filtered = ' num2str(size(rem_img_1,2))]);
    
%     figure(); imshow(img_mask);
    mod_mask = img_mask;
    ne_filt = ones(11,11);
    mod_mask = imfilter(img_mask, ne_filt)-3;
    mod_mask(mod_mask < 0) = 0;
    mod_mask = imdilate(mod_mask, d_shape);
    mod_mask = imerode(mod_mask, e_shape);
    mod_mask = imgaussfilt(mod_mask.*gauss_mul, gauss_var).*gauss_mul;
    mod_mask(mod_mask>1) = 1;
    mod_mask = 1 - mod_mask;
%     figure(); imshow(mod_mask);
    
    new_image = repmat(imgs_orig{cam_s},[1 1 3]);
    new_image(:,:,1) = imgs_orig{cam_s};
    new_image(:,:,2) = imgs_orig{cam_s}.*(mod_mask)+img_mask;
    new_image(:,:,3) = imgs_orig{cam_s}.*(mod_mask);
%     figure(); imshow(new_image);
    imshow(new_image);
    
%     Xs = 1:size(all_metrics,1);
% %     figure(1);
%     subplot(3,1,1), imshow(mod_mask),
%     subplot(3,1,2), imshow(new_image),
%     subplot(3,1,3), plot(Xs, all_metrics, Xs, ones(size(Xs))*metric_th);

    imwrite(new_image, sprintf('../data/processed/gray-%04d.png', frame));
end

% =========================================================================
%% Feature matching and subtraction                 GRAYSCALE
% =========================================================================
close all;

% Parameters
frame           = 45;
frame_start     = 9;
frame_end       = size(rem_frames,2);
frame_dist      = 1;
block_size      = 20;
half_size       = block_size/2;
metric_th       = 0.15;
e_shape_size    = 7;
e_shape_offset  = 1.25;
e_shape_type    = 'disk';
d_shape_size    = 6;
d_shape_offset  = 1.05;
d_shape_type    = 'disk';
gauss_mul       = 1.25;
gauss_mul_off   = 0.1;
gauss_var       = 1.25;
gauss_var_off   = 0.1;
ne_size         = 11; 
ne_min          = 2;
ne_offset       = 1;

% Patch parameters
f_length        = K(1,1);   % Focal length in pixels
height_w        = 0.1;      % Height in meters
d_max           = 25;       % Maximum world distance from camera
d_min           = 4;        % Minimum world distance from camera
min_b_size      = 4;        % Minimum box size
b_ratio         = min_b_size * d_max; % Min patch box size ratio

% Mask Related
mask_layers     = 3;        % Number of mask layers
d_dist          = d_max - d_min; % Distance interval

total_pts = 0;

for frame = frame_start:frame_end
    if rem_frames(frame).scanned == 0
        continue;
    end
    
    if isempty(rem_frames(frame).points) 
        break;
    end
    
    % Initialization
    cam_i = frame - frame_dist;
    cam_i_valid = rem_frames(frame).views(:,(cam_i - rem_frames(frame).first_id));
    cam_s = rem_frames(frame).image_id;
    pts = rem_frames(frame).points;
    
    % Useful variables
    [img_h, img_w] = size(imgs{cam_s});
    
    % prepare the transformation for every point
    T_1 = P_velo_to_img*inv_poses{cam_i};
    T_s = P_velo_to_img*inv_poses{cam_s};
    
    % and an empty mask
    img_mask = zeros(img_h, img_w, mask_layers);
    
    % Save sample picture removed points locations
    rem_img = smartProjectToImage(rem_frames(frame).points, T_s, poses{frame}(1:3,4),...
        vehicleSpeed(poses{frame-1}, poses{frame}), 1.5);
%     display(vehicleSpeed(poses{frame-1}, poses{frame}));
    % remove unused points
    rem_img = int16(rem_img);
    rem_img(:, rem_img(1,:) > img_w) = [];
    rem_img(:, rem_img(2,:) > img_h) = [];
    
    % show the intermediate result
    plotRemovedPoints(rem_img, imgs_orig{cam_s}, 1, 'Original Image');
    pause;
    
    % save metrics... and other statistics
    all_metrics = zeros(size(rem_img, 2), 1);
    total_pts_1 = 0;
    
    % other rem_img
    rem_img_1 = zeros(2, size(pts, 2));
    
    % image borders
    top     = block_size;
    left    = block_size;
    bottom  = img_h - block_size;
    right   = img_w - block_size;
    
    % Visualization .......................................................
%     fig_patch_s     = figure('Name', 'Samble patch');
%                       axes('position', [0 0 1 1]);
%     fig_patch_i     = figure('Name', 'Test patch');
%                       axes('position', [0 0 1 1]);
%     fig_method_r    = figure('Name', 'Method Result');
%                       axes('position', [0 0 1 1]);
%     fig_img_s       = figure('Name', 'Sample Image');
%                       axes('position', [0 0 1 1]), ...
%                       imshow(imgs_orig{cam_s});
%     hold on;
%     plot_h_s        = plot(300, 300, 'rs', 'MarkerSize', 2, 'MarkerEdgeColor', [1,0,0]);
%     hold off;
    % ---------------------------------------------------------------------

    for i = 1:size(pts,2)
        % skip points not visible by sensor
        if ~cam_i_valid(i)
            continue;
        end
        
        % and not visible by at least one camera
        p_cam_i = projectToImage(pts(:,i), T_1);
        p_cam_s = projectToImage(pts(:,i), T_s);
        if isempty(p_cam_i) || p_cam_i(1) > right || p_cam_i(2) > bottom ...
                            || p_cam_i(1) < left  || p_cam_i(2) < top || ...
           isempty(p_cam_s) || p_cam_s(1) > right || p_cam_s(2) > bottom ...
                            || p_cam_s(1) < left  || p_cam_s(2) < top
            continue;
        end
        
        % now we are ready to work
        total_pts_1 = total_pts_1 + 1;
        
        % 3D point in both frames...
        point_is = inv_poses{cam_s}*pts(:,i);
        point_i  = inv_poses{cam_i}*pts(:,i);
        
        % =================================================================
        % extract features from both images...
        % HOG patches =================================
        % ----------------- SURF Descriptor -------------------------------
%         [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'SURF', 'SURFSize', 128);
%         [feat_1, valid_pts_1] = extractFeatures(imgs{cam_i}, p_cam_i', 'Method', 'SURF', 'SURFSize', 128);
%         
%         metric = sum(sum((feat_s - feat_1).^2));
% %         metric = max((feat_s - feat_1).^2);
% %         metric = corr2(feat_s, feat_1);
% %         metric = max(conv2(feat_s, feat_1));
% %         metric = max(abs(xcorr2(feat_s, feat_1)));

        % ----------------- SIFT Descriptor -------------------------------
        %     ... missing for now :(
        
        % Region-Based Features ===========================================
        % ----------------- MSER Descriptor -------------------------------
%         [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'MSER');
%         [feat_1, valid_pts_1] = extractFeatures(imgs{cam_i}, p_cam_i', 'Method', 'MSER');
%         
%         metric = sum(sum((feat_s - feat_1).^2));
% %         metric = max((feat_s - feat_1).^2);
% %         metric = corr2(feat_s, feat_1);
% %         metric = max(conv2(feat_s, feat_1));
% %         metric = max(abs(xcorr2(feat_s, feat_1)));

        % Binary Features =================================================
        % ----------------- FREAK Descriptor ------------------------------
%         [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'FREAK');
%         [feat_i, valid_pts_i] = extractFeatures(imgs{cam_i}, p_cam_i', 'Method', 'FREAK');
                      
        % ---------------- BRISK Descriptor -------------------------------
%         [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'BRISK');
%         [feat_i, valid_pts_i] = extractFeatures(imgs{cam_i}, p_cam_i', 'Method', 'BRISK');
        
        % ---------------- BRIEF Descriptor -------------------------------
        %     ... missing for now :(
        
        % ---------------- ORB Descriptor ---------------------------------
        %     ... missing for now :(
        
%         if feat_s.NumFeatures == feat_i.NumFeatures
%             metric = sum(xor(feat_s.Features, feat_i.Features));
%             metric = metric/feat_s.NumBits;
%         else
%             metric = metric_th*10;
%         end
        
        % Intensity patches (pixel blocks) ================================
% %         [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'Block');
% %         [feat_1, valid_pts_1] = extractFeatures(imgs{cam_1}, p_cam_1', 'Method', 'Block');
%         
        b_size_s = round(b_ratio/point_is(1));
        b_size_i = round(b_ratio/point_i(1));
        
        feat_s = extractImageBlock(imgs{cam_s}, p_cam_s, b_size_i);
        feat_i = extractImageBlock(imgs{cam_i}, p_cam_i, b_size_i);
        
%         feat_s = imresize(feat_s, b_size_i/b_size_s);
%         feat_s = imresize(feat_s, b_size_i/size(feat_s,1)); % for robustness
        
        if std(feat_s(:)) == 0 || std(feat_s(:)) == 0
            continue;
        end
        
        % SSD ------------------------------------
%         method_res = (feat_s - feat_i).^2;
%         metric = sum(sum(method_res));
        
        % Normalized Xcorrelation ----------------
        method_res = normxcorr2(feat_s, feat_i);
        metric = 1 - max(method_res(:));
% %         metric = max((feat_s - feat_1).^2);
% %         metric = corr2(feat_s, feat_1);
% %         metric = max(conv2(feat_s, feat_1));
% %         metric = max(abs(xcorr2(feat_s, feat_1)));

        m_color = [0 0 1];
        
        if metric > metric_th
            rem_img_1(:,i) = p_cam_i;
            uv = int16(p_cam_s);
            mask_layer = ceil(min(abs(point_is(1)-d_min)/d_dist,1)*mask_layers);
            img_mask(uv(2), uv(1), mask_layer) = 1;
            m_color = [1 0 0];
        end
        
         % Visualization....................................................
%         pause;
%         
%         figure(fig_patch_s), imshow(feat_s);
%         
%         figure(fig_patch_i), imshow(feat_i);
%         
%         figure(fig_method_r), surf(method_res), shading flat;
% 
%         plot_h_s.XData = p_cam_s(1);
%         plot_h_s.YData = p_cam_s(2);
%         plot_h_s.MarkerSize = block_size;
%         plot_h_s.MarkerEdgeColor = m_color;
        % =================================================================
        
        all_metrics(i) = metric;
        
    end
    
    all_metrics(all_metrics <= 0) = [];
    
    rem_img_1 = int16(rem_img_1);
    rem_img_1(:, rem_img_1(1,:) < 1) = [];
    rem_img_1(:, rem_img_1(2,:) < 1) = [];
    
%     plotRemovedPoints(rem_img_1, imgs{cam_i}, 2, 'Other Image');
    
%     figure(); plot(all_metrics);
    display(['Total projected points for frame ' num2str(frame) ':']);
    display(['  ==> sample = ' num2str(size(rem_img,2))]);
    display(['  ==> cam_1  = ' num2str(total_pts_1)]);
    display(['  ==> filtered = ' num2str(size(rem_img_1,2))]);
    
    total_pts = total_pts + size(rem_img_1,2);
    
%     figure(); imshow(sum(img_mask, 3));
%     figure();
    mod_mask = img_mask;
    final_mask = zeros(img_h, img_w);
    middle_layer = floor(mask_layers/2) + 1;
    for layer_i = 1:mask_layers
        ne_s = ne_size + ne_offset*(middle_layer - layer_i);
        ne_filt = ones(ne_s, ne_s);
        
        mod_mask = imfilter(img_mask(:,:,layer_i), ne_filt) - ne_min;
        mod_mask(mod_mask < 0) = 0;
        
        e_shape_s = round(e_shape_size + e_shape_offset*(middle_layer - layer_i));
        d_shape_s = round(d_shape_size + d_shape_offset*(middle_layer - layer_i));
        e_shape = strel(e_shape_type, e_shape_s); 
        d_shape = strel(d_shape_type, d_shape_s);
        mod_mask = imdilate(mod_mask, d_shape);
        mod_mask = imerode(mod_mask, e_shape);
        
        gauss_var_s = gauss_var + gauss_var_off*(middle_layer - layer_i);
        gauss_mul_s = gauss_mul + gauss_mul_off*(middle_layer - layer_i);
        mod_mask = imgaussfilt(mod_mask.*gauss_mul_s, gauss_var_s).*gauss_mul_s;
        
        mod_mask(mod_mask<=0) = 0;
        final_mask = final_mask + mod_mask;
        
%         imshow(final_mask);
%         pause;
    end
    final_mask(final_mask>1) = 1;
    final_mask = 1 - final_mask;
%     figure(); imshow(final_mask);
    
%     new_image = repmat(imgs_orig{cam_s},[1 1 3]);
    new_image(:,:,1) = imgs_orig{cam_s};
    new_image(:,:,2) = imgs_orig{cam_s}.*(final_mask);
    new_image(:,:,3) = imgs_orig{cam_s}.*(final_mask);
%     figure(); imshow(new_image);
    imshow(new_image);
    
%     Xs = 1:size(all_metrics,1);
% %     figure(1);
%     subplot(3,1,1), imshow(mod_mask),
%     subplot(3,1,2), imshow(new_image),
%     subplot(3,1,3), plot(Xs, all_metrics, Xs, ones(size(Xs))*metric_th);

    imwrite(new_image, sprintf('../data/processed/gray-%04d.png', frame));
end

display(['Total points removed: ' num2str(total_pts)]);

% =========================================================================
%% Feature matching and subtraction                 COLOR BETTER
% =========================================================================
close all;

% Parameters
frame           = 48;
frame_start     = 9;
frame_end       = size(rem_frames,2);
frame_dist      = 2;
block_size      = 20;
half_size       = block_size/2;
metric_th       = 0.05;
e_shape_size    = 7;
e_shape_offset  = 1.25;
e_shape_type    = 'disk';
e_shape         = strel(e_shape_type, e_shape_size);
d_shape_size    = 6;
d_shape_offset  = 1.05;
d_shape_type    = 'disk';
d_shape         = strel(d_shape_type, d_shape_size);
gauss_mul       = 1.25;
gauss_mul_off   = 0.1;
gauss_var       = 1.25;
gauss_var_off   = 0.1;
ne_size         = 11; 
ne_min          = 2;
ne_offset       = 1;

% Patch parameters
f_length        = K(1,1);   % Focal length in pixels
height_w        = 0.1;      % Height in meters
d_max           = 25;       % Maximum world distance from camera
d_min           = 4;        % Minimum world distance from camera
min_b_size      = 5;        % Minimum box size
b_ratio         = min_b_size * d_max; % Min patch box size ratio

% Mask Related
mask_layers     = 1;        % Number of mask layers
d_dist          = d_max - d_min; % Distance interval

total_pts = 0;

% for frame = frame_start:frame_end
%     if rem_frames(frame).scanned == 0
%         continue;
%     end
%     
%     if isempty(rem_frames(frame).points) 
%         break;
%     end
    
    % Initialization
    cam_i = frame - frame_dist - 7;
    cam_i_valid = rem_frames(frame).views(:,(cam_i - rem_frames(frame).first_id));
    cam_s = rem_frames(frame).image_id - 7;
    pts = rem_frames(frame).points;
    
    
    % Useful variables
    [img_h, img_w, ~] = size(imgs{cam_s});
    
    % prepare the transformation for every point
    T_1 = P_velo_to_img*inv_poses{cam_i};
    T_s = P_velo_to_img*inv_poses{cam_s};
    
    % and an empty mask
    img_mask = zeros(img_h, img_w);
    
    % full masks
    full_mask_i = ones(img_h, img_w);
    full_mask_s = full_mask_i;
    
    % Save sample picture removed points locations
    rem_img = projectToImage(rem_frames(frame).points, T_s);
    
    % remove unused points
    rem_img = int16(rem_img);
    rem_img(:, rem_img(1,:) > img_w) = [];
    rem_img(:, rem_img(2,:) > img_h) = [];
    
    % show the intermediate result
%     plotRemovedPoints(rem_img, imgs_orig{cam_s}, 1, 'Original Image');
    
    % save metrics... and other statistics
    all_metrics = zeros(size(rem_img, 2), 1);
    dep_metrics = zeros(size(rem_img, 2), 1);
    total_pts_1 = 0;
    
    % other rem_img
    rem_img_1 = zeros(2, size(pts, 2));
    
    % image borders
    top     = block_size;
    left    = block_size;
    bottom  = img_h - block_size;
    right   = img_w - block_size;
    
    % Visualization .......................................................
%     fig_patch_s     = figure('Name', 'Samble patch');
%                       axes('position', [0 0 1 1]);
%     fig_patch_i     = figure('Name', 'Test patch');
%                       axes('position', [0 0 1 1]);
%     fig_method_rr   = figure('Name', 'Method Result');
%                       axes('position', [0 0 1 1]);
%     fig_method_rg   = figure('Name', 'Method Result');
%                       axes('position', [0 0 1 1]);
%     fig_method_rb   = figure('Name', 'Method Result');
%                       axes('position', [0 0 1 1]);
%     fig_img_s       = figure('Name', 'Sample Image');
%                       axes('position', [0 0 1 1]), ...
%                       imshow(imgs_orig{cam_s});
%     hold on;
%     plot_h_s        = plot(300, 300, 'rs', 'MarkerSize', 2, 'MarkerEdgeColor', [1,0,0]);
%     hold off;
    % ---------------------------------------------------------------------

    for i = 1:size(pts,2)
        % skip points not visible by sensor
        if ~cam_i_valid(i)
            continue;
        end
        
        % and not visible by at least one camera
        p_cam_i = projectToImage(pts(:,i), T_1);
        p_cam_s = projectToImage(pts(:,i), T_s);
        if isempty(p_cam_i) || p_cam_i(1) > right || p_cam_i(2) > bottom ...
                            || p_cam_i(1) < left  || p_cam_i(2) < top || ...
           isempty(p_cam_s) || p_cam_s(1) > right || p_cam_s(2) > bottom ...
                            || p_cam_s(1) < left  || p_cam_s(2) < top
            continue;
        end
        
        % now we are ready to work
        total_pts_1 = total_pts_1 + 1;
        
        % 3D point in both frames...
        point_is = inv_poses{cam_s}*pts(:,i);
        point_i  = inv_poses{cam_i}*pts(:,i);
        
        % =================================================================
        % extract features from both images...
        % Intensity patches (pixel blocks) ================================
% %         [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'Block');
% %         [feat_1, valid_pts_1] = extractFeatures(imgs{cam_1}, p_cam_1', 'Method', 'Block');
%         
        b_size_s = round(b_ratio/point_is(1));
        b_size_i = round(b_ratio/point_i(1));
        
        feat_s = extractImageBlock(imgs{cam_s}, p_cam_s, b_size_s);
        feat_i = extractImageBlock(imgs{cam_i}, p_cam_i, b_size_i);
        
%         feat_s = blockWarp(feat_s, feat_i, point_is, inv_poses{cam_i}*poses{cam_s}, K, P_velo_to_img);
%         feat_s = imresize(feat_s, b_size_i/b_size_s);
%         feat_s = imresize(feat_s, b_size_i/size(feat_s,1)); % for robustness
        feat_s = smartResize(feat_s, feat_i);
        
        err_function = true;
        if min(min(std(feat_s))) > 0.001 && min(min(std(feat_i))) > 0.001
           if err_function
                % SSD ----------------------------------------------
                metric = sum((feat_s(:) - feat_i(:)).^2)/(b_size_i^2);
            else
                % Normalized Xcorrelation ----------------
                method_res = normxcorr2(feat_s(:,:,1), feat_i(:,:,1));
                method_res(:,:,3) = normxcorr2(feat_s(:,:,3), feat_i(:,:,3));
                method_res(:,:,2) = normxcorr2(feat_s(:,:,2), feat_i(:,:,2));
                metric = 1 - max(max(method_res));
                % %         metric = max((feat_s - feat_1).^2);
                % %         metric = corr2(feat_s, feat_1);
                % %         metric = max(conv2(feat_s, feat_1));
                % %         metric = max(abs(xcorr2(feat_s, feat_1)));
            end
            
            m_color = [0 0 1];
            
            all_metrics(i) = max(max(metric));
            
            if metric > metric_th;
                rem_img_1(:,i) = p_cam_i;
                uv = int16(p_cam_s);
%                 mask_layer = ceil(min(abs(point_is(1)-d_min)/d_dist,1)*mask_layers);
                img_mask(uv(2), uv(1)) = 1;
                m_color = [1 0 0];
                
                continue;
            end
        end
        
%         b_size_s = int16(b_size_s*0.5);
%         b_size_i = int16(b_size_i*0.5);
        
        % Perform the depth SSD
        feat_s = extractImageBlock(depths{cam_s}, p_cam_s, b_size_s);
        feat_i = extractImageBlock(depths{cam_i}, p_cam_i, b_size_i);
        
        %         feat_s = blockWarp(feat_s, feat_i, point_is, inv_poses{cam_i}*poses{cam_s}, K, P_velo_to_img);
        feat_s = smartResize(feat_s, feat_i);
        
        metric = sum((feat_s(:) - feat_i(:)).^2)/b_size_i^2;
        
        dep_metrics(i) = metric;
        
        if metric > metric_th;%*(sum(feat_s(:).^2))
            rem_img_1(:,i) = p_cam_i;
            uv = int16(p_cam_s);
%             mask_layer = ceil(min(abs(point_is(1)-d_min)/d_dist,1)*mask_layers);
            img_mask(uv(2), uv(1)) = 1;
            m_color = [1 0 0];
        end
        
        % Save the full masks .............................................
        uv_s = int16(p_cam_s);
        uv_i = int16(p_cam_i);
        
        full_mask_i(uv_i(2), uv_i(1)) = 1 + (point_i(1) - d_max)/d_max;
        full_mask_s(uv_s(2), uv_s(1)) = 1 + (point_is(1) - d_max)/d_max;
        
        
        % Visualization....................................................
%         pause;
%         
%         figure(fig_patch_s), imshow(feat_s);
%         
%         figure(fig_patch_i), imshow(feat_i);
%         
%         figure(fig_method_rr), surf(method_res(:,:,1)), shading flat;
%         figure(fig_method_rg), surf(method_res(:,:,2)), shading flat;
%         figure(fig_method_rb), surf(method_res(:,:,3)), shading flat;
% 
%         plot_h_s.XData = p_cam_s(1);
%         plot_h_s.YData = p_cam_s(2);
%         plot_h_s.MarkerSize = block_size;
%         plot_h_s.MarkerEdgeColor = m_color;
        % =================================================================
        
        
        
    end
    
    all_metrics(all_metrics <= 0) = [];
    dep_metrics(dep_metrics <= 0) = [];
    
    rem_img_1 = int16(rem_img_1);
    rem_img_1(:, rem_img_1(1,:) < 1) = [];
    rem_img_1(:, rem_img_1(2,:) < 1) = [];
    
%     plotRemovedPoints(rem_img_1, imgs{cam_i}, 2, 'Other Image');
%     
%     figure(); plot(all_metrics);
%     figure(); plot(dep_metrics); title('Dep_metrics');
    display(['Total projected points for frame ' num2str(frame) ':']);
    display(['  ==> sample = ' num2str(size(rem_img,2))]);
    display(['  ==> cam_1  = ' num2str(total_pts_1)]);
    display(['  ==> filtered = ' num2str(size(rem_img_1,2))]);
    
    total_pts = total_pts + size(rem_img_1,2);
%     
%     figure(); imshow(sum(img_mask, 3));
%     figure();
    mod_mask = imdilate(img_mask, d_shape);
    mod_mask = imerode(mod_mask, e_shape);
    final_mask = imgaussfilt(mod_mask.*gauss_mul, gauss_var).*gauss_mul;
%     mod_mask(mod_mask>1) = 1;
    final_mask(final_mask>1) = 1;
    final_mask = 1 - final_mask;
%     figure(); imshow(final_mask);
    
%     new_image = repmat(imgs_orig{cam_s},[1 1 3]);
    new_image(:,:,1) = imgs_orig{cam_s}(:,:,1);
    new_image(:,:,2) = imgs_orig{cam_s}(:,:,2).*(final_mask);
    new_image(:,:,3) = imgs_orig{cam_s}(:,:,3).*(final_mask);
%     figure(); imshow(new_image);
    imshow(new_image);
    
%     Xs = 1:size(all_metrics,1);
% %     figure(1);
%     subplot(3,1,1), imshow(mod_mask),
%     subplot(3,1,2), imshow(new_image),
%     subplot(3,1,3), plot(Xs, all_metrics, Xs, ones(size(Xs))*metric_th);

%     imwrite(new_image, sprintf('../data/processed/gray-%04d.png', frame));
% end

display(['Total points removed: ' num2str(total_pts)]);
% =========================================================================
%% Feature matching and subtraction              COLOR
% =========================================================================
close all;

% Parameters
frame_start = 9;
frame_end   = size(rem_frames,2);
frame_dist  = 1;
block_size  = 15;
half_size   = block_size/2;
metric_th   = 0.8;
e_shape     = strel('disk',8);
d_shape     = strel('disk',7);
gauss_mul   = 1.75;
gauss_var   = 4;

for frame = frame_start:frame_end
    if rem_frames(frame).scanned == 0
        continue;
    end
    
    % Initialization
    cam_i = frame - frame_dist;
    cam_i_valid = rem_frames(frame).views(:,(cam_i - rem_frames(frame).first_id));
    cam_s = rem_frames(frame).image_id;
    pts = rem_frames(frame).points;
    
    % Useful variables
    [img_h, img_w, ~] = size(imgs{cam_s});
    
    % prepare the transformation for every point
    T_1 = P_velo_to_img*inv_poses{cam_i};
    T_s = P_velo_to_img*inv_poses{cam_s};
    
    % and an empty mask
    img_mask = zeros(img_h, img_w);
    
    % Save sample picture removed points locations
    rem_img = projectToImage(rem_frames(frame).points, T_s);
    
    % remove unused points
    rem_img = int16(rem_img);
    rem_img(:, rem_img(1,:) > img_w) = [];
    rem_img(:, rem_img(2,:) > img_h) = [];
    
    % show the intermediate result
%     plotRemovedPoints(rem_img, imgs{cam_s}, 1, 'Original Image');
    
    % save metrics... and other statistics
    all_metrics = zeros(size(rem_img, 2), 1);
    total_pts_1 = 0;
    
    % other rem_img
    rem_img_1 = zeros(2, size(pts, 2));
    
    % image borders
    top     = block_size;
    left    = block_size;
    bottom  = img_h - block_size;
    right   = img_w - block_size;
    
    for i = 1:size(pts,2)
        % skip points not visible by sensor
        if ~cam_i_valid(i)
            continue;
        end
        
        % and not visible by at least one camera
        p_cam_i = projectToImage(pts(:,i), T_1);
        p_cam_s = projectToImage(pts(:,i), T_s);
        if isempty(p_cam_i) || p_cam_i(1) > right || p_cam_i(2) > bottom ...
                            || p_cam_i(1) < left  || p_cam_i(2) < top || ...
           isempty(p_cam_s) || p_cam_s(1) > right || p_cam_s(2) > bottom ...
                            || p_cam_s(1) < left  || p_cam_s(2) < top
            continue;
        end
        
        % now we are ready to work
        total_pts_1 = total_pts_1 + 1;
        
        % extract features from both images...
        %     [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'SURF', 'SURFSize', 128);
        %     [feat_1, valid_pts_1] = extractFeatures(imgs{cam_1}, p_cam_1', 'Method', 'SURF', 'SURFSize', 128);
        %     [feat_s, valid_pts_s] = extractFeatures(imgs{cam_s}, p_cam_s', 'Method', 'Block');
        %     [feat_1, valid_pts_1] = extractFeatures(imgs{cam_1}, p_cam_1', 'Method', 'Block');
        
        uv = int16(p_cam_s);
        feat_s = imgs{cam_s}(uv(2)-half_size:uv(2)+half_size, uv(1)-half_size:uv(1)+half_size,:);
        uv = int16(p_cam_i);
        feat_i = imgs{cam_i}(uv(2)-half_size:uv(2)+half_size, uv(1)-half_size:uv(1)+half_size,:);
        
%         metric = sum(sum(sum((feat_s - feat_1).^2)));
%         if metric > metric_th
        metric = max(mean([abs(corr(feat_s(:,:,1), feat_i(:,:,1))), ...
                           abs(corr(feat_s(:,:,2), feat_i(:,:,2))), ...
                           abs(corr(feat_s(:,:,3), feat_i(:,:,3)))]));
        if metric < metric_th
            rem_img_1(:,i) = p_cam_i;
            uv = int16(p_cam_s);
            img_mask(uv(2), uv(1)) = 1;
        end
        
        %     [index_pairs, metric] = matchFeatures(features_tgt, features_src, 'Metric', 'SSD');
        all_metrics(i) = metric;
        %     matched_pts_tgt = valid_pts_tgt(index_pairs(:,1),:);
        %     matched_pts_src = valid_pts_src(index_pairs(:,2),:);
        
    end
    
    all_metrics(all_metrics <= 0) = [];
    
    rem_img_1 = int16(rem_img_1);
    rem_img_1(:, rem_img_1(1,:) < 1) = [];
    rem_img_1(:, rem_img_1(2,:) < 1) = [];
    
%     plotRemovedPoints(rem_img_1, imgs{cam_1}, 2, 'Other Image');
    
%     figure(); plot(all_metrics);
    display(['Total projected points for frame ' num2str(frame) ':']);
    display(['  ==> sample = ' num2str(size(rem_img,2))]);
    display(['  ==> cam_1  = ' num2str(total_pts_1)]);
    display(['  ==> filtered = ' num2str(size(rem_img_1,2))]);
    
%     figure(); imshow(img_mask);
    
    mod_mask = imdilate(img_mask, d_shape);
    mod_mask = imerode(mod_mask, e_shape);
    mod_mask = imgaussfilt(mod_mask.*gauss_mul, gauss_var).*gauss_mul;
    mod_mask(mod_mask>1) = 1;
%     figure(); imshow(mod_mask);
    
    mod_mask(:,:,2) = 0; %mod_mask(:,:,1);
    mod_mask(:,:,3) = 0;
%     mod_mask(:,:,1) = 0;
    
    new_image = imgs{cam_s}+(mod_mask);
%     figure(); imshow(new_image);
    imshow(new_image);
    
    imwrite(new_image, sprintf('../data/processed/color-%04d.png', frame));
end

% =========================================================================
%% Frame: cam by cam
% =========================================================================
% project to image plane (exclude luminance)
first_time = true;
frame = 40;
if ~rem_frames(frame).scanned
    error('Requested frame has not been scanned');
end
for cam = 1:size(rem_frames(frame).views,2)
    cam_id = cam + rem_frames(frame).first_id - 1;
    cam_pts_valid = rem_frames(frame).views(:,cam);
    cam_pts = rem_frames(frame).points(:, cam_pts_valid>0);
    % pts{:}(3,:) = -pts{:}(3,:);
    % figure('Position',[20 100 size(imgs{cam},2) size(imgs{cam},1)]); axes('Position',[0 0 1 1]);
%     figure(1);
%     imshow(imgs{cam_id}); hold on;
    % project to image plane (exclude luminance)
%     velo_img = project(rem_frames(frame).views{frame}(1:3,:)',P_velo_to_img*inv_poses{cam})';
    velo_img = projectToImage(cam_pts, P_velo_to_img*inv_poses{cam_id});
%     pts = R_pts*rem_frames(frame).views{cam};
%     tr_points = pts;
%     pts(3,:) = tr_points(1,:);
%     pts(2,:) = -tr_points(3,:);
%     pts(1,:) = -tr_points(2,:);
%     velo_img = projectToImage(pts,P*P_velo2cam);
    
    % remove unused points
    velo_img(:, velo_img(1,:) > 1242) = [];
    velo_img(:, velo_img(2,:) > 375) = [];
    
    % save as integers and swap xy with row column
    rem_img = int16(velo_img);
    % rem_img = velo_img;
    % rem_img(1,:) = int16(rem_img(2,:));
    % rem_img(2,:) = int16(velo_img(1,:));
    
    % Show removed points on image
    plotRemovedPoints(velo_img, imgs{cam_id}, 1);
%     for i=1:size(velo_img,1)
%         plot(velo_img(1,:), velo_img(2,:), 'r*', 'MarkerSize', 2, 'MarkerEdgeColor', [1,0,0]);
%     end
    
    % Show image with removed points
    img = imgs{cam_id};
    % for i = 1:size(rem_img,2)
    %     img(rem_img(2,i),rem_img(1,i)) = 1.0;
    % end
    
    figure(2);
    imshow(img);
    
    if first_time
        velo_img_last = velo_img;
        img_last = img;
        first_time = false;
        continue;
    end
    
    [features_tgt, valid_pts_tgt] = extractFeatures(img_last, velo_img_last', 'Method', 'SURF');
    [features_src, valid_pts_src] = extractFeatures(img, velo_img', 'Method', 'SURF');
    
    index_pairs = matchFeatures(features_tgt, features_src, 'Metric', 'SSD');
    matched_pts_tgt = valid_pts_tgt(index_pairs(:,1),:);
    matched_pts_src = valid_pts_src(index_pairs(:,2),:);
    
    figure(2);
    showMatchedFeatures(img, img_last, matched_pts_src, matched_pts_tgt);
    
    [F, inliers] = estimateFundamentalMatrix(matched_pts_tgt, matched_pts_src, ...
        'Method', 'RANSAC', 'NumTrials', 4000, 'DistanceThreshold', 0.1);
    
    % Show Matches ------------------------------------------
    % figure;
    good_inliers = size(inliers(inliers<1), 1);
    showMatchedFeatures(img, img_last, matched_pts_src(~inliers,:), matched_pts_tgt(~inliers,:));
    
    velo_img_last = velo_img;
    img_last = img;
    
    % cam = cam + 1;
    pause
end

% =========================================================================
%% Image Subtraction using GICP Transform for warping
% =========================================================================
close all;

% Parameters
frame       = 45;
frame_start = 9;
frame_end   = size(rem_frames,2);
frame_dist  = 1;
metric_th   = 0.8;
gauss_mul   = 1.75;
gauss_var   = 3;

% for frame = frame_start:frame_end
%     if rem_frames(frame).scanned == 0
%         continue;
%     end
    
    % Initialization
    cam_i = frame - frame_dist;
    img_i = imgs_orig{cam_i};
    img_s = imgs_orig{frame};
    
    figure(1);
    subplot(2,1,1), imshow(img_s), title('Sample Image');
    subplot(2,1,2), imshow(img_i), title('Working Image');
    
    rel_pose_s = inv_poses{frame}*poses{cam_i};
    R = rel_pose_s(1:3,1:3);
    t = R_pts(1:3,1:3)*rel_pose_s(1:3,4);%/1000;
%     t = [0 0 0]';
    
    n_plane = P_velo_to_img(3,1:3)';
    d_plane = P_velo_to_img(3,4)*1000;
    
    Hx = R - (t*n_plane')/d_plane;
    H = K*Hx*inv(K);
    
    Rfixed = imref2d(size(img_i));
    tform = projective2d(H);
    warped = imwarp(img_s, tform,'FillValues', 0,'OutputView',Rfixed);
    
%     [n_rows, n_cols] = size(img_i);
%     
%     fx = K(1,1);
%     fy = K(2,2);
%     cx = K(1,3);
%     cy = K(2,3);
% 
%     mul_c = 0.00215;
%     df = -fx*0.5*4.56e-3; %+5*d_plane/1000;
% 
% %     proj_img = img_i;
%     proj_img = zeros(n_rows, n_cols);
%     bounds = [0, n_rows+1; 0, n_cols+1];
%     p_count = 0;
% %     T = [[R, -R*t]; 0 0 0 1];
%     T = [[R, t]; 0 0 0 1];
%     cc = [ cx*mul_c, cy*mul_c, df, 0]';
% %     rc = -[[1:n_cols], 1:n_rows, 0, -1]';
% %     Xs = T*(rc+cc);
% %     uv = [K, [0 0 0]']*Xs;
%     P0 = [K, [ 0 0 0 ]'];
%     % uvs = zeros(n_rows, n_cols, 2);
%     for r = 1:n_rows
%         for c = 1:n_cols
%     %         uv = P0*[(R'*([c/fx-cx; r/fy - cy; fx]-t));1];
%             uv = P0*T*([-c*mul_c, -r*mul_c, 0, 1]'+cc);
%             u = int16(uv(2)/uv(3));
%             v = int16(uv(1)/uv(3));
%     %         uvs(r,c) = [u,v];
%             if u > 0 && u <= n_rows && v > 0 && v <= n_cols
%                 proj_img(u,v) = img_s(r,c);
%                 p_count = p_count+1;
%             end
%         end
%     end
%     
%     figure(), imshow(proj_img);
    
    size(warped(warped > 0))
    figure(), imshow(warped);
% end
