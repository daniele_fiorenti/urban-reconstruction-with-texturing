function [ mask ] = gaussMask( size, sigma )
%GAUSSMASK Creates a circular mask with gaussian distribution
if ~exist('size','var')
    size = 31;
end
if ~exist('sigma','var')
    sigma = size/3.16;
end
mask = fspecial('gaussian', [size size], sigma);
mask = mask./max(max(mask));
end

