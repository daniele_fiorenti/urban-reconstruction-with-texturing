function [ points ] = loadLidarPoints( file )
%LOADLIDARPOINTS Load lidar points from specified file
%   Given the specified file this function loads all points in that file in
%   homogeneous coordinates
    fin = fopen(file, 'r');
    cloud = 1;
    n = fscanf(fin, '%d', 1);
    while ~feof(fin)
        if isnan(n)
            break;
        end
        pts = ones(4,n);
        for i = 1:n
            pts(1:3,i) = fscanf(fin, '%f %f %f\n', 3)';
        end
        points{cloud} = pts;
        cloud = cloud + 1;
        n = fscanf(fin, '%d', 1);
    end
    fclose(fin);
end

