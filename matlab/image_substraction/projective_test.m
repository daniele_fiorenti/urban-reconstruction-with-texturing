%% Various tests to improve homography and image subtraction
% K = [9.842439e+02 0.000000e+00 6.900000e+02; ... %Calibration Matrix
%     0.000000e+00 9.808141e+02 2.331966e+02; ...
%     0.000000e+00 0.000000e+00 1.000000e+00];
K = [7.215377e+02 0.000000e+00 6.095593e+02; ...
     0.000000e+00 7.215377e+02 1.728540e+02; ...
     0.000000e+00 0.000000e+00 1.000000e+00];
% K = [9.842439e+02 0.000000e+00 6.210000e+02; ... %Calibration Matrix
%     0.000000e+00 9.808141e+02 1.875966e+02; ...
%     0.000000e+00 0.000000e+00 1.000000e+00];
W = [0 -1 0; 1 0 0; 0 0 1]; %Needed for essential svd

img_tgt = im2double(imread(sprintf('../data/right/%010d.png', 70)));
img_src = im2double(imread(sprintf('../data/right/%010d.png', 71)));
img_tgt_color = im2double(imread(sprintf('../data/left_color/%010d.png', 0)));
img_src_color = im2double(imread(sprintf('../data/left_color/%010d.png', 1)));

%% Testing Section
tformEstimate = imregcorr(img_src, img_tgt);

Rfixed = imref2d(size(img_tgt));
movingReg = imwarp(img_src,tformEstimate,'OutputView',Rfixed);

figure, imshowpair(img_tgt,movingReg,'falsecolor');

[optimizer, metric] = imregconfig('monomodal');
movingRegistered = imregister(img_src, img_tgt,...
    'affine', optimizer, metric,'InitialTransformation',tformEstimate);

figure
imshowpair(img_tgt, movingRegistered,'Scaling','joint');

figure
imshowpair(img_tgt, movingRegistered, 'diff');
%% Calculation Section
% Smooth images ---------------------------------------------
gauss_sigma = 1.4;
img_tgt_smooth = imgaussfilt(img_tgt, gauss_sigma);
img_src_smooth = imgaussfilt(img_src, gauss_sigma);
img_tgt_color_smooth = imgaussfilt(img_tgt_color, gauss_sigma);
img_src_color_smooth = imgaussfilt(img_src_color, gauss_sigma);

% Get Features-----------------------------------------------
pts_tgt = detectSURFFeatures(img_tgt_smooth, 'MetricThreshold', 10);
pts_src = detectSURFFeatures(img_src_smooth, 'MetricThreshold', 10);
[features_tgt, valid_pts_tgt] = extractFeatures(img_tgt_smooth, pts_tgt);
[features_src, valid_pts_src] = extractFeatures(img_src_smooth, pts_src);

% Match Features-----------------------------------------
index_pairs = matchFeatures(features_tgt, features_src);
matched_pts_tgt = valid_pts_tgt(index_pairs(:,1));
matched_pts_src = valid_pts_src(index_pairs(:,2));

[F, inliers] = estimateFundamentalMatrix(matched_pts_tgt, matched_pts_src, ...
    'Method', 'RANSAC', 'NumTrials', 4000, 'DistanceThreshold', 0.1);

% Show Matches ------------------------------------------
% figure;
good_inliers = size(inliers(inliers>0), 1);
showMatchedFeatures(img_src, img_tgt, matched_pts_src(inliers,:), matched_pts_tgt(inliers,:));
% matched_pts_src(inliers,:).Location;
tform = fitgeotrans(matched_pts_src(inliers,:).Location, matched_pts_tgt(inliers,:).Location, 'projective');
% tform = fitgeotrans(matched_pts_src.Location, matched_pts_tgt.Location, 'polynomial', 2);
Rfixed = imref2d(size(img_tgt));
registered1 = imwarp(img_src,tform,'FillValues', 0,'OutputView',Rfixed);
figure, imshow(img_tgt), title('Original');
figure, imshow(img_src), title('Start');
figure, imshow(registered1), title('Moved');
fig_diff = registered1*2 - img_tgt*2;
fig_diff(fig_diff < 0) = 0;
figure, imshow(fig_diff), title('Diff');
figure, imshowpair(img_tgt, registered1, 'diff');
imwrite(img_tgt, '../data/image1.png');
imwrite(img_src, '../data/image2.png');
imwrite(registered1, '../data/image2_warped.png');
imwrite(fig_diff, '../data/image2_warped-image1.png');
imwrite(im2bw(fig_diff,0.4), '../data/subtraction_mask.png');
%%
% % Homography Estimation-----------------------------------------
% [H, inlier_pts_src, inlier_pts_tgt] = estimateGeometricTransform(matched_pts_src, ...
%     matched_pts_tgt,'projective', 'MaxNumTrials', 4000, 'Confidence', 99, 'MaxDistance', 4.5);
% 
% % Image warping ------------------------------------------------
% output_view = imref2d(size(img_tgt_smooth));
% warped_img = im2double(imwarp(img_src, H, 'OutputView', output_view));

% Compute Fundamental, Essential and Camera Extrinsics -----------
% [fLMedS, inliers] = estimateFundamentalMatrix(matched_pts_src,matched_pts_tgt, 'NumTrials', 2000);
% figure,
% showMatchedFeatures(img_src, img_tgt, matched_pts_src, matched_pts_tgt,'montage','PlotOptions',{'ro','go','y--'}),
% title('Putative point matches');
% 
% figure,
% showMatchedFeatures(img_src, img_tgt, matched_pts_src(inliers,:),matched_pts_tgt(inliers,:),'montage','PlotOptions',{'ro','go','y--'}),
% title('Point matches after outliers were removed');

P0 = [K, zeros(3,1)];

E = K'*F*K;
[U, S, V] = svd(E);

Xt = [0; 0; 25; 1];
uv0 = P0*Xt;

% First test
R = U*W*V';
t = U(:,3);
uv1 = [R, -R*t]*Xt;
z = ((R(1,:) - uv1(1)*R(3,:))*t)/((R(1,:) - uv1(1)*R(3,:))*uv1);
X0 = uv0*z;
X1 = R'*(X0 - t);
test_iter = 1;

if X1(3) < 0 || X0(3) < 0
    % Second Test
    R = U*W'*V'; % R changed
    t = U(:,3);
    uv1 = [R, -R*t]*Xt;
    z = ((R(1,:) - uv1(1)*R(3,:))*t)/((R(1,:) - uv1(1)*R(3,:))*uv1);
    X0 = uv0*z;
    X1 = R'*(X0 - t);
    test_iter = 2;
    
%     if X1(3) < 0 || X0(3) < 0
%         % Third Test
%         R = U*W'*V'; % R changed
%         t = -U(:,3); % t changed
%         uv1 = [R, -R*t]*Xt;
%         z = ((R(1,:) - uv1(1)*R(3,:))*t)/((R(1,:) - uv1(1)*R(3,:))*uv1);
%         X0 = uv0*z;
%         X1 = R'*(X0 - t);
%         test_iter = 3;
%         
%         if X1(3) < 0 || X0(3) < 0
%             % Fourth Test
%             R = U*W*V';
%             t = -U(:,3); % t changed
%             uv1 = [R, -R*t]*Xt;
%             z = ((R(1,:) - uv1(1)*R(3,:))*t)/((R(1,:) - uv1(1)*R(3,:))*uv1);
%             X0 = uv0*z;
%             X1 = R'*(X0 - t);
%             test_iter = 4;
%         end
%     end
end
% if det(R) < 0
%     R = -R;
% end
P = K*[R, -R*t];
% P = K*[R', -R'*t];
% P = K*[R,-t];

% X0 = [0; 0; 15];
% 
% % First test
% R = U*W*V';
% t = U(:,3);
% X1 = R'*(X0 - t);
% iteration = 1;
% if X1(3) < 0
%     R = U*W'*V';
%     X1 = R'*(X0 - t);
%     iteration = 2;
%     if X1(3) < 0
%         t = -U(:,3);
%         X1 = R'*(X0 - t);
%         iteration = 3;
%         if X1(3) < 0
%             R = U*W*V';
%             X1 = R'*(X0 - t);
%             iteration = 4;
%         end
%     end
% end
% 
% P = K*[R', -R'*t];
% [F, inliers] = estimateFundamentalMatrix(matched_pts_tgt, matched_pts_src, ...
%     'Method', 'RANSAC', 'NumTrials', 2000, 'DistanceThreshold', 1);
% E = K'*F*K;
% [U, S, V] = svd(E);
% R = U*W*V';
% t = U(:,3);
% % P = K*[R,t];
% P = K*[R',-R'*t];
% test_p = [test_p(1)/test_p(3); test_p(2)/test_p(3)];

% [U,S,V] = svd(F);
% e = U(:,3);
% P1 = K*[-vgg_contreps(e)*F e];

% Plot source image with points on it
figure; imshow(img_src); title('P Source Image');
hold on;
showMatchedFeatures(img_src, img_tgt, matched_pts_src(inliers(:)), matched_pts_tgt(inliers(:)));
hold off;

pts_inlier = zeros(size(inliers,1), 2);
pts_inlier_t = zeros(size(inliers,1),2);
pts_zero = zeros(size(inliers,1),1);
for i = 1: size(inliers,1)
    if inliers(i) == 1 && ~isempty(matched_pts_src(i).Location)
    pts_inlier(i,:) = matched_pts_src(i).Location;
    pts_inlier_t(i,:) = matched_pts_tgt(i).Location;
    pts_zero(i) = [pts_inlier(i,:),1]*F*[pts_inlier_t(i,:),1]';
    end
end

plot(pts_zero);
% solve
% pts_inlier = inv(F)*[pts_inlier, ones(size(inliers,1),1)]';
% % pts_inlier_t = [pts_inlier_t, ones(size(inliers,1),1)]';
% 
% pts_xy(1,:) = pts_inlier(1,:)./pts_inlier(3,:);
% pts_xy(2,:) = pts_inlier(2,:)./pts_inlier(3,:);
% 
% figure; imshow(img_src); title('P Source Image');
% hold on;
% plot(pts_xy(1,:), pts_xy(2,:), 'r+', 'MarkerSize', 40, 'MarkerEdgeColor', [1,0,0]);
% plot(pts_inlier_t(:,1), pts_inlier_t(:,2), 'r*', 'MarkerSize', 5, 'MarkerEdgeColor', [0,0,1]);
% % showMatchedFeatures(img_src, img_tgt, matched_pts_src(inliers(:)), matched_pts_tgt(inliers(:)));
% hold off;

cam_C = -inv(R)*t;
if det(R) < 0
    cam_R = -R;
else
    cam_R = R;
end
figure;
plotCamera('Location',cam_C,'Orientation',cam_R,'Size',5, 'Color', [1, 0, 0], 'AxesVisible', true);
hold on;
plotCamera('Location', [0,0,0],'Orientation', eye(3,3),'Size',5, 'Color', [0 0 1], 'AxesVisible', true);
hold off;

camPars = cameraParameters('IntrinsicMatrix', K);
camP = cameraMatrix(camPars,K*R,K*t);

rand_points = [rand(2, 10)*1; ones(1,10)*6; ones(1,10)];
p1_pts = rand_points(1:2,:);
p0_pts = p1_pts;
for i = 1:10
    p1 = P*rand_points(:,i);
%     p1 = (rand_points(:,i)'*camP)';
    p1_pts(:,i) = [p1(1)/p1(3); p1(2)/p1(3)];
    uv = P0*rand_points(:,i);
    p0_pts(:,i) = [uv(1)/uv(3); uv(2)/uv(3)];
end

% p1_pts = [p1_pts(2,:);p1_pts(1,:)]; %invert coordinates
p1_origin = P*[0;0;15;1];
p1_origin = [p1_origin(1)/p1_origin(3); p1_origin(2)/p1_origin(3)];
p0_origin = P0*[0;0;15;1];
p0_origin = [p0_origin(1)/p0_origin(3); p0_origin(2)/p0_origin(3)];

% Plot points in 3D space along with cameras
figure,
scatter3(0, 0, 0, 'r+', 'MarkerEdgeColor', [1,0,0]); % Cam 1
hold on;
scatter3(t(1), t(2), t(3), 'r+', 'MarkerEdgeColor', [0,1,0]); % Cam 2
% hold on;
% plot(0, 0, 0, t(1), t(2), t(3), '--'); % Cam to Cam
% hold on;
scatter3(rand_points(1,:), rand_points(2,:), rand_points(3,:), ...
    'r*',  'MarkerEdgeColor', [0,0,1]); % Random points
hold off;
% Plot target image with points on it
figure; imshow(img_tgt); title('Target Image');
hold on;
plot(p0_pts(1,:), p0_pts(2,:), 'r*', 'MarkerSize', 20, 'MarkerEdgeColor', [1,0,0]);
plot(p0_origin(1), p0_origin(2), 'r+', 'MarkerSize', 40, 'MarkerEdgeColor', [0,0,1]);
hold off;

% Plot source image with points on it
figure; imshow(img_src); title('Source Image');
hold on;
plot(p1_pts(1,:), p1_pts(2,:), 'r*', 'MarkerSize', 20, 'MarkerEdgeColor', [1,0,0]);
plot(p1_origin(1), p1_origin(2), 'r+', 'MarkerSize', 40, 'MarkerEdgeColor', [0,0,1]);
hold off;

%% Set the 3D image plane
% point_C2 = inv(K)*[R,t]*[300; 130; 0; 1];
% % point_C2 = R'*([15; 15; 0] - t);
% p_normal = [R,t]*[0; 0; 1; 1];
% p_normal = p_normal/norm(p_normal);
% plane3d = imagept2plane(P0, img_src, point_C2, p_normal);

n_rows = size(img_src(:,1), 1);
n_cols = size(img_src(1,:), 2);
fx = K(1,1);
fy = K(2,2);
cx = K(1,3);
cy = K(2,3);

mul_c = 0.00215;
df = -fx/1000;

proj_img = img_tgt;
bounds = [0, n_rows+1; 0, n_cols+1];
p_count = 0;
T = [R, -R*t];
% uvs = zeros(n_rows, n_cols, 2);
for r = 1:n_rows
    for c = 1:n_cols
%         uv = P0*[(R'*([c/fx-cx; r/fy - cy; fx]-t));1];
        uv = P0*[([R,t]*[-(c - cx)*mul_c; -(r - cy)*mul_c; df; 1]);1];
        u = int16(uv(2)/uv(3));
        v = int16(uv(1)/uv(3));
%         uvs(r,c) = [u,v];
        if u > 0 && u <= n_rows && v > 0 && v <= n_cols
            proj_img(u,v) = img_src(r,c);
            p_count = p_count+1;
        end
    end
end
      
imshow(img_tgt);
figure();
imshow(proj_img);
figure();
imshowpair(img_tgt, proj_img, 'diff');
% %z = -1/c * (a*x + b*y)
% p_z = -1/p_normal(3) * (p_normal(1)*1242 + p_normal(2)*375);
% point_C2 = p_normal' * [100; 100; p_z];
% 
% fy = K(2,2);
% fx = K(1,1);
% 
% Y = -400/fy;
% Z = -1/fy;
%% Subtraction part
figure();
imshow(img_tgt);
pause;
imshow(img_src);
pause;
imshow(warped_img);
pause;
imshow(img_tgt);
pause;
figure, imshow(warped_img), title('Warped Image');
% Image subtraction -----------------------------------------

% Subtract the points ---
diff_img = img_tgt - warped_img;

% Do not allow negative values
diff_img(diff_img < 0) = 0;

diff_mask = im2bw(diff_img, 0.1);
%-----------------------------------------
% figure; imshowpair(original, Ir, 'diff');
% title('Image Pair');

% figure; imshow(orig_mask);
% title('Subtraction Mask');
diff_img = diff_img.*diff_mask;

% orig_filt = imwarp(orig_filt, tform.invert,'OutputView',outputView);
figure, imshow(diff_mask), title('Image Subtraction Mask');
figure, imshow(diff_img), title('Image Subtraction Result');
% imwrite(diff_img, sprintf('../data/processed/move-%04d.png', frame));