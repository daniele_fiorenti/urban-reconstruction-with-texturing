function [ block ] = extractImageBlock( image, point, block_size )
%EXTRACTIMAGEBLOCK Extracts a block from image given real coordinates point
% and block size
hs = (block_size-1)/2;
min_col = int16(ceil(point(1)-hs));
max_col = int16(ceil(point(1)+hs));
min_row = int16(ceil(point(2)-hs));
max_row = int16(ceil(point(2)+hs));



% min_bc = 1 + max(min_c - min_col, 0);
% max_bc = block_size - max(max_col - max_c, 0);
% min_br = 1 + max(min_r - min_row, 0);
% max_br = block_size - max(max_row - max_r, 0);
% 
% bh = max_row - min_row + 1;
% bw = max_col - min_col + 1;

if size(image,3) > 1
    [h, w, ~] = size(image);
    min_c = max(min_col, 1);
    max_c = min(max_col, w);
    min_r = max(min_row, 1);
    max_r = min(max_row, h);
%     block = zeros(bh, bw, size(image,3));
    block = image(min_r:max_r, min_c:max_c,:);
    [h, w, ~] = size(block);
    m = min(h,w);
    block = block(1:m,1:m, :);
%     imshow(block);
else % Questo else ha un senso???
    [h, w] = size(image);
    min_c = max(min_col, 1);
    max_c = min(max_col, w);
    min_r = max(min_row, 1);
    max_r = min(max_row, h);
%     block = zeros(bh, bw);
    block = image(min_r:max_r, min_c:max_c);
    
    [h, w] = size(block);
    m = min(h,w);
    block = block(1:m,1:m);
end
% 
% % add padding with zeros
% 
% block(1:min_br,:)       = 0;
% block(end:max_br,:)     = 0;
% block(:, 1:min_bc)      = 0;
% block(:, end:max_bc)    = 0;
end

