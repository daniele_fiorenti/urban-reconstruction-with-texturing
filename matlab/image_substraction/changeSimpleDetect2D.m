function [ output_args ] = changeSimpleDetect2D( rig_pack, frames, threshold, ...
    start_frame, frame_diff, load_folder, save_file, err_function, ...
    intelliP, depths_folder, depths_format )
%CHANGEDETECT2D find change detection in 2D
close all;

% Parameters
frame           = 45;
frame_start     = start_frame;
frame_end       = size(frames,1) + start_frame;
frame_dist      = 2;
block_size      = 20;
half_size       = block_size/2;
metric_th       = threshold;
e_shape_size    = 7;
e_shape_offset  = 1.25;
e_shape_type    = 'disk';
d_shape_size    = 6;
d_shape_offset  = 1.05;
d_shape_type    = 'disk';
gauss_mul       = 1.25;
gauss_mul_off   = 0.1;
gauss_var       = 1.25;
gauss_var_off   = 0.1;
ne_size         = 11; 
ne_min          = 2;
ne_offset       = 1;

% Patch parameters
% f_length        = K(1,1);   % Focal length in pixels
d_max           = 30;       % Maximum world distance from camera
depth_max       = 80;       % Maximum depth from camera
d_min           = 4;        % Minimum world distance from camera
min_box_size    = 5;
b_ratio         = min_box_size * d_max; % Min patch box size ratio

% Mask Related
mask_layers     = 1;        % Number of mask layers
d_dist          = d_max - d_min; % Distance interval

total_pts = 0;

if exist('depths_folder', 'var') && exist('depths_format','var')
    load_depths = true;
else
    load_depths = false;
end

if exist('err_function', 'var')
    err_function = err_function == 'SSD';
else
    err_function = true;
end

if ~exist('intelliP','var')
    intelliP = false;
end

for frame = frame_start:frame_end-1
    % Initialization
    cam_s = frame+1;
    cam_i = cam_s - frame_dist;
    pts = frames{frame-frame_start+1};
    
    inv_pose_i = rig_pack.inv_poses{cam_i - frame_start + frame_diff};
    inv_pose_s = rig_pack.inv_poses{cam_s - frame_start + frame_diff};
    pose_i = rig_pack.poses{cam_i - frame_start + frame_diff};
    pose_s = rig_pack.poses{cam_s - frame_start + frame_diff};
    veh_speed_s = vehicleSpeed(rig_pack.poses{cam_s - frame_start + frame_diff - 1}, pose_s);
    veh_speed_i = vehicleSpeed(rig_pack.poses{cam_i - frame_start + frame_diff - 1}, pose_i);
    
    P = rig_pack.cam_matrix;
    
    img_s = loadKittiImages(load_folder, cam_s-1, cam_s-1);
    img_i = loadKittiImages(load_folder, cam_i-1, cam_i-1);
    if load_depths
        
    end
     
    % Useful variables
    [img_h, img_w, ~] = size(img_s);
    
    % prepare the transformation for every point
    T_1 = P*inv_pose_i;
    T_s = P*inv_pose_s;
    
    % and an empty mask
    img_mask = zeros(img_h, img_w, mask_layers);
    
%     % full masks
%     full_mask_i = ones(img_h, img_w);
%     full_mask_s = full_mask_i;
    
    % Save sample picture removed points locations
    if intelliP
        rem_img = intelliProject(pts, T_s, cam_s, pose_s(1:3,4), veh_speed_s);
    else
        rem_img = projectToImage(pts, T_s);
    end
    
    % remove unused points
    rem_img = int16(rem_img);
    rem_img(:, rem_img(1,:) > img_w) = [];
    rem_img(:, rem_img(2,:) > img_h) = [];
    
    % show the intermediate result
%     plotRemovedPoints(rem_img, img_s, 1, 'Original Image');
    
    % save metrics... and other statistics
    all_metrics = zeros(size(rem_img, 2), 1);
    if load_depths
        dep_metrics = zeros(size(rem_img,2),1);
    end
    total_pts_1 = 0;
    
    % other rem_img
    rem_img_1 = zeros(2, size(pts, 2));
    
    % image borders
    top     = block_size;
    left    = block_size;
    bottom  = img_h - block_size;
    right   = img_w - block_size;
    
    % Visualization .......................................................
%     fig_patch_s     = figure('Name', 'Samble patch');
%                       axes('position', [0 0 1 1]);
%     fig_patch_i     = figure('Name', 'Test patch');
%                       axes('position', [0 0 1 1]);
%     fig_method_rr   = figure('Name', 'Method Result');
%                       axes('position', [0 0 1 1]);
%     fig_method_rg   = figure('Name', 'Method Result');
%                       axes('position', [0 0 1 1]);
%     fig_method_rb   = figure('Name', 'Method Result');
%                       axes('position', [0 0 1 1]);
%     fig_img_s       = figure('Name', 'Sample Image');
%                       axes('position', [0 0 1 1]), ...
%                       imshow(img_s);
%     hold on;
%     plot_h_s        = plot(300, 300, 'rs', 'MarkerSize', 2, 'MarkerEdgeColor', [1,0,0]);
%     hold off;
    % ---------------------------------------------------------------------

    for i = 1:size(pts,2)
        
        % and not visible by at least one camera
        if intelliP
            p_cam_i = intelliProject(pts(:,i), T_1, cam_i, pose_i(1:3,4), veh_speed_i);
            p_cam_s = intelliProject(pts(:,i), T_s, cam_s, pose_s(1:3,4), veh_speed_s);
        else
            p_cam_i = projectToImage(pts(:,i), T_1);
            p_cam_s = projectToImage(pts(:,i), T_s);
        end
        if isempty(p_cam_i) || p_cam_i(1) > right || p_cam_i(2) > bottom ...
                            || p_cam_i(1) < left  || p_cam_i(2) < top || ...
           isempty(p_cam_s) || p_cam_s(1) > right || p_cam_s(2) > bottom ...
                            || p_cam_s(1) < left  || p_cam_s(2) < top
            continue;
        end
        
        % now we are ready to work
        total_pts_1 = total_pts_1 + 1;
        
        
        rem_img_1(:,i) = p_cam_i;
        uv = int16(p_cam_s);
        mask_layer = 1;
        img_mask(uv(2), uv(1), mask_layer) = 1;
        
        
        % Visualization....................................................
%         pause;
%         
%         figure(fig_patch_s), imshow(feat_s);
%         
%         figure(fig_patch_i), imshow(feat_i);
%         
%         figure(fig_method_rr), surf(method_res(:,:,1)), shading flat;
%         figure(fig_method_rg), surf(method_res(:,:,2)), shading flat;
%         figure(fig_method_rb), surf(method_res(:,:,3)), shading flat;
% 
%         plot_h_s.XData = p_cam_s(1);
%         plot_h_s.YData = p_cam_s(2);
%         plot_h_s.MarkerSize = block_size;
%         plot_h_s.MarkerEdgeColor = m_color;
        % =================================================================
        
    end
    
%     all_metrics(all_metrics <= 0) = [];
    
    rem_img_1 = int16(rem_img_1);
    rem_img_1(:, rem_img_1(1,:) < 1) = [];
    rem_img_1(:, rem_img_1(2,:) < 1) = [];
    
%     plotRemovedPoints(rem_img_1, img_i, 2, 'Other Image');
    
%     figure(1); plot(all_metrics); title('NCC errors');
%     if load_depths
%         dep_metrics(dep_metrics <= 0) = [];
%         figure(2); plot(dep_metrics); title('Depth SSD error');
%     end

    display(['Total projected points for frame ' num2str(frame+1) ':']);
    display(['  ==> sample = ' num2str(size(rem_img,2))]);
    display(['  ==> cam_1  = ' num2str(total_pts_1)]);
    display(['  ==> filtered = ' num2str(size(rem_img_1,2))]);
    display(['  ==> ncc passed = ' num2str(length(all_metrics))]);
    if load_depths
        display(['  ==> depths passed = ' num2str(length(dep_metrics))]);
    end
    
    total_pts = total_pts + size(rem_img_1,2);
    
%     figure(); imshow(sum(img_mask, 3));
%     figure();
    mod_mask = img_mask;
    final_mask = zeros(img_h, img_w);
    middle_layer = floor(mask_layers/2) + 1;
    for layer_i = 1:mask_layers
        ne_s = ne_size + ne_offset*(middle_layer - layer_i);
        ne_filt = ones(ne_s, ne_s);
        
        mod_mask = imfilter(img_mask(:,:,layer_i), ne_filt) - ne_min;
        mod_mask(mod_mask < 0) = 0;
        
        e_shape_s = round(e_shape_size + e_shape_offset*(middle_layer - layer_i));
        d_shape_s = round(d_shape_size + d_shape_offset*(middle_layer - layer_i));
        e_shape = strel(e_shape_type, e_shape_s); 
        d_shape = strel(d_shape_type, d_shape_s);
        mod_mask = imdilate(mod_mask, d_shape);
        mod_mask = imerode(mod_mask, e_shape);
        
        gauss_var_s = gauss_var + gauss_var_off*(middle_layer - layer_i);
        gauss_mul_s = gauss_mul + gauss_mul_off*(middle_layer - layer_i);
        mod_mask = imgaussfilt(mod_mask.*gauss_mul_s, gauss_var_s).*gauss_mul_s;
        
        mod_mask(mod_mask<=0) = 0;
        final_mask = final_mask + mod_mask;
        
%         imshow(final_mask);
%         pause;
    end
    final_mask(final_mask>1) = 1;
    final_mask = 1 - final_mask;
%     figure(); imshow(final_mask);
    
%     new_image = repmat(img_s,[1 1 3]);
    new_image(:,:,1) = img_s(:,:,1);
    new_image(:,:,2) = img_s(:,:,2).*(final_mask);
    new_image(:,:,3) = img_s(:,:,3).*(final_mask);
%     figure(3); imshow(new_image);
%     imshow(new_image);
    
%     Xs = 1:size(all_metrics,1);
% %     figure(1);
%     subplot(3,1,1), imshow(mod_mask),
%     subplot(3,1,2), imshow(new_image),
%     subplot(3,1,3), plot(Xs, all_metrics, Xs, ones(size(Xs))*metric_th);

%     imwrite(new_image, sprintf(save_file, frame));
    imwrite(final_mask, sprintf(save_file, frame));
end

display(['Total points removed: ' num2str(total_pts)]);


end