%% Experimenting with 2D only
% Loading section
close all;
clear all;
clc;

dataset_number = '0095';
path_workspace = '/mnt/WinPartition/Users/danie/workspace';
path_dataset = strcat(path_workspace,'/dataset_', dataset_number);
output_folder = strcat('/mnt/WinPartition/Users/danie/Dropbox/Polimi/Thesis/dataset_output/', dataset_number);
path_output = strcat(output_folder, '/output_0-267');
masks_folder = '/exp_masks_matlab';
masks_diff_folder = '/exp_masks_matlab_red';
mkdir(path_output, masks_folder);
mkdir(path_output, masks_diff_folder);

[velo_to_cams, K] = loadCalibration(strcat(path_workspace,'/calibration'));
% velo2cam = loadCalibrationRigid(fullfile(strcat(path_workspace,'/calibration'),'calib_velo_to_cam.txt'));
cam_calib = loadCalibrationCamToCam(strcat(path_workspace,'/calibration/calib_cam_to_cam.txt'));

camera_id = 2;
imgs_s = 0;
% imgs_f = 255;

% switch camera_id
%     case 0
%         rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_00/data'), imgs_s, imgs_f); % left gray
%     case 1
%         rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_01/data'), imgs_s, imgs_f); % right gray
%     case 2
%         rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_02/data'), imgs_s, imgs_f); % left color
%     case 3
%         rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_03/data'), imgs_s, imgs_f); % right color
%     otherwise
%         rig_pack.images = loadKittiImages(strcat(path_dataset, '/sync/image_02/data'), 1); % left color
%         camera_id = 2;
% end

% rig_pack.depths = loadKittiImages('../data/depths/', imgs_s+1, imgs_f+1, 'depth-%04d.png');
% rig_pack.gt_mask = loadKittiImages('../data/masks/', imgs_s, imgs_f, 'mask_%03d.png');
% rig_pack.size = imgs_f - imgs_s + 1;
% if iscell(rig_pack.images) % è un set di immagini o è una sola immagine?
%     rig_pack.img_h = size(rig_pack.images{1}, 1);
%     rig_pack.img_w = size(rig_pack.images{1}, 2);
% else
%     rig_pack.img_h = size(rig_pack.images, 1);
%     rig_pack.img_w = size(rig_pack.images, 2);
% end

% Aggiungo 1 perché in matlab tutto parte da 1
camera_id = camera_id + 1; 

% Precompute matrix transformations
% Faccio la trasformazione
R_cam_to_rect = eye(4);
R_cam_to_rect(1:3,1:3) = cam_calib.R_rect{camera_id};

% Riga 217 readme devkit: Y = P_rect_xx * R_rect_00 * (R|T)_velo_to_cam * X
P_velo_act_to_image_plane = cam_calib.P_rect{camera_id}*R_cam_to_rect*velo_to_cams{camera_id};
% P = cam_calib.P_rect{camera_id};
% P_velo2cam = R_cam_to_rect*velo_to_cams{camera_id};
% 
% R_y = [0 0 -1 0; 0 1 0 0; 1 0 0 0; 0 0 0 1];
% R_z = [0 -1 0 0; 1 0 0 0; 0 0 1 0; 0 0 0 1];
% 
% R_pts = R_z*R_y;

rig_pack.K = K;
rig_pack.cam_matrix = P_velo_act_to_image_plane;
rig_pack.poses = loadLidarPoses(strcat(path_output, '/all_lidar_poses.txt'));
% inv_poses sono le inverse di poses, sono qui per non ricalcolarle ogni volta
rig_pack.inv_poses = cellfun(@inv, rig_pack.poses, 'UniformOutput', false);


%% Here we will prepare the frames (CAUTION: Very slow)
% It is done here mainly to prepare data for the false negatives

% gt_frames sono le cloud dei punti rimossi, ogni cloud è salvata su una riga
minf = 10;
frame_s = imgs_s + minf;
wdst_frames = loadVelodynePoints(-1, strcat(path_output, '/wdst/rem_points/'), '000%d.bin', 3, frame_s); 
% plain_frames = loadVelodynePoints(-1, strcat(path_output, '/plain_clouds/'), '000%d.bin', 3, frame_s);

% for i = 1:length(wdst_frames)
%     wdst_frames{i} = intelliPreparePoints(wdst_frames{i}, ...                                        % p_in
%                                         rig_pack.cam_matrix*rig_pack.inv_poses{i+8}, ...         % T
%                                         i + frame_s, ...                                         % frame_id
%                                         rig_pack.poses{i+8}(1:3,4), ...                          % origin
%                                         vehicleSpeed(rig_pack.poses{i+9}, rig_pack.poses{i+8})); % veh_speed
% end

%% Generating and Saving Depth maps
depths_path = strcat(path_output, '/depths/');
generate_depths = false;
if(generate_depths)
    orig_frames = loadVelodynePoints(-1, strcat(path_dataset, '/sync/velodyne_points/data/'), '%010d.bin', 4, frame_s, 247);

    mkdir(depths_path);
    depth_file = strcat(depths_path, 'depth-%04d.png');

    % gauss_depths_path = strcat(path_output, '/gaussian_depths/');
    % mkdir(gauss_depths_path);
    % gaussian_depth_file = strcat(gauss_depths_path, 'gaussian-depth-%04d.png');

    frame_end = size(orig_frames,1) + frame_s;
    frame_diff = minf;

    for frame = frame_s:frame_end-1
        cam_s = frame+1;

        pose_s = rig_pack.poses{cam_s - frame_s + frame_diff};
        inv_pose_s = rig_pack.inv_poses{cam_s - frame_s + frame_diff};

        pts_orig = orig_frames{frame-frame_s+1};
    %     pts_plain = plain_frames{frame-frame_s+1};

        %%%%%%%%%%%%%%%%%%% Plot point cloud %%%%%%%%%%%%%%%%%%%%
    %     pts_plain = inv_pose_s*pts_plain;
    %     velo_cloud = [pts_orig(1,:); pts_orig(2,:); pts_orig(3,:)]';
    %     plain_cloud = [pts_plain(1,:); pts_plain(2,:); pts_plain(3,:)]';
    %     
    % %     pcshow(velo_cloud);
    % %     pause(1);
    % %     pcshow(plain_cloud);
    % 
    %     velo_pc = pointCloud(velo_cloud);
    %     plain_pc = pointCloud(plain_cloud);
    % 
    %     pcshowpair(velo_pc, plain_pc);
    % 
    %     xlabel('X');
    %     ylabel('Y');
    %     zlabel('Z');
    %     pause(1);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        [depth_image_s,~,~] = convertToDepthMap( pts_orig', P_velo_act_to_image_plane);
    %     projected_image = projectVeloToImage( pts_orig', P_velo_act_to_image_plane);

    %     [depth_plain_image_s,~,~] = convertToDepthMap( pts_plain', P_velo_act_to_image_plane, inv_pose_s);
    %     [depth_gauss_image_s,~] = createGaussianDepthMap ( pts', P_velo_to_img*inv_pose_s, 1); 

    %     imshow(depth_image_s);

        imwrite(depth_image_s, sprintf(depth_file, frame));
    %     imwrite(1-depth_gauss_image_s, sprintf(gaussian_depth_file, frame));
    end
end

%% Save the masks from 2D patch subtraction
changeDetect2D(rig_pack, ...                                               % rig_pack
               wdst_frames, ...                                            % frames
               0.1, ...                                                    % threshold
               frame_s, ...                                                % start_frame
               minf, ...                                                   % frame_diff
               strcat(path_dataset, '/sync/image_02/data'), ...            % load_folder
               strcat(path_output, masks_folder, '/mask_%04d.png'), ...    % save_file
             strcat(path_output, masks_diff_folder, '/mask_%04d.png'), ... % save_file
               'NCC', ...                                                  % err_function
               false, ...                                                  % intelliP
               depths_path, ...                                            % depths_folder
               'depth-%04d.png', ...                                       % depths_format
               false, ...                                                  % depth_validation
               false);                                                     % patch_validation
