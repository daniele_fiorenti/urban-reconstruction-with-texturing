function [ p_out ] = prepareSmartPoints( p_in, T, origin, veh_speed, def_speed )
%PROJECTTOIMAGE Shift points in point cloud
% input points should be in homogeneous coordinate system
% get the principal plane normal
P_normal = T(3,:);

% get the points which are in front of camera
p_pos = P_normal*p_in;
p_out = p_in;
p_out(:,p_pos < 0) = [];

% precompute some variables
max_d = max(p_out(1,:)) - origin(1);

% max_d = max_d*3/4;

% display('smartProject');

if ~exist('def_speed', 'var')
    def_speed = 1.5;
end

if ~exist('veh_speed', 'var')
    veh_speed = 0;
end
f_speed = def_speed + veh_speed;

% shift the points according to their angle
fr = [1 0];   % forward ray
w = 0.5/pi;  % omega
for i=1:size(p_out, 2)
    theta = acos(dot(p_out(1:2,i),fr)/norm(p_out(1:2,i)));
    rel_d = 1 - (norm(p_out(1:3,i) - origin))/max_d;
%     f_speed = veh_speed + def_speed*sign(p2_in(2,i));
    p_out(1,i) = p_out(1,i) - theta*w*f_speed*rel_d;
end

end

