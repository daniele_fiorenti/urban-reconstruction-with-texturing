function [ poses ] = loadLidarPoses( file, first, last )
%LOADLIDARPOSES Load the lidar poses
%   Load the lidar poses from the specified file and interval
    fin = fopen(file,'r');
    n = fscanf(fin, '%d', 1);
    poses{n} = 0;
    for i = 1:n
        poses{i} = fscanf(fin, '%f %f %f %f', [4 4])';
    end
    fclose(fin);
    if ~exist('first', 'var')
        return
    end
    first_ = abs(first);
    last_ = first_;
    if exist('last', 'var') && last >= first_
        last_ = last;
    end
    poses = poses(first_:last_);
end

