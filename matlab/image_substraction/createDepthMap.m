function [ depth_image ] = createDepthMap( velo_points, T, w, h )
%CONVERTTODEPTHMAP Project velo points into depth map
%   Detailed explanation goes here
pts = velo_points(velo_points(:,1) > 4, :);
pts = pts(pts(:,1) < 30, :);

if ~exist('w', 'var')
    w = 1242;
end
if ~exist('h', 'var')
    h = 375;
end

[~,idx] = sort(pts(:,1));
pts = pts(idx,:);

depth_image = ones(h,w);

max_d = pts(end,1);

p = T*pts';
p(1,:) = p(1,:)./p(3,:);
p(2,:) = p(2,:)./p(3,:);
p(3,:) = [];

depth_points = zeros(size(p,2), 3);

for i = 1:size(pts,1)
   if isempty(p(i)) || p(1,i) > w || p(2,i) > h || p(1,i) < 1  || p(2,i) < 1
       continue;
   end
   depth_points(i,:) = [p(:,i)' (1 - (max_d - pts(i,1))/max_d)];
end

depth_points(depth_points(:,1) <= 0, :) = [];

DT = delaunayTriangulation(depth_points(:,1:2));

for r = 1:h
    for c = 1:w
        p = [c r];
        tr_id = DT.pointLocation(p);
        
        if isnan(tr_id)
            continue
        end
        
        tri = DT.ConnectivityList(tr_id,:);
        v1 = depth_points(tri(1),:);
        v2 = depth_points(tri(2),:);
        v3 = depth_points(tri(3),:);
        
        v1_norm = norm(p - v1(1:2));
        v2_norm = norm(p - v2(1:2));
        v3_norm = norm(p - v3(1:2));
        
        S = v1_norm + v2_norm + v3_norm;
        depth_image(r,c) = v1_norm*v1(3)/S + v2_norm*v2(3)/S + v3_norm*v3(3)/S;
%         [~, i] = min([v1_norm v2_norm v3_norm]);
%         verts = [v1; v2; v3];
%         min_v = verts(i, :);
%         depth_image(r,c) = min_v(3);
    end
end
end

