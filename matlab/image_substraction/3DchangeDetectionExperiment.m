%% Pre-processing and pre-loading of all necessary data
% Loading section
close all;
clear all;
clc;
[velo_to_cams, K] = loadCalibration('../data/calibration');
velo2cam = loadCalibrationRigid(fullfile('../data/calibration','calib_velo_to_cam.txt'));
cam_calib = loadCalibrationCamToCam('../data/calibration/calib_cam_to_cam.txt');

camera_id = -1;
imgs_s = 35;
imgs_f = 255;

switch camera_id
    case 0
        rig_pack.images = loadKittiImages('../data/left/', imgs_s, imgs_f);
    case 1
        rig_pack.images = loadKittiImages('../data/right/', imgs_s, imgs_f);
    case 2
        rig_pack.images = loadKittiImages('../data/left_color/', imgs_s, imgs_f);
    case 3
        rig_pack.images = loadKittiImages('../data/right_color/', imgs_s, imgs_f);
    otherwise
        rig_pack.images = loadKittiImages('../data/left_color/', 1);
        camera_id = 2;
end

% rig_pack.depths = loadKittiImages('../data/depths/', imgs_s+1, imgs_f+1, 'depth-%04d.png');
% rig_pack.gt_mask = loadKittiImages('../data/masks/', imgs_s, imgs_f, 'mask_%03d.png');
rig_pack.size = imgs_f - imgs_s + 1;
if iscell(rig_pack.images)
    rig_pack.img_h = size(rig_pack.images{1}, 1);
    rig_pack.img_w = size(rig_pack.images{1}, 2);
else
    rig_pack.img_h = size(rig_pack.images, 1);
    rig_pack.img_w = size(rig_pack.images, 2);
end

camera_id = camera_id + 1;

% Precompute matrix transformations
R_cam_to_rect = eye(4);
R_cam_to_rect(1:3,1:3) = cam_calib.R_rect{camera_id};
P_velo_to_img = cam_calib.P_rect{camera_id}*R_cam_to_rect*velo_to_cams{camera_id};%*I2V;
P = cam_calib.P_rect{camera_id};
P_velo2cam = R_cam_to_rect*velo_to_cams{camera_id};

R_y = [0 0 -1 0; 0 1 0 0; 1 0 0 0; 0 0 0 1];
R_z = [0 -1 0 0; 1 0 0 0; 0 0 1 0; 0 0 0 1];

R_pts = R_z*R_y;

rig_pack.cam_matrix = P_velo_to_img;
rig_pack.poses = loadLidarPoses('../data/all_lidar_poses.txt');
rig_pack.inv_poses = cellfun(@inv, rig_pack.poses, 'UniformOutput', false);


%% Here we will prepare the ground truth from lidar
% It is done here mainly to prepare data for the false negatives
gt_frames = loadVelodynePoints(-1, '../data/cd_points_new/plain_clouds/', '000%d.bin', 3, 8);
% [GTP,GFP,GTN] = getFast3Dmetrics(rig_pack, gt_frames, 8, imgs_f - imgs_s-9, imgs_s+1);
[GTP,GFP,GTN] = get3DMetrics(rig_pack, gt_frames, 8, imgs_f - imgs_s-9, imgs_s);

%% And now performing the cycle for all precision recall of both orig and ours
% Perform now for other values
% Recall        = TP/(TP + FN)
% Precision     = TP/(TP + FP)
% Specificity   = TN/(TP + FP)
OrigPR = [0 0];
OrigSR = [0 0];
PR = [0 0];
SR = [0 0];

for i = 0:8
    display('---- iteration started....');
    
    wdst_frames = loadVelodynePoints(-1, ...
        sprintf('../data/cd_points/wdst/eval_%d/', i), '000%d.bin', 3, 8);
    [TP,FP, ~] = get3DMetrics(rig_pack, wdst_frames, 8, imgs_f - imgs_s - 9, imgs_s);
    
%     orig_frames = loadVelodynePoints(-1, ...
%         sprintf('../data/cd_points/orig/eval_%d/', i), '000%d.bin', 3, 8);
%     [OTP,OFP, ~] = get3DMetrics(rig_pack, orig_frames, 8, imgs_f - imgs_s - 9, imgs_s+1);
    
    FN = GTP - TP;
    TN = GTN - FP;
    
    TP = TP(GTP>0);
    TN = TN(GTP>0);
    FP = FP(GTP>0);
    FN = FN(GTP>0);
    
    [iPR iSR] = computeROCdata(TP, TN, FP, FN,...
        sprintf('../experiments/wdst/eval_%02d_', i));
    
%     OFN = GTP - OTP;
%     OTN = GTN - OFP;
%     
%     OTP = OTP(GTP>0);
%     OTN = OTN(GTP>0);
%     OFP = OFP(GTP>0);
%     OFN = OFN(GTP>0);
%     
%     [iOPR iOSR] = computeROCdata(OTP, OTN, OFP, OFN,...
%         sprintf('../experiments/orig/eval_%02d_', i));
    
%     R = TP./(TP + FN);
%     P = TP./(TP + FP);
%     S = TN./(TN + FP);
%     
%     R = R(GTP>0);
%     P = P(GTP>0);
%     S = S(GTP>0);
%     
%     R = R(~isnan(R));
%     P = P(P>0);
%     S = S(~isnan(S));
%     
%     OFN = GTP - OTP;
%     OTN = GTN - OFP
%     OR = OTP./(OTP + OFN);
%     OP = OTP./(OTP + OFP);
%     OS = OTN./(OTN + OFP);
%     
%     OR = OR(GTP>0);
%     OP = OP(GTP>0);
%     OS = OS(GTP>0);
%     
%     OR = OR(~isnan(OR));
%     OP = OP(OP>0);
%     OS = OS(~isnan(OS));
    
%     OrigPR = [OrigPR; mean(iOPR)];
%     OrigSR = [OrigSR; mean(iOSR)];
    PR = [PR; mean(iPR)];
    SR = [SR; mean(iSR)];
    
    display('---- iteration done');
end

% prepare and save the data
% OrigPR(1,:) = [];
% OrigSR(1,:) = [];
% save('../experiments/orig_avg_PR', 'OrigPR');
% save('../experiments/orig_avg_SR', 'OrigSR');

PR(1,:) = [];
SR(1,:) = [];
save('../experiments/wdst_avg_PR', 'PR');
save('../experiments/wdst_avg_SR', 'SR');

%% Here we will perform the Various Ploting


% % Precision = TP/(TP + FP)
% scores = rand(1000, 1); 
% targets = round(scores + 0.5*(rand(1000,1) - 0.5));
% figure
% [Xpr,Ypr,Tpr,AUCpr] = perfcurve(R, P, 1, 'xCrit', 'reca', 'yCrit', 'prec');
% plot(Xpr,Ypr)
% xlabel('Recall'); ylabel('Precision')
% title(['Precision-recall curve (AUC: ' num2str(AUCpr) ')'])