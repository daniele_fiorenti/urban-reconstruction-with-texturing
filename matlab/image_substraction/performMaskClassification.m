function [ TP, TN, FP, FN ] = performMaskClassification( masks_folder, mask_format,...
                                            gt_masks_folder, gt_mask_format, ...
                                            mask_start, mask_end, mask_border, negate)
%PERFORMMASKCLASSIFICATION crete the data for classification curves based
%on masks
if ~exist('mask_start', 'var')
    mask_start = 0;
end

if ~exist('mask_border', 'var')
    mask_border = 0;
end

if ~exist('mask_end', 'var')
    mask_end = mask_start;
end

TP(mask_end - mask_start + 1) = 0;
TN = TP;
FP = TP;
FN = TP;

if ~exist('negate', 'var')
    negate = true;
end

for i = 1:mask_end - mask_start + 1
   gt_mask = loadKittiImages(gt_masks_folder, i+mask_start-1, i+mask_start-1, gt_mask_format);
   mask = loadKittiImages(masks_folder, i+mask_start-1, i+mask_start-1, mask_format);
   
%    figure(1), imshow(gt_mask);
%    figure(2), imshow(mask);
   
   if negate
       mask = 1 - mask;
   end
   
   if mask_border > 0
       mask(1:mask_border,:) = gt_mask(1:mask_border,:);
       mask(end-mask_border+1:end,:) = gt_mask(end-mask_border+1:end,:);
       mask(:, 1:mask_border) = gt_mask(:, 1:mask_border);
       mask(:, end-mask_border+1:end) = gt_mask(:, end-mask_border+1:end);
   end
   
   fin_mask = mask.*gt_mask;
   or_mask = mask + gt_mask;
   TP(i) = length(fin_mask(fin_mask>0.01));
   FP(i) = length(mask(mask>0.01)) - TP(i);
   TN(i) = length(or_mask(or_mask == 0));
   FN(i) = length(gt_mask(gt_mask>0.01)) - TP(i);
end

end