function plotRemovedPoints( points, image, figure_id, figure_title )
%PLOTIMAGEWITHREMOVEDPOINTS Plots an image with removed points on it
if ~exist('figure_title','var')
    figure_title = 'Removed Points Projection';
end

% Render the image if any
if exist('image', 'var')
    if exist('figure_id', 'var')
%         close(figure_id);
        figure(figure_id);
    else
        figure();
    end
    imshow(image);
    title(figure_title); 
end
hold on;

% Render removed points
for i=1:size(points,1)
    plot(points(1,:), points(2,:), 'r*', 'MarkerSize', 2, 'MarkerEdgeColor', [1,0,0]);
end

hold off;
end

