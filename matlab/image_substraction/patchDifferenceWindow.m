function [ out_window ] = patchDifferenceWindow( window, patch, diff_method, cropped )
%PATCHDIFFERENCEWINDOW Computes the window from patch differences with
%specified method
% half sizes
sh = int16(floor(size(patch, 2)/2));
sv = int16(floor(size(patch, 1)/2));
add1h = int16(mod(size(patch, 2) + 1, 2));
add1v = int16(mod(size(patch, 1) + 1, 2));
% create output window
[wsv, wsh] = size(window);
out_window = ones(wsv, wsh);

% see which method to use and perform the difference
if exist('diff_method', 'var') && strcmp(diff_method,'SSD')
    for r = sv+1:wsv-sv
        for c = sh+1:wsh-sh
            window_block = window(r-sv+add1v:r+sv, c-sh+add1h:c+sh);
            out_window(r,c) = sum(sum((window_block-patch).^2));
        end
    end
else
    for r = sv+1:wsv-sv
        for c = sh+1:wsh-sh
            window_block = window(r-sv+add1v:r+sv, c-sh+add1h:c+sh);
            out_window(r,c) = sum(sum(abs(window_block-patch)));
        end
    end
end

if exist('cropped','var') && cropped == true
    out_window = out_window(sv+1:wsv-sv, sh+1:wsh-sh);
end

end

