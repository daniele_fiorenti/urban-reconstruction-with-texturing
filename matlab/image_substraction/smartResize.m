function [ r_patch ] = smartResize( patch1, patch2 )
%SMARTRESIZE Resizes a patch into another
if size(patch2,3) > 1
    [h, w, ~] = size(patch2);
else
    [h,w] = size(patch2);
end
r_patch = imresize(patch1, [h w]);


end

