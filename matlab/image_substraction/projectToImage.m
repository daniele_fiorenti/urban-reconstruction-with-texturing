function [ p_out ] = projectToImage( p_in, T, positive_only )
%PROJECTTOIMAGE Projects points to image plane
% input points should be in homogeneous coordinate system
% get the principal plane normal
P_normal = T(3,:);

% get the points which are in front of camera
p_pos = P_normal*p_in;
p2_in = p_in;
p2_in(:,p_pos < 0) = [];

% do the transformation
p_out = T*p2_in;

% get the inhomogeneous coordinates
p_out(1,:) = p_out(1,:)./p_out(3,:);
p_out(2,:) = p_out(2,:)./p_out(3,:);
p_out(3,:) = [];

% check if positive only or not empty
if ~exist('positive_only', 'var') || positive_only
    p_out(:, p_out(1,:) < 1) = [];
    p_out(:, p_out(2,:) < 1) = [];
end

end

