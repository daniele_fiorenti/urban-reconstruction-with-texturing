%% Various tests to improve homography and image subtraction
K = [9.842439e+02 0.000000e+00 6.900000e+02; ... %Calibration Matrix
     0.000000e+00 9.808141e+02 2.331966e+02; ...
     0.000000e+00 0.000000e+00 1.000000e+00]

img_tgt = im2double(imread(sprintf('../data/left/%010d.png', 4)));
img_src = im2double(imread(sprintf('../data/left/%010d.png', 0)));
img_tgt_color = im2double(imread(sprintf('../data/left_color/%010d.png', 0)));
img_src_color = im2double(imread(sprintf('../data/left_color/%010d.png', 1)));

% Smooth images ---------------------------------------------
gauss_sigma = 1.2;
img_tgt_smooth = imgaussfilt(img_tgt, gauss_sigma);
img_src_smooth = imgaussfilt(img_src, gauss_sigma);
img_tgt_color_smooth = imgaussfilt(img_tgt_color, gauss_sigma);
img_src_color_smooth = imgaussfilt(img_src_color, gauss_sigma);

% Get Features-----------------------------------------------
pts_tgt = detectSURFFeatures(img_tgt_smooth);%, 'MetricThreshold', 100);
pts_src = detectSURFFeatures(img_src_smooth);%, 'MetricThreshold', 100);
[features_tgt, valid_pts_tgt] = extractFeatures(img_tgt_smooth, pts_tgt);
[features_src, valid_pts_src] = extractFeatures(img_src_smooth, pts_src);

% Match Features-----------------------------------------
index_pairs = matchFeatures(features_tgt, features_src);
matched_pts_tgt = valid_pts_tgt(index_pairs(:,1));
matched_pts_src = valid_pts_src(index_pairs(:,2));

% Homography Estimation-----------------------------------------
[H, inlier_pts_src, inlier_pts_tgt] = estimateGeometricTransform(matched_pts_src, ...
    matched_pts_tgt,'projective', 'MaxNumTrials', 4000, 'Confidence', 99, 'MaxDistance', 4.5);

% Image warping ------------------------------------------------
output_view = imref2d(size(img_tgt_smooth));
warped_img = im2double(imwarp(img_src, H, 'OutputView', output_view));

figure();
imshow(img_tgt);
pause;
figure, imshow(img_src);
pause;
figure, imshow(warped_img);
pause;
figure, imshow(img_tgt);
pause;
figure, imshow(warped_img), title('Warped Image');
% Image subtraction -----------------------------------------

% Subtract the points ---
diff_img = img_tgt - warped_img;

% Do not allow negative values
diff_img(diff_img < 0) = 0;

diff_mask = im2bw(diff_img, 0.1);
%-----------------------------------------
% figure; imshowpair(original, Ir, 'diff');
% title('Image Pair');

% figure; imshow(orig_mask);
% title('Subtraction Mask');
diff_img = diff_img.*diff_mask;

% orig_filt = imwarp(orig_filt, tform.invert,'OutputView',outputView);
figure, imshow(diff_mask), title('Image Subtraction Mask');
figure, imshow(diff_img), title('Image Subtraction Result');
% imwrite(diff_img, sprintf('../data/processed/move-%04d.png', frame));