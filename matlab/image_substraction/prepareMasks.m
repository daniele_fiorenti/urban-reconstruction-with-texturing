function prepareMasks( multiplier, sigma )
all_masks = loadKittiImages('../data/masks/', 0, 267, 'mask_%03d.png');
all_gt_masks = cell(268,1);
for i=1:268
all_gt_masks{i} = im2bw(imgaussfilt(all_masks{i}*multiplier, sigma));
end
for i=1:268
imwrite(all_gt_masks{i}, sprintf('../data/gt_masks/mask-%03d.png', i-1));
end