function [ velo_points ] = loadVelodynePoints( id, folder, filename_format, byte_format, minf, amount )
%LOADVELODYNEPOINTS load velodyne points from specified folder and file
if ~exist('folder', 'var')
    folder = '../data/velo/';
end

if ~exist('filename_format', 'var')
    filename_format = '/%010d.bin';
end

if ~exist('byte_format', 'var')
    byte_format = 4;
end

filepath = [folder filename_format];

if ~exist('minf', 'var')
    minf = 0;
end

if ~exist('amount', 'var')
    d = dir([folder, '/*.bin']);
    amount = length(d(not([d.isdir])));
end

if id < 0
    display(['Loading Files of ' folder '...']);
    velo_points = cell(amount,1);
    for id = 1:amount
        filename = sprintf(filepath, id + minf - 1);
        fid = fopen(filename, 'rb');
        velo_points{id} = fread(fid,[byte_format inf],'single')';
        fclose(fid);
        velo_points{id}(:,4) = 1;
        velo_points{id} = velo_points{id}';
    end
else
    fid = fopen(sprintf(filepath, id - 1),'rb');
    velo_points = fread(fid,[byte_format inf],'single')';
    fclose(fid);
    velo_points(:,4) = 1;
    velo_points = velo_points';
end

display('...Done!');
end

