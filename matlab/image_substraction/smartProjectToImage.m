function [ p_out ] = smartProjectToImage( p_in, T, origin, veh_speed, def_speed, positive_only )
%PROJECTTOIMAGE Shift and projects points to image plane
% input points should be in homogeneous coordinate system
% get the principal plane normal
P_normal = T(3,:);

% get the points which are in front of camera
p_pos = P_normal*p_in;
p2_in = p_in;
p2_in(:,p_pos < 0) = [];

% precompute some variables
max_d = max(p2_in(1,:)) - origin(1);

% max_d = max_d*3/4;

% display('smartProject');

if ~exist('def_speed', 'var')
    def_speed = 1.5;
end

if ~exist('veh_speed', 'var')
    veh_speed = 0;
end
f_speed = def_speed + veh_speed;

% shift the points according to their angle
fr = [1 0];   % forward ray
w = 0.5/pi;  % omega
for i=1:size(p2_in, 2)
    theta = acos(dot(p2_in(1:2,i),fr)/norm(p2_in(1:2,i)));
    rel_d = 1 - (norm(p2_in(1:3,i) - origin))/max_d;
%     f_speed = veh_speed + def_speed*sign(p2_in(2,i));
    p2_in(1,i) = p2_in(1,i) - theta*w*f_speed*rel_d;
end

% do the transformation
p_out = T*p2_in;

% get the inhomogeneous coordinates
p_out(1,:) = p_out(1,:)./p_out(3,:);
p_out(2,:) = p_out(2,:)./p_out(3,:);
p_out(3,:) = [];

% check if positive only or not empty
if ~exist('positive_only', 'var') || positive_only
    p_out(:, p_out(1,:) < 1) = [];
    p_out(:, p_out(2,:) < 1) = [];
end

end

