function [ frames, poses ] = loadFastRemFrames( poses_file, rem_points_file, max_clouds )
%LOADFASTREMFRAMES Loads removed points frames in a convinient way
%   Detailed explanation goes here
%   This datastructure includes: removed points, view rays and poses

if ~exist(poses_file, 'file') && ~exist(rem_points_file,'file')
    error('loadLidarRemPoints: unable to find specified files');
end

% Open files
poses_fin = fopen(poses_file, 'r');
rem_pts_fin = fopen(rem_points_file, 'r');

% Set the upper bound limit of poses and rigs
n_rigs = fscanf(poses_fin, '%d', 1);
if exist('max_clouds', 'var')
    n_rigs = min(n_rigs, max(0, max_clouds));
end

% Set variable lengths
frames = struct('scanned', 0, ...
                'image_id', 1, ...
                'pose', zeros(4,4), ...
                'points', [], ...
                'views', [], ...
                'first_id', 1, ...
                'last_id', n_rigs);

% Read poses
poses{n_rigs} = 0;
for i = 1:n_rigs
    poses{i} = fscanf(poses_fin, '%f %f %f %f', [4 4])';
    frames(i).pose = poses{i};
end

% Read points
scanned_rig = fscanf(rem_pts_fin, '%d', 1) + 1;
n_points = fscanf(rem_pts_fin, '%d', 1);
while ~feof(rem_pts_fin) && scanned_rig <= n_rigs 
    if isnan(n_points)
        break;
    end
    % Initialize all data
    frames(scanned_rig).image_id = scanned_rig;
    frames(scanned_rig).points = ones(4, n_points);
    frames(scanned_rig).views = zeros(n_points, n_rigs);
    frames(scanned_rig).scanned = 1;
    min_view = n_rigs;
    max_view = 0;
    
    % Get every point with its views idx matrices...
    for i = 1:n_points
        frames(scanned_rig).points(1:3, i) = fscanf(rem_pts_fin, '%f %f %f', 3)';
        n_views = fscanf(rem_pts_fin, '%d', 1);
        views = fscanf(rem_pts_fin, '%d', n_views) + 1;
        min_view = min(min(views), min_view);
        max_view = max(max(views), max_view);
        frames(scanned_rig).views(i, views) = 1;
        
    end
    
    % Set the limits...
    frames(scanned_rig).first_id = min_view;
    frames(scanned_rig).last_id = max_view;
    
    % Get rid of unused data
    frames(scanned_rig).views = ...
        frames(scanned_rig).views(:, min_view:max_view);
    
    % Continue the cycle checks
    scanned_rig = fscanf(rem_pts_fin, '%d', 1) + 1;
    n_points = fscanf(rem_pts_fin, '%d', 1);
end

% Close files
fclose(poses_fin);
fclose(rem_pts_fin);
end


