function generatePRplot( PR, GTP, tit )
%GENERATEPRPLOT 
if exist('GTP', 'var')
    PR = PR(GTP>0,:);
end

if ~exist('tit', 'var')
    tit = 'Precision-Recall'
end

[~, idx] = sort(PR(:,1));
PR = PR(idx,:);

plot(1 - PR(:,1), PR(:,2))
title(tit)
xlabel('1 - Precision')
ylabel('Recall')
end

