%% Optical Flow on Kitti dataset (Test only)
% This is my first experience with matlab computer vision toolbox

%% Image loading
clear all;
img1 = imread('0000000000.png');
img2 = imread('0000000001.png');

v_img1 = img1(:);

figure;
imshowpair(img1, img2, 'diff');

% Try optical flow
figure;
opt_flow = opticalFlowLK('NoiseThreshold', 0.009);

% frameGray1 = rgb2gray(img1);
flow1 = estimateFlow(opt_flow,img1);

imshow(img1)
hold on
plot(flow1,'DecimationFactor',[5 5],'ScaleFactor',10)
hold off

% frameGray2 = rgb2gray(img2);
flow2 = estimateFlow(opt_flow,img2);

imshow(img2)
hold on
plot(flow2,'DecimationFactor',[5 5],'ScaleFactor',10);
hold off

%% Matches finder all dataset
% Process the first semi cycle
% Load images -----------------------------------------------
left_tgt = imread(sprintf('../data/left/%010d.png', 0));
right_tgt = imread(sprintf('../data/right/%010d.png', 0));
l_color_tgt = imread(sprintf('../data/left_color/%010d.png', 0));
r_color_tgt = imread(sprintf('../data/right_color/%010d.png', 0));

% Smooth images ---------------------------------------------
gauss_sigma = 1.2;
left_tgt = imgaussfilt(left_tgt, gauss_sigma);
right_tgt = imgaussfilt(right_tgt, gauss_sigma);
l_color_tgt = imgaussfilt(l_color_tgt, gauss_sigma);
r_color_tgt = imgaussfilt(r_color_tgt, gauss_sigma);

% Get Features-----------------------------------------------
pts_l_tgt = detectSURFFeatures(left_tgt);%, 'MetricThreshold', 100);
pts_r_tgt = detectSURFFeatures(right_tgt);%, 'MetricThreshold', 100);
[features_l_tgt, valid_pts_l_tgt] = extractFeatures(left_tgt, pts_l_tgt);
[features_r_tgt, valid_pts_r_tgt] = extractFeatures(right_tgt, pts_r_tgt);

% Done with target images processing -----------------------------
% Perform loop processing -----------------------------------------
for frame = 1:266
    % Load images -----------------------------------------------
    left_src = imread(sprintf('../data/left/%010d.png', frame));
    right_src = imread(sprintf('../data/right/%010d.png', frame));
    l_color_src = imread(sprintf('../data/left_color/%010d.png', frame));
    r_color_src = imread(sprintf('../data/right_color/%010d.png', frame));
    
    % Smooth images ---------------------------------------------
    gauss_sigma = 1.2;
    left_src = imgaussfilt(left_src, gauss_sigma);
    right_src = imgaussfilt(right_src, gauss_sigma);
    l_color_src = imgaussfilt(l_color_src, gauss_sigma);
    r_color_src = imgaussfilt(r_color_src, gauss_sigma);
    
    % Get Features-----------------------------------------
    pts_l_src = detectSURFFeatures(left_src);%, 'MetricThreshold', 100);
    pts_r_src = detectSURFFeatures(right_src);%, 'MetricThreshold', 100);
    [features_l_src, valid_pts_l_src] = extractFeatures(left_src, pts_l_src);
    [features_r_src, valid_pts_r_src] = extractFeatures(right_src, pts_r_src);
    
    % Match Features-----------------------------------------
    index_pairs_l = matchFeatures(features_l_tgt, features_l_src);
    index_pairs_r = matchFeatures(features_r_tgt, features_r_src);
    
    matched_pts_l_tgt = valid_pts_l_tgt(index_pairs_l(:,1));
    matched_pts_l_src = valid_pts_l_src(index_pairs_l(:,2));
    
    matched_pts_r_tgt = valid_pts_r_tgt(index_pairs_r(:,1));
    matched_pts_r_src = valid_pts_r_src(index_pairs_r(:,2));
    
    % Homography Estimation-----------------------------------------
    [H_l, inlier_pts_l_src, inlier_pts_l_tgt] = estimateGeometricTransform(matched_pts_l_src,matched_pts_l_tgt,'projective');
    [H_r, inlier_pts_r_src, inlier_pts_r_tgt] = estimateGeometricTransform(matched_pts_r_src,matched_pts_r_tgt,'projective');
    %-----------------------------------------
    output_view_l = imref2d(size(left_tgt));
    output_view_r = imref2d(size(right_tgt));
    
    warped_l = im2double(imwarp(left_src, H_l, 'OutputView', output_view_l));
    warped_r = im2double(imwarp(right_src, H_r, 'OutputView', output_view_r));
    
    % Image subtraction -----------------------------------------
    % Right half by the right image
    % Left half by the left image
    left_tgt_d = im2double(left_tgt);
    right_tgt_d = im2double(right_tgt);
    
    % Subtract the points ---
    left_tgt_d = left_tgt_d - warped_l;
    right_tgt_d = right_tgt_d - warped_r;
    
    % Compose the final image: half left and half right
    final_img = left_tgt_d;
    [rows, cols] = size(final_img);
    half_cols = int16(cols/2);
    final_img(:,half_cols:cols) = right_tgt_d(:, half_cols:cols);
    
    % Do not allow negative values
    final_img(final_img < 0) = 0;
    
    final_mask = im2bw(final_img, 0.1);
    %-----------------------------------------
    % figure; imshowpair(original, Ir, 'diff');
    % title('Image Pair');
    
    % figure; imshow(orig_mask);
    % title('Subtraction Mask');
    final_img = final_img.*final_mask;
    
    % orig_filt = imwarp(orig_filt, tform.invert,'OutputView',outputView);
    imshow(final_mask);
    imwrite(final_img, sprintf('../data/processed/move-%04d.png', frame));
    % title('Minus Mask');
    
    % Shift the images
    left_tgt = left_src;
    right_tgt = right_src;
    l_color_tgt = l_color_src;
    r_color_tgt = l_color_src;
    
    % Get Features-----------------------------------------------
    pts_l_tgt = pts_l_src;
    pts_r_tgt = pts_r_src;
    features_l_tgt = features_l_src;
    valid_pts_l_tgt = valid_pts_l_src;
    features_r_tgt = features_r_src;
    valid_pts_r_tgt = valid_pts_r_src;
    
end
%% Test Moving Objects differences
