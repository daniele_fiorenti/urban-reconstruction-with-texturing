#include <iostream>

#include "filters/chain_filter.h"
#include "utility/cloud_visualizer.hpp"
#include "utility/kitti_utility.hpp"
#include "filters/filter_parser.hpp"

// --------------
// -----Main-----
// --------------

/*
 * Compilando questo file troviamo un programma che si chiama filtering a cui deve essere passato un file del tipo example.config
 */
int
main (int argc, char** argv)
{
	// --------------------------------------
	// -----Parse Command Line Arguments-----
	// --------------------------------------
	if(argc < 2){
		return 0;
	}

	int filters_added(0);
	int arguments = argc - 1;	// Consider the last argument as filename
	std::string filepath(argv[argc - 1]);

	if(argc == 2){
		//PROFILER_INIT_TIMER(TOPIC_ALL_EXECUTION_TIME, "Without visualisation...");
		//PROFILER_START_TIMER(TOPIC_ALL_EXECUTION_TIME);
		PROFILER_INIT_TIMER("Main", "Total execution time");
		PROFILER_START_TIMER("Main");
		FilterParser parser(argv[1]);
        boost::shared_ptr<CloudVisualizer> visualizer(new CloudVisualizer(1500, 800, "Point Cloud Filtering"));
        parser.parseAndShow<pcl::PointXYZ>(visualizer);
		//viz->setBackgroundColor(0.05, 0.05, 0.1);

		//PROFILER_STOP_TIMER(TOPIC_ALL_EXECUTION_TIME);
		PROFILER_STOP_TIMER("Main");
		ISAVE_TO_FILE("results.txt", PROFILER_SHOW_STATISTICS + ISHOW_ARCHIVE);

        visualizer->render(50000, 50);
        visualizer->waitClose();
		//parser.parse<pcl::PointXYZI>();
	}
}
