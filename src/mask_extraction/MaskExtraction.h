/*
 * MaskExtraction.h
 *
 *  Created on: June 22, 2016
 *      Author: Daniele
 */

#ifndef MASKEXTRACTION_H
#define MASKEXTRACTION_H

#include "../pipeline/Module.h"

using namespace pipeline;

class MaskExtraction: public Module
{
public:
    MaskExtraction();

    virtual ~MaskExtraction();

    void setScene(Scene * scene);
    bool process(CameraRig * rig, int iteration_index);
    CameraRig * getOutputCameraRig();
    bool hasFinished();
    void finalize();

    void enableSavingMaskImages(bool enable, std::string const &masks_folder);
    void enableSavingDiffImages(bool enable, std::string const &diffs_folder);
    void enablePatchValidation(bool enable, double metric_th = 0.05);

    void enableDepthValidation(bool enable);
//    void enableDepthValidation(bool enable, const std::string depth_folder="");

private:
    // Module related
    bool _finished;
    CameraRig* _last_rigs[2];
    Scene * _scene;

    std::string _output_path;

    //Parameters for mask extraction
    int _frame_dist;
    int _block_size;

    // Patch Validation Parameters;
    int _d_max;
    int _min_box_size;
    int _b_ratio;

    // Depth Validation Parameters
    cv::Mat _depth_s;
    cv::Mat _depth_i;
    int _d_ratio;

    // Internal Variables
    std::string _masks_folder;
    std::string _diffs_folder;
    double _metric_th;

    // Flags
    bool _save_masks_images_ON;
    bool _save_diffs_images_ON;
    bool _patch_thresholding_ON;
    bool _depth_validation_ON;

    // The four cam to rect matrices
    Eigen::Matrix<double, 3, 4> _P_velo_to_img;

    void changeDetect2D();
    std::vector<Eigen::Vector2i> projectToImage(RemovedPointsContainer removed_points_container, Eigen::Matrix<double, 3, 4> T);
    std::shared_ptr<Eigen::Vector2d> projectToImage(RemovedPoint removed_point, Eigen::Matrix<double, 3, 4> T);

    cv::Mat extractImageBlock(cv::Mat &image, std::shared_ptr<Eigen::Vector2d> point, int block_size);
};


#endif // MASKEXTRACTION_H
