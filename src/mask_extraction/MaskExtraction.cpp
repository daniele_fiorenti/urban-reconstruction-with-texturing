/*
 * MaskExtraction.cpp
 *
 *  Created on: June 22, 2016
 *      Author: Daniele
 */

#include "MaskExtraction.h"
#include "../pipeline/CameraRig.h"
#include <Eigen/LU>

#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <boost/regex.hpp>
#include <boost/accumulators/statistics/variance.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> // cv windows
#include <opencv2/imgproc/imgproc.hpp>

#include "../utility/mini_profiler.h"
#include "../utility/helper_functions.h"

MaskExtraction::MaskExtraction() : Module("MaskExtraction", false),
    _frame_dist(2),
    _block_size(20),
    _d_max(30),
    _min_box_size(5),
    _patch_thresholding_ON(false),
    _depth_validation_ON(false),
    _metric_th(0.05),
    _save_masks_images_ON(false),
    _save_diffs_images_ON(false),
    _masks_folder("/mnt/WinPartition/Users/danie/Dropbox/Polimi/Thesis/dataset_output/exp_masks_cpp"),
    _diffs_folder("/mnt/WinPartition/Users/danie/Dropbox/Polimi/Thesis/dataset_output/exp_masks_cpp_diff")

{
    _b_ratio = _min_box_size*_d_max;
    _d_ratio = 2*_d_max;

    _P_velo_to_img = pipeline::CameraRig::getCalibration().camToRect.velo_to_image_plane[2];

    // Nullify the last rigs...
    _last_rigs[0] = _last_rigs[1] = nullptr;

    // Register profiler topic
    PROFILER_INIT_TIMER(name(), "Mask extraction time");
}

MaskExtraction::~MaskExtraction() { }

bool MaskExtraction::process(CameraRig* rig, int iteration_index){

    PROFILER_START_TIMER(name());
    std::cout << name() << " process()" << std::endl;

    if(!rig){
        std::cerr << name() << " Received null rig\n";
        return false;
    }

    if(!_last_rigs[0]){
        _last_rigs[0] = _last_rigs[1] = rig;
    }
    // "Rotate" the last rigs
    _last_rigs[1] = _last_rigs[0];
    _last_rigs[0] = rig;

    if(_last_rigs[0]->getID() != _last_rigs[1]->getID()){
        changeDetect2D();
    }
    PROFILER_STOP_TIMER(name());

    return true;
}

void MaskExtraction::changeDetect2D(){
    std::cout << name() << " changeDetect2D()" << std::endl;

    CameraRig *cam_i = _last_rigs[1]; // cam_i è il frame precedente
    CameraRig *cam_s = _last_rigs[0];

//    std::cerr << "cam_i->getID(): " << cam_i->getID() << std::endl;
//    std::cerr << "cam_s->getID(): " << cam_s->getID() << std::endl;

    Eigen::Matrix4d &pose_i = cam_i->getPose();
    Eigen::Matrix4d &pose_s = cam_s->getPose();

    Eigen::Matrix4d inv_pose_i = pose_i.inverse();
    Eigen::Matrix4d inv_pose_s = pose_s.inverse();

    cv::Mat &img_s = cam_s->getCamera().image.picture;
    cv::Mat &img_i = cam_i->getCamera().image.picture;

    int img_h = img_i.size().height;
    int img_w = img_i.size().width;

    Eigen::Matrix<double, 3, 4> T_i = _P_velo_to_img * inv_pose_i;
    Eigen::Matrix<double, 3, 4> T_s = _P_velo_to_img * inv_pose_s;

    cv::Mat img_mask(img_h, img_w, CV_8UC3, cv::Scalar(0,0,0)); // inizializzo la maschera, riga 136 matlab

    if(_depth_validation_ON) {
        _depth_s = cam_s->getCamera().image.depth_map;
        _depth_i = cam_i->getCamera().image.depth_map;

        Eigen::Matrix4d relative_pose = inv_pose_i*pose_s;
        float relative_pose_x_transl = relative_pose(0,3);

        _depth_i = _depth_i + relative_pose_x_transl/_d_max*255;
    }

//    std::vector<Eigen::Vector2i> rem_img_raw = projectToImage(cam_i->getCamera().removed_points_container, T_i);
//    std::vector<Eigen::Vector2i> rem_img;
//    for(Eigen::Vector2i &rem_img_elem: rem_img_raw){
//        if(rem_img_elem(0) <= img_w && rem_img_elem(1) <= img_h) {
//            rem_img.push_back(rem_img_elem);
//        }
//    }

    int top     = _block_size;
    int left    = _block_size;
    int bottom  = img_h - _block_size;
    int right   = img_w - _block_size;

    // A for loop for OpenMP
    #pragma omp parallel for
//    for(auto &point: cam_i->getCamera().removed_points_container.removed_points){
    for(int i = 0; i < cam_i->getCamera().removed_points_container.size(); i++) {
        auto &point = cam_i->getCamera().removed_points_container.removed_points.at(i);
//        std::cerr << "omp_get_num_threads(): " << omp_get_num_threads() << std::endl;
//        std::cerr << "cam_i->getID(): " << cam_i->getID() << std::endl;
        std::shared_ptr<Eigen::Vector2d> p_cam_i = projectToImage(point, T_i);
        std::shared_ptr<Eigen::Vector2d> p_cam_s = projectToImage(point, T_s);

        if (!(p_cam_i) || (*p_cam_i)(0) > right || (*p_cam_i)(1) > bottom || (*p_cam_i)(0) < left  || (*p_cam_i)(1) < top ||
                !(p_cam_s) || (*p_cam_s)(0) > right || (*p_cam_s)(1) > bottom || (*p_cam_s)(0) < left  || (*p_cam_s)(1) < top) {
            continue;
        } // 211 matlab code

        Eigen::Vector4d point_vector(point.point(0), point.point(1), point.point(2), 1.0);
        Eigen::Vector4d point_is = inv_pose_s*point_vector;
        Eigen::Vector4d point_i  = inv_pose_i*point_vector;
        if(_patch_thresholding_ON) {
            int b_size_s = static_cast<int>(roundf(_b_ratio/static_cast<float>(point_is(0))));
            int b_size_i = static_cast<int>(roundf(_b_ratio/static_cast<float>(point_i(0))));

            cv::Mat feat_s = extractImageBlock(img_s, p_cam_s, b_size_s);
            cv::Mat feat_i = extractImageBlock(img_i, p_cam_i, b_size_i);

            // Resize feat_s to feat_i only if each patch is not zero
            if(feat_s.size().width > 0.0 && feat_s.size().height > 0.0 &&
               feat_i.size().width > 0.0 && feat_i.size().height > 0.0) {

                // Resize feat_s to size of feat_i (Matlab does in a slightly different way)
                cv::resize(feat_s, feat_s, feat_i.size());

                // Let's calculate the standard deviation for feat_s and feat_i for Red, Green and Blue
                std::vector<cv::Mat> feat_s_channels(3);
                std::vector<cv::Mat> feat_i_channels(3);

                cv::split(feat_s,feat_s_channels);
                cv::split(feat_i,feat_i_channels);

                double stdDev_s_blue  = stdDevMatrix(feat_s_channels[0]);
                double stdDev_s_green = stdDevMatrix(feat_s_channels[1]);
                double stdDev_s_red   = stdDevMatrix(feat_s_channels[2]);
                double stdDev_i_blue  = stdDevMatrix(feat_i_channels[0]);
                double stdDev_i_green = stdDevMatrix(feat_i_channels[1]);
                double stdDev_i_red   = stdDevMatrix(feat_i_channels[2]);

                double min_std = 0.1;

                if (stdDev_s_blue > min_std && stdDev_i_blue > min_std &&
                        stdDev_s_green > min_std && stdDev_i_green > min_std &&
                        stdDev_s_red > min_std && stdDev_i_red > min_std)
                {
                    // Calculate NCC
                    std::vector<cv::Mat> result(3);
                    cv::matchTemplate(feat_s_channels[0], feat_i_channels[0], result[0], CV_TM_CCORR_NORMED);
                    cv::matchTemplate(feat_s_channels[1], feat_i_channels[1], result[1], CV_TM_CCORR_NORMED);
                    cv::matchTemplate(feat_s_channels[2], feat_i_channels[2], result[2], CV_TM_CCORR_NORMED);

                    cv::Mat final_image;
                    merge(result, final_image);

                    double min, max;
                    cv::minMaxLoc(final_image, &min, &max);

                    double metric_patch = 1 - max;
    //                std::cout << "metric: " << metric << std::endl;

                    // Guardo se la metrica ha superato la threshold, se passa la threshold allora è un punto effettivamente in movimento
    //                std::cout << "metric_patch: " << metric_patch << std::endl;
                    if(metric_patch > _metric_th){
    //                    std::cout << "i: " << i << " point: " << point.point(0) << " " << point.point(1) << " " << point.point(2) << std::endl;
                        Eigen::Vector2i uv(static_cast<int>((*p_cam_i)(0)+0.5), static_cast<int>((*p_cam_i)(1)+0.5));
    //                    std::cerr << "uv: " << uv(0) << ", " << uv(1) << std::endl;
                        img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[0] = 255; // Blue channel
                        img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[1] = 255; // Green channel
                        img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[2] = 255; // Red channel
                    }
                }
            }
        } else { // !_patch_thresholding_ON
//            std::cout << "i: " << i << " point: " << point.point(0) << " " << point.point(1) << " " << point.point(2) << std::endl;
            Eigen::Vector2i uv(static_cast<int>((*p_cam_i)(0)+0.5), static_cast<int>((*p_cam_i)(1)+0.5));
//            std::cerr << "uv: " << uv(0) << ", " << uv(1) << std::endl;
            img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[0] = 255; // Blue channel
            img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[1] = 255; // Green channel
            img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[2] = 255; // Red channel
        }

        if(_depth_validation_ON) {
            int b_size_s = static_cast<int>(roundf(_d_ratio/static_cast<float>(point_is(0))));
            int b_size_i = static_cast<int>(roundf(_d_ratio/static_cast<float>(point_i(0))));

            cv::Mat feat_s = extractImageBlock(_depth_s, p_cam_s, b_size_s);
            cv::Mat feat_i = extractImageBlock(_depth_i, p_cam_i, b_size_i);

//            std::cout << "feat_s.size(): " << feat_s.size() << std::endl;
//            std::cout << "feat_i.size(): " << feat_i.size() << std::endl;

            if(feat_i.size() != cv::Size(0,0)){
                cv::resize(feat_s, feat_s, feat_i.size());

                cv::Mat diff = feat_s - feat_i;
                cv::Mat squared_diff = diff.mul(diff);
                double metric_depth = cv::sum( squared_diff )[0] / (b_size_i*b_size_i);

                if(metric_depth > _metric_th) {
                    Eigen::Vector2i uv(static_cast<int>((*p_cam_s)(0)+0.5), static_cast<int>((*p_cam_s)(1)+0.5));
                    img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[0] = 255; // Blue channel
                    img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[1] = 255; // Green channel
                    img_mask.at<cv::Vec3b>(cv::Point(uv(0),uv(1)))[2] = 255; // Red channel
                }
            }
        }
    }

    // Once we have generated the points of the mask we have to "fill the holes"

//    cv::namedWindow( "Extracted Mask", cv::WINDOW_AUTOSIZE );
//    cv::imshow( "Extracted Mask", img_mask );
//    cv::waitKey(0);

    cv::Mat mod_mask = img_mask.clone();

    // ne_s = ne_size + ne_offset*(middle_layer - layer_i);
    // ne_filt = ones(ne_s, ne_s);
    // mod_mask = imfilter(img_mask(:,:,layer_i), ne_filt) - ne_min;

    cv::Mat oMatVerticalKernel = cv::Mat(1, 11, CV_32FC1);
    for (int nColItr = 0; nColItr < 11; nColItr++){
        oMatVerticalKernel.at<float>(0, nColItr) = 1.0f;
    }
    cv::Mat oMatHorizontalKernel;
    transpose(oMatVerticalKernel, oMatHorizontalKernel);

    cv::filter2D(mod_mask, mod_mask, -1, oMatVerticalKernel);
    cv::filter2D(mod_mask, mod_mask, -1, oMatHorizontalKernel);

    //Dilate and Erode
    int d_shape_s = 10;
    int e_shape_s = 7;

    cv::Mat d_shape = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(d_shape_s, d_shape_s));
    cv::Mat e_shape = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(e_shape_s, e_shape_s));

    cv::dilate(mod_mask, mod_mask, d_shape, cv::Point(-1, -1), 1, 1, 1);
    cv::erode(mod_mask, mod_mask, e_shape, cv::Point(-1, -1), 2, 1, 1);

    int MAX_KERNEL_LENGTH = 5;
    for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ){
        cv::GaussianBlur( mod_mask, mod_mask, cv::Size( i, i ), 0, 0 );
    }

    cv::Mat changeImg(img_h, img_w, CV_8UC3, cv::Scalar::all(0)); // inizializzo la maschera, riga 136 matlab
    cv::Mat mod_mask_inv(img_h, img_w, CV_8UC3, cv::Scalar::all(0));

    cv::bitwise_not ( mod_mask, mod_mask_inv );

    img_i.copyTo(changeImg, mod_mask_inv);

    if(_save_masks_images_ON){
        char cam_i_ID_Formatted[25];
        sprintf(cam_i_ID_Formatted, "%04d", cam_i->getID());

        imwrite( _masks_folder+"/"+cam_i_ID_Formatted+".png", mod_mask );
    }
    if(_save_diffs_images_ON){
      char cam_i_ID_Formatted[25];
      sprintf(cam_i_ID_Formatted, "%04d", cam_i->getID());

        imwrite( _diffs_folder+"/"+cam_i_ID_Formatted+".png", changeImg );
    }

//    cv::namedWindow( "changeImg", cv::WINDOW_AUTOSIZE );
//    cv::imshow( "changeImg", changeImg );
//    cv::waitKey(1000);
}

CameraRig* MaskExtraction::getOutputCameraRig() {
    return _last_rigs[0];
}

void MaskExtraction::setScene(Scene* scene) {
    _scene = scene;
 }

void MaskExtraction::finalize() {
    _finished = true;
}

inline bool MaskExtraction::hasFinished() {
    return _finished;
}

// Private methods
std::vector<Eigen::Vector2i> MaskExtraction::projectToImage(RemovedPointsContainer removed_points_container, Eigen::Matrix<double, 3, 4> T){
//    std::cout << "projectToImage(RemovedPointsContainer removed_points_container, Eigen::Matrix<double, 3, 4> T)" << std::endl;

    Eigen::RowVector4d P_normal;
    P_normal << T(2,0), T(2,1), T(2,2), T(2,3);

//    std::vector<Eigen::Vector4f> p2_in;
    std::vector<Eigen::Vector2i> p_out;

    for(auto& p: removed_points_container.removed_points){
        Eigen::Vector4d p_in(p.point(0), p.point(1), p.point(2), 1.0);
        float p_pos = P_normal * p_in;

        if(p_pos >= 0) {
//            p2_in.push_back(p_in); // Per costruire p2_in come nel codice matlab
            Eigen::Vector3d p_out_vec3 = T*p_in;
            p_out_vec3(0) = p_out_vec3(0)/p_out_vec3(2);
            p_out_vec3(1) = p_out_vec3(1)/p_out_vec3(2);

            Eigen::Vector2i p_out_vec2;
            p_out_vec2 << static_cast<int>(p_out_vec3(0)+0.5), static_cast<int>(p_out_vec3(1)+0.5);

            if(p_out_vec2(0) >= 1 && p_out_vec2(1) >=1){
                p_out.push_back(p_out_vec2);
            }
        }
    }
    return p_out;
}

std::shared_ptr<Eigen::Vector2d> MaskExtraction::projectToImage(RemovedPoint removed_point, Eigen::Matrix<double, 3, 4> T){
//    std::cout << "projectToImage(RemovedPoint removed_point, Eigen::Matrix<double, 3, 4> T)" << std::endl;
//    std::cout << "T: \n" << T << std::endl;

    Eigen::Vector4d P_normal;
    P_normal << T(2,0), T(2,1), T(2,2), T(2,3);

    Eigen::Vector4d p_in(removed_point.point(0), removed_point.point(1), removed_point.point(2), 1.0);

    double p_pos = P_normal.transpose() * p_in;

    std::shared_ptr<Eigen::Vector2d> p_out_vec2;
    if(p_pos >= 0) {
//            p2_in.push_back(p_in); // Per costruire p2_in come nel codice matlab
        Eigen::Vector3d p_out_vec3 = T*p_in;
        p_out_vec3(0) = p_out_vec3(0)/p_out_vec3(2);
        p_out_vec3(1) = p_out_vec3(1)/p_out_vec3(2);

        p_out_vec2 = std::make_shared<Eigen::Vector2d>();
        *p_out_vec2 << p_out_vec3(0), p_out_vec3(1);

        if((*p_out_vec2)(0) >= 1 && (*p_out_vec2)(1) >=1){
//            std::cout << "*p_out_vec2: " << (*p_out_vec2)(0) << " " << (*p_out_vec2)(1) << " " << std::endl;
            return p_out_vec2;
        }
    }
    return nullptr;
}

cv::Mat MaskExtraction::extractImageBlock(cv::Mat &image, std::shared_ptr<Eigen::Vector2d> point, int block_size){
    double hs = ((double)block_size-1)/2;

    int min_col = static_cast<int>(ceil((*point)(0)-hs));
    int max_col = static_cast<int>(ceil((*point)(0)+hs));
    int min_row = static_cast<int>(ceil((*point)(1)-hs));
    int max_row = static_cast<int>(ceil((*point)(1)+hs));

    int img_h = image.size().height;
    int img_w = image.size().width;

    int min_c = std::max(min_col, 1);
    int max_c = std::min(max_col, img_w);
    int min_r = std::max(min_row, 1);
    int max_r = std::min(max_row, img_h);

    // The PATCH
    cv::Mat block = image( cv::Rect(min_c, min_r, max_c-min_c, max_r-min_r) );

    int block_h = block.size().height;
    int block_w = block.size().width;

    // Here we make the patch SQUARE
    int m = std::min(block_h, block_w);
    if(m >= 1){
        block = block(cv::Rect(1, 1, m-1, m-1));
    }
    return block;
}

// Public methods
void MaskExtraction::enableSavingMaskImages(bool enable, std::string const &masks_folder){
    _save_masks_images_ON = enable;
    if(enable){
        _masks_folder = masks_folder;
    }

    if(!_masks_folder.empty()){
        createFolder(_masks_folder);
    }
}

void MaskExtraction::enableSavingDiffImages(bool enable, std::string const &diffs_folder){
    _save_diffs_images_ON = enable;
    if(enable){
        _diffs_folder = diffs_folder;
    }

    if(!_diffs_folder.empty()){
        createFolder(_diffs_folder);
    }
}

void MaskExtraction::enablePatchValidation(bool enable, double metric_th){
    _patch_thresholding_ON = enable;
    _metric_th = metric_th;
}

void MaskExtraction::enableDepthValidation(bool enable) {
    _depth_validation_ON = enable;
}
