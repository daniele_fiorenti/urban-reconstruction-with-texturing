/*
 * DepthMapExtraction.cpp
 *
 *  Created on: September 2, 2016
 *      Author: Daniele
 */

#include "DepthMapExtraction.h"
#include "../pipeline/CameraRig.h"
#include <Eigen/LU>

#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <boost/regex.hpp>
#include <boost/accumulators/statistics/variance.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> // cv windows
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/eigen.hpp> // eigen2cv & cv2eigen
#include <opencv2/calib3d.hpp> // convertPointsToHomogeneous

#include "../utility/mini_profiler.h"
#include "../utility/helper_functions.h"

#include <pcl/common/io.h> // needed for copyPointCloud
#include <stdexcept>

DepthMapExtraction::DepthMapExtraction() : Module("DepthMapExtraction", false),
    _save_depth_images_ON(false),
    _depth_folder("/mnt/WinPartition/Users/danie/Dropbox/Polimi/Thesis/dataset_output/depths_cpp"),
    _velo_cloud()

{
    _P_velo_to_img = pipeline::CameraRig::getCalibration().camToRect.velo_to_image_plane[2];

    // Nullify the last rig...
    _current_rig = nullptr;

    // Register profiler topic
    PROFILER_INIT_TIMER(name(), "Depth map extraction time");
}

DepthMapExtraction::~DepthMapExtraction() { }

bool DepthMapExtraction::process(CameraRig* rig, int iteration_index){

    PROFILER_START_TIMER(name());
    std::cout << name() << " process()" << std::endl;

    if(!rig){
        std::cerr << name() << " Received null rig\n";
        return false;
    }

    _current_rig = rig;
    generateDepthMap();
    PROFILER_STOP_TIMER(name());

    return true;
}

void DepthMapExtraction::generateDepthMap() {
    Eigen::Matrix4d &current_pose = _current_rig->getPose();
    Eigen::Matrix4d inv_pose = current_pose.inverse();

    cv::Mat &img_s = _current_rig->getCamera().image.picture;
    int img_h = img_s.size().height;
    int img_w = img_s.size().width;
    _current_rig->getLidar().cloud.clone(_velo_cloud); // clone current cloud to velo_cloud
    _velo_cloud.sort(); // Ordino i punti in ordine crescente rispetto alla x
//    std::cerr << "_velo_cloud.size(): " << _velo_cloud.size() << std::endl;
    cv::Mat depth_image(img_h, img_w, CV_8UC3, cv::Scalar::all(255));
    cv::Mat velo_points_cv = PoinXYZToMat(_velo_cloud.cloud_shared_ptr());
    cv::copyMakeBorder(velo_points_cv, velo_points_cv, 0, 1, 0, 0, cv::BORDER_CONSTANT, cv::Scalar(1)); // Padding matrix with ones

    cv::Mat inv_pose_s_cv;
    cv::eigen2cv(inv_pose, inv_pose_s_cv);
    cv::Mat p_velo_act = inv_pose_s_cv*velo_points_cv;
    // Remove Points Behind Camera and take points max depth exploiting the fact that p_velo_act is sorted wrt x
    for(int i = 0; i < p_velo_act.cols; i++) {
        if(p_velo_act.at<double>(0,i) > 0){
            p_velo_act = p_velo_act.colRange(i, p_velo_act.cols-1);
            break;
        }
    }
    if(countNonZero(p_velo_act) < 1) { // p_velo_act is not empty
      throw std::runtime_error("Cloud cannot be empty!");
    }
    double max_depth = p_velo_act.at<double>(0, p_velo_act.cols-1);

    cv::Mat P_velo_to_img_cv;
    cv::eigen2cv(_P_velo_to_img, P_velo_to_img_cv);
    cv::Mat p_img_plane = P_velo_to_img_cv * p_velo_act;

    cv::convertPointsFromHomogeneous(p_img_plane.t(), p_img_plane);
//    p_img_plane.row(0) = p_img_plane.row(0) / p_img_plane.row(2);
//    p_img_plane.row(1) = p_img_plane.row(1) / p_img_plane.row(2);
//    p_img_plane = p_img_plane.t();

    for(int i = 0; i < p_img_plane.size().height; i++) {
        if(p_img_plane.at<double>(i,0) > img_w ||
                p_img_plane.at<double>(i,1) > img_h ||
                p_img_plane.at<double>(i,0) < 1 ||
                p_img_plane.at<double>(i,1) < 1){
            continue;
        }

        if(p_velo_act.at<double>(0,i) > 0) {
            int uv_0 = static_cast<int>(p_img_plane.at<double>(i,0));
            int uv_1 = static_cast<int>(p_img_plane.at<double>(i,1));
            depth_image.at<cv::Vec3b>(cv::Point(uv_0,uv_1))[0] = p_velo_act.at<double>(0,i) / max_depth * 255; // B channel
            depth_image.at<cv::Vec3b>(cv::Point(uv_0,uv_1))[1] = p_velo_act.at<double>(0,i) / max_depth * 255; // G channel
            depth_image.at<cv::Vec3b>(cv::Point(uv_0,uv_1))[2] = p_velo_act.at<double>(0,i) / max_depth * 255; // R channel
        }
    }

    cv::bitwise_not ( depth_image, depth_image );
    int d_shape_s = 10;
    cv::Mat d_shape = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(d_shape_s, d_shape_s));
    cv::dilate(depth_image, depth_image, d_shape, cv::Point(-1, -1), 1, 1, 1);

    int MAX_KERNEL_LENGTH = 5;
    for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ){
        cv::GaussianBlur( depth_image, depth_image, cv::Size( i, i ), 0, 0 );
    }

//    cv::namedWindow( "depthImg", cv::WINDOW_AUTOSIZE );
//    cv::imshow( "depthImg", depth_image );
//    cv::waitKey(1000);

    if(_save_depth_images_ON){
        char rigID_Formatted[25];
        sprintf(rigID_Formatted, "%04d", _current_rig->getID());

        imwrite( _depth_folder+"/"+rigID_Formatted+".png", depth_image );
    }
    _current_rig->getCamera().image.depth_map = depth_image;
}


CameraRig* DepthMapExtraction::getOutputCameraRig() {
    return _current_rig;
}

void DepthMapExtraction::setScene(Scene* scene) {
    _scene = scene;
 }

void DepthMapExtraction::finalize() {
    _finished = true;
}

inline bool DepthMapExtraction::hasFinished() {
    return _finished;
}

// Private methods
void DepthMapExtraction::enableSavingDepthImages(bool enable, std::string const &masks_folder){
    _save_depth_images_ON = enable;
    if(enable){
        _depth_folder = masks_folder;
    }

    if(!_depth_folder.empty()){
        createFolder(_depth_folder);
    }
}

