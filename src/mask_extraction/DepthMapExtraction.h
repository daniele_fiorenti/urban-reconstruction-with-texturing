/*
 * DepthMapExtraction.h
 *
 *  Created on: September 2, 2016
 *      Author: Daniele
 */

#ifndef DEPTHMAPEXTRACTION_H
#define DEPTHMAPEXTRACTION_H

#include "../pipeline/Module.h"

using namespace pipeline;

class DepthMapExtraction: public Module
{
public:
    DepthMapExtraction();

    virtual ~DepthMapExtraction();

    void setScene(Scene * scene);
    bool process(CameraRig * rig, int iteration_index);
    CameraRig * getOutputCameraRig();
    bool hasFinished();
    void finalize();

    void enableSavingDepthImages(bool enable, std::string const &masks_folder);
    void generateDepthMap();

private:
    // Module related
    bool _finished;
    CameraRig* _current_rig;
    Scene * _scene;

    std::string _output_path;

    // Internal Variables
    std::string _depth_folder;
    Eigen::Matrix<double, 3, 4> _P_velo_to_img;
    Cloud _velo_cloud;

    // Flags
    bool _save_depth_images_ON;
    bool _patch_thresholding_ON;

};

#endif // DEPTHMAPEXTRACTION_H
