/*
 * pcl_filter_main.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: george
 */

#include <iostream>

#include <pcl/io/ply_io.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>

#include "cloud_cleaning/MovingObjectsRemoval.h"
#include "cloud_cleaning/ChangeDetectionWDST.h"
#include "cloud_cleaning/ChangeDetectionOriginal.h"
#include "filters/MultiFilter.h"

#include "pipeline/Pipeline.h"

#include "data_loading/CalibrationLoader.h"
#include "data_loading/LidarLoader.h"
#include "data_loading/IMULoader.h"
#include "cloud_processing/CloudTransformer.h"
#include "cloud_processing/PoseCalculator.h"
#include "cloud_processing/Registration.h"
#include "cloud_processing/GICPRegistration.h"
#include "cloud_processing/CloudAccumulator.h"
#include "cloud_processing/CloudFilter.h"
#include "cloud_processing/CloudMultiFilter.h"
#include "cloud_processing/PosesSaver.h"
#include "cloud_processing/PosesLoader.h"

#include "visualization/CloudVisualizerSimple.h"

#include "utility/mini_profiler.h"

#define PI 3.14159265358979

/*
 * Fa un confronto tra il change detection di postica e quello del paper
 */

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
	using namespace pipeline;
    using namespace data_loading;
	using namespace cloud_processing;
	using namespace cloud_cleaning;
	using namespace visualization;

	PROFILER_INIT_TIMER("Main", "Total execution time");
	PROFILER_START_TIMER("Main");
	std::cout << "Setting up the modules...\n";

    std::string dataset = "/mnt/WinPartition/Users/danie/workspace/dataset_0095";
	int start_position 	= 35;
	int end_position	= 255;

	double min_r, max_r, step_r;
	double min_theta, max_theta, step_theta;

	std::ifstream eval_file("eval_params.txt", std::ios::out);
	std::string line;
	std::getline(eval_file, line);
    std::cout << "line " << line << " endline\n";
	{
		std::stringstream ss;
        ss << line;
		double n;
        ss >> min_r >> max_r >> n;
		step_r = (max_r - min_r)/(n-1);
	}
	std::getline(eval_file, line);
	{
		std::stringstream ss;
		ss << line;
		double n;
		ss >> min_theta >> max_theta >> n;
		min_theta *= PI/180.0; // In rad
		max_theta *= PI/180.0; // In rad
		step_theta = (max_theta - min_theta)/(n-1);
	}

    int start_file = 0;

	std::getline(eval_file, line);
	{
		std::stringstream ss;
		ss << line;
		ss >> start_file;
	}
	eval_file.close();

	int file_i = start_file;

	MultiFilter post_registration_filter;
	post_registration_filter.addCropBox(50.0f, 50.0f, 4.2f);

	std::cout << "Evaluating with:\n-- "
			<< "min_r = " << min_r << "\tmax_r = " << max_r << "\tstep_r = " << step_r << "\n-- "
			<< "min_theta = " << min_theta << "\tmax_theta = " << max_theta << "\tstep_theta = " << step_theta << "\n";

    CalibrationLoader calib_loader;
    calib_loader.loadCalibrationVeloToCam(dataset + "/calibration/calib_velo_to_cam.txt");
    calib_loader.loadCalibrationImuToVelo(dataset + "/calibration/calib_imu_to_velo.txt");
    calib_loader.loadCalibrationCamToRect(dataset + "/calibration/calib_cam_to_cam.txt");

	for(double p_r = min_r; p_r <= max_r; p_r += step_r){
		for(double p_theta = min_theta; p_theta <= max_theta; p_theta += step_theta){
            LidarLoader *lidar_loader = new LidarLoader(dataset + "/velodyne_points/data",
                                                        dataset + "/velodyne_points/timestamps.txt");
			//	lidar_loader->setBoundingBoxLimits(15.0f, 20.0f, 2.25f);
			lidar_loader->setFilesRange(start_position, end_position);

            IMULoader *imu_loader = new IMULoader(dataset + "/oxts/data",
                                                  dataset + "/oxts/timestamps.txt");
			imu_loader->setFilesRange(start_position, end_position);

			ChangeDetectionWDST* change_detector = new ChangeDetectionWDST();
			change_detector->setSkippedFrames(1);
			change_detector->setMaxPreceedingClouds(10);
			change_detector->setMinPreceedingClouds(8);
			change_detector->enablePostProcess(false, 6);
			change_detector->enableFloorSeparation(true);
			change_detector->enableDynamicRemovedPointsSwapping(true);
			change_detector->enableVoxelizedAlgorithm(true, 0.3);

			change_detector->getParameters().sigma_r = p_r;
			change_detector->getParameters().search_radius = p_theta;
			change_detector->getParameters().lambda_theta = p_theta/3;
			change_detector->getParameters().sigma_theta = p_theta/3;
			change_detector->getParameters().precompute();

			std::string eval_folder = std::string("../output/wdst/eval_") + std::to_string(file_i) + "/";
			if(!boost::filesystem::exists(eval_folder.c_str())){
				boost::filesystem::create_directory(eval_folder);
			}
            change_detector->enableSavingPointsBinaryFile(true, eval_folder);
			change_detector->enableInformationStoring(true, "../output/wdst/info.txt");

			Pipeline pipeLine;
			pipeLine.addModule(lidar_loader);
			pipeLine.addModule(imu_loader);
			pipeLine.addModule(new PosesLoader());
			pipeLine.addModule(new CloudMultiFilter(post_registration_filter));
			pipeLine.addModule(new CloudTransformer());
			pipeLine.addModule(change_detector);

			std::cout << "Starting the pipeline...\n";
			pipeLine.init(Pipeline::PARALLEL_AUTO);

			file_i++;
		}
	}

	file_i = start_file;

//	for(double p_r = min_r; p_r <= max_r; p_r += step_r){
//		for(double p_theta = min_theta; p_theta <= max_theta; p_theta += step_theta){
//			LidarLoader *lidar_loader = new LidarLoader(dataset + "/velodyne_points/data",
//					dataset + "/velodyne_points/timestamps.txt",
//					dataset + "/calibration/calib_imu_to_velo.txt");
//			//	lidar_loader->setBoundingBoxLimits(15.0f, 20.0f, 2.25f);
//			lidar_loader->setFilesRange(start_position, end_position);
//
//			IMULoader *imu_loader = new IMULoader(dataset + "/oxts/data", dataset + "/oxts/timestamps.txt");
//			imu_loader->setFilesRange(start_position, end_position);
//
//			ChangeDetectionOriginal *change_detector = new ChangeDetectionOriginal();
//			change_detector->setSkippedFrames(1);
//			change_detector->setMaxPreceedingClouds(10);
//			change_detector->setMinPreceedingClouds(8);
//			change_detector->enablePostProcess(false, 6);
//			change_detector->enableFloorSeparation(true);
//			change_detector->enableDynamicRemovedPointsSwapping(false);
//			change_detector->enableVoxelizedAlgorithm(false, 0.3);
//
//			change_detector->getParameters().sigma_r = p_r;
//			change_detector->getParameters().search_radius = p_theta;
//			change_detector->getParameters().sigma_theta = p_theta;
//			change_detector->getParameters().lambda_theta = 0.9;
//			change_detector->getParameters().precompute();
//
//			std::string eval_folder = std::string("../output/orig/eval_") + std::to_string(file_i) + "/";
//			if(!boost::filesystem::exists(eval_folder.c_str())){
//				boost::filesystem::create_directory(eval_folder);
//			}
//			change_detector->enableSavingPointsBinaryFile(true, eval_folder);
//			change_detector->enableInformationStoring(true, "../output/orig/info.txt");
//
//			Pipeline pipeLine;
//			pipeLine.addModule(lidar_loader);
//			pipeLine.addModule(imu_loader);
//			pipeLine.addModule(new PosesLoader());
//			pipeLine.addModule(new CloudMultiFilter(post_registration_filter));
//			pipeLine.addModule(new CloudTransformer());
//			pipeLine.addModule(change_detector);
//
//			std::cout << "Starting the pipeline...\n";
//			pipeLine.init(Pipeline::PARALLEL_AUTO);
//
//			file_i++;
//		}
//	}


	PROFILER_STOP_TIMER("Main");
	//pcl::visualization::PCLVisualizer vis;
	PROFILER_SHOW_STATISTICS;

}
