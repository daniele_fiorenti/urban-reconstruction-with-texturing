/*
 * registrator.h
 *
 *  Created on: May 8, 2015
 *      Author: george
 */

#ifndef REGISTRATION_REGISTRATOR_H_
#define REGISTRATION_REGISTRATOR_H_

#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <string>
#include <vector>

#include <pcl/common/common_headers.h>

#include "../utility/cloud_visualizer.hpp"

template <typename PointT>
class Registrator {
public:
	typedef typename boost::shared_ptr<pcl::PointCloud<PointT> > cloud_ptr;

	Registrator();
	Registrator(boost::shared_ptr<CloudVisualizer> viewer);

	bool parseLineTimestamp(const std::string &line);
	bool parseLineIMU(const std::string &line);
	bool parseLineLidarTimestamps(const std::string &start,
								  const std::string &end,
								  const std::string &sample);

	void setIMUtoLidarCalibration(Eigen::Matrix4f &matrix);

	void prepare();

	virtual void addPointCloud(cloud_ptr points, int index);

	virtual void finalize();

	virtual ~Registrator();

	double totalTime();

	typename boost::shared_ptr<pcl::PointCloud<PointT> > getFinalCloud();

	void setVisualizer(boost::shared_ptr<CloudVisualizer> vis){
		viewer = vis;
	}

protected:
	struct Timestamp{
		unsigned int h;
		unsigned int m;
		double s;

		bool operator>(const Timestamp& other){
			return ((this->h > other.h)
					|| ((this->h == other.h) && (this->m > other.m))
					|| ((this->h == other.h) && (this->m == other.m) && (this->s > other.s)));
		}

		bool operator<(const Timestamp& other){
			return ((this->h < other.h)
					|| ((this->h == other.h) && (this->m < other.m))
					|| ((this->h == other.h) && (this->m == other.m) && (this->s < other.s)));
		}

		bool operator==(const Timestamp& other){
			return (this->h == other.h) && (this->m == other.m) && (this->s == other.s);
		}

		double operator-(const Timestamp& other){
			return (double)(h - other.h)*3600.0
					+ (double)(m - other.m)*60.0
					+ (s - other.s);
		}
	};

	struct IMUData{
		double lat; 		//	latitude of the oxts-unit (deg)
		double lon;			//	longitude of the oxts-unit (deg)
		double alt;			//	altitude of the oxts-unit (m)
		double roll;		//	roll angle (rad),    0 = level, positive = left side up,      range: -pi   .. +pi
		double pitch;		//	pitch angle (rad),   0 = level, positive = front down,        range: -pi/2 .. +pi/2
		double yaw;			//  heading (rad),       0 = east,  positive = counter clockwise, range: -pi   .. +pi
		double vel_north;	//  velocity towards north (m/s)
		double vel_east;	//  velocity towards east (m/s)
		double vel_fw;		//  forward velocity, i.e. parallel to earth-surface (m/s)
		double vel_left;	//  leftward velocity, i.e. parallel to earth-surface (m/s)
		double vel_up;		//  upward velocity, i.e. perpendicular to earth-surface (m/s)
		double acc_x;		//  acceleration in x, i.e. in direction of vehicle front (m/s^2)
		double acc_y;		//  acceleration in y, i.e. in direction of vehicle left (m/s^2)
		double acc_z;		//  acceleration in z, i.e. in direction of vehicle top (m/s^2)
		double acc_fw;		//  forward acceleration (m/s^2)
		double acc_left;	//  leftward acceleration (m/s^2)
		double acc_up;		//  upward acceleration (m/s^2)
		double w_x;			//  angular rate around x (rad/s)
		double w_y;			//  angular rate around y (rad/s)
		double w_z;			//  angular rate around z (rad/s)
		double w_fw;		//  angular rate around forward axis (rad/s)
		double w_left;		//  angular rate around leftward axis (rad/s)
		double w_up;		//  angular rate around upward axis (rad/s)
		double pos_accuracy;//  velocity accuracy (north/east in m)
		double vel_accuracy;//  velocity accuracy (north/east in m/s)
		int navstat;		//  navigation status (see navstat_to_string)
		int numsats;		//  number of satellites tracked by primary GPS receiver
		int posmode;		//  position mode of primary GPS receiver (see gps_mode_to_string)
		int velmode;		//  velocity mode of primary GPS receiver (see gps_mode_to_string)
		int orimode;		//  orientation mode of primary GPS receiver (see gps_mode_to_string)

		Timestamp timestamp;	//Timestamp of the data
		Eigen::Matrix4f pose;	//Pose of the data relative to the origin
		Eigen::Matrix4f relative_pose; //Pose relative to the last pose
	};

	struct LidarTimestamp{
		Timestamp start;
		Timestamp sample;
		Timestamp end;
	};

	cloud_ptr cloud;

	std::vector<IMUData> imu;
	std::vector<Timestamp> times;
	std::vector<LidarTimestamp> vel_times;
	Eigen::Matrix4f calibration;

	virtual void assignTimestamps();
	virtual void calculatePoses();

	virtual void prepare_();

	boost::shared_ptr<CloudVisualizer> viewer;

private:
	boost::regex timestamp_regex;
};

template<typename PointT>
inline double Registrator<PointT>::totalTime() {
	imu[imu.size()-1].timestamp - imu[0].timestamp;
}

template<typename PointT>
inline void Registrator<PointT>::finalize() { }

template<typename PointT>
inline typename boost::shared_ptr<pcl::PointCloud<PointT> > Registrator<PointT>::getFinalCloud() {
	return cloud;
}

template<typename PointT>
inline void Registrator<PointT>::setIMUtoLidarCalibration(
		Eigen::Matrix4f &matrix) {
	calibration = matrix;
}

template<typename PointT>
inline Registrator<PointT>::Registrator(boost::shared_ptr<CloudVisualizer> viz) {
	timestamp_regex = boost::regex("[ \\t]*[0-9-]*[ \\t]*([0-9]*)\\:([0-9]*)\\:([0-9]*\\.[0-9]*)[ \\t]*");
	cloud = cloud_ptr(new pcl::PointCloud<PointT>());
	calibration = Eigen::Matrix4f::Identity();
	viewer = viz;
}

template<typename PointT>
inline Registrator<PointT>::Registrator() {
	timestamp_regex = boost::regex("[ \\t]*[0-9-]*[ \\t]*([0-9]*)\\:([0-9]*)\\:([0-9]*\\.[0-9]*)[ \\t]*");
	cloud = cloud_ptr(new pcl::PointCloud<PointT>());
	calibration = Eigen::Matrix4f::Identity();
}

template<typename PointT>
inline void Registrator<PointT>::prepare_() { }

template<typename PointT>
inline bool Registrator<PointT>::parseLineTimestamp(const std::string &line) {
	boost::cmatch groups;

	if(boost::regex_match(line.c_str(), groups, timestamp_regex)){
		Timestamp t;
		t.h = std::atoi(groups[1].str().c_str());
		t.m = std::atoi(groups[2].str().c_str());
		t.s = std::atof(groups[3].str().c_str());
		times.push_back(t);

		//cout << t.h << ":" << t.m << ":" << t.s << endl;
		return true;
	}
	return false;
}

template<typename PointT>
inline bool Registrator<PointT>::parseLineLidarTimestamps(
		const std::string& start, const std::string& end,
		const std::string& sample) {
	boost::cmatch groups_start;
	boost::cmatch groups_end;
	boost::cmatch groups_sample;

	if(boost::regex_match(start.c_str(), groups_start, timestamp_regex) &&
	   boost::regex_match(end.c_str(), groups_end, timestamp_regex) &&
	   boost::regex_match(sample.c_str(), groups_sample, timestamp_regex)){
		LidarTimestamp t;

		t.start.h = std::atoi(groups_start[1].str().c_str());
		t.start.m = std::atoi(groups_start[2].str().c_str());
		t.start.s = std::atof(groups_start[3].str().c_str());

		t.end.h = std::atoi(groups_end[1].str().c_str());
		t.end.m = std::atoi(groups_end[2].str().c_str());
		t.end.s = std::atof(groups_end[3].str().c_str());

		t.sample.h = std::atoi(groups_sample[1].str().c_str());
		t.sample.m = std::atoi(groups_sample[2].str().c_str());
		t.sample.s = std::atof(groups_sample[3].str().c_str());

		vel_times.push_back(t);

		//cout << t.h << ":" << t.m << ":" << t.s << endl;
		return true;
	}
}

template<typename PointT>
inline bool Registrator<PointT>::parseLineIMU(const std::string &line) {
	IMUData data;
	std::stringstream ss(line);
	ss >> data.lat;
	ss >> data.lon;
	ss >> data.alt;
	ss >> data.roll;
	ss >> data.pitch;
	ss >> data.yaw;
	ss >> data.vel_north;
	ss >> data.vel_east;
	ss >> data.vel_fw;
	ss >> data.vel_left;
	ss >> data.vel_up;
	ss >> data.acc_x;
	ss >> data.acc_y;
	ss >> data.acc_z;
	ss >> data.acc_fw;
	ss >> data.acc_left;
	ss >> data.acc_up;
	ss >> data.w_x;
	ss >> data.w_y;
	ss >> data.w_z;
	ss >> data.w_fw;
	ss >> data.w_left;
	ss >> data.w_up;
	ss >> data.pos_accuracy;
	ss >> data.vel_accuracy;
	ss >> data.navstat;
	ss >> data.numsats;
	ss >> data.posmode;
	ss >> data.velmode;
	ss >> data.orimode;

	imu.push_back(data);
}

template<typename PointT>
inline void Registrator<PointT>::prepare() {
	assignTimestamps();
	calculatePoses();

	prepare_();

}

template<typename PointT>
inline void Registrator<PointT>::addPointCloud(cloud_ptr points, int index) { }

template<typename PointT>
inline void Registrator<PointT>::assignTimestamps() {
	for(int i = 0; i < imu.size() && i < times.size(); i++){
			imu[i].timestamp = times[i];
	}
}

template<typename PointT>
inline void Registrator<PointT>::calculatePoses() {

}

template<typename PointT>
inline Registrator<PointT>::~Registrator() {
	//closeDemo();
}

#endif /* REGISTRATION_REGISTRATOR_H_ */
