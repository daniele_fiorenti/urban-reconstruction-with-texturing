/*
 * kitti_registrator.cpp
 *
 *  Created on: May 8, 2015
 *      Author: george
 */
#ifndef REGISTRATION_KITTI_REGISTRATOR_HPP_
#define REGISTRATION_KITTI_REGISTRATOR_HPP_

#include "kitti_registrator.h"

#define EARTH_POLAR_RADIUS 		6356752.3
#define EARTH_POL_SQR_RADIUS	EARTH_POLAR_RADIUS * EARTH_POLAR_RADIUS
#define EARTH_ECUATOR_RADIUS 	6378137.0
#define EARTH_ECU_SQR_RADIUS	EARTH_ECUATOR_RADIUS * EARTH_ECUATOR_RADIUS
#define EARTH_RADIUS			6378137		//Shortcut for KITTI DATASET only
#define DEG_TO_RAD 				3.14159265358979/180.00

static inline double getEarthRadius(double lat){
	using namespace std;
	double acos2 = EARTH_ECUATOR_RADIUS * cos(lat) * EARTH_ECUATOR_RADIUS * cos(lat);
	double bsin2 = EARTH_POLAR_RADIUS * sin(lat) * EARTH_POLAR_RADIUS * sin(lat);
	return sqrt((acos2 * EARTH_ECU_SQR_RADIUS + bsin2 * EARTH_POL_SQR_RADIUS) / (acos2 + bsin2));
}

static inline double lerp(double a, double b, double x){
	return a + x*(b-a);
}

static inline double point_to_origin_distance(double x, double y, double z){
	return std::sqrt(x*x + y*y + z*z);
}

template<typename PointT>
void KittiRegistrator<PointT>::addPointCloud(cloud_ptr points, int index) {
	Eigen::Matrix4f transform;
	PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
	if(index < 1){
		transform = this->imu[index].pose;// * this->calibration;
		pcl::transformPointCloud(*(input_filter.reapplyFilters(points)), *(this->cloud), transform);
		pcl::transformPointCloud(*(env_filter.reapplyFilters(points)), *env, transform);
		*last_sampled = *env;
		return;
	}
	transform = this->imu[index-1].pose * this->imu[index].relative_pose;// * this->calibration;

	pcl::transformPointCloud(*(env_filter.reapplyFilters(points)), *env, transform);

	if(this->viewer){
		this->viewer->showIterationStep(env, last_sampled, this->cloud, REG_DEMO_FRAME_DURATION * 10);
	}

	PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
	Eigen::Matrix4f rel_transform;
	total_fitness += performRegistration(env, last_sampled, last_sampled, rel_transform);
	PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
	transform = rel_transform * transform;
	rel_transform = rel_transform * this->imu[index].relative_pose;
	pcl::transformPointCloud(*(input_filter.reapplyFilters(points)), *points, transform);
	*(this->cloud) += *(points);

	this->imu[index].pose = transform;
	this->imu[index].relative_pose = rel_transform;
	PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);

	overall_distance += point_to_origin_distance(rel_transform(0,3),rel_transform(1,3),rel_transform(2,3));
}

template<typename PointT>
double KittiRegistrator<PointT>::performRegistration(cloud_ptr source,
		cloud_ptr target, cloud_ptr aligned, Eigen::Matrix4f& transform) {
	return 0;
}

template<typename PointT>
void KittiRegistrator<PointT>::finalize() {
	this->cloud = output_filter.reapplyFilters(this->cloud);
	IARCHIVE(TOPIC_TRIP_INFO, "Time:     " + boost::to_string(this->totalTime()) + " sec");
	IARCHIVE(TOPIC_TRIP_INFO, "Distance: " + boost::to_string(overall_distance) + " m");
	IARCHIVE(TOPIC_REGISTRATION, "Total Fitness:    " + boost::to_string(total_fitness));
	if(this->viewer){
		IARCHIVE(TOPIC_REGISTRATION, "Total Iterations: " + boost::to_string(iter_counter));
	}
}

template<typename PointT>
void KittiRegistrator<PointT>::prepare_() {
	if(env_filter.size() == 0){
		env_filter.addCropBox(40.0, 60.0, 4.0).addApproximateVoxelGrid(0.05, 0.05, 0.05);
	}
	if(input_filter.size() == 0){
		input_filter.addCropBox(24.0, 60.0, 8.0, 0.0)
						.addVoxelGrid(0.05, 0.05, 0.05, 2);
	}
	if(output_filter.size() == 0){
		output_filter.addVoxelGrid(0.05, 0.05, 0.05, 1);
		//.addRadiusOutlier(0.5, 12);
	}
}

template<typename PointT>
inline void KittiRegistrator<PointT>::calculatePoses() {
	using namespace std;
	double scale = cos(this->imu[0].lat * DEG_TO_RAD);
	double coeff = 0.0; // For Interpolation purposes...

	Eigen::Matrix4f matrix = Eigen::Matrix4f::Identity();
	Eigen::Matrix4f inv_mat;
	Eigen::Matrix4f last_inv;

	double lat = this->imu[0].lat;
	double lon = this->imu[0].lon;
	double alt = 0.0;

	double cosZ = cos(this->imu[0].yaw);
	double sinZ = sin(this->imu[0].yaw);
	double cosY = cos(this->imu[0].pitch);
	double sinY = sin(this->imu[0].pitch);
	double cosX = cos(this->imu[0].roll);
	double sinX = sin(this->imu[0].roll);

	//With interpolation...
	for(int i=0; i<this->imu.size(); i++){
		//The first pose should not be interpolated,
		if(i > 0){
#ifdef REG_IMU_INTERPOLATE
			coeff = (this->vel_times[i].sample - this->imu[i-1].timestamp)
										/(this->imu[i].timestamp - this->imu[i-1].timestamp);
			cosZ = lerp(cosZ, cos(this->imu[i].yaw), coeff);
			sinZ = lerp(sinZ, sin(this->imu[i].yaw), coeff);
			cosY = lerp(cosY, cos(this->imu[i].pitch), coeff);
			sinY = lerp(sinY, sin(this->imu[i].pitch), coeff);
			cosX = lerp(cosX, cos(this->imu[i].roll), coeff);
			sinX = lerp(sinX, sin(this->imu[i].roll), coeff);
			lat = lerp(this->imu[i-1].lat, this->imu[i].lat, coeff);
			lon = lerp(this->imu[i-1].lon, this->imu[i].lon, coeff);
			alt = lerp(this->imu[i-1].alt, this->imu[i].alt, coeff) - this->imu[0].alt;
#else
			cosZ = cos(this->imu[i].yaw);
			sinZ = sin(this->imu[i].yaw);
			cosY = cos(this->imu[i].pitch);
			sinY = sin(this->imu[i].pitch);
			cosX = cos(this->imu[i].roll);
			sinX = sin(this->imu[i].roll);
			lat = this->imu[i].lat;
			lon = this->imu[i].lon;
			alt = this->imu[i].alt - this->imu[0].alt;
#endif
		}
		//scale = cos(imu[i].lat * DEG_TO_RAD);

		matrix(0,0) = cosZ*cosY;
		matrix(0,1) = cosZ*sinY*sinX - sinZ*cosX;
		matrix(0,2) = cosZ*sinY*cosX + sinZ*sinX;

		matrix(1,0) = sinZ*cosY;
		matrix(1,1) = sinZ*sinY*sinX + cosZ*cosX;
		matrix(1,2) = sinZ*sinY*cosX - cosZ*sinX;

		matrix(2,0) = -sinY;
		matrix(2,1) = cosY*sinX;
		matrix(2,2) = cosY*cosX;

		matrix(0,3) = scale * lon * EARTH_RADIUS * DEG_TO_RAD;
		matrix(1,3) = scale * EARTH_RADIUS * log( tan((90 + lat) * DEG_TO_RAD * 0.5));
		matrix(2,3) = alt;

		if(i==0){
			inv_mat = (matrix * this->calibration).inverse();
			last_inv = inv_mat;
		} else {
			last_inv = this->imu[i-1].pose.inverse();
		}

		this->imu[i].pose = inv_mat * matrix * this->calibration;
		this->imu[i].relative_pose = last_inv * this->imu[i].pose;
	}
}

template<typename PointT>
inline KittiRegistrator<PointT>::~KittiRegistrator() {
}

#endif
