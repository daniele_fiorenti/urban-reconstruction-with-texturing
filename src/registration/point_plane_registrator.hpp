/*
 * point_plane_registrator.hpp
 *
 *  Created on: May 12, 2015
 *      Author: george
 */

#ifndef  REGISTRATION_POINT_PLANE_REGISTRATOR_HPP
#define  REGISTRATION_POINT_PLANE_REGISTRATOR_HPP

#include "point_plane_registrator.h"

template<typename PointT>
inline double PointPlaneRegistrator<PointT>::performRegistration(cloud_ptr source,
		cloud_ptr target, cloud_ptr aligned, Eigen::Matrix4f& transform) {
	PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
	boost::shared_ptr<pcl::PointCloud<pcl::PointNormal> > points_src (new pcl::PointCloud<pcl::PointNormal>());
	boost::shared_ptr<pcl::PointCloud<pcl::PointNormal> > points_tgt (new pcl::PointCloud<pcl::PointNormal>());
	boost::shared_ptr<pcl::PointCloud<pcl::PointNormal> > reg_result = points_src;
	norm_est.setInputCloud (source);
	norm_est.compute (*points_src);
	norm_est.setInputCloud (target);
	norm_est.compute (*points_tgt);
	pcl::copyPointCloud (*source, *points_src);
	pcl::copyPointCloud (*target, *points_tgt);
	double fit;

	if(!this->viewer){
		estimator.setInputSource(points_src);
		estimator.setInputTarget(points_tgt);
		PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
		PROFILER_START_TIMER(TIMER_REGISTRATION_COMPUTATION);
		estimator.align(*reg_result);
		PROFILER_STOP_TIMER(TIMER_REGISTRATION_COMPUTATION);
		PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
		if(estimator.hasConverged()){
			transform = estimator.getFinalTransformation ();
			IPRINT("PointPlane Estimator", " -> Converged!!  Fitness Score: "
					+ boost::to_string(estimator.getFitnessScore()));
		}
		else{
			IPRINT("PointPlane Estimator", " -> Estimator FAILED!!!");
		}
		fit = estimator.getFitnessScore();
	}
	else{
		int iterations = estimator.getMaximumIterations() / REG_DEMO_SUBITERATIONS;
		int maxCorrespondence = estimator.getMaxCorrespondenceDistance();
		estimator.setMaximumIterations(REG_DEMO_SUBITERATIONS);
		// Run the same optimization in a loop and visualize the results
		Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev;
		boost::shared_ptr<pcl::PointCloud<pcl::PointNormal> > reg_result = points_src;

		estimator.setInputTarget(points_tgt);
		bool converged = false;

		for (int i = 0; i < iterations && !converged; ++i)
		{
			estimator.setInputSource (points_src);
			PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
			PROFILER_START_TIMER(TIMER_REGISTRATION_COMPUTATION);
			estimator.align (*reg_result);
			PROFILER_STOP_TIMER(TIMER_REGISTRATION_COMPUTATION);
			PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
			//accumulate transformation between each Iteration
			Ti = estimator.getFinalTransformation () * Ti;

			//check if it is converged... to skip some useless iterations...
			converged = fabs ((estimator.getLastIncrementalTransformation () - prev).sum ()) < estimator.getTransformationEpsilon ();

			prev = estimator.getLastIncrementalTransformation ();

			points_src = reg_result;

			// visualize current state
			this->viewer->template showIterationStep<pcl::PointNormal>(points_src, REG_DEMO_FRAME_DURATION);

			IPRINT("PointPlane Estimator", "Iteration " + boost::to_string(i+1) + ":\tFitness Score: "
								+ boost::to_string(estimator.getFitnessScore(1.0)));

			fit += estimator.getFitnessScore();
			this->iter_counter++;
		}
		//get back the old results
		estimator.setMaximumIterations(REG_DEMO_SUBITERATIONS * iterations);
		estimator.setMaxCorrespondenceDistance (maxCorrespondence);

		//Return the final transform
		transform = Ti;
	}
	pcl::copyPointCloud (*reg_result, *aligned);
	PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
	return fit;
}

#endif /*  */
