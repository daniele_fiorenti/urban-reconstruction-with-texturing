/*
 * linear_registrator.h
 *
 *  Created on: May 13, 2015
 *      Author: george
 */

#ifndef REGISTRATION_LINEAR_REGISTRATOR_H_
#define REGISTRATION_LINEAR_REGISTRATOR_H_

#include "kitti_registrator.hpp"

template<typename PointT>
class LinearRegistrator : public KittiRegistrator<PointT> {
public:
	enum Estimator{
		IterativeClosestPoint, GeneralizedIterativeClosestPoint, NormalDistributionTransform
	};

	void setMinTransfThreshold(double epsilon){
		if(epsilon > 0) { estimator->setTransformationEpsilon(epsilon); }
	}
	void setMaxIterations(unsigned int max_iters){
		if(max_iters > 0) { estimator->setMaximumIterations(max_iters); }
	}
	void setMaxCorrispondence(double corr){
		if(corr > 0) { estimator->setMaxCorrespondenceDistance (corr); }
	}
	void setEuclideanFitness(double fitness){
		if(fitness > 0) { estimator->setEuclideanFitnessEpsilon(fitness); }
	}

	LinearRegistrator(Estimator est): KittiRegistrator<PointT>(){
		_preinit(est);
		_init(estimator);
	}

	LinearRegistrator(Estimator est, boost::shared_ptr<CloudVisualizer> viewer): KittiRegistrator<PointT>(viewer){
		_preinit(est);
		_init(estimator);
	}

	LinearRegistrator(boost::shared_ptr<pcl::Registration<PointT, PointT> > estimator
			= boost::shared_ptr<pcl::Registration<PointT, PointT> >(new pcl::IterativeClosestPoint<PointT, PointT>()))
	: KittiRegistrator<PointT>() {
		is_gicp = false;
		_init(estimator);
	}

	LinearRegistrator(boost::shared_ptr<CloudVisualizer> viewer,
			boost::shared_ptr<pcl::Registration<PointT, PointT> > estimator
			= boost::shared_ptr<pcl::Registration<PointT, PointT> >(new pcl::IterativeClosestPoint<PointT, PointT>()))
	: KittiRegistrator<PointT>(viewer) {
		is_gicp = false;
		_init(estimator);
	}

	LinearRegistrator(boost::shared_ptr<pcl::GeneralizedIterativeClosestPoint<PointT, PointT> > estimator)
	: KittiRegistrator<PointT>() {
		is_gicp = true;
		_init(estimator);
	}

	LinearRegistrator(boost::shared_ptr<CloudVisualizer> viewer,
			boost::shared_ptr<pcl::GeneralizedIterativeClosestPoint<PointT, PointT> > estimator)
	: KittiRegistrator<PointT>(viewer) {
		is_gicp = true;
		_init(estimator);
	}
protected:
	typedef typename boost::shared_ptr<pcl::PointCloud<PointT> > cloud_ptr;
	double performRegistration(cloud_ptr source, cloud_ptr target, cloud_ptr aligned, Eigen::Matrix4f &transform);

private:
	boost::shared_ptr<pcl::Registration<PointT, PointT> > estimator;
	bool is_gicp;

	void _preinit(Estimator est){
		is_gicp = false;
		switch(est){
		case GeneralizedIterativeClosestPoint:{
			estimator = boost::shared_ptr<pcl::Registration<PointT, PointT> >
			(new pcl::GeneralizedIterativeClosestPoint<PointT, PointT>());
			is_gicp = true;
			break;
		}
		case NormalDistributionTransform: {
			boost::shared_ptr<pcl::NormalDistributionsTransform<PointT, PointT> > ndt =
					boost::shared_ptr<pcl::NormalDistributionsTransform<PointT, PointT> >
			(new pcl::NormalDistributionsTransform<PointT, PointT>());
			ndt->setStepSize(1.0);
			ndt->setResolution(0.05);
			estimator = ndt;
			break;
		}
		default:{
			estimator = boost::shared_ptr<pcl::Registration<PointT, PointT> >
			(new pcl::IterativeClosestPoint<PointT, PointT>());
			break;
		}
		}
	}

	void _init(boost::shared_ptr<pcl::Registration<PointT, PointT> > est){
		estimator = est;
		estimator->setMaxCorrespondenceDistance (REG_MAX_CORRESPONDENCE);
		estimator->setMaximumIterations(REG_MAX_ITERATIONS);
		estimator->setTransformationEpsilon(REG_TRANSFORMATION_EPSILON);
		//estimator.setEuclideanFitnessEpsilon(1);
	}
};

#endif /* REGISTRATION_LINEAR_REGISTRATOR_H_ */
