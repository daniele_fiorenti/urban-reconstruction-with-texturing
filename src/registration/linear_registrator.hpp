/*
 * linear_registrator.hpp
 *
 *  Created on: May 13, 2015
 *      Author: george
 */

#ifndef REGISTRATION_LINEAR_REGISTRATOR_HPP
#define REGISTRATION_LINEAR_REGISTRATOR_HPP

#include "linear_registrator.h"

/*
 * Sono i tre tipi di registrazione
 */

template<typename PointT>
inline double LinearRegistrator<PointT>::performRegistration(cloud_ptr source,
		cloud_ptr target, cloud_ptr aligned, Eigen::Matrix4f& transform) {

	PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
	cloud_ptr points_src = source;
	cloud_ptr points_tgt = target;
	cloud_ptr reg_result = points_src;
	double fit;

	if(!this->viewer){
		estimator->setInputSource(points_src);
		estimator->setInputTarget(points_tgt);
		PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
		PROFILER_START_TIMER(TIMER_REGISTRATION_COMPUTATION);
		estimator->align(*reg_result);
		PROFILER_STOP_TIMER(TIMER_REGISTRATION_COMPUTATION);
		PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
		if(estimator->hasConverged()){
			transform = estimator->getFinalTransformation ();
			if(is_gicp){
				pcl::transformPointCloud(*points_src, *reg_result, transform);
			}
			IPRINT("Linear Estimator", " -> Converged!!  Fitness Score: "
					+ boost::to_string(estimator->getFitnessScore()));
		}
		else{
			IPRINT("Linear Estimator", " -> Estimator FAILED!!!");
		}
		fit = estimator->getFitnessScore();
	}
	else{
		int iterations = estimator->getMaximumIterations() / REG_DEMO_SUBITERATIONS;
		int maxCorrespondence = estimator->getMaxCorrespondenceDistance();
		estimator->setMaximumIterations(REG_DEMO_SUBITERATIONS);
		// Run the same optimization in a loop and visualize the results
		Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev;
		cloud_ptr reg_result = points_src;

		estimator->setInputTarget(points_tgt);
		bool converged = false;

		for (int i = 0; i < iterations && !converged; ++i)
		{
			estimator->setInputSource (points_src);

			PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
			PROFILER_START_TIMER(TIMER_REGISTRATION_COMPUTATION);
			estimator->align (*reg_result);
			PROFILER_STOP_TIMER(TIMER_REGISTRATION_COMPUTATION);
			PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);

			//accumulate transformation between each Iteration
			Ti = estimator->getFinalTransformation () * Ti;

			//check if it is converged... to skip some useless iterations...
			converged = fabs ((estimator->getLastIncrementalTransformation () - prev).sum ()) < estimator->getTransformationEpsilon ();

			prev = estimator->getLastIncrementalTransformation ();

			if(is_gicp){
				pcl::transformPointCloud(*points_src, *reg_result, estimator->getFinalTransformation ());
			}
			points_src = reg_result;

			// visualize current state
			this->viewer->showIterationStep(points_src, REG_DEMO_FRAME_DURATION);

			IPRINT("Linear Estimator", "Iteration " + boost::to_string(i+1) + ":\tFitness Score: "
					+ boost::to_string(estimator->getFitnessScore(1.0)));

			fit += estimator->getFitnessScore();
			this->iter_counter++;
		}
		//get back the old results
		estimator->setMaximumIterations(REG_DEMO_SUBITERATIONS * iterations);
		estimator->setMaxCorrespondenceDistance (maxCorrespondence);

		//Return the final transform
		transform = Ti;
	}
	pcl::copyPointCloud (*reg_result, *aligned);

	PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
	return fit;
}

#endif /*  */
