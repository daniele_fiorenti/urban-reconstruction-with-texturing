/*
 * imu_registrator.hpp
 *
 *  Created on: May 13, 2015
 *      Author: george
 */

#ifndef REGISTRATION_IMU_REGISTRATOR_HPP
#define REGISTRATION_IMU_REGISTRATOR_HPP

#include "imu_registrator.h"

/*
 * Applica la pose ma non fa la registration
 */

/*static inline double point_to_origin_distance(double x, double y, double z){
	return std::sqrt(x*x + y*y + z*z);
}
*/
template<typename PointT>
inline void IMURegistrator<PointT>::addPointCloud(cloud_ptr points, int index) {
	PROFILER_START_TIMER(TIMER_REGISTRATION_PREPARATION);
	pcl::transformPointCloud(*(this->input_filter.reapplyFilters(points)), *points, this->imu[index].pose);
	*(this->cloud) += *(points);
	PROFILER_STOP_TIMER(TIMER_REGISTRATION_PREPARATION);
	this->overall_distance += point_to_origin_distance(this->imu[index].pose(0,3),this->imu[index].pose(1,3),this->imu[index].pose(2,3));
}

#endif /*  */
