/*
 * imu_registrator.h
 *
 *  Created on: May 13, 2015
 *      Author: george
 */

#ifndef REGISTRATION_IMU_REGISTRATOR_H_
#define REGISTRATION_IMU_REGISTRATOR_H_

#include "kitti_registrator.h"

template<typename PointT>
class IMURegistrator: public KittiRegistrator<PointT> {
public:
	typedef typename boost::shared_ptr<pcl::PointCloud<PointT> > cloud_ptr;
	IMURegistrator(): KittiRegistrator<PointT>() { }

	IMURegistrator(boost::shared_ptr<CloudVisualizer> viewer)
		: KittiRegistrator<PointT>(viewer) { }

	void addPointCloud(cloud_ptr points, int index);

	protected:
		void setupEstimator(){ }
};

#endif /* REGISTRATION_ICP_REGISTRATOR_H_ */
