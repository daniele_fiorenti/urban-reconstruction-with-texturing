/*
 * FloorRemoval.h
 *
 *  Created on: Jun 30, 2015
 *      Author: george
 */

#ifndef FILTERS_FLOORREMOVAL_H_
#define FILTERS_FLOORREMOVAL_H_

#include <pcl/filters/filter.h>
#include <unordered_map>

#include "../pipeline/Cloud.h"

namespace filtering {

using namespace pipeline;

class FloorRemoval: public pcl::Filter<Cloud::pointType> {
public:
	FloorRemoval(float cell_size_x, float cell_size_y, int min_points = 1, float floor_height = -1.0f);

	void setCellSize(float cell_size_x, float cell_size_y);
	void setMaxSlopeDifference(float max_slope_in_percentage);
	void setMinCellPoints(int min_points);
	void setFloorHeight(float floor_height);

	void filter(Filter<Cloud::pointType>::PointCloud &output_cloud);
	void filter(Filter<Cloud::pointType>::PointCloud &output_no_floor, Filter<Cloud::pointType>::PointCloud &the_floor);
	void filterFast(Filter<Cloud::pointType>::PointCloud &output_no_floor, Filter<Cloud::pointType>::PointCloud &the_floor);

protected:
	void applyFilter(Filter<Cloud::pointType>::PointCloud &output_cloud);

private:
	struct Cell{
		float min_height = std::numeric_limits<float>::max();
		float max_height = -std::numeric_limits<float>::max();
		float floor_height;
		bool is_floor = false;
		std::vector<int> indices;
	};

	std::unordered_map<int, Cell> _grid;
	float _inverse_size_x;
	float _inverse_size_y;
	float _min_points;
	float _max_slope;
	float _floor_height;
};

} /* namespace filtering */

#endif /* FILTERS_FLOORREMOVAL_H_ */
