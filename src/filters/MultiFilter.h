/*
 * MultiFilter.h
 *
 *  Created on: Jun 16, 2015
 *      Author: george
 */

#ifndef FILTERS_MULTIFILTER_H_
#define FILTERS_MULTIFILTER_H_

#include <pcl/filters/filter.h>
#include "../pipeline/data_types.h"
#include "../pipeline/Cloud.h"

using namespace pipeline;

class MultiFilter: public pcl::Filter<Cloud::pointType> {
public:
	typedef std::shared_ptr<pcl::Filter<Cloud::pointType> > filter_shared_ptr;

	MultiFilter();
	virtual ~MultiFilter();

    void addVoxelGrid(double leaf_size_x, double leaf_size_y, double leaf_size_z,
                      int min_voxel_points = -1, bool downsample_all_data = false);
    void addApproximateVoxelGrid(double leaf_size_x, double leaf_size_y, double leaf_size_z,
                                 bool downsample_all_data = true);
	void addPassThrough(std::string const & field_name, double min, double max);
    void addFrustumCulling(double hFOV_deg, double vFOV_deg, double np_dist, double fp_dist,
                           Eigen::Matrix4f camera_pose = Eigen::Matrix4f::Identity());
	void addGridMinimum(double resolution);
	void addLocalMaximum(double radius);
	void addRadiusOutlier(double radius, int min_points = -1);
	void addStatisticalOutlier(int meanK, double std_dev_multiplier);
    void addCropBox(double box_x, double box_y, double box_z,
                    double t_x = 0.0, double t_y = 0.0, double t_z = 0.0,
                    double r_x = 0.0, double r_y = 0.0, double r_z = 0.0);
	void addCropBox(double box_x, double box_y, double box_z, const Eigen::Matrix4f& transform);
	void addFloorRemoval(double cell_size_x, double cell_size_y, int min_points = 1, double floor_height = -1);
	void addFilter(filter_shared_ptr const & filter);
	void addFilter(Filter<Cloud::pointType> * filter);

	void filter(Filter<Cloud::pointType>::PointCloud &output_cloud);
	void filter(Cloud::pclCloud::Ptr const & in_cloud, Cloud::pclCloud::Ptr const & out_cloud);
	void filterFast(Cloud::pclCloud::Ptr & in_cloud, Cloud::pclCloud::Ptr & out_cloud);

	int size();
	bool empty();
	void clear();

protected:
	void applyFilter(Filter<Cloud::pointType>::PointCloud &output_cloud);

private:
	Filter<Cloud::pointType>::PointCloud::Ptr _intermediate_cloud;
	Filter<Cloud::pointType>::PointCloud::Ptr _output_cloud;
	std::vector<filter_shared_ptr> _filters;
};

#endif /* FILTERS_MULTIFILTER_H_ */
