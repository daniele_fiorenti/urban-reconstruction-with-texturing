/*
 * chain_filter.h
 *
 *  Created on: Apr 28, 2015
 *      Author: george
 */

#ifndef FILTERS_CHAIN_FILTER_H_
#define FILTERS_CHAIN_FILTER_H_

#include <pcl/common/common_headers.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/grid_minimum.h>
#include <pcl/filters/local_maximum.h>
#include <pcl/filters/bilateral.h>
#include <pcl/filters/crop_box.h>


template <typename PointT>
class ChainFilter : public pcl::Filter<PointT>{
public:
	typedef typename boost::shared_ptr<pcl::PointCloud<PointT> > cloud_ptr;
	typedef typename boost::shared_ptr<const pcl::PointCloud<PointT> > const_cloud_ptr;
	ChainFilter();
	ChainFilter(const_cloud_ptr inputCloud);
	ChainFilter(const_cloud_ptr inputCloud, cloud_ptr outputCloud);
	ChainFilter& addVoxelGrid(double leaf_size_x, double leaf_size_y, double leaf_size_z,
							int min_voxel_points = -1, bool downsample_all_data = true);
	ChainFilter& addApproximateVoxelGrid(double leaf_size_x, double leaf_size_y, double leaf_size_z,
							bool downsample_all_data = true);
	ChainFilter& addPassThrough(std::string filter_name, double min, double max);
	//ChainFilter applyConditionalRemoval();
	ChainFilter& addFrustumCulling(double hFOV_deg, double vFOV_deg, double np_dist, double fp_dist,
									 Eigen::Matrix4f camera_pose = Eigen::Matrix4f::Identity());
	ChainFilter& addGridMinimum(double resolution);
	ChainFilter& addLocalMaximum(double radius);
	ChainFilter& addRadiusOutlier(double radius, int min_points = -1);
	ChainFilter& addStatisticalOutlier(int meanK, double std_dev_multiplier);
	ChainFilter& addCropBox(double box_x, double box_y, double box_z,
			double t_x = 0.0, double t_y = 0.0, double t_z = 0.0,
			double r_x = 0.0, double r_y = 0.0, double r_z = 0.0);
	ChainFilter& addCropBox(double box_x, double box_y, double box_z, const Eigen::Matrix4f& transform);

	ChainFilter& addFilter(pcl::Filter<PointT> &filter);

	ChainFilter& clearFilters();

	int size(){
		return filters.size();
	}

	void setInputCloud(const const_cloud_ptr cloud_in);
	typename pcl::PCLBase<PointT>::PointCloudPtr reapplyFilters(const_cloud_ptr inputCloud);
	typename pcl::PCLBase<PointT>::PointCloudPtr getOutput();

protected:
	void applyFilter(pcl::PointCloud<PointT> &output);

private:
	cloud_ptr in_cloud_ptr;
	cloud_ptr out_cloud_ptr;
	int filtering_index;

	std::vector<boost::shared_ptr<pcl::Filter<PointT> > > filters;
};

template<typename PointT>
ChainFilter<PointT>::ChainFilter() {
	in_cloud_ptr = cloud_ptr(new pcl::PointCloud<PointT>());
	out_cloud_ptr = cloud_ptr(new pcl::PointCloud<PointT>());
	filtering_index = 0;
}

template<typename PointT>
ChainFilter<PointT>::ChainFilter(const_cloud_ptr inputCloud) {
	in_cloud_ptr = cloud_ptr(new pcl::PointCloud<PointT>());
	*in_cloud_ptr = *inputCloud;
	out_cloud_ptr = cloud_ptr(new pcl::PointCloud<PointT>());
	filtering_index = 0;
}

template<typename PointT>
ChainFilter<PointT>::ChainFilter(const_cloud_ptr inputCloud, cloud_ptr outputCloud) {
	in_cloud_ptr = cloud_ptr(new pcl::PointCloud<PointT>());
	*in_cloud_ptr = *inputCloud;
	out_cloud_ptr = outputCloud;
	filtering_index = 0;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addVoxelGrid(double leaf_size_x,
		double leaf_size_y, double leaf_size_z, int min_voxel_points, bool downsample_all_data) {

	boost::shared_ptr<pcl::VoxelGrid<PointT> > filt(new pcl::VoxelGrid<PointT>());
	filt->setLeafSize (leaf_size_x, leaf_size_y, leaf_size_z);
	filt->setDownsampleAllData(downsample_all_data);
	if(min_voxel_points > 0){ filt->setMinimumPointsNumberPerVoxel(min_voxel_points);}

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addApproximateVoxelGrid(
		double leaf_size_x, double leaf_size_y, double leaf_size_z, bool downsample_all_data) {

	boost::shared_ptr<pcl::ApproximateVoxelGrid<PointT> > filt(new pcl::ApproximateVoxelGrid<PointT>());
	filt->setLeafSize (leaf_size_x, leaf_size_y, leaf_size_z);
	filt->setDownsampleAllData(downsample_all_data);

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addPassThrough(std::string filter_name, double min, double max) {

	if(!filter_name.empty()){
		boost::shared_ptr<pcl::PassThrough<PointT> > filt(new pcl::PassThrough<PointT>());
		filt->setFilterFieldName(filter_name);
		filt->setFilterLimits(min, max);

		filters.push_back(filt);
	}
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addFrustumCulling(double hFOV_deg,
		double vFOV_deg, double np_dist, double fp_dist, Eigen::Matrix4f camera_pose) {

	boost::shared_ptr<pcl::FrustumCulling<PointT> > filt(new pcl::FrustumCulling<PointT>());
	filt->setHorizontalFOV(hFOV_deg);
	filt->setVerticalFOV(vFOV_deg);
	filt->setNearPlaneDistance(np_dist);
	filt->setFarPlaneDistance(fp_dist);
	filt->setCameraPose(camera_pose);

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addGridMinimum(double resolution) {

	boost::shared_ptr<pcl::GridMinimum<PointT> > filt(new pcl::GridMinimum<PointT>(resolution));

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addLocalMaximum(double radius) {

	boost::shared_ptr<pcl::LocalMaximum<PointT> > filt(new pcl::LocalMaximum<PointT>());
	filt->setRadius(radius);

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addRadiusOutlier(double radius,
		int min_points) {

	boost::shared_ptr<pcl::RadiusOutlierRemoval<PointT> > filt(new pcl::RadiusOutlierRemoval<PointT>());
	filt->setRadiusSearch(radius);
	if(min_points > 0){ filt->setMinNeighborsInRadius(min_points); }

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addStatisticalOutlier(int meanK,
		double std_dev_multiplier) {

	boost::shared_ptr<pcl::StatisticalOutlierRemoval<PointT> > filt(new pcl::StatisticalOutlierRemoval<PointT>());
	filt->setMeanK(meanK);
	filt->setStddevMulThresh(std_dev_multiplier);

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addCropBox(double box_x,
		double box_y, double box_z, double t_x, double t_y, double t_z,
		double r_x, double r_y, double r_z) {

	Eigen::Vector4f min = Eigen::Vector4f::Identity();
	Eigen::Vector4f max = Eigen::Vector4f::Identity();
	Eigen::Vector3f rotation;

	min[0] = -box_x/2.0 + t_x;	max[0] = box_x/2.0 + t_x;
	min[1] = -box_y/2.0 + t_y;	max[1] = box_y/2.0 + t_y;
	min[2] = -box_z/2.0 + t_z;	max[2] = box_z/2.0 + t_z;
	rotation[0] = r_x;
	rotation[1] = r_y;
	rotation[2] = r_z;

	boost::shared_ptr<pcl::CropBox<PointT> > filt(new pcl::CropBox<PointT>());
	filt->setMin(min);
	filt->setMax(max);
	filt->setRotation(rotation);

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addCropBox(double box_x,
		double box_y, double box_z, const Eigen::Matrix4f& transform) {
	Eigen::Vector4f min = Eigen::Vector4f::Identity();
	Eigen::Vector4f max = Eigen::Vector4f::Identity();

	min[0] = -box_x/2.0;	max[0] = box_x/2.0;
	min[1] = -box_y/2.0;	max[1] = box_y/2.0;
	min[2] = -box_z/2.0;	max[2] = box_z/2.0;

	boost::shared_ptr<pcl::CropBox<PointT> > filt(new pcl::CropBox<PointT>());
	filt->setMin(min);
	filt->setMax(max);
	Eigen::Affine3f mat(transform);
	filt->setTransform(mat);

	filters.push_back(filt);
	return *this;
}

template<typename PointT>
typename pcl::PCLBase<PointT>::PointCloudPtr ChainFilter<PointT>::reapplyFilters(
		const_cloud_ptr inputCloud) {
	cloud_ptr in_cloud = cloud_ptr(new pcl::PointCloud<PointT>());
	cloud_ptr out_cloud = cloud_ptr(new pcl::PointCloud<PointT>());

	*in_cloud = *inputCloud;

	for(int i = 0; i < filters.size(); i++){
		filters[i]->setInputCloud(in_cloud);
		filters[i]->filter(*out_cloud);
		in_cloud.swap(out_cloud);
	}
	return out_cloud;
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::addFilter(
		pcl::Filter<PointT>& filter) {
	filters.push_back(boost::shared_ptr<pcl::Filter<PointT> >(filter));
}

template<typename PointT>
ChainFilter<PointT>& ChainFilter<PointT>::clearFilters() {
	filters.clear();
	filtering_index = 0;
}

template<typename PointT>
void ChainFilter<PointT>::setInputCloud(const const_cloud_ptr cloud_in) {
	*in_cloud_ptr = *cloud_in;
	filtering_index = 0;
}

template<typename PointT>
typename pcl::PCLBase<PointT>::PointCloudPtr ChainFilter<PointT>::getOutput() {
	if(!in_cloud_ptr){
		std::cerr <<  "[ChainFilter]" << ": Input cloud not set";
	}
	else if(filtering_index < filters.size()){
		for(int i = filtering_index; i < filters.size(); i++){
			filters[i]->setInputCloud(in_cloud_ptr);
			filters[i]->filter(*out_cloud_ptr);
			in_cloud_ptr.swap(out_cloud_ptr);
		}
		filtering_index = filters.size();
	}
	return out_cloud_ptr;
}

template<typename PointT>
void ChainFilter<PointT>::applyFilter(pcl::PointCloud<PointT>& output) {
	if(!in_cloud_ptr){
		std::cerr <<  "[ChainFilter]" << ": Input cloud not set";
		return;
	}

	cloud_ptr out_cloud = cloud_ptr(&output);

	for(int i = filtering_index; i < filters.size(); i++){
		filters[i]->setInputCloud(in_cloud_ptr);
		filters[i]->filter(*out_cloud);
		*in_cloud_ptr = *out_cloud;
	}
	filtering_index = filters.size();
	output = *out_cloud;
}

#endif /* FILTERS_CHAIN_FILTER_H_ */
