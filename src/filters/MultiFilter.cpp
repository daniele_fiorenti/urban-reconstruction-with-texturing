/*
 * MultiFilter.cpp
 *
 *  Created on: Jun 16, 2015
 *      Author: george
 */

#include "MultiFilter.h"

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/grid_minimum.h>
#include <pcl/filters/local_maximum.h>
#include <pcl/filters/bilateral.h>
#include <pcl/filters/crop_box.h>

#include "FloorRemoval.h"

using namespace pcl;

MultiFilter::MultiFilter(): _intermediate_cloud(new Filter<Cloud::pointType>::PointCloud()),
							_output_cloud(new Filter<Cloud::pointType>::PointCloud()){ }

MultiFilter::~MultiFilter() {
	clear();
}

void MultiFilter::addVoxelGrid(double leaf_size_x, double leaf_size_y,
		double leaf_size_z, int min_voxel_points, bool downsample_all_data) {
	VoxelGrid<Cloud::pointType>* filt = new VoxelGrid<Cloud::pointType>();
	filt->setLeafSize (leaf_size_x, leaf_size_y, leaf_size_z);
	filt->setDownsampleAllData(downsample_all_data);
	if(min_voxel_points > 0){ filt->setMinimumPointsNumberPerVoxel(min_voxel_points);}

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addApproximateVoxelGrid(double leaf_size_x,
		double leaf_size_y, double leaf_size_z, bool downsample_all_data) {
	ApproximateVoxelGrid<Cloud::pointType> * filt = new ApproximateVoxelGrid<Cloud::pointType>();
	filt->setLeafSize (leaf_size_x, leaf_size_y, leaf_size_z);
	filt->setDownsampleAllData(downsample_all_data);

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addPassThrough(std::string const & field_name, double min,
		double max) {
	if(!field_name.empty()){
		PassThrough<Cloud::pointType> * filt = new PassThrough<Cloud::pointType>();
		filt->setFilterFieldName(field_name);
		filt->setFilterLimits(min, max);

		_filters.push_back(filter_shared_ptr(filt));
	}
}

void MultiFilter::addFrustumCulling(double hFOV_deg, double vFOV_deg,
		double np_dist, double fp_dist, Eigen::Matrix4f camera_pose) {
	FrustumCulling<Cloud::pointType> * filt = new FrustumCulling<Cloud::pointType>();
	filt->setHorizontalFOV(hFOV_deg);
	filt->setVerticalFOV(vFOV_deg);
	filt->setNearPlaneDistance(np_dist);
	filt->setFarPlaneDistance(fp_dist);
	filt->setCameraPose(camera_pose);

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addGridMinimum(double resolution) {
	_filters.push_back(filter_shared_ptr(new GridMinimum<Cloud::pointType>(resolution)));
}

void MultiFilter::addLocalMaximum(double radius) {
	LocalMaximum<Cloud::pointType> * filt = new LocalMaximum<Cloud::pointType>();
	filt->setRadius(radius);

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addRadiusOutlier(double radius, int min_points) {
	RadiusOutlierRemoval<Cloud::pointType> * filt = new pcl::RadiusOutlierRemoval<Cloud::pointType>();
	filt->setRadiusSearch(radius);
	if(min_points > 0){ filt->setMinNeighborsInRadius(min_points); }

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addStatisticalOutlier(int meanK, double std_dev_multiplier) {
	StatisticalOutlierRemoval<Cloud::pointType> * filt = new StatisticalOutlierRemoval<Cloud::pointType>();
	filt->setMeanK(meanK);
	filt->setStddevMulThresh(std_dev_multiplier);

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addCropBox(double box_x, double box_y, double box_z,
		double t_x, double t_y, double t_z, double r_x, double r_y,
		double r_z) {
	Eigen::Vector4f min = Eigen::Vector4f::Identity();
	Eigen::Vector4f max = Eigen::Vector4f::Identity();
	Eigen::Vector3f rotation;

	min[0] = -box_x/2.0 + t_x;	max[0] = box_x/2.0 + t_x;
	min[1] = -box_y/2.0 + t_y;	max[1] = box_y/2.0 + t_y;
	min[2] = -box_z/2.0 + t_z;	max[2] = box_z/2.0 + t_z;
	rotation[0] = r_x;
	rotation[1] = r_y;
	rotation[2] = r_z;

	CropBox<Cloud::pointType> * filt = new CropBox<Cloud::pointType>();
	filt->setMin(min);
	filt->setMax(max);
	filt->setRotation(rotation);

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addCropBox(double box_x, double box_y, double box_z,
		const Eigen::Matrix4f& transform) {
	Eigen::Vector4f min = Eigen::Vector4f::Identity();
	Eigen::Vector4f max = Eigen::Vector4f::Identity();

	min[0] = -box_x/2.0;	max[0] = box_x/2.0;
	min[1] = -box_y/2.0;	max[1] = box_y/2.0;
	min[2] = -box_z/2.0;	max[2] = box_z/2.0;

	CropBox<Cloud::pointType> * filt = new CropBox<Cloud::pointType>();
	filt->setMin(min);
	filt->setMax(max);
	Eigen::Affine3f mat(transform);
	filt->setTransform(mat);

	_filters.push_back(filter_shared_ptr(filt));
}

void MultiFilter::addFilter(filter_shared_ptr const & filter) {
	_filters.push_back(filter);
}

void MultiFilter::filter(Filter<Cloud::pointType>::PointCloud& output_cloud) {
	if(_filters.empty()){
		return;
	}else if(_filters.size() == 1){
		_filters.front()->setInputCloud(input_);
		_filters.front()->filter(output_cloud);
		return;
	}
	_filters.front()->setInputCloud(input_);
	_filters.front()->filter(*_output_cloud);
	for(auto filter = _filters.begin() + 1; filter != _filters.end(); filter++){
		_intermediate_cloud.swap(_output_cloud);
		(*filter)->setInputCloud(_intermediate_cloud);
		(*filter)->filter(*_output_cloud);
	}
	output_cloud = *_output_cloud;
}

void MultiFilter::filter(Cloud::pclCloud::Ptr const & in_cloud,
		Cloud::pclCloud::Ptr const & out_cloud) {
	if(_filters.empty()){
		return;
	}
	_filters.front()->setInputCloud(in_cloud);
	_filters.front()->filter(*out_cloud);
	for(auto filter = _filters.begin() + 1; filter != _filters.end(); filter++){
		std::cout << "...................... secondary filters engaged......\n";
		_intermediate_cloud->swap(*out_cloud);
		(*filter)->setInputCloud(_intermediate_cloud);
		(*filter)->filter(*out_cloud);
	}
}

void MultiFilter::filterFast(Cloud::pclCloud::Ptr & in_cloud,
		Cloud::pclCloud::Ptr & out_cloud) {
	if(_filters.empty()){
		return;
	}
	_filters.front()->setInputCloud(in_cloud);
	_filters.front()->filter(*out_cloud);
	for(auto filter = _filters.begin() + 1; filter != _filters.end(); filter++){
		in_cloud.swap(out_cloud);
		(*filter)->setInputCloud(in_cloud);
		(*filter)->filter(*out_cloud);
	}
}

int MultiFilter::size() {
	return _filters.size();
}

void MultiFilter::clear() {
	_filters.clear();
}

void MultiFilter::addFilter(Filter<Cloud::pointType>* filter) {
	_filters.push_back(filter_shared_ptr(filter));
}

void MultiFilter::addFloorRemoval(double cell_size_x, double cell_size_y,
		int min_points, double floor_height) {
	_filters.push_back(filter_shared_ptr(new filtering::FloorRemoval(
			cell_size_x, cell_size_y,
			min_points, floor_height)));
}

bool MultiFilter::empty() {
	return _filters.empty();
}

void MultiFilter::applyFilter(
		Filter<Cloud::pointType>::PointCloud& output_cloud) {
	filter(output_cloud);
}
