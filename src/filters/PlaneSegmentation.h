/*
 * PlaneSegmentation.h
 *
 *  Created on: Aug 6, 2015
 *      Author: george
 */

#ifndef FILTERS_PLANESEGMENTATION_H_
#define FILTERS_PLANESEGMENTATION_H_

#include <pcl/filters/filter.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <unordered_map>

#include "../pipeline/Cloud.h"

namespace filtering {

using namespace pipeline;

class PlaneSegmentation: public pcl::Filter<Cloud::pointType> {
public:
	PlaneSegmentation(double distance_threshold);
	virtual ~PlaneSegmentation();

	void setDistanceThreshold(double distance_threshold);
	void setEpsilonAngle(double angle_rad);
	void setMainAxis(const Eigen::Vector3f &axis);

	void filter(Filter<Cloud::pointType>::PointCloud &output_cloud);
	void filter(Filter<Cloud::pointType>::PointCloud &output_no_planes, Filter<Cloud::pointType>::PointCloud &the_planes);
protected:
	void applyFilter(Filter<Cloud::pointType>::PointCloud &output_cloud);

private:
	pcl::SACSegmentation<Cloud::pointType> _segmentator;

	void filterPrivate(Filter<Cloud::pointType>::PointCloud &output_no_planes,
			Filter<Cloud::pointType>::PointCloud *the_planes);
};

} /* namespace filtering */

#endif /* FILTERS_PLANESEGMENTATION_H_ */
