/*
 * FloorRemoval.cpp
 *
 *  Created on: Jun 30, 2015
 *      Author: george
 */

#include "FloorRemoval.h"

namespace filtering {


FloorRemoval::FloorRemoval(float cell_size_x, float cell_size_y,
		int min_points, float floor_height)
	: _inverse_size_x(1.0f/cell_size_x), _inverse_size_y(1.0f/cell_size_y),
	  _max_slope(0.3f), _min_points(std::max(min_points, 1)),
	  _floor_height(floor_height){ }

void FloorRemoval::setCellSize(float cell_size_x, float cell_size_y) {
	_inverse_size_x = 1.0f/cell_size_x;
	_inverse_size_y = 1.0f/cell_size_y;
}

void FloorRemoval::setMaxSlopeDifference(float max_slope) {
	_max_slope = max_slope;
}

void FloorRemoval::setMinCellPoints(int min_points) {
	_min_points = std::max(min_points, 1);
}

void FloorRemoval::setFloorHeight(float floor_height) {
	_floor_height = floor_height;
}

void FloorRemoval::filter(
		Filter<Cloud::pointType>::PointCloud& output_cloud) {
	Filter<Cloud::pointType>::PointCloud the_floor;
	filter(output_cloud, the_floor);
}

void FloorRemoval::applyFilter(
		Filter<Cloud::pointType>::PointCloud& output_cloud) {
	filter(output_cloud);
}

/**
 * Filter function using HashMap
 */
void FloorRemoval::filterFast(
		Filter<Cloud::pointType>::PointCloud& output_no_floor,
		Filter<Cloud::pointType>::PointCloud& the_floor) {

	// Setup all the needed variables
	float min_max_th = _max_slope / std::min(_inverse_size_x, _inverse_size_y);
	int cell_id_x, cell_id_y, cell_id;

	Filter<Cloud::pointType>::PointCloud::ConstPtr input;
	bool const input_copy = input_.get() == &output_no_floor;
	// Check if output and input coincide
	if(input_copy){
		// If yes than make a copy and work on it..
		input = Filter<Cloud::pointType>::PointCloud::ConstPtr
				(new Filter<Cloud::pointType>::PointCloud(*input_));
	}else{
		// otherwise just copy the address
		input = input_;
	}

	// Clear everything
	_grid.clear();
	output_no_floor.clear();
	the_floor.clear();

	// First Pass:
	// Find the min_max of the cloud
	Eigen::Vector4f min_p, max_p;
	pcl::getMinMax3D(*input, min_p, max_p);
	int cell_id_y_multiplier = static_cast<int>((max_p[0] - min_p[0])*_inverse_size_x);
	float grid_min_x = min_p[0];
	float grid_min_y = min_p[1];

	// Second Pass:
	// Divide the cloud in 2D grid... and populate each cell
	int point_i = 0;
	for(auto& point: input->points){
		// Calculate the cell id
		cell_id_x = static_cast<int>(floor(point.x*_inverse_size_x - grid_min_x));
		cell_id_y = static_cast<int>(floor(point.y*_inverse_size_y - grid_min_y));
		cell_id = cell_id_x + cell_id_y * cell_id_y_multiplier;

		//std::cout << "FF >> cell_id_x = " << cell_id_x << ", cell_id_y = " << cell_id_y
		//		  << ", cell_id = " << cell_id << "\n";

		// Access the cell through its id
		auto& cell = _grid[cell_id];

		// Check for min_max
		if(point.z < cell.min_height){
			cell.min_height = point.z;
		}
		if(point.z > cell.max_height){
			cell.max_height = point.z;
		}

		//std::cout << "FF >> point_" << point_i << ".z = " << point.z << "\n";
		// Add point index to cell's indices
		cell.indices.push_back(point_i);
		++point_i;
	}

	// Third Pass:
	// Look for floor points, put them in the_floor cloud
	// The remaining points move to output cloud
	for(auto& grid_pair: _grid){
		auto& cell = grid_pair.second;
		// If there are no points in cell, skip it
		if(cell.indices.empty()){ continue; }

		//std::cout << "FF >> cell: points = " << cell.indices.size()
			//	  << " , max_height = " << cell.max_height << ", min_height = "
				//  << cell.min_height << "\n";

		// If the cell has it's min_max greater than th
		// Or if it has less points than needed copy to output cloud
		if(cell.max_height - cell.min_height > min_max_th ||
				cell.indices.size() < _min_points || cell.max_height > _floor_height){
			for(auto& index_: cell.indices){
				output_no_floor.push_back(input->points[index_]);
			}
		}

		// Now we're pretty sure the cell is a floor
		// so copy it to the_floor cloud
		else{
			for(auto& index_: cell.indices){
				the_floor.push_back(input->points[index_]);
			}
		}
	}

	// Done.
}

/**
 * Filter function using Array
 */
void FloorRemoval::filter(
		Filter<Cloud::pointType>::PointCloud& output_no_floor,
		Filter<Cloud::pointType>::PointCloud& the_floor) {

	// Setup all the needed variables
	float min_max_th = _max_slope / std::min(_inverse_size_x, _inverse_size_y);
	int cell_id_x, cell_id_y;

	Filter<Cloud::pointType>::PointCloud::ConstPtr input;
	bool const input_copy = input_.get() == &output_no_floor;
	// Check if output and input coincide
	if(input_copy){
		// If yes than make a copy and work on it..
		input = Filter<Cloud::pointType>::PointCloud::ConstPtr
				(new Filter<Cloud::pointType>::PointCloud(*input_));
	}else{
		// otherwise just copy the address
		input = input_;
	}

	// Clear everything
	output_no_floor.clear();
	the_floor.clear();

	// First Pass:
	// Find the min_max of the cloud
	Eigen::Vector4f min_p, max_p;
	pcl::getMinMax3D(*input, min_p, max_p);
	int cell_id_y_multiplier = static_cast<int>((max_p[0] - min_p[0])*_inverse_size_x);
	float grid_min_x = min_p[0];
	float grid_min_y = min_p[1];

	int x_cells = static_cast<int>((max_p[0] - min_p[0])*_inverse_size_x) + 2;
	int y_cells = static_cast<int>((max_p[1] - min_p[1])*_inverse_size_y) + 2;

	// Create the grid
	Cell grid[x_cells][y_cells];

	// And precompute the floor height for every cell
	int center_x = x_cells/2;
	int center_y = y_cells/2;
	float size_x = 1.0/_inverse_size_x;
	float size_y = 1.0/_inverse_size_y;
//	for(int x = 0; x < x_cells; x++){
//		// Get the distance from center
//		float x_dist_sqr = static_cast<float>(center_x - x)*size_x;
//		// square distance along x axis
//		x_dist_sqr = x_dist_sqr*x_dist_sqr;
//		for(int y = 0; y < y_cells; y++){
//			// Get the distance from center along y axis
//			float y_dist = static_cast<float>(center_y - y)*size_y;
//			// Compute the minimum allowed floor height from center
//			grid[x][y].floor_height = _floor_height + _max_slope * sqrt(x_dist_sqr + y_dist*y_dist);
//		}
//	}

	// Setup the central cell floor height to the specified one
	grid[center_x][center_y].floor_height = _floor_height;

	// Second Pass:
	// Divide the cloud in 2D grid... and populate each cell
	int point_i = 0;
	for(auto& point: input->points){
		// Calculate the cell id
		cell_id_x = static_cast<int>(floor((point.x - grid_min_x)*_inverse_size_x)) + 1;
		cell_id_y = static_cast<int>(floor((point.y - grid_min_y)*_inverse_size_y)) + 1;

//		std::cout << "FF >> cell_id_x = " << cell_id_x << ", cell_id_y = " << cell_id_y << "\n";

		// Access the cell through its id
		auto& cell = grid[cell_id_x][cell_id_y];

		// Check for min_max
		if(point.z < cell.min_height){
			cell.min_height = point.z;
		}
		if(point.z > cell.max_height){
			cell.max_height = point.z;
		}

		//std::cout << "FF >> point_" << point_i << ".z = " << point.z << "\n";
		// Add point index to cell's indices
		cell.indices.push_back(point_i);
		++point_i;
	}

	// Precompute the neighborhood relative slopes
	float rel_slope_x  = size_x * _max_slope;
	float rel_slope_y  = size_y * _max_slope;
	float rel_slope_xy = sqrt(size_x*size_x + size_y*size_y)*_max_slope;

	// Third Pass:
	// Look for floor cells, and mark them as such
	for(int x = 1; x < x_cells - 1; x++){
		for(int y = 1; y < y_cells - 1; y++){
			// Get the cell from the grid
			auto& cell = grid[x][y];

			if(cell.indices.empty()){
				cell.max_height = -std::numeric_limits<float>::max();
			}

			// Mark the cell as floor cell is passed all tests
			cell.is_floor = !cell.indices.empty() &&
					(cell.max_height - cell.min_height <= rel_slope_xy) &&
							(cell.indices.size() >= _min_points);

			cell.floor_height = _floor_height;
		}
	}


//	// Fourth Pass:
//	// Search the neighborhood of each floor cell
//	// if the relative slope is less than the threshold
//	for(int x = 1; x < x_cells - 1; x++){
//		for(int y = 1; y < y_cells - 1; y++){
//			// Get the cell from the grid
//			auto& cell = grid[x][y];
//
//			// If the cell is not a floor then copy it to output cloud
//			if(!cell.is_floor){
//				for(auto& index_: cell.indices){
//					output_no_floor.push_back(input->points[index_]);
//				}
//				continue;
//			}
//
//			// Search for neighborhood cells for relative slope
//			cell.is_floor =
//					((grid[x-1][y].is_floor * fabs(grid[x-1][y].max_height - cell.max_height)) <= rel_slope_x)
//				&&  ((grid[x+1][y].is_floor * fabs(grid[x+1][y].max_height - cell.max_height)) <= rel_slope_x)
//				&&	((grid[x][y-1].is_floor * fabs(grid[x][y-1].max_height - cell.max_height)) <= rel_slope_y)
//				&&	((grid[x][y+1].is_floor * fabs(grid[x][y+1].max_height - cell.max_height)) <= rel_slope_y)
//				// Diagonal cells
//				&&	((grid[x-1][y-1].is_floor * fabs(grid[x-1][y-1].max_height - cell.max_height)) <= rel_slope_xy)
//				&&	((grid[x-1][y+1].is_floor * fabs(grid[x-1][y+1].max_height - cell.max_height)) <= rel_slope_xy)
//				&&	((grid[x+1][y-1].is_floor * fabs(grid[x+1][y-1].max_height - cell.max_height)) <= rel_slope_xy)
//				&&	((grid[x+1][y+1].is_floor * fabs(grid[x+1][y+1].max_height - cell.max_height)) <= rel_slope_xy);
//
//			// Now if the cell is floor then move its points to floor cloud
//			if(cell.is_floor){
//				for(auto& index_: cell.indices){
//					the_floor.push_back(input->points[index_]);
//				}
//			}else{
//				for(auto& index_: cell.indices){
//					output_no_floor.push_back(input->points[index_]);
//				}
//			}
//		}
//	}

	// Commodity lambdas...
	auto add_to_floor = [&](Cell &cell){
//		std::cout << "--------------- Floor cell: \n\t- is_floor = " << cell.is_floor
//				<< "\t[" << cell.min_height << ", " << cell.max_height << "] " << cell.floor_height
//				<< " -> " << cell.indices.size() << " p\n";
//		std::cin.get();
		for(auto& index_: cell.indices){
			the_floor.push_back(input->points[index_]);
		}
	};

	auto add_to_no_floor = [&](Cell &cell){
		for(auto& index_: cell.indices){
			output_no_floor.push_back(input->points[index_]);
		}
	};
	// Fourth Pass:
	// Search the neighborhood of each floor cell
	// if the relative slope is less than the threshold

	// Pass 4.a: propagate the floor heights,
	// empty and non-floor heights keep the height of neighbors
	// Pass 4.a.1: setup the main axis floor height
	// First X axis --> Horizontal
	for(int i = 1; i < x_cells/2 - 1; i++){
		int right = center_x + i;
		int left = center_x - i;
		// Start with cells on the right of the central one
		auto& cell_right = grid[right][center_y];
		if(cell_right.indices.empty()
				|| !cell_right.is_floor   // TODO: Maybe is_floor field is not needed
				|| (cell_right.max_height > grid[right-1][center_y].floor_height + rel_slope_x)){
			cell_right.is_floor = false;
			cell_right.floor_height = grid[right-1][center_y].floor_height;
			add_to_no_floor(cell_right);
		} else {
			cell_right.is_floor = true;
			cell_right.floor_height = cell_right.max_height;
			add_to_floor(cell_right);
		}
		// And continue with the cells on left
		auto& cell_left = grid[left][center_y];
		if(cell_left.indices.empty()
				|| !cell_left.is_floor
				|| (cell_left.max_height > grid[left+1][center_y].floor_height + rel_slope_x)){
			cell_left.is_floor = false;
			cell_left.floor_height = grid[left+1][center_y].floor_height;
			add_to_no_floor(cell_left);
		} else {
			cell_left.is_floor = true;
			cell_left.floor_height = cell_left.max_height;
			add_to_floor(cell_left);
		}
	}

	// Then X axis --> Vertical
	for(int i = 1; i < y_cells/2 - 1; i++){
		int up = center_y + i;
		int down = center_y - i;
		// Start with cells on the up of the central one
		auto& cell_up = grid[center_x][up];
		if(cell_up.indices.empty()
				|| !cell_up.is_floor
				|| (cell_up.max_height > grid[center_x][up-1].floor_height + rel_slope_y)){
			cell_up.is_floor = false;
			cell_up.floor_height = grid[center_x][up-1].floor_height;
			add_to_no_floor(cell_up);
		} else {
			cell_up.is_floor = true;
			cell_up.floor_height = cell_up.max_height;
			add_to_floor(cell_up);
		}
		// And continue with the cells on down
		auto& cell_down = grid[center_x][down];
		if(cell_down.indices.empty()
				|| !cell_down.is_floor
				|| (cell_down.max_height > grid[center_x][down+1].floor_height + rel_slope_y)){
			cell_down.is_floor = false;
			cell_down.floor_height = grid[center_x][down+1].floor_height;
			add_to_no_floor(cell_down);
		} else {
			cell_down.is_floor = true;
			cell_down.floor_height = cell_down.max_height;
			add_to_floor(cell_down);
		}
	}

	// Pass 4.b: Now going for the remaining cells
	int n_left = -1;
	int n_right = 1;
	int n_down = -1;
	int n_up = 1;
	// Quadrants:
	// q2 | q1
	// ---+---
	// q3 | q4
	for(int x = 1; x < x_cells/2 - 1; x++){
		for(int y = 1; y < y_cells/2 - 1; y++){
			int up 		= center_y + y;
			int down 	= center_y - y;
			int right 	= center_x + x;
			int left 	= center_x - x;
			auto& cell_q1 = grid	[right]	[up];
			auto& cell_q2 = grid	[left]	[up];
			auto& cell_q3 = grid	[left]	[down];
			auto& cell_q4 = grid	[right]	[down];

			float max_q1 = std::max({grid[right-1][up].floor_height,
							   	     grid[right][up-1].floor_height,
								     grid[right-1][up-1].floor_height});
			float max_q2 = std::max({grid[left+1][up].floor_height,
									 grid[left][up-1].floor_height,
									 grid[left+1][up-1].floor_height});
			float max_q3 = std::max({grid[left+1][down].floor_height,
									 grid[left][down+1].floor_height,
									 grid[left+1][down+1].floor_height});
			float max_q4 = std::max({grid[right-1][down].floor_height,
									 grid[right][down+1].floor_height,
									 grid[right-1][down+1].floor_height});

			// Compute Cell in quadrant 1
			if(cell_q1.indices.empty()
					|| !cell_q1.is_floor
					|| (cell_q1.max_height > max_q1 + min_max_th)){
				cell_q1.is_floor = false;
				cell_q1.floor_height = max_q1;
				add_to_no_floor(cell_q1);
			} else {
				cell_q1.is_floor = true;
				cell_q1.floor_height = cell_q1.max_height;
				add_to_floor(cell_q1);
			}

			// Compute Cell in quadrant 2
			if(cell_q2.indices.empty()
					|| !cell_q2.is_floor
					|| (cell_q2.max_height > max_q2 + min_max_th)){
				cell_q2.is_floor = false;
				cell_q2.floor_height = max_q2;
				add_to_no_floor(cell_q2);
			} else {
				cell_q2.is_floor = true;
				cell_q2.floor_height = cell_q2.max_height;
				add_to_floor(cell_q2);
			}

			// Compute Cell in quadrant 3
			if(cell_q3.indices.empty()
					|| !cell_q3.is_floor
					|| (cell_q3.max_height > max_q3 + min_max_th)){
				cell_q3.is_floor = false;
				cell_q3.floor_height = max_q3;
				add_to_no_floor(cell_q3);
			} else {
				cell_q3.is_floor = true;
				cell_q3.floor_height = cell_q3.max_height;
				add_to_floor(cell_q3);
			}

			// Compute Cell in quadrant 4
			if(cell_q4.indices.empty()
					|| !cell_q4.is_floor
					|| (cell_q4.max_height > max_q4 + min_max_th)){
				cell_q4.is_floor = false;
				cell_q4.floor_height = max_q4;
				add_to_no_floor(cell_q4);
			} else {
				cell_q4.is_floor = true;
				cell_q4.floor_height = cell_q4.max_height;
				add_to_floor(cell_q4);
			}

		}
	}
//	for(int x = 1; x < x_cells - 1; x++){
//		for(int y = 1; y < y_cells - 1; y++){
//			// Get the cell from the grid
//			auto& cell = grid[x][y];
//
//			// If the cell is not a floor then copy it to output cloud
//			if(!cell.is_floor){
//				for(auto& index_: cell.indices){
//					output_no_floor.push_back(input->points[index_]);
//				}
//				continue;
//			}
//
//			// Search for neighborhood cells for relative slope
//			cell.is_floor =
//					((grid[x-1][y].is_floor * fabs(grid[x-1][y].max_height - cell.max_height)) <= rel_slope_x)
//					&&  ((grid[x+1][y].is_floor * fabs(grid[x+1][y].max_height - cell.max_height)) <= rel_slope_x)
//					&&	((grid[x][y-1].is_floor * fabs(grid[x][y-1].max_height - cell.max_height)) <= rel_slope_y)
//					&&	((grid[x][y+1].is_floor * fabs(grid[x][y+1].max_height - cell.max_height)) <= rel_slope_y)
//					// Diagonal cells
//					&&	((grid[x-1][y-1].is_floor * fabs(grid[x-1][y-1].max_height - cell.max_height)) <= rel_slope_xy)
//					&&	((grid[x-1][y+1].is_floor * fabs(grid[x-1][y+1].max_height - cell.max_height)) <= rel_slope_xy)
//					&&	((grid[x+1][y-1].is_floor * fabs(grid[x+1][y-1].max_height - cell.max_height)) <= rel_slope_xy)
//					&&	((grid[x+1][y+1].is_floor * fabs(grid[x+1][y+1].max_height - cell.max_height)) <= rel_slope_xy);
//
//			// Now if the cell is floor then move its points to floor cloud
//			if(cell.is_floor){
//				add_to_floor(cell);
//			}else{
//				add_to_no_floor(cell);
//			}
//		}
//	}

	// Done.
}

} /* namespace filtering */
