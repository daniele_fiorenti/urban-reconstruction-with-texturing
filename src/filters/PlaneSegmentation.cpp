/*
 * PlaneSegmentation.cpp
 *
 *  Created on: Aug 6, 2015
 *      Author: george
 */

#include "PlaneSegmentation.h"

/*
 * Rimuove il pavimento in maniera naive
 */

namespace filtering {

PlaneSegmentation::PlaneSegmentation(double distance_threshold) {
	// Optional
	_segmentator.setOptimizeCoefficients(true);

	// Mandatory
	_segmentator.setModelType(pcl::SACMODEL_PLANE);
	_segmentator.setMethodType(pcl::SAC_RANSAC);
	_segmentator.setDistanceThreshold(fabs(distance_threshold));
}

PlaneSegmentation::~PlaneSegmentation() {
	// Nothing for now...
}

void PlaneSegmentation::filter(
		Filter<Cloud::pointType>::PointCloud& output_cloud) {
	filterPrivate(output_cloud, nullptr);
}

void PlaneSegmentation::filter(
		Filter<Cloud::pointType>::PointCloud& output_no_planes,
		Filter<Cloud::pointType>::PointCloud& the_planes) {
	filterPrivate(output_no_planes, &the_planes);
}

void PlaneSegmentation::setDistanceThreshold(double distance_threshold) {
	_segmentator.setDistanceThreshold(fabs(distance_threshold));
}

void PlaneSegmentation::setEpsilonAngle(double angle_rad) {
	_segmentator.setEpsAngle(angle_rad);
}

void PlaneSegmentation::setMainAxis(const Eigen::Vector3f& axis) {
	_segmentator.setAxis(axis);
}

void PlaneSegmentation::applyFilter(
		Filter<Cloud::pointType>::PointCloud& output_cloud) {
	filter(output_cloud);
}

void PlaneSegmentation::filterPrivate(
		Filter<Cloud::pointType>::PointCloud& output_no_planes,
		Filter<Cloud::pointType>::PointCloud* the_planes) {

	// Check if there is any need to copy the planes...
	bool copy_planes = the_planes != nullptr;

	// Check if it is the same input and output cloud...
	bool same_cloud = input_.get() == &output_no_planes;

	// And make a copy if they are the same...
	Filter<Cloud::pointType>::PointCloud::ConstPtr temp_cloud;
	if(same_cloud){
		temp_cloud = Filter<Cloud::pointType>::PointCloud::ConstPtr(new Filter<Cloud::pointType>::PointCloud(*input_));
	}else{
		temp_cloud = input_;
	}

	// Create the needed variables..
	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

	// Perform the segmentation
	_segmentator.setInputCloud (temp_cloud);
	_segmentator.segment (*inliers, *coefficients);

	// Sort the indices...
	std::sort(inliers->indices.begin(), inliers->indices.end());

	// Clear the output cloud... just in case..
	output_no_planes.clear();

	// For convenience just create a input cloud reference
	auto& input_cloud = *temp_cloud;

	std::cerr << "============ Temp_cloud.size() = " << input_cloud.size() << "\n";
	// Start index extraction loop
	if(copy_planes){
		// Temporary variables and shortcuts...
		auto& planes_cloud = *the_planes;
		int index_i = 0;
		int i;
		// Start the separation...
		for(i = 0; i < temp_cloud->size() && index_i < inliers->indices.size(); i++){
			auto& point_i = input_cloud[i];
			if(i == inliers->indices[index_i]){
				planes_cloud.push_back(point_i);
				index_i++;
			}else{
				output_no_planes.push_back(point_i);
			}
		}
		// Copy the rest...
		for(; i < temp_cloud->size(); i++){
			output_no_planes.push_back(input_cloud[i]);
		}
	}else{
		// Temporary variables and shortcuts...
		int index_i = 0;
		int i;
		// Start the separation...
		for(i = 0; i < temp_cloud->size() && index_i < inliers->indices.size(); i++){
			auto& point_i = input_cloud[i];
			if(i != inliers->indices[index_i]){
				output_no_planes.push_back(point_i);
			}else{
				index_i++;
			}
		}
		// Copy the rest...
		for(; i < temp_cloud->size(); i++){
			output_no_planes.push_back(input_cloud[i]);
		}
	}
	std::cerr << "============ Output_cloud.size() = " << output_no_planes.size() << "\n";
}

} /* namespace filtering */
