/*
#Point-Cloud Config File EXAMPLE
#Parameters: b_* boolean, d_* double, i_* integer, s_* string, M_* matrix(TO BE IMPLEMENTED)
#Available filters:
#	VoxelGrid: 					voxel_grid: d_leaf_x d_leaf_y d_leaf_z [i_min_points] [b_downsample_all_data]
#	Approximate VoxelGrid:		approx_voxel_grid: d_leaf_x d_leaf_y d_leaf_z
#	Bilateral					NOT_YET_IMPLEMENTED
#	Conditional Removal			cond_removal: TO BE IMPLEMENTED
#	Frustum Culling				frustum_cull: d_hFOV d_vFOV d_near_plane d_far_plane [M_camera_pose_matrix]
#	Grid Minimum				grid_min: d_resolution
#	Local Maximum				local_max: d_radius
#	CropBox						crop_box: d_box_x d_box_y d_box_z [d_t_x  d_t_y  d_t_z]
#	Median						NOT_YET_IMPLEMENTED
#	Pass Through				pass_through: s_filter_name d_min d_max
#	Radius Outlier Removal 		radius_outlier: d_radius [i_min_points]
#	Statistical Outlier Removal stat_outlier: i_kmean d_std_dev_multiplier
#
#	Blank lines means a section is completed
#
#	Configuration Details
#	config: <-- used to configure dataset/folder loading, should be the very first keyword
#	| registrator: [icp|gicp|ndt|point_plane] <-- the registrator, should be the first
#													  property of config
#	| [input_filter|output_filter|iteration_filter] <-- select filter to be configured
#		these are the filters to be used by registrators:
#				- input_filter: is applied to every newly read point cloud (from each file)
#				- output_filter: is applied when the registration has completed
#				- iteration_filter: is applied to registering point clouds, to speed up the registration
#		the order is not important
#
#	Loading details:
#	load:  <-- the load keyword, should be before any viewport keyword
#	load: show	<-- use show to visualize the loaded point cloud
#	load: demo  <-- shows registration steps for data sets loading
#			the two above can be mixed... (load: show demo or load: demo show)
#	| file: filepath.extension <-- load a point cloud from the specified file
#	| folder: folderpath/ <-- load all point clouds from files with .bin extension
#	| dataset: datasetfolder <-- load synchronized with odometry point clouds
#		-dataset folder should contain "oxts/data/", "oxts/timestamps.txt" and
#				"velodyne_points/data/" and "velodyne_points/timestamps.txt"
#		-you can also specify the load range for dataset and folder
#						(E.g. | dataset: ../dataset1 [12:15])
#						this will load all pointclouds from file 12 to (15-1)
#	| pcd: filepath.pcd <-- load a pointcloud saved in pcd format

config:
	| registrator: point_plane
	| input_filter:
		+ crop_box: 			24.0 60.0 8.0 0.0
		+ voxel_grid: 			0.1 0.1 0.1 2
	| iteration_filter:
		+ crop_box: 			40.0 60.0 3.0
		+ approx_voxel_grid: 	0.05 0.05 0.05
	| output_filter:
		+ voxel_grid: 			0.1 0.1 0.1 2

load: show demo
	#| folder: ../data/lidar/
	#| file: test.bin
	| dataset: ../data/dataset_0095 [178:300]
	#| pcd: output/good/pointplane_0104_1M.pcd

viewport: "Voxel Centered" : "VoxelGrid"	#Viewport Name and description
	+ crop_box: 20.0 30.0 6.0
	+ voxel_grid: 0.2 0.2 0.2 2
	+ radius_outlier: 0.8 4

viewport: "Car Extractor" : "attempt to extract cars"
	+ pass_through: intensity 0.0 0.15
	+ radius_outlier: 1.0 25

end
*/

#ifndef FILTERS_FILTER_PARSER_H
#define FILTERS_FILTER_PARSER_H

#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/obj_io.h>

#include "../utility/cloud_visualizer.hpp"
#include "../utility/info_printer.h"

using namespace std;

class FilterParser
{
public:
    FilterParser(string configFile) : filepath(configFile) { }
    template<typename PointT>
    void parseAndShow(boost::shared_ptr<CloudVisualizer> viewer = boost::shared_ptr<CloudVisualizer>());
private:
    string filepath;

    template<typename PointT>
    bool parse_filter(ifstream& input, int &line_counter,
			ChainFilter<PointT> &chain_filter, std::vector<string> &details);
};

#endif // CONFIGPARSER_H
