#ifndef FILTERS_FILTER_PARSER_HPP
#define FILTERS_FILTER_PARSER_HPP

#include "filter_parser.h"
#include "../utility/kitti_utility.hpp"
#include "../registration/kitti_registrator.hpp"
#include "../registration/point_plane_registrator.hpp"
#include "../registration/linear_registrator.hpp"
#include "../registration/imu_registrator.hpp"
#include "chain_filter.h"

/*
 * Filter_parser interpreta oggetti del tipo example.config
 * Utilizza i file dentro la cartella registration
 */

/*
#Point-Cloud Config File EXAMPLE
#Parameters: b_* boolean, d_* double, i_* integer, s_* string, M_* matrix(TO BE IMPLEMENTED)
#Available filters:
#	VoxelGrid: 					voxel_grid: d_leaf_x d_leaf_y d_leaf_z [i_min_points] [b_downsample_all_data]
#	Approximate VoxelGrid:		approx_voxel_grid: d_leaf_x d_leaf_y d_leaf_z
#	Bilateral					NOT_YET_IMPLEMENTED
#	Conditional Removal			cond_removal: TO BE IMPLEMENTED
#	Frustum Culling				frustum_cull: d_hFOV d_vFOV d_near_plane d_far_plane [M_camera_pose_matrix]
#	Grid Minimum				grid_min: d_resolution
#	Local Maximum				local_max: d_radius
#	CropBox						crop_box: d_box_x d_box_y d_box_z [d_t_x  d_t_y  d_t_z]
#	Median						NOT_YET_IMPLEMENTED
#	Pass Through				pass_through: s_filter_name d_min d_max
#	Radius Outlier Removal 		radius_outlier: d_radius [i_min_points]
#	Statistical Outlier Removal stat_outlier: i_kmean d_std_dev_multiplier
#
#	Blank lines means a section is completed
#
#	Configuration Details
#	config: <-- used to configure dataset/folder loading, should be the very first keyword
#	| registrator: [icp|gicp|ndt|point_plane] <-- the registrator, should be the first
#													  property of config
#	Usage: | registrator: icp [d_max_corrispondence] [i_max_iterations] [d_min_transformation]
#			to set a field to its default, set it to 0
#	| [input_filter|output_filter|iteration_filter] <-- select filter to be configured
#		these are the filters to be used by registrators:
#				- input_filter: is applied to every newly read point cloud (from each file)
#				- output_filter: is applied when the registration has completed
#				- iteration_filter: is applied to registering point clouds, to speed up the registration
#		the order is not important
#
#	Loading details:
#	load:  <-- the load keyword, should be before any viewport keyword
#	load: show	<-- use show to visualize the loaded point cloud
#	load: demo  <-- shows registration steps for data sets loading
#			the two above can be mixed... (load: show demo or load: demo show)
#	| file: filepath.extension <-- load a point cloud from the specified file
#	| folder: folderpath/ <-- load all point clouds from files with .bin extension
#	| dataset: datasetfolder <-- load synchronized with odometry point clouds
#		-dataset folder should contain "oxts/data/", "oxts/timestamps.txt" and
#				"velodyne_points/data/" and "velodyne_points/timestamps.txt"
#		-you can also specify the load range for dataset and folder
#						(E.g. | dataset: ../dataset1 [12:15])
#						this will load all pointclouds from file 12 to (15-1)
#	| pcd: filepath.pcd <-- load a pointcloud saved in pcd format

config:
	| registrator: point_plane
	| input_filter:
		+ crop_box: 			24.0 60.0 8.0 0.0
		+ voxel_grid: 			0.1 0.1 0.1 2
	| iteration_filter:
		+ crop_box: 			40.0 60.0 3.0
		+ approx_voxel_grid: 	0.05 0.05 0.05
	| output_filter:
		+ voxel_grid: 			0.1 0.1 0.1 2

load: show demo
	#| folder: ../data/lidar/
	#| file: test.bin
	| dataset: ../data/dataset_0095 [178:300]
	#| pcd: output/good/pointplane_0104_1M.pcd

viewport: "Voxel Centered" : "VoxelGrid"	#Viewport Name and description
	+ crop_box: 20.0 30.0 6.0
	+ voxel_grid: 0.2 0.2 0.2 2
	+ radius_outlier: 0.8 4

viewport: "Car Extractor" : "attempt to extract cars"
	+ pass_through: intensity 0.0 0.15
	+ radius_outlier: 1.0 25

end
*/


template<typename PointT>
void FilterParser::parseAndShow(boost::shared_ptr<CloudVisualizer> viewer) {
	PROFILER_INIT_TIMER(TIMER_PARSING, "Parsing file time");
	namespace fs = boost::filesystem;
	if(!fs::is_regular_file(filepath)){
		IERROR("Parser", filepath + "  is not a regular file");
		return;
	}
	ifstream input(filepath.c_str());

	if(!input.good() || input.eof()){
		IERROR("Parser", "Unable to parse file " + filepath);
		input.close();
		return;
	}

	//Check the visualizer integrity
	bool launch_viewer = false;
	if(!viewer){
		IWARN("Parser", "Instantiating new renderer...");
		viewer = boost::shared_ptr<CloudVisualizer>(new CloudVisualizer(1000, 1000, "Cloud Filtering From Parser"));
		launch_viewer = true;
	}

	PROFILER_START_TIMER(TIMER_PARSING);

	//Regular expressions
	boost::regex end_keyword("[ \\t]*end.*");
	boost::regex empty_line("[ \\t]*");
	boost::regex comment_only("[ \\t]*(#.*)");
	boost::regex load_keyword("[ \\t]*load:[ \\t]*(show)?[ \\t]*(demo)?[ \\t]*(#.*)?");
	boost::regex config_keyword("[ \\t]*config:[ \\t]*(#.*)?");
	boost::regex reg_line("[ \\t]*\\|[ \\t]*registrator[ \\t]*:[ \\t]*([0-9a-zA-Z\\.\\/_-~]+)(([ \\t]+[0-9]+(\\.[0-9]+)?)*)?[ \\t]*(#.*)?");
	boost::regex reg_filter_line("[ \\t]*\\|[ \\t]*(input_filter|output_filter|iteration_filter)[ \\t]*:[ \\t]*(#.*)?");
	boost::regex load_line("[ \\t]*\\|[ \\t]*(file|folder|dataset|pcd|ply)[ \\t]*:[ \\t]*([0-9a-zA-Z\\.\\/_-~ ]+)[ \\t]*(\\[[ \\t]*([0-9]+)[ \\t]*:[ \\t]*([0-9]+)[ \\t]*\\])?[ \\t]*(#.*)?");
	boost::regex vp_keyword("[ \\t]*viewport:[ \\t]*\\\"([0-9a-zA-Z\\.\\/_-~ ]+)\\\"([ \\t]*:[ \\t]*\\\"(.+)\\\")?[ \\t]*(#.*)?");

	//Matcher
	boost::cmatch groups;

	// Test only...
	//pcl::PointCloud<PointT> pcl_cloud;
	//nicp::Cloud n_cloud;
	//NICPConverter<PointT> converter;
	//converter.convert(pcl_cloud, n_cloud);

	//String holding the whole line read
	string line = "";
	//Vector to store all split strings
	vector<string> fields;
	vector<string> subfields;
	//Line counter to output some warnings
	int line_counter = 0;

	//Needed to flag the positive outcome of loading phase
	bool load_ok = false;

	//Needed to flag whenever to force visualization of the original cloud or not,
	//It will be forced to true if there were no view ports created
	bool force_show = true;

	//Set the chain filters for input, output and at every registration iteration
	ChainFilter<PointT> in_fil, out_fil, iter_fil;

	// Registration for data sets, default is IMU only
	boost::shared_ptr<KittiRegistrator<PointT> > reg = boost::shared_ptr<KittiRegistrator<PointT> >(new IMURegistrator<PointT>());

	//Define the point clouds
	boost::shared_ptr<pcl::PointCloud<PointT> > cloud_orig(new pcl::PointCloud<PointT>());
	//Start the parsing...
	IPRINT("Parser", "Start parsing...");
	while(input.good() && !input.eof()){
		line_counter++;
		//Scan the next line in the input file
		std::getline(input, line);

		//Ignore blank lines...
		if(boost::regex_match(line, empty_line)){ continue; }

		//Search for lines containing comments only...
		else if(boost::regex_match(line, comment_only)){ continue; }

		//The first effective keyword should always be "config:", ignore everything else
		else if(!load_ok && boost::regex_match(line.c_str(), groups, config_keyword)){
			bool reg_ok = false;
			while(input.good() && !input.eof()){
				line_counter++;
				std::getline(input, line);

				//Blank line means the section is completed
				if(boost::regex_match(line, empty_line)){
					break;
				}
				//Search for lines containing comments only...
				else if(boost::regex_match(line, comment_only)){ continue; }

				//Check for registration methods
				else if(boost::regex_match(line.c_str(), groups, reg_line)){
					reg_ok = true;
					if(groups[1] == "icp"){
						reg = boost::shared_ptr<KittiRegistrator<PointT> >(new LinearRegistrator<PointT>());
					}
					else if(groups[1] == "gicp"){
						reg = boost::shared_ptr<KittiRegistrator<PointT> >
						(new LinearRegistrator<PointT>(LinearRegistrator<PointT>::GeneralizedIterativeClosestPoint));
					}
					else if(groups[1] == "ndt"){
						reg = boost::shared_ptr<KittiRegistrator<PointT> >
						(new LinearRegistrator<PointT>(LinearRegistrator<PointT>::NormalDistributionTransform));
					}
					else if(groups[1] == "point_plane"){
						reg = boost::shared_ptr<KittiRegistrator<PointT> >(new PointPlaneRegistrator<PointT>());
					}
					else if(groups[1] != "imu"){
						reg_ok = false;
					}
					//Now parse the coefficients
					if(reg_ok && groups.size() > 3){
						std::vector<std::string> fields;
						//Trim the fields
						string f_fields = groups[2];
						boost::trim(f_fields);
						//Split the fields
						boost::split(fields, f_fields, boost::is_any_of(" "), boost::token_compress_on);
						// Now the criteria order: MaxCorrispondence, MaxIterations, MinTransfEpsilon, EuclideanFitness
						int progress = 0;
						if(progress < fields.size()){
							reg->setMaxCorrispondence(atof(fields[progress].c_str()));
						}
						if(++progress < fields.size()){
							reg->setMaxIterations(atof(fields[progress].c_str()));
						}
						if(++progress < fields.size()){
							reg->setMinTransfThreshold(atof(fields[progress].c_str()));
						}
						if(++progress < fields.size()){
							reg->setEuclideanFitness(atof(fields[progress].c_str()));
						}
					}
				}
				else if(reg_ok && boost::regex_match(line.c_str(), groups, reg_filter_line)){
					vector<string> details;
					if(groups[1] == "input_filter"){
						if(parse_filter<PointT>(input, line_counter, in_fil, details)){
							reg->setInputFilter(in_fil);
						}
						else{
							IERROR("Parser", "Failed to parse filter block at: " + boost::to_string(line_counter));
						}
					}
					else if(groups[1] == "output_filter"){
						if(parse_filter<PointT>(input, line_counter, out_fil, details)){
							reg->setOutputFilter(out_fil);
						}
						else{
							IERROR("Parser", "Failed to parse filter block at: " + boost::to_string(line_counter));
						}
					}
					else if(groups[1] == "iteration_filter"){	//It should be iteration filter
						if(parse_filter<PointT>(input, line_counter, iter_fil, details)){
							reg->setRegistrationFilter(iter_fil);
						}
						else{
							IERROR("Parser", "Failed to parse filter block at: " + boost::to_string(line_counter));
						}
					}
				}
				else{
					IERROR("Parser", "Corrupt config line at: " + boost::to_string(line_counter));
				}
			}
		}
		//The secoond effective keyword should always be "load:", ignore everything else
		else if(!load_ok && boost::regex_match(line.c_str(), groups, load_keyword)){
			// Check whether to show original loaded cloud or not
			bool show = (groups[1] == "show") || (groups[2] == "show");

			// This is to avoid saving clouds when loading from .pcd files
			bool saveCloud = true;
			int filesLoaded = 0;

			if((groups[1] == "demo") || (groups[2] == "demo")){
				reg->setVisualizer(viewer);
			}
			//Load all the file and folder list
			while(input.good() && !input.eof()){
				line_counter++;
				std::getline(input, line);

				//Blank line means the section is completed
				if(boost::regex_match(line, empty_line)){
					break;
				}
				else if(boost::regex_match(line.c_str(), groups, load_line)){
					//String to print any optional info
					string optional;
					//Trim the filename string
					string f_name = groups[2];
					boost::trim(f_name);
					PROFILER_STOP_TIMER(TIMER_PARSING);
					//Dispatch loading file or folder or error
					if(groups[1] == "file"){
						*(cloud_orig) += *(KittiUtility::loadFromFile<PointT>(f_name));
						saveCloud = false;
						filesLoaded++;
					}
					else if(groups[1] == "folder"){
						if(groups.size() > 5){
							int min = atoi(groups[4].str().c_str());
							int max = atoi(groups[5].str().c_str());
							*(cloud_orig) += *(KittiUtility::loadFromFolder<PointT>(f_name, min, max));
						}
						else{
							*(cloud_orig) += *(KittiUtility::loadFromFolder<PointT>(f_name));
						}
					}
					else if(groups[1] == "dataset"){
						int min = atoi(groups[4].str().c_str());
						int max = atoi(groups[5].str().c_str());
						if(max > 0){
							*(cloud_orig) += *(KittiUtility::loadSynchronized<PointT>(f_name, reg, min, max));
							optional = groups[3].str();
						}
						else{
							*(cloud_orig) += *(KittiUtility::loadSynchronized<PointT>(f_name, reg));
						}
					}
					else if(groups[1] == "pcd"){
						pcl::io::loadPCDFile<PointT> (f_name, *cloud_orig);
						saveCloud = false;
						filesLoaded++;
					}
					else if(groups[1] == "ply"){
						pcl::io::loadPLYFile<PointT> (f_name, *cloud_orig);
						saveCloud = false;
						filesLoaded++;
					}
					load_ok = true;
					IARCHIVE_CLOUD("Loaded Cloud '" + f_name + "' " + optional, cloud_orig);
					PROFILER_START_TIMER(TIMER_PARSING);
					if(saveCloud || filesLoaded > 1){
#ifdef SAVE_TO_PCD
						pcl::io::savePCDFileBinary(SAVE_FOLDER + "pcd/output_loaded.pcd", *(cloud_orig));
#endif
#ifdef SAVE_TO_PLY
						pcl::io::savePLYFileBinary(SAVE_FOLDER + "ply/output_loaded.ply", *(cloud_orig));
#endif
					}
				}
				//Check for commented lines
				else if(boost::regex_match(line.c_str(), groups, comment_only)){ continue; }
				// Undefined line
				else {
					IERROR("Parser", "Failed to parse load line: " + boost::to_string(line_counter));
				}
			}

			if(!load_ok){
				IERROR("Parser", "Corrupt load section");
				input.close();
				return;
			}
			//Load finished, good to go
			if(show){
				viewer->addPointCloud<PointT>(cloud_orig, "Original Cloud", "Original Cloud");
				force_show = false;
			}
		}

		// If end keyword spotted then stop the parsing
		else if(boost::regex_match(line, end_keyword)){
			break;
		}
		else if(load_ok && boost::regex_match(line.c_str(), groups, vp_keyword)){
			string viewportName = groups[1];
			string description = "no description";
			vector<string> details;
			if(groups.size() > 3 && !groups[3].str().empty()){ description = groups[3].str(); }
			ChainFilter<PointT> chain_filter(cloud_orig);
			if(parse_filter<PointT>(input, line_counter, chain_filter,details)){
				boost::shared_ptr<pcl::PointCloud<PointT> > filtered_cloud = chain_filter.getOutput();
				IARCHIVE_CLOUD("Filtered-" + description, filtered_cloud);
				int v = viewer->addPointCloud<PointT>(filtered_cloud, viewportName, description);
				for(int i = 0; i < details.size(); i++){
					viewer->addDetail(v, details[i]);
				}
				force_show = false;
#ifdef SAVE_TO_PCD
				pcl::io::savePCDFileBinary(SAVE_FOLDER + "pcd/output_bin_vp_" + boost::to_string(v) + ".pcd",
										*(filtered_cloud));
#endif
#ifdef SAVE_TO_PLY
				pcl::io::savePLYFileBinary(SAVE_FOLDER + "ply/output_vp_" + boost::to_string(v) + ".ply",
										*(filtered_cloud));
#endif
			}
			else{
				IERROR("Parser", "Failed to parse line: " + boost::to_string(line_counter));
			}
		}
	}
	//If viewer initialized here, then launch it also here in blocking mode
	if(launch_viewer && load_ok){
		viewer->render();
		viewer->waitClose();
	}
	//Check if no cloud is visualized. If yes then show the original..
	if(force_show){
		viewer->addPointCloud<PointT>(cloud_orig, "Original Cloud", "Original Cloud");
	}
	input.close();
	PROFILER_STOP_TIMER(TIMER_PARSING);
	IARCHIVE("Parser", "Lines read: " + boost::to_string(line_counter));
}


template<typename PointT>
inline bool FilterParser::parse_filter(ifstream& input, int &line_counter,
		ChainFilter<PointT> &chain_filter, std::vector<string> &details) {

	//Regular expressions
	boost::regex empty_line("[ \\t]*");
	boost::regex comment_only("[ \\t]*(#.*)");
	boost::regex field_line("[ \\t]*\\+[ \\t]*([A-Za-z0-9_-]+):[ \\t]*((([ \\t]+[a-zA-Z]+)+|([ \\t]+-?[0-9]+(\\.[0-9]+)?)+)*)[ \\t]*(#.*)?");

	//Matcher
	boost::cmatch groups;

	//Input cursor position
	int in_pos;

	//String holding the whole line read
	string line = "";
	//Vector to store all split strings
	vector<string> fields;

	//Begin fields check
	while(input.good() && !input.eof()){
		//Save input position
		in_pos = input.tellg();

		line_counter++;
		std::getline(input, line);

		//Blank line means the section is completed
		if(boost::regex_match(line, empty_line)){
			break;
		}

		//Check for commented lines
		else if(boost::regex_match(line.c_str(), groups, comment_only)){ continue; }

		//Begin fields check
		else if(boost::regex_match(line.c_str(), groups, field_line)){
			//Trim the fields
			string f_fields = groups[2];
			boost::trim(f_fields);
			//Split the fields
			boost::split(fields, f_fields, boost::is_any_of(" "), boost::token_compress_on);

			//Lower case the filter type
			string lower_name = groups[1];
			boost::to_lower(lower_name);

			//Add to viewport details
			details.push_back(groups[1].str() + ": " + f_fields);

			//Filters Check:
			if(lower_name == "voxel_grid"){
				if(fields.size() == 5){
					bool all = (fields[4] == "true" || fields[4] == "t" || fields[4] == "y");
					chain_filter.addVoxelGrid(atof(fields[0].c_str()),
							atof(fields[1].c_str()),
							atof(fields[2].c_str()),
							atoi(fields[3].c_str()), all);
				} else if(fields.size() == 4){
					chain_filter.addVoxelGrid(atof(fields[0].c_str()),
							atof(fields[1].c_str()),
							atof(fields[2].c_str()),
							atoi(fields[3].c_str()));
				} else if(fields.size() == 3){
					chain_filter.addVoxelGrid(atof(fields[0].c_str()),
							atof(fields[1].c_str()),
							atof(fields[2].c_str()));
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "approx_voxel_grid"){
				if(fields.size() == 4){
					bool all = (fields[3] == "true" || fields[3] == "t" || fields[3] == "y");
					chain_filter.addApproximateVoxelGrid(atof(fields[0].c_str()),
							atof(fields[1].c_str()),
							atof(fields[2].c_str()), all);
				} else if(fields.size() == 3){
					chain_filter.addApproximateVoxelGrid(atof(fields[0].c_str()),
							atof(fields[1].c_str()),
							atof(fields[2].c_str()));
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "frustum_cull"){
				if(fields.size() == 13){
					//TO BE IMPLEMENTED...
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "grid_min"){
				if(fields.size() == 1){
					chain_filter.addGridMinimum(atof(fields[0].c_str()));
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "local_max"){
				if(fields.size() == 1){
					chain_filter.addLocalMaximum(atof(fields[0].c_str()));
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "pass_through"){
				if(fields.size() == 3){
					chain_filter.addPassThrough(fields[0], atof(fields[1].c_str()), atof(fields[2].c_str()));
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "radius_outlier"){
				if(fields.size() == 2){
					chain_filter.addRadiusOutlier(atof(fields[0].c_str()), (int)atof(fields[1].c_str()));
				} else if(fields.size() == 1){
					chain_filter.addRadiusOutlier(atof(fields[0].c_str()));
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "stat_outlier"){
				if(fields.size() == 2){
					chain_filter.addStatisticalOutlier((int)atof(fields[0].c_str()), atof(fields[1].c_str()));
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			else if(lower_name == "crop_box"){
				if(fields.size() <= 9 && fields.size() >= 3){
					double data[9] = {0};
					for(unsigned int i = 0; i < fields.size(); i++){
						data[i] = atof(fields[i].c_str());
					}
					chain_filter.addCropBox(data[0], data[1], data[2],
							data[3], data[4], data[5],
							data[6], data[7], data[8]);
				} else{
					IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				}
			}
			/*######################################################################################*/
			/*																						*/
			/*  						else if(ADD OTHER FILTERS HERE)								*/
			/*																						*/
			/*######################################################################################*/

			else{
				IERROR("Parser", "Wrong number of values, filter not parsed at line: " + boost::to_string(line_counter));
				details.erase(details.end());
			}
		}

		//Check for commented lines
		else if(boost::regex_match(line.c_str(), groups, comment_only)){ continue; }

		//None of the above went good, so maybe an error occurred
		else{
			break;
		}
	}	//end_ while

	//restore the position
	line_counter--;
	input.seekg(in_pos);

	return details.size() > 0;
}

#endif
