/*
 * pcl_filter_main.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: george
 */

#include <iostream>

#include <pcl/io/ply_io.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>

#include "cloud_cleaning/MovingObjectsRemoval.h"
#include "cloud_cleaning/ChangeDetectionWDST.h"
#include "cloud_cleaning/ChangeDetectionOriginal.h"
#include "filters/MultiFilter.h"

#include "pipeline/Pipeline.h"

#include "data_loading/LidarLoader.h"
#include "data_loading/IMULoader.h"
#include "data_loading/CalibrationLoader.h"
#include "cloud_processing/CloudTransformer.h"
#include "cloud_processing/PoseCalculator.h"
#include "cloud_processing/Registration.h"
#include "cloud_processing/GICPRegistration.h"
#include "cloud_processing/CloudAccumulator.h"
#include "cloud_processing/CloudFilter.h"
#include "cloud_processing/CloudMultiFilter.h"
#include "cloud_processing/PosesSaver.h"
#include "cloud_processing/PosesLoader.h"
#include "cloud_processing/PlainCloudSaver.h"

#include "visualization/CloudVisualizerSimple.h"

#include "utility/mini_profiler.h"
#include "utility/helper_functions.h"

#define PI 3.14159265358979

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
    using namespace pipeline;
    using namespace data_loading;
    using namespace cloud_processing;
    using namespace cloud_cleaning;
    using namespace visualization;

    PROFILER_INIT_TIMER("Main", "Total execution time");
    PROFILER_START_TIMER("Main");
    std::cout << "Setting up the modules...\n";

    std::string dataset_number = "0091";
    std::string workspace = "/mnt/WinPartition/Users/danie/workspace";
    std::string dataset = workspace + "/dataset_" + dataset_number;
    int start_position = 0;
    int end_position = countFiles(dataset+"/sync/image_00/data")-1;

    switch(argc){
    case(2):
        dataset_number = std::string(argv[1]);
        dataset = workspace + "/dataset_" + dataset_number;
        end_position = countFiles(dataset+"/sync/image_00/data")-1;
        break;
    case(3):
        start_position 	= atoi(argv[1]);
        end_position	= atoi(argv[2]);
        break;
    case(4):
        dataset_number = std::string(argv[1]);
        dataset = workspace + "/dataset_" + dataset_number;
        start_position 	= atoi(argv[2]);
        end_position	= atoi(argv[3]);
        break;
    }
    std::string output_folder = "/mnt/WinPartition/Users/danie/Dropbox/Polimi/Thesis/dataset_output/";
    std::string output_path = output_folder+dataset_number+"/output"+"_"+std::to_string(start_position)+"-"+std::to_string(end_position)+"_Vallet";

    /// TODO: FOR VIDEO: VERY GOOD RESULTS IN CLUTTERED ENVIRONMENT FOR _0091: [100, 155] sigma_r = 0.15, sigma_theta = 0.1
    /// TODO: FOR VIDEO: car and pedestrian example in _0104: [0, 60] sigma_r = 15, sigma_theta = 0.1;
    /// TODO: Look for ground points in _0091: there is a barrier --> possible problem...

    CalibrationLoader calib_loader;
    calib_loader.loadCalibrationVeloToCam(workspace + "/calibration/calib_velo_to_cam.txt");
    calib_loader.loadCalibrationImuToVelo(workspace + "/calibration/calib_imu_to_velo.txt");
    calib_loader.loadCalibrationCamToRect(workspace + "/calibration/calib_cam_to_cam.txt");

    LidarLoader *lidar_loader = new LidarLoader(dataset + "/sync/velodyne_points/data",
                                                dataset + "/sync/velodyne_points/timestamps.txt");

//	lidar_loader->setBoundingBoxLimits(15.0f, 20.0f, 2.25f);
    lidar_loader->setFilesRange(start_position, end_position);

    IMULoader *imu_loader = new IMULoader(dataset + "/sync/oxts/data",
                                          dataset + "/sync/oxts/timestamps.txt");

    imu_loader->setFilesRange(start_position, end_position);

    CloudVisualizerSimple visualizer;

    MultiFilter output_final_filter;
    output_final_filter.addVoxelGrid(0.2, 0.2, 0.2, 1, true);
    //output_final_filter.addRadiusOutlier(0.5, 10);

    MultiFilter registration_filter;
//	registration_filter.addCropBox(20.0f, 40.0f, 4.2f);
    registration_filter.addVoxelGrid(0.075, 0.075, 0.075);
    //pre_registration_filter.addFloorRemoval(0.1, 0.1);

    MultiFilter post_registration_filter;
    post_registration_filter.addCropBox(50.0f, 50.0f, 4.2f);
    //pre_registration_filter.addFloorRemoval(0.1, 0.1);

    CloudAccumulator *cloud_accum = new CloudAccumulator();
    cloud_accum->setOutputFilter(output_final_filter);
    cloud_accum->enableDynamicFusion(true, 0.1f);
    cloud_accum->enableDiscriminatoryDownsampling(true);
    cloud_accum->registerBundlerFileSaving(output_path + "/bundler/points.out",
                                           output_path + "/bundler/cams_out.txt",
                                           output_path + "/bundler/lidars_out.txt");
    cloud_accum->enableVisualization(&visualizer);

    GICPRegistration * registration = new GICPRegistration();
//	Registration * registration = new Registration(&visualiser);
    registration->setVisualizer(&visualizer);
//	registration->setPreRegistrationFilter(registration_filter);
    registration->setMaxIterations(50);
    registration->setMinTransfThreshold(1e-3);
    registration->setMaxCorrispondenceDistance(4.0f);
//	registration->enableFloorSeparation(true);

    MovingObjectsRemoval object_removal(5, 0.2f, 2);
    object_removal.setCriticalDistance(4.5f);
    object_removal.setMinPreceedingClouds(3);
    //object_removal.setSearchRadius(0.1f);
    //object_removal.enableVisualization(&visualiser);

    ChangeDetectionWDST change_detector;
    change_detector.enableVisualization(&visualizer);
    change_detector.setSkippedFrames(1);
    change_detector.setMaxPreceedingClouds(10);
    change_detector.setMinPreceedingClouds(10);
    change_detector.enablePostProcess(false, 6);
    change_detector.enableFloorSeparation(true);
    change_detector.enableDynamicRemovedPointsSwapping(true);
    change_detector.enableVoxelizedAlgorithm(true, 0.3, 3);

    change_detector.enableInformationStoring(true, output_path+"/wdst/wdst_info.txt");
    change_detector.enableSavingPointsBinaryFile(true, output_path+"/wdst/rem_points/");
    change_detector.enableSavingRemovedPoints(true, output_path+"/wdst/rem_points_out.txt");
    //change_detector.enableDebug(true);

    ChangeDetectionOriginal change_detector_original;
    change_detector_original.enableVisualization(&visualizer);
    change_detector_original.setSkippedFrames(1);
    change_detector_original.setMaxPreceedingClouds(10);
    change_detector_original.setMinPreceedingClouds(10);
    change_detector_original.enablePostProcess(false, 6);
    change_detector_original.enableFloorSeparation(true);
    change_detector_original.enableDynamicRemovedPointsSwapping(false);
    change_detector_original.enableInformationStoring(true, output_path+"/change_detection/orig_info.txt");
    change_detector_original.enableSavingPointsBinaryFile(true, output_path+"/change_detection/rem_points/");
    change_detector_original.enableSavingRemovedPoints(true, output_path+"/change_detection/rem_points_out.txt");

//  change_detector_original.enableVoxelizedAlgorithm(true, 0.3);
//	change_detector_original.enableDebug(true);

    double p_theta = PI/180.0;
    change_detector.getParameters().sigma_r = 0.15;
//	change_detector.getParameters().search_radius = p_theta;
//	change_detector.getParameters().lambda_theta = p_theta/3;
    change_detector.getParameters().sigma_theta = 0.1*p_theta;
    change_detector.getParameters().precompute();
//
//
    change_detector_original.getParameters().sigma_r = 0.15;
    change_detector_original.getParameters().search_radius = 0.5*p_theta;
    change_detector_original.getParameters().sigma_theta = 0.1*p_theta;
    change_detector_original.getParameters().lambda_theta = 0.3*p_theta;

//	change_detector_original.getParameters().precompute();

    Eigen::Vector3f cam_pos = { 10, 40, 20};
    Eigen::Vector3f view_pos = { 10, 0, 1};
    //Qui viene lanciato il thread #2
    visualizer.startRendering();
    visualizer.showCoordinateAxes(true);
    visualizer.setCameraPose(cam_pos, view_pos);

    std::cout << "Setting up the pipeline...\n";
    Pipeline pipeLine;
    pipeLine.addModule(lidar_loader);
    pipeLine.addModule(imu_loader);
//	pipeLine.addModule(new CloudMultiFilter(pre_registration_filter));
    pipeLine.addModule(new PoseCalculator(false));
//	pipeLine.addModule(new CloudTransformer());
    pipeLine.addModule(registration);
    pipeLine.addModule(new PosesSaver(output_path + "/all_lidar_poses.txt"));
//  pipeLine.addModule(new PosesLoader(dataset + "/output/poses_" + dataset_number + ".txt", start_position));
    pipeLine.addModule(new CloudMultiFilter(post_registration_filter));
    //pipeLine.addModule(&object_removal);
    pipeLine.addModule(new CloudTransformer());
    pipeLine.addModule(&change_detector_original);
//    pipeLine.addModule(&change_detector);
    pipeLine.addModule(cloud_accum);
    pipeLine.addModule(new PlainCloudSaver(output_path+"/plain_clouds/"));


    std::cout << "Starting the pipeline...\n";
    //*** Before this line no computation is done. After this line all computations have been done ***
    pipeLine.init(Pipeline::PARALLEL_AUTO);
//  pipeLine.init(Pipeline::SEQUENTIAL);

    Cloud& final_cloud = cloud_accum->getFinalCloud();
    //visualizer.clear();
    //visualizer.addPointCloud(final_cloud.cloud_shared_ptr(), "Final Cloud", Color::WHITE);
    PROFILER_STOP_TIMER("Main");
    //pcl::visualization::PCLVisualizer vis;
    PROFILER_SHOW_STATISTICS;
    IDEBUG("Main", "Received final cloud with " + std::to_string(final_cloud.size()) + " points");
    //vis.addPointCloud(final_cloud.cloud_shared_ptr(), "final cloud");

    pcl::io::savePLYFile(output_path+"/final_cloud.ply", final_cloud.cloud(), true);
    IDEBUG("Main", "Saving the bundler file");
    //cloud_accum->saveToFileBundler("../output/bundler/points.out", "../output/bundler/cams_out.txt", 1.421f * 0.2f);
    IDEBUG("Main", "Waiting for visualizer to end");
    //vis.spin();
    //vis.resetStoppedFlag();
    visualizer.waitStop();
    //vis.spin();
    //IDEBUG("Main", "Removing the point cloud");
    //vis.resetStoppedFlag();
    //if(vis.removePointCloud("final cloud")){
        //IDEBUG("Main", "Point cloud removed");
    //}else{
        //IDEBUG("Main", "Point cloud NOT removed");
    //}
    //vis.spin();


}
