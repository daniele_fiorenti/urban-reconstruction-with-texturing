/*
 * pcl_filter_main.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: george
 */

#include <iostream>

#include <pcl/io/ply_io.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>

#include "cloud_cleaning/MovingObjectsRemoval.h"
#include "cloud_cleaning/ChangeDetectionWDST.h"
#include "cloud_cleaning/ChangeDetectionOriginal.h"
#include "filters/MultiFilter.h"

#include "pipeline/Pipeline.h"

#include "data_loading/CalibrationLoader.h"
#include "data_loading/LidarLoader.h"
#include "data_loading/IMULoader.h"
#include "cloud_processing/CloudTransformer.h"
#include "cloud_processing/PoseCalculator.h"
#include "cloud_processing/Registration.h"
#include "cloud_processing/GICPRegistration.h"
#include "cloud_processing/CloudAccumulator.h"
#include "cloud_processing/CloudFilter.h"
#include "cloud_processing/CloudMultiFilter.h"
#include "cloud_processing/PosesSaver.h"
#include "cloud_processing/PosesLoader.h"
#include "cloud_processing/PlainCloudSaver.h"

#include "visualization/CloudVisualizerSimple.h"

#include "utility/mini_profiler.h"

/*
 * Invece di rieseguire la registrazione si possono usare le pose preparate
 */

#define PI 3.14159265358979

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
	using namespace pipeline;
    using namespace data_loading;
    using namespace cloud_processing;
	using namespace cloud_cleaning;
	using namespace visualization;

	PROFILER_INIT_TIMER("Main", "Total execution time");
	PROFILER_START_TIMER("Main");
	std::cout << "Setting up the modules...\n";

    std::string dataset_number = "0104";
	int start_position 	= 100;
	int end_position	= 155;
    std::string dataset = "/mnt/WinPartition/Users/danie/workspace/dataset_" + dataset_number;

	/// TODO: FOR VIDEO: VERY GOOD RESULTS IN CLUTTERRED ENVIRONMENT FOR _0091: [100, 155] sigma_r = 0.15, sigma_theta = 0.1
	/// TODO: FOR VIDEO: car and pedestrian example in _0104: [0, 60] sigma_r = 15, sigma_theta = 0.1;
	/// TODO: Look for ground points in _0091: there is a barrier --> possible problem...

    CalibrationLoader calib_loader;
    calib_loader.loadCalibrationVeloToCam(dataset + "/calibration/calib_velo_to_cam.txt");
    calib_loader.loadCalibrationImuToVelo(dataset + "/calibration/calib_imu_to_velo.txt");
    calib_loader.loadCalibrationCamToRect(dataset + "/calibration/calib_cam_to_cam.txt");

    LidarLoader *lidar_loader = new LidarLoader(dataset + "/velodyne_points/data",
                                                dataset + "/velodyne_points/timestamps.txt");
//	lidar_loader->setBoundingBoxLimits(15.0f, 20.0f, 2.25f);
//	lidar_loader->setFilesRange(start_position, end_position);

    IMULoader *imu_loader = new IMULoader(dataset + "/oxts/data",
                                          dataset + "/oxts/timestamps.txt");
//	imu_loader->setFilesRange(start_position, end_position);

	GICPRegistration * registration = new GICPRegistration();
//	Registration * registration = new Registration(&visualiser);
//	registration->setVisualizer(&visualiser);
//	registration->setPreRegistrationFilter(registration_filter);
	registration->setMaxIterations(50);
	registration->setMinTransfThreshold(1e-6);
	registration->setMaxCorrispondenceDistance(4.0f);
//	registration->enableFloorSeparation(true);

	std::cout << "Setting up the pipeline...\n";
	Pipeline pipeLine;
	pipeLine.addModule(lidar_loader);
	pipeLine.addModule(imu_loader);
	pipeLine.addModule(new PoseCalculator(false));
	pipeLine.addModule(registration);
    pipeLine.addModule(new PosesSaver(dataset + "/output/poses_" + dataset_number + ".txt"));

	std::cout << "Starting the pipeline...\n";
	pipeLine.init(Pipeline::PARALLEL_AUTO);

	std::cout << "...... Pipeline DONE!\n";
}
