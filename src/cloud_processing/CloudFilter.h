/*
 * CloudFilter.h
 *
 *  Created on: Jun 12, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_CLOUDFILTER_H_
#define CLOUD_PROCESSING_CLOUDFILTER_H_

#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"

namespace cloud_processing {
using namespace pipeline;

class CloudFilter: public Module {
public:
	CloudFilter(pcl::Filter<Cloud::pointType>& filter);
	virtual ~CloudFilter();

	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();
	void setScene(Scene * scene){ }

private:
	CameraRig * _current_rig;
	pcl::Filter<Cloud::pointType>& _filter;
	bool _finished;
};

} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_CLOUDFILTER_H_ */
