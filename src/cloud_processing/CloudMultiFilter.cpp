/*
 * CloudMultiFilter.cpp
 *
 *  Created on: Jun 17, 2015
 *      Author: george
 */

#include "CloudMultiFilter.h"

/*
 * Come cloud filter ma può contenere più filtri
 */

namespace cloud_processing {

CloudMultiFilter::CloudMultiFilter(MultiFilter& filter) : Module("CloudMultiFilter", false),
		_finished(false), _current_rig(nullptr), _filter(filter) {
	IDEBUG("CloudMultiFilter", "Received multi-filter reference...");
}

CloudMultiFilter::~CloudMultiFilter() { }

bool CloudMultiFilter::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
	_current_rig = rig;
	if(!rig){
		return false;
	}

	_current_rig->getLidar().cloud.filter(_filter);
	return true;
}

CameraRig* CloudMultiFilter::getOutputCameraRig() {
	return _current_rig;
}

bool CloudMultiFilter::hasFinished() {
	return _finished;
}

void CloudMultiFilter::finalize() {
	_finished = true;
}

} /* namespace cloud_processing */
