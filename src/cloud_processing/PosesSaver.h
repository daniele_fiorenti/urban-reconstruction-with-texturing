/*
 * PosesSaver.h
 *
 *  Created on: Aug 12, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_POSESSAVER_H_
#define CLOUD_PROCESSING_POSESSAVER_H_

#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"

#include <Eigen/Core>

namespace cloud_processing {
using namespace pipeline;

class PosesSaver: public Module {
public:
	PosesSaver(std::string const &filepath = "../output/all_lidar_poses.txt");

	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();
	void setScene(Scene * scene){ }

private:
	CameraRig * _current_rig;
	bool _finished;
	std::vector<Eigen::Matrix4d> _poses;
	std::string _filepath;
};

} /* namespace pipeline */

#endif /* CLOUD_PROCESSING_POSESSAVER_H_ */
