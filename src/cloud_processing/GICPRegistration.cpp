/*
 * GICPRegistration.cpp
 *
 *  Created on: Jun 10, 2015
 *      Author: george
 */

#include "GICPRegistration.h"
#include "../config.h"
#include "../utility/mini_profiler.h"
#include "../utility/info_printer.h"

#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/features/normal_3d.h>

/*
 * Qui viene fatta la registrazione delle nuvole.
 */

//#define CALLBACK_ENABLED
#define VAR_DEBUG(v) \
	std::cout << "----- " << #v << " = " << v << "\n";

namespace cloud_processing {

GICPRegistration::GICPRegistration(): Module("GICPRegistration", true),
		_finished(false),
		_current_rig(nullptr),
		_viewer(nullptr),
		_target_set(false),
		_floor_separation_ON(false),
		_last_pose(Eigen::Matrix4d::Identity()),
		_filter_set(false)
{

	_aligner.setMaxCorrespondenceDistance (REG_MAX_CORRESPONDENCE);
	_aligner.setMaximumIterations(REG_MAX_ITERATIONS);
	_aligner.setTransformationEpsilon(REG_TRANSFORMATION_EPSILON);

	PROFILER_INIT_TIMER(TIMER_REGISTRATION_COMPUTATION, "Point cloud registration computation time");
}

GICPRegistration::GICPRegistration(visualization::CloudVisualizerSimple* viewer)
 : GICPRegistration() {
	_viewer = viewer;
#ifdef CALLBACK_ENABLED
	typedef pcl::PointCloud<pcl::PointNormal> normal_cloud;
	 boost::function<void (const normal_cloud &cloud_src, const std::vector<int> &indices_src,
			 const normal_cloud &cloud_tgt, const std::vector<int> &indices_tgt)> update_visualizer;
	update_visualizer = boost::bind (
			&GICPRegistration::registrationCallBack,
			this, _1, _2, _3, _4);

	_aligner.registerVisualizationCallback(update_visualizer);
#endif
}

GICPRegistration::~GICPRegistration() { }

void GICPRegistration::setPreRegistrationFilter(MultiFilter& filter) {
	_filter = filter;
	_filter_set = true;
}

MultiFilter& GICPRegistration::getPreRegistrationFilter() {
	return _filter;
}

void GICPRegistration::swapFloorTransformations(Eigen::Matrix4d& T,
		const Eigen::Matrix4d& F) {
////
//	T(2,0) = F(2,0);
//	T(2,1) = F(2,1);
//	T(2,2) = F(2,2);  //TODO: Consider a faster AVX performance implementation
	T(3,4) = T(3,4) - F(3,4);

//	T.row(2) = F.row(2);

//	// See if there is need for normalization...
//	if(std::fabs(T.determinant() - 1.0) <= std::numeric_limits<double>::epsilon()){
//		std::cout << "===== SWAP_FLOOR_MATRICES: Final rotation matrix is not normalized\n";
//		Eigen::Vector4f n_T = T.col(3);
//		T.normalize();
//		T.col(3) = n_T;
//	}
}

void GICPRegistration::setScene(Scene* scene) { }

void GICPRegistration::setVisualizer(visualization::CloudVisualizerSimple* viewer) {
	_viewer = viewer;
}

bool GICPRegistration::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
	_current_rig = rig;
	if(!rig){
        std::cerr << "Received null rig\n";
		return false;
	}

	// Check for floor operations
	bool need_floor_stitching = false;
	if(_floor_separation_ON){
		if(rig->createCloud("floor")){
			_floor_removal->setInputCloud(rig->getCloud().cloud_shared_ptr());
			_floor_removal->filter(rig->getCloud().cloud(), rig->getCloud("floor").cloud());
			need_floor_stitching = true;
		}
	}

	if(!_target_set){
		_last_pose = _current_rig->getPose();
		if(_filter_set){
			_filter.filter(rig->getCloud().cloud_shared_ptr(), _target.cloud_shared_ptr());
			_target.transform(_last_pose);
			if(_floor_separation_ON){
				_filter.filter(rig->getCloud("floor").cloud_shared_ptr(), _floor_target.cloud_shared_ptr());
				_floor_target.transform(_last_pose);
			}
		}else{
			pcl::transformPointCloud(rig->getCloud().cloud(), _target.cloud(), _last_pose);
			if(_floor_separation_ON){
				pcl::transformPointCloud(rig->getCloud("floor").cloud(), _floor_target.cloud(), _last_pose);
			}
		}

		_target_set = true;
		return true;
	}

	_last_pose = _last_pose * _current_rig->getRelativePose();
	if(_filter_set){
		_filter.filter(rig->getCloud().cloud_shared_ptr(), _source.cloud_shared_ptr());
		_source.transform(_last_pose);
		if(_floor_separation_ON){
			_filter.filter(rig->getCloud("floor").cloud_shared_ptr(), _floor_source.cloud_shared_ptr());
			_floor_source.transform(_last_pose);
		}
	}else{
		pcl::transformPointCloud(rig->getCloud().cloud(), _source.cloud(), _last_pose);
		if(_floor_separation_ON){
			pcl::transformPointCloud(rig->getCloud("floor").cloud(), _floor_source.cloud(), _last_pose);
		}
	}
	IPRINT("GICPRegistration", "Cloud " + std::to_string(iteration_index) + " points = " + std::to_string(_source.size()));

	// Some auxiliary variables
	Eigen::Matrix4d T;

	if(!_viewer){
		_aligner.setInputSource(_source.cloud_shared_ptr());
		_aligner.setInputTarget(_target.cloud_shared_ptr());
		PROFILER_START_TIMER(TIMER_REGISTRATION_COMPUTATION);
		_aligner.align(_target_tmp.cloud());
		PROFILER_STOP_TIMER(TIMER_REGISTRATION_COMPUTATION);
		if(_aligner.hasConverged()){
			T = _aligner.getFinalTransformation().cast<double>();

			// See if floor was separated and perform the floor alignment
			if(_floor_separation_ON){
				_aligner.setInputSource(_floor_source.cloud_shared_ptr());
				_aligner.setInputTarget(_floor_target.cloud_shared_ptr());
				_aligner.align(_floor_target_tmp.cloud());

				if(_aligner.hasConverged()){
					swapFloorTransformations(T, _aligner.getFinalTransformation().cast<double>());
				}
			}

			_source.transform(T);

			_last_pose = T * _last_pose;
			_current_rig->getPose() = _last_pose;
			_current_rig->getRelativePose() = T * _current_rig->getRelativePose();
			IPRINT("PointPlane Estimator", " -> Converged!!  Fitness Score: "
					+ std::to_string(_aligner.getFitnessScore()));
		}
		else{
			IPRINT("PointPlane Estimator", " -> Estimator FAILED!!!");
		}
	}
	else{
		// Clean the viewer and add the last target as reference
		//_viewer->clear();
		_viewer->setTitle("GICP Registration", visualization::Color::GREY);
		//_viewer->addPointCloud(_target_tmp, "last_target", visualization::Color::GREY);

		int iterations = _aligner.getMaximumIterations() / REG_DEMO_SUBITERATIONS;
		int maxCorrespondence = _aligner.getMaxCorrespondenceDistance();
		_aligner.setMaximumIterations(REG_DEMO_SUBITERATIONS);
		// Run the same optimization in a loop and visualize the results
		Eigen::Matrix4d Ti = Eigen::Matrix4d::Identity ();
		Eigen::Matrix4f prev = Eigen::Matrix4f::Identity ();

		_aligner.setInputTarget(_target.cloud_shared_ptr());

		bool converged = false;
		_viewer->addPointCloud(_target, "target", visualization::Color::BLUE);
		_viewer->addPointCloud(_source, "source", visualization::Color::RED);

		if(_floor_separation_ON){
			_viewer->addPointCloud(_floor_target, "floor_target", visualization::Color::BLUE);
			_viewer->addPointCloud(_floor_source, "floor_source", visualization::Color::RED);
		}

		for (int i = 0; i < iterations && !converged; ++i)
		{
			_aligner.setInputSource (_source.cloud_shared_ptr());
			PROFILER_START_TIMER(TIMER_REGISTRATION_COMPUTATION);
			_aligner.align (_target_tmp.cloud());
			PROFILER_STOP_TIMER(TIMER_REGISTRATION_COMPUTATION);

			// Print the fitness score...
			IPRINT("PointPlane Estimator", "Iteration " + boost::to_string(i+1) + ":\tFitness Score: "
								+ std::to_string(_aligner.getFitnessScore(1.0)));

			auto last_inc_transformation = _aligner.getLastIncrementalTransformation();
			converged = fabs ((last_inc_transformation - prev).sum ()) < _aligner.getTransformationEpsilon ();
//			converged = _aligner.hasConverged();
			prev = last_inc_transformation;

//			VAR_DEBUG(_aligner.getTransformationEpsilon());
//			VAR_DEBUG(fabs ((last_inc_transformation - prev).sum ()));
//			VAR_DEBUG(converged);
////			VAR_DEBUG(prev);

			//accumulate transformation between each Iteration
			T = _aligner.getFinalTransformation().cast<double>();

			// See if floor was separated and perform the floor alignment
			if(_floor_separation_ON){
				_aligner.setInputSource(_floor_source.cloud_shared_ptr());
				_aligner.setInputTarget(_floor_target.cloud_shared_ptr());
				_aligner.align(_floor_target_tmp.cloud());

				if(_aligner.hasConverged()){
					swapFloorTransformations(T, _aligner.getFinalTransformation().cast<double>());
//					T = _aligner.getFinalTransformation() * T;
				}

				_floor_source.swap(_floor_target_tmp);

				// Visualize the current floor state
				_viewer->addPointCloud(_floor_source, "floor_source", visualization::Color::RED);

				_aligner.setInputTarget(_target.cloud_shared_ptr());
			}

			Ti = T * Ti;

			//check if it is converged... to skip some useless iterations...
//			converged = fabs ((_aligner.getLastIncrementalTransformation () - prev).sum ()) < _aligner.getTransformationEpsilon ();
//			prev = _aligner.getLastIncrementalTransformation ();

//			converged = fabs((T - prev).sum()) < _aligner.getTransformationEpsilon();
//			prev = T;

			_source.transform(T);

			// visualize current state
			_viewer->addPointCloud(_source, "source", visualization::Color::RED);
		}
		//get back the old results
		_aligner.setMaximumIterations(REG_DEMO_SUBITERATIONS * iterations);
		_aligner.setMaxCorrespondenceDistance (maxCorrespondence);

		//Return the final transform
		_last_pose = Ti * _last_pose;

        //std::cout << "Ti: " << Ti << std::endl;
        //std::cout << "_last_pose <<" <<  _last_pose << std::endl;

		_current_rig->getPose() = _last_pose;
		_current_rig->getRelativePose() = Ti * _current_rig->getRelativePose();
	}

	if(need_floor_stitching){
		rig->getCloud() += rig->getCloud("floor");
		rig->clearCloud("floor");
	}
	_target.swap(_source);
	return true;
}

CameraRig* GICPRegistration::getOutputCameraRig() {
	return _current_rig;
}

bool GICPRegistration::hasFinished() {
	return _finished;
}

void GICPRegistration::finalize() {
	if(_viewer){
		_viewer->removePointCloud("target");
		_viewer->removePointCloud("source");
	}
	_finished = true;
}

void GICPRegistration::setMinTransfThreshold(double epsilon){
	if(epsilon > 0) { _aligner.setTransformationEpsilon(epsilon); }
}

void GICPRegistration::setMaxIterations(unsigned int max_iters){
	if(max_iters > 0) { _aligner.setMaximumIterations(max_iters); }
}

void GICPRegistration::setMaxCorrispondenceDistance(double corr){
	if(corr > 0) { _aligner.setMaxCorrespondenceDistance (corr); }
}

void GICPRegistration::setEuclideanFitness(double fitness){
	if(fitness > 0) { _aligner.setEuclideanFitnessEpsilon(fitness); }
}

void GICPRegistration::enableFloorSeparation(bool enable) {
	_floor_separation_ON = enable;
	if(enable){
		// Initialize filter
		_floor_removal = std::shared_ptr<filtering::FloorRemoval>
						(new filtering::FloorRemoval(0.25, 0.25, 1, -1.25));
		_floor_removal->setMaxSlopeDifference(0.2);

		// Clear the potential last clouds
		_floor_source.clear();
		_floor_target.clear();
	}
}

} /* namespace cloud_processing */
