/*
 * PosesLoader.cpp
 *
 *  Created on: Sep 11, 2015
 *      Author: george
 */

#include "PosesLoader.h"

/*
 * Legge le pose da file salvate da PoseCalculator (attraverso PosesSaver)
 */

namespace cloud_processing {

PosesLoader::PosesLoader(const std::string& filepath, int start): Module("PosesLoader", false),
		_filepath(filepath), _finished(false), _current_rig(nullptr){
	_file_in.open(filepath, std::ios::in);

	// Skip the first line, it shows the number of frames
	std::string skip;
	std::getline(_file_in, skip);

	// Skip the first start poses...
	if(start > 0){
		for(int row = 0; row < 4*(start-1); row++){
			std::getline(_file_in, skip);
		}
		std::string line;
		Eigen::Matrix4d pose;
		auto pos = _file_in.tellg();
		for(int row = 0; row < 4; row++){
			std::stringstream ss;
			std::getline(_file_in, line);
			ss << line;
			ss >> pose(row,0) >> pose(row,1) >> pose(row,2) >> pose(row,3);
		}
		_file_in.seekg(pos);
		_first_inv_pose = pose.inverse();
	} else {
		_first_inv_pose = Eigen::Matrix4d::Identity();
	}
	_last_inv_pose = Eigen::Matrix4d::Identity();
}

bool PosesLoader::process(CameraRig* rig,
		int iteration_index) {
	if(rig){
		_current_rig = rig;
		std::string line;
		Eigen::Matrix4d pose;
		for(int row = 0; row < 4; row++){
			std::stringstream ss;
			// First row...
			std::getline(_file_in, line);
			ss << line;
			ss >> pose(row,0) >> pose(row,1) >> pose(row,2) >> pose(row,3);
		}
		rig->getPose() = _first_inv_pose * pose;
		rig->getRelativePose() = _last_inv_pose * rig->getPose();
		_last_inv_pose = pose.inverse();
		return true;
	}
	return false;
}

CameraRig* PosesLoader::getOutputCameraRig() {
	return _current_rig;
}

bool PosesLoader::hasFinished() {
	return _finished;
}

void PosesLoader::finalize() {
	if(!_finished){
		_file_in.close();
		_finished = true;
	}
}

} /* namespace cloud_processing */
