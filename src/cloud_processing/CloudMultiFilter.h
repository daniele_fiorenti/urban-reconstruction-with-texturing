/*
 * CloudMultiFilter.h
 *
 *  Created on: Jun 17, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_CLOUDMULTIFILTER_H_
#define CLOUD_PROCESSING_CLOUDMULTIFILTER_H_

#include "../filters/MultiFilter.h"
#include "../pipeline/Module.h"

namespace cloud_processing {
using namespace pipeline;

class CloudMultiFilter: public pipeline::Module {
public:
	CloudMultiFilter(MultiFilter &filter);
	virtual ~CloudMultiFilter();

	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();
	void setScene(Scene * scene){ }

private:
	CameraRig * _current_rig;
	MultiFilter& _filter;
	bool _finished;
};

} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_CLOUDMULTIFILTER_H_ */
