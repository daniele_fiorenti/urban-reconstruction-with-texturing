/*
 * CloudTransformer.h
 *
 *  Created on: Jun 10, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_CLOUDTRANSFORMER_H_
#define CLOUD_PROCESSING_CLOUDTRANSFORMER_H_

#include "../pipeline/Module.h"

namespace cloud_processing {
using namespace pipeline;

class CloudTransformer: public Module {
public:
	CloudTransformer();
	virtual ~CloudTransformer();

	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();

private:
	CameraRig * _current_rig;
	Eigen::Matrix4d _last_pose;
	bool _finished;
};

} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_CLOUDTRANSFORMER_H_ */
