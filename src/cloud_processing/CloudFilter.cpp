/*
 * CloudFilter.cpp
 *
 *  Created on: Jun 12, 2015
 *      Author: george
 */

#include "CloudFilter.h"

#include "../utility/info_printer.h"

/*
 * Fa passare la nuvola in un filtro
 */

namespace cloud_processing {

CloudFilter::CloudFilter(pcl::Filter<Cloud::pointType>& filter): Module("CloudFilter", false),
		_finished(false), _current_rig(nullptr), _filter(filter) {
	IDEBUG("CloudFilter", "Received filter reference...");
}

CloudFilter::~CloudFilter() { }

bool CloudFilter::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
	_current_rig = rig;
	if(!rig){
		return false;
	}

	_current_rig->getLidar().cloud.filter(_filter);
	return true;
}

CameraRig* CloudFilter::getOutputCameraRig() {
	return _current_rig;
}

bool CloudFilter::hasFinished() {
	return _finished;
}

void CloudFilter::finalize() {
	_finished = true;
}

} /* namespace cloud_processing */
