/*
 * GICPRegistration.h
 *
 *  Created on: Jun 10, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_GICPREGISTRATION_H_
#define CLOUD_PROCESSING_GICPREGISTRATION_H_

#include "../pipeline/Module.h"

#include <pcl/registration/gicp.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/registration_visualizer.h>

#include "../filters/MultiFilter.h"
#include "../filters/FloorRemoval.h"
#include "../visualization/CloudVisualizerSimple.h"

namespace cloud_processing {
using namespace pipeline;

class GICPRegistration: public Module {
public:
	GICPRegistration();
	GICPRegistration(visualization::CloudVisualizerSimple* viewer);
	~GICPRegistration();

	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();

	void setVisualizer(visualization::CloudVisualizerSimple * viewer);

	void setPreRegistrationFilter(MultiFilter &filter);
	MultiFilter& getPreRegistrationFilter();

	void enableFloorSeparation(bool enable);

	void setMinTransfThreshold(double epsilon);
	void setMaxIterations(unsigned int max_iters);
	void setMaxCorrispondenceDistance(double correspondence);
	void setEuclideanFitness(double fitness);

private:
	pcl::GeneralizedIterativeClosestPoint<Cloud::pointType, Cloud::pointType> _aligner;
	Cloud _source;
	Cloud _target_tmp;
	Cloud _target;

	// Floor separation related ============================
	Cloud _floor_source;
	Cloud _floor_target_tmp;
	Cloud _floor_target;
	bool _floor_separation_ON;	// Turn on floor separation
	std::shared_ptr<filtering::FloorRemoval> _floor_removal;

	CameraRig * _current_rig;
	bool _finished;
	bool _target_set;

	Eigen::Matrix4d _last_pose;
	Eigen::Matrix4d _last_bad_pose;
	Eigen::Matrix4d _relative_pose;

	MultiFilter _filter;
	bool _filter_set;

	visualization::CloudVisualizerSimple * _viewer;

	void swapFloorTransformations(Eigen::Matrix4d &T, const Eigen::Matrix4d &F);
};

} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_GICPREGISTRATION_H_ */
