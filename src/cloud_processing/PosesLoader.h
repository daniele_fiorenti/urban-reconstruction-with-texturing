/*
 * PosesLoader.h
 *
 *  Created on: Sep 11, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_POSESLOADER_H_
#define CLOUD_PROCESSING_POSESLOADER_H_

#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"

#include <Eigen/Core>

namespace cloud_processing {
using namespace pipeline;

class PosesLoader: public Module {
public:
	PosesLoader(std::string const &filepath = "../output/all_lidar_poses.txt", int start = 0);

	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();
	void setScene(Scene * scene){ }

private:
	CameraRig * _current_rig;
	bool _finished;
	Eigen::Matrix4d _last_inv_pose;
	Eigen::Matrix4d _first_inv_pose;
	std::string _filepath;
	std::ifstream _file_in;
};

} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_POSESLOADER_H_ */
