/*
 * CloudAccumulator.h
 *
 *  Created on: Jun 12, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_CLOUDACCUMULATOR_H_
#define CLOUD_PROCESSING_CLOUDACCUMULATOR_H_

#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"
#include "../visualization/CloudVisualizerSimple.h"

#include <pcl/features/principal_curvatures.h>
#include <pcl/features/normal_3d.h>

namespace cloud_processing {
using namespace pipeline;

class CloudAccumulator: public Module {
public:
	CloudAccumulator();
    CloudAccumulator(std::string const & calibration_file);
	virtual ~CloudAccumulator();

	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();

	void enableDynamicFusion(bool enable, float fuse_voxel_radius = 0.143f);
	void enableDiscriminatoryDownsampling(bool enable);

	void enableVisualization(visualization::CloudVisualizerSimple * visualizer);

    void saveToFileBundler(std::string const & points_file_out,
                           std::string const & cameras_file_out,
                           std::string const & lidars_file_out,
                           float search_radius);

    void registerBundlerFileSaving(std::string const & points_file_out,
                                   std::string const & cameras_file_out,
                                   std::string const & lidars_file_out);

	void setOutputFilter(pcl::Filter<Cloud::pointType>& filter);
	Cloud& getFinalCloud();

	Cloud& getTestCloud(){
		return _test_cloud;
	}
private:
	struct PointRay{
		int cam_index;
		double ray_sqr_length;

		bool operator<(PointRay const & other) const {
			return ray_sqr_length < other.ray_sqr_length;
		}
	};

	Cloud _final_cloud;
	CameraRig *_current_rig;

	std::vector<CameraRig*> _all_rigs;
	std::vector<RigBox> _boxes;
	pcl::Filter<Cloud::pointType> *_filter;
	bool _finished;

	Cloud _test_cloud;

	bool _fusion_enabled;
	float _fuse_voxel_size;
	bool _discriminate_enabled;


	std::string _bundler_file_points;
	std::string _bundler_file_cameras;
    std::string _bundler_file_lidars;
	bool _bundler_save;

	pcl::NormalEstimation<Cloud::pointType, pcl::PointNormal> _normal_estimation;

	void fuse(Cloud const & other);

	visualization::CloudVisualizerSimple * _viewer;
};

} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_CLOUDACCUMULATOR_H_ */
