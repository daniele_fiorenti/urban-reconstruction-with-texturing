/*
 * PoseCalculator.h
 *
 *  Created on: Jun 10, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_POSECALCULATOR_H_
#define CLOUD_PROCESSING_POSECALCULATOR_H_

#include "../pipeline/Module.h"
#include "Eigen/Core"

namespace cloud_processing {
using namespace pipeline;

class PoseCalculator: public Module {
public:
	PoseCalculator(bool interpolate = true);
	virtual ~PoseCalculator();
	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();

	void enablePoseInterpolation(bool enable);
private:
	CameraRig* _current_rig;
	Eigen::Matrix4d _first_inverse;
	Eigen::Matrix4d _last_pose;
	Eigen::Matrix4d _last_relative_pose;
	IMU _last_imu;
	double _scale;
	double _starting_altitude;
	bool _first_iteration;
	bool _finished;
	bool _interpolate;

	// Needed to output the positions
	std::vector<Eigen::Matrix4d> _all_poses;
};

} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_POSECALCULATOR_H_ */
