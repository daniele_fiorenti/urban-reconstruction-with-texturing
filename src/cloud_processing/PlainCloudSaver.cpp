/*
 * PlainCloudSaver.cpp
 *
 *  Created on: Sep 12, 2015
 *      Author: george
 */

#include "PlainCloudSaver.h"
#include "../utility/helper_functions.h"
/*
 * Salva una pointcloud in un file
 */

namespace cloud_processing {

PlainCloudSaver::PlainCloudSaver(const std::string& folder): Module("PlainCloudSaver", false),
		_folderpath(folder), _finished(false), _current_rig(nullptr){
    createFolder(_folderpath);
}

bool PlainCloudSaver::process(CameraRig* rig,
		int iteration_index) {
	if(rig){
		// Save the points in a binary file
		std::fstream _bin_file (_folderpath + "000" + std::to_string(rig->getID()) + ".bin",
				std::ios::out | std::ios::binary);
		for(int i = 0; i < rig->getCloud().size(); ++i){
			//Some calculations to fill a[]
			auto& p = rig->getCloud()[i];
			_bin_file.write((char*)&p.x, sizeof(float)*3);
		}
		_bin_file.close();
		return true;
	}
	return false;
}

CameraRig* PlainCloudSaver::getOutputCameraRig() {
	return _current_rig;
}

bool PlainCloudSaver::hasFinished() {
	return _finished;
}

void PlainCloudSaver::finalize() {
	if(!_finished){
		_finished = true;
	}
}

} /* namespace cloud_processing */
