/*
 * PosesSaver.cpp
 *
 *  Created on: Aug 12, 2015
 *      Author: george
 */

#include "PosesSaver.h"

/*
 * Salva le pose su file
 */

namespace cloud_processing {

PosesSaver::PosesSaver(const std::string& filepath): Module("PosesSaver", false),
		_filepath(filepath), _finished(false), _current_rig(nullptr){ }

bool PosesSaver::process(CameraRig* rig, int iteration_index) {
	if(rig){
		_current_rig = rig;
		_poses.push_back(rig->getPose());
		return true;
	}
	return false;
}

CameraRig* PosesSaver::getOutputCameraRig() {
	return _current_rig;
}

bool PosesSaver::hasFinished() {
	return _finished;
}

void PosesSaver::finalize() {
	if(!_finished){
		// Save poses
		std::ofstream poses_out(_filepath);
		poses_out << _poses.size() << "\n";
		for(auto& pose: _poses){
            // We need this for the mask extraction!
            poses_out << pose << "\n";

            // Only Translations
            // auto col_3 = pose.col(3);
            // poses_out << col_3[0] << " " << col_3[1] << " " << col_3[2] << std::endl;
		}
		poses_out.close();
		_finished = true;
	}
}

} /* namespace cloud_processing */

