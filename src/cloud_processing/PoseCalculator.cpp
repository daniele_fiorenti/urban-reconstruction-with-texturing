/*
 * PoseCalculator.cpp
 *
 *  Created on: Jun 10, 2015
 *      Author: george
 */

#include "PoseCalculator.h"

#include "../utility/helper_functions.h"
#include "../utility/info_printer.h"

/*
 * Serve a calcolare la pose globale e relativa delle pointcloud partendo dai dati dell'imu.
 */

namespace cloud_processing {

//Dato che la timestamp dell'imu e del lidar non sono perfettamente uguali si possono interpolare! Il consiglio è di non farlo perché è pesante
PoseCalculator::PoseCalculator(bool interpolate)
	: Module("PoseCalculator", false), _current_rig(nullptr),
	  _finished(false), _scale(1.0f), _starting_altitude(0.0f),
	  _last_pose(Eigen::Matrix4d::Identity()), _last_relative_pose(Eigen::Matrix4d::Identity()),
	  _first_iteration(true), _interpolate(interpolate){ }

PoseCalculator::~PoseCalculator()
{  }

inline void PoseCalculator::setScene(Scene* scene) { }

bool PoseCalculator::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
	_current_rig = rig;
	if(!rig){
		return false;
	}
	Eigen::Matrix4d matrix;
	IMU &cur_imu = _current_rig->getImu();

	if(_first_iteration){
		_scale = std::cos(cur_imu.lat * math_deg_to_rad);
		_starting_altitude = cur_imu.alt;
		fastPoseMatrixComposition(matrix, cur_imu.lat, cur_imu.lon, cur_imu.alt,
				cur_imu.roll, cur_imu.pitch, cur_imu.yaw, _scale);

		//_current_rig->getLidar().pose = Eigen::Matrix4d::Identity();
		_current_rig->getPose() = Eigen::Matrix4d::Identity();

		_first_inverse = matrix.inverse();
		std::cout << name() << " >> Matrix inverse: \n" << _first_inverse << "\n";
		_first_iteration = false;
		_last_imu = _current_rig->getImu();
		IDEBUG("PoseCalculator", "Received first pose, setup the inverse");
		_all_poses.push_back(Eigen::Matrix4d::Identity());
		return true;
	}

	if(_interpolate && (_current_rig->getLidar().timestamp < cur_imu.timestamp)){
		Timestamp& start = _last_imu.timestamp;
		Timestamp& end = cur_imu.timestamp;

		float coeff = fabs((_current_rig->getLidar().timestamp - start)
						/ (end - start));
		fastPoseMatrixComposition(matrix,
				lerp(_last_imu.lat, cur_imu.lat, coeff),
				lerp(_last_imu.lon, cur_imu.lon, coeff),
				lerp(_last_imu.alt, cur_imu.alt, coeff),
				angle_lerp(_last_imu.roll, cur_imu.roll, coeff),
				angle_lerp(_last_imu.pitch, cur_imu.pitch, coeff),
				angle_lerp(_last_imu.yaw, cur_imu.yaw, coeff),
				_scale);
	}
	else{
		fastPoseMatrixComposition(matrix, cur_imu.lat, cur_imu.lon, cur_imu.alt,
						cur_imu.roll, cur_imu.pitch, cur_imu.yaw, _scale);
	}
	//_current_rig->getLidar().pose = _first_inverse * matrix;
	_current_rig->getPose() = _first_inverse * matrix;
	_current_rig->getRelativePose() = _last_pose.inverse() * _current_rig->getPose();
    _last_pose = _current_rig->getPose();
    _last_relative_pose = _current_rig->getRelativePose();
    _last_imu = _current_rig->getImu();
    _all_poses.push_back(_last_pose);
	return true;
}

CameraRig* PoseCalculator::getOutputCameraRig() {
	return _current_rig;
}

bool PoseCalculator::hasFinished() {
	return _finished;
}

void PoseCalculator::finalize() {
	_finished = true;

	// Write down all the poses...
    std::ofstream poses_out("/mnt/WinPartition/Users/danie/workspace/dataset_0091"
                            "/output/raw_lidar_poses.txt");
	poses_out << _all_poses.size() << "\n";
	for(auto& pose: _all_poses){
		poses_out << pose << "\n";
	}
	poses_out.close();
}

void PoseCalculator::enablePoseInterpolation(bool enable) {
	_interpolate = enable;
}

} /* namespace cloud_processing */
