/*
 * CloudAccumulator.cpp
 *
 *  Created on: Jun 12, 2015
 *      Author: george
 */

#include "CloudAccumulator.h"
#include "../pipeline/CameraRig.h"
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/io.h>

#include <pcl/kdtree/kdtree_flann.h>

#include <fstream>
#include <iostream>

#include "../utility/helper_functions.h"

#define SQRT_2 1.414213563
#define BUNDLER_MAX_RAYS 2

/*
 * È l'ultimo modulo nella pipeline: riceve una nuvola e ha una nuvola globale.
 * Quando riceve una nuvola la aggiunge alla nuvola globale. Questo può essere fatto in maniera intelligente,
 * ovvero controllando che per ogni punto della nuova nuvola se ci sono dei punti intorno, se non ci sono lo aggiunge.
 */

namespace cloud_processing {

// Constructor
CloudAccumulator::CloudAccumulator(): Module("CloudAccumulator", false),
			_finished(false),
			_filter(nullptr),
			_current_rig(nullptr),
			_fusion_enabled(false),
			_discriminate_enabled(false),
			_viewer(nullptr),
			_bundler_save(false),
            _fuse_voxel_size(0.143f)
{ }

// Destructor
CloudAccumulator::~CloudAccumulator() { }

void CloudAccumulator::setScene(Scene* scene) { }

// Pointclouds are fused togheter
bool CloudAccumulator::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
	_current_rig = rig;
    if(!rig) {
        std::cout << "[CloudAccumulator] Received null rig\n";
        return false;
    }

	std::cout << name() << ": Received points = " << std::to_string(rig->getLidar().cloud.size()) << "\n";
    if(_fusion_enabled && _final_cloud.size() > 0){
        // fuse is the smart way to add a pointcloud to another
		fuse(rig->getCloud());
	}else{
        // ...while += is the dumb way
		_final_cloud += rig->getLidar().cloud;
	}
	Eigen::Vector4f min, max;
    // Get the minimum and maximum values on each of the 3 (x-y-z) dimensions in a given pointcloud
	pcl::getMinMax3D(rig->getCloud().cloud(), min, max);
	_all_rigs.push_back(rig);
	_boxes.push_back(RigBox(*rig));
	return true;
}

CameraRig* CloudAccumulator::getOutputCameraRig() {
	return _current_rig;
}

bool CloudAccumulator::hasFinished() {
	return _finished;
}

// finalize function
void CloudAccumulator::finalize() {
	// If already finished, then return
	if(_finished) return;

    // Check if discriminate was enabled and perform it.
    // _discriminate_enabled abilita la decimazioni dei punti in base alla loro curvatura,
    // i punti su una superficie piana verranno decimati in modo più aggressivo.
    if(_discriminate_enabled){
		// Setup the normal estimator
		boost::shared_ptr<pcl::search::KdTree<Cloud::pointType> > tree;
		tree = boost::shared_ptr<pcl::search::KdTree<Cloud::pointType> >(new pcl::search::KdTree<Cloud::pointType> ());
		_normal_estimation.setSearchMethod (tree);
		_normal_estimation.setKSearch (50);

		// Calculate curvature of all points..
		pcl::PointCloud<pcl::PointNormal> points_curvature;
		_normal_estimation.setInputCloud(_final_cloud.cloud_shared_ptr());
		_normal_estimation.compute(points_curvature);
		pcl::copyPointCloud(_final_cloud.cloud(), points_curvature);

		// Get the max min curvature
		double min_curvature = std::numeric_limits<double>::max();
		double max_curvature = 0.0;
		for(auto& point: points_curvature.points){
			if(point.curvature < min_curvature){
				min_curvature = point.curvature;
			}
			else if(point.curvature > max_curvature){
				max_curvature = point.curvature;
			}
		}

		std::cout << name() << " >> max_curvature = " << max_curvature
				<< ", min_curvature = " << min_curvature << "\n";
		// Divide the min max interval in slots
		double number_of_slots = 2;
		double half_curvature = (max_curvature + min_curvature)/3.0;
		double slot_interval = (half_curvature - min_curvature)/number_of_slots;

		// Now create the necessary amount of clouds
		std::vector<Cloud> slot_clouds;
		slot_clouds.resize(number_of_slots);

		// Assign each point to its slot_cloud based on its curvature
		Cloud::pointType copy_point;
		int slot_offset;
		for(auto& point: points_curvature.points){
			pcl::copyPoint(point, copy_point);
			if(point.curvature < half_curvature){
				slot_clouds[1] += copy_point;
			}else{
				slot_clouds[0] += copy_point;
			}
			/*
			if(point.curvature < half_curvature && point.curvature > min_curvature){
				slot_offset = static_cast<int>((half_curvature - point.curvature) / slot_interval);
				if(slot_offset > number_of_slots - 1){
					std::cout << "potential SegFault: half_curvature = " << half_curvature
							<< "   point.curvature = " << point.curvature << "\n";
					continue;
				}
				slot_clouds[slot_offset] += copy_point;
			}
			else if(point.curvature >= half_curvature){
				slot_clouds[0] += copy_point;
			}
			else{
				slot_clouds.back() += copy_point;
			}*/
		}

		// Prepare and filter the slot clouds accordingly
        double min_leaf = _fuse_voxel_size;//0.1;
		double max_leaf = 0.7;
		double slot_leaf_increment = (max_leaf - min_leaf)/number_of_slots;

		// The filter
		MultiFilter slot_filter;

		// For visualization purposes..
		auto duration = std::chrono::duration<double, std::milli>(4000);

		// For bundler saving purposes...
		std::vector<std::pair<Cloud::pointType, std::vector<int> > > campoints;
		std::vector<pcl::KdTreeFLANN<Cloud::pointType> > trees;
		// some additional search variables
		std::vector<int> search_indices;
		std::vector<float> sqr_distances;

		if(_bundler_save){
			// Get compute kd-trees of every cloud
			for(auto& rig: _all_rigs){
				trees.push_back(pcl::KdTreeFLANN<Cloud::pointType>());
				trees.back().setInputCloud(rig->getCloud().cloud_shared_ptr());
			}
		}

		// Add also the cloud to the final_cloud...
		// But first clear it
        _final_cloud.clear(); // Removes all points in a cloud and sets the width and height to 0.
		for(int i = 0; i < number_of_slots; i++){
            double leaf_size = min_leaf + i*slot_leaf_increment;
			slot_filter.clear();
            slot_filter.addVoxelGrid(leaf_size, leaf_size, leaf_size);
			slot_clouds[i].filter(slot_filter);
			_final_cloud += slot_clouds[i];

			// Save to bundler file
			if(_bundler_save){
				const float search_radius = SQRT_2 * leaf_size/2.0;
				std::vector<int> indices;
				std::vector<float> sqr_distances;
				int point_i;

				#pragma omp parallel for default(shared) private(point_i, indices, sqr_distances)
				for(point_i = 0; point_i < slot_clouds[i].size(); point_i++){
					auto& p = slot_clouds[i][point_i];
					std::vector<PointRay> rays;

                    int tree_i;
					for(tree_i = 0; tree_i < trees.size(); tree_i++){
						if(_boxes[tree_i].isOutsideBBox(p)){
							continue;
						}
						if(trees[tree_i].radiusSearch(p, search_radius, indices, sqr_distances)){
							// Get the ray squared distance
							float x = p.x - _boxes[tree_i].origin[0];
							float y = p.y - _boxes[tree_i].origin[1];
							float z = p.z - _boxes[tree_i].origin[2];

							PointRay ray;
							ray.cam_index = tree_i;
							ray.ray_sqr_length = x*x + y*y + z*z;

							rays.push_back(ray);
						}
					}

					// Sort the vector for shortest rays first
					std::sort(rays.begin(), rays.end());

					// Create the vector to put the closest rays
					std::vector<int> ray_indices;

					for(int i = 0; i < BUNDLER_MAX_RAYS && i < rays.size(); i++){
						ray_indices.push_back(rays[i].cam_index);
					}

					if(ray_indices.empty()){
						std::cerr << "Point " << p << " has no origin ray!!...\n";
						continue;
					}

					#pragma omp critical
					{
						campoints.push_back(std::pair<Cloud::pointType, std::vector<int> >(p, ray_indices));
					}
				}
			}

			// For visualization purposes...
			if(_viewer){
				_viewer->clear();
				_viewer->addPointCloud(slot_clouds[i], "discriminate", visualization::Color::CYAN);
                std::this_thread::sleep_for(duration);
			}
		} // End of point cloud slots fusion loop

		// Save to bundler if requested
		if(_bundler_save){
			std::cout << name() << " >> Saving bundler file...\n";
			std::ofstream file_out(_bundler_file_points);

			// Write the header section
			file_out << "# Bundle file v0.3" << std::endl;
            // <num_cameras> <num_points>
			file_out << _all_rigs.size() << " " << campoints.size() << std::endl;

			// write down every point with its position, color and viewlist
			for(auto& campoint: campoints){
				// <position>
				file_out << campoint.first.x << " "
						<< campoint.first.y << " "
						<< campoint.first.z << std::endl;
				// <color>
				file_out << "0 0 0" << std::endl;

				// <view list>
				// <list length>
				file_out << campoint.second.size() << " ";
				// <camera> <key> <x> <y>
				for(auto& ray: campoint.second){
					file_out << ray << " 0 0 0 ";
				}
				file_out << std::endl;
			}

			// close the file
			file_out.close();

			// write down camera positions
			std::ofstream cams_out(_bundler_file_cameras);
            std::ofstream lidars_out(_bundler_file_lidars);
			// header: number of cameras
			// each row represents a camera position
			cams_out << _all_rigs.size() << std::endl;
            lidars_out << _all_rigs.size() << std::endl;
			for(auto& rig: _all_rigs){
                // Daniele Code
                Eigen::Matrix4d cameraMatrix = pipeline::CameraRig::getCalibration().calibration_velo_to_cam * rig->getPose();
                cams_out << cameraMatrix << std::endl;
                auto col_3 = rig->getPose().col(3);
                lidars_out << col_3[0] << " " << col_3[1] << " " << col_3[2] << std::endl;

                //cams_out << rig->getPose() << std::endl;

                //std::cout << "rig->getPose() \n" << rig->getPose() << std::endl;
                //std::cout << "rig->getLidar().pose \n" << rig->getLidar().pose << std::endl;

                // Postica code
                //auto col_3 = rig->getPose().col(3);
                //cams_out << col_3[0] << " " << col_3[1] << " " << col_3[2] << std::endl;
			}
			// close the file
			cams_out.close();

			std::cout << "Bundler files have been saved successfully" << std::endl;
		}
		// Done
	}
    else if(_filter){ // !_discriminate_enabled
		_final_cloud.filter(*_filter);
		if(_bundler_save){
            saveToFileBundler(_bundler_file_points, _bundler_file_cameras, _bundler_file_lidars, 0.2f);
		}
	}
	_finished = true;
}

void CloudAccumulator::setOutputFilter(pcl::Filter<Cloud::pointType>& filter) {
	_filter = &filter;
}

void CloudAccumulator::enableDynamicFusion(bool enable, float fuse_voxel_radius) {
	_fusion_enabled = enable;
	if(enable){
		_fuse_voxel_size = fuse_voxel_radius;
	}
}

void CloudAccumulator::enableDiscriminatoryDownsampling(bool enable) {
	_discriminate_enabled = enable;
}

void CloudAccumulator::enableVisualization(visualization::CloudVisualizerSimple* visualizer) {
	_viewer = visualizer;
}

void CloudAccumulator::registerBundlerFileSaving(const std::string& points_file_out,
                                                 const std::string& cameras_file_out,
                                                 const std::string& lidars_file_out) {
	_bundler_file_points = points_file_out;
	_bundler_file_cameras = cameras_file_out;
    _bundler_file_lidars = lidars_file_out;

    createFolderFromFilename(_bundler_file_points);
    createFolderFromFilename(_bundler_file_cameras);
    createFolderFromFilename(_bundler_file_lidars);

	_bundler_save = !(cameras_file_out.empty() && points_file_out.empty());
}

void CloudAccumulator::fuse(const Cloud& other) {
	// Put the final cloud in kd-tree
	pcl::KdTreeFLANN<Cloud::pointType> tree;
	// TODO: Consider incrementally loading the tree
	// instead of reloading it completely every cycle
	tree.setInputCloud(_final_cloud.cloud_shared_ptr());
	// TODO: Set parameter like search radius
	float search_radius = _fuse_voxel_size;
	// some additional dummy variables
	std::vector<int> search_indices;
	std::vector<float> sqr_distances;

	// Check if visualization was required...
	if(_viewer){
		// For visualization purposes...
		Cloud skip_points_cloud;
		Cloud accepted_points_cloud;

		// Start the loop for every point in the new cloud
		for(auto& point: other.cloud().points){
			// If there are no points in final cloud within search radius
			// of the considered point, then add it to the final cloud
			if(tree.radiusSearch(point, search_radius, search_indices, sqr_distances) == 0){
				_final_cloud += point;
				accepted_points_cloud += point;
			}else{
				skip_points_cloud += point;
			}
		}
		// Clear the viewer and add the newly created clouds
		_viewer->clear();
		_viewer->setTitle("Point Cloud Fusion");
		_viewer->addPointCloud(_final_cloud, "final_cloud");
		_viewer->addPointCloud(accepted_points_cloud, "accepted_cloud", visualization::Color::GREEN);
		_viewer->addPointCloud(skip_points_cloud, "rejected_cloud", visualization::Color::RED);

		// Make a pause...
        auto duration = std::chrono::duration<double, std::milli>(1500);
        std::this_thread::sleep_for(duration);

		// Clear and load only the final cloud
		_viewer->clear();
		_viewer->addPointCloud(_final_cloud, "final_cloud", visualization::Color::GREY);
	}
	else {
		// Just do the simple stuff
		// Start the loop for every point in the new cloud
		for(auto& point: other.cloud().points){
			// If there are no points in final cloud within search radius
			// of the considered point, then add it to the final cloud
			if(tree.radiusSearch(point, search_radius, search_indices, sqr_distances) == 0){
				_final_cloud += point;
			}
		}
	}
}

// Viene chiamato in finalize()
void CloudAccumulator::saveToFileBundler(const std::string& filepath,
                                         const std::string & cameras_file_out,
                                         const std::string & lidars_file_out,
                                         float search_radius) {
	std::ofstream file_out(filepath);
	// used only as shortcut
	size_t nr_points = _final_cloud.size();
	size_t nr_cams = _all_rigs.size();

	// kd-trees for every camera_rig
	std::vector<pcl::KdTreeFLANN<Cloud::pointType> > trees;
	// cameras per point
	std::vector<int> camera_indices;

	// Write the header section
	file_out << "# Bundle file v0.3" << std::endl;
	// <num_cameras> <num_points>
	file_out << nr_cams << " " << nr_points << std::endl;

	// Get compute kd-trees of every cloud
	for(auto& rig: _all_rigs){
		trees.push_back(pcl::KdTreeFLANN<Cloud::pointType>());
		trees.back().setInputCloud(rig->getCloud().cloud_shared_ptr());
	}

	std::cout << "CA >> trees.size() = " << trees.size() << std::endl;

	// dummy vectors
	std::vector<int> indices;
	std::vector<float> sqr_distances;

	int camera_i = 0;
	// prepare the kd-trees of all clouds...
	for(auto& point: _final_cloud.cloud().points){
		// <position>
		file_out << point.x << " " << point.y << " " << point.z << std::endl;
		// <color>
		file_out << "0 0 0" << std::endl;

		// prepare view list
		camera_indices.clear();
		camera_i = 0;
		Eigen::Vector4f point_vector(point.data);
		for(auto& tree: trees){
			indices.clear();
			// check in which clouds point is present
			if(tree.radiusSearch(point, search_radius, indices, sqr_distances)){
				camera_indices.push_back(camera_i);
			}
			++camera_i;
		}

		// <view list>
		file_out << camera_indices.size() << " ";
		for(auto& cam_index: camera_indices){
			// <camera> <key> <x> <y>
			file_out << cam_index << " 0 0 0 ";
		}
		if(camera_indices.empty()){
			_test_cloud += point;
		}
		file_out << std::endl;
	}

	// close the file
	file_out.close();

    // write down lidar and camera positions
	std::ofstream cams_out(cameras_file_out);
    std::ofstream lidars_out(lidars_file_out);
	// header: number of cameras
	// each row represents a camera position
	cams_out << nr_cams << std::endl;
    lidars_out << nr_cams << std::endl;
	for(auto& rig: _all_rigs){
        auto col_3 = rig->getPose().col(3);
        // Only translation for the lidar
        lidars_out << col_3[0] << " " << col_3[1] << " " << col_3[2] << std::endl;
        // The whole pose for the camera
        cams_out << pipeline::CameraRig::getCalibration().calibration_velo_to_cam * rig->getPose() << std::endl;
	}
	// close the file
	cams_out.close();

	std::cout << "Bundler files have been saved successfully" << std::endl;
}

Cloud& CloudAccumulator::getFinalCloud() {
	return _final_cloud;
}

} /* namespace cloud_processing */

