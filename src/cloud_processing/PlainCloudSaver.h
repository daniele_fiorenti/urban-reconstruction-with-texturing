/*
 * PlainCloudSaver.h
 *
 *  Created on: Sep 12, 2015
 *      Author: george
 */

#ifndef CLOUD_PROCESSING_PLAINCLOUDSAVER_H_
#define CLOUD_PROCESSING_PLAINCLOUDSAVER_H_

#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"

#include <Eigen/Core>

namespace cloud_processing {
using namespace pipeline;

class PlainCloudSaver: public Module {
public:
	PlainCloudSaver(std::string const &folder = "../output/plain_clouds/");

	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();
	void setScene(Scene * scene){ }

private:
	CameraRig * _current_rig;
	bool _finished;
	std::string _folderpath;
};
} /* namespace cloud_processing */

#endif /* CLOUD_PROCESSING_PLAINCLOUDSAVER_H_ */
