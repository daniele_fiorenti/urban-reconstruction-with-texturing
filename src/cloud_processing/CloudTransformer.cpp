/*
 * CloudTransformer.cpp
 *
 *  Created on: Jun 10, 2015
 *      Author: george
 */

#include "CloudTransformer.h"

namespace cloud_processing {

CloudTransformer::CloudTransformer(): Module("CloudTransformer", false),
		_current_rig(nullptr), _finished(false), _last_pose(Eigen::Matrix4d::Identity())
{ }

CloudTransformer::~CloudTransformer() { }

inline void CloudTransformer::setScene(Scene* scene) { }

// Trasforma le pointcloud del lidar della rig rispetto alla pose globale
bool CloudTransformer::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
	_current_rig = rig;
	if(!rig){
        std::cout << "[CloudTransformer] Received null rig\n";
		return false;
	}
	/*
	auto final_transform = _last_pose.inverse() * _current_rig->getRelativePose();
	_last_pose = _current_rig->getPose();
	_current_rig->getLidar().cloud.transform(final_transform);
	*/
	_current_rig->getLidar().cloud.transform(_current_rig->getPose());
	return true;
}

inline CameraRig* CloudTransformer::getOutputCameraRig() {
	return _current_rig;
}

inline bool CloudTransformer::hasFinished() {
	return _finished;
}

void CloudTransformer::finalize() {
	_finished = true;
}

} /* namespace cloud_processing */
