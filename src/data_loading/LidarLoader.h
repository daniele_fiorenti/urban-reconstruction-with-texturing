/*
 * LidarLoader.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef DATA_LOADING_LIDARLOADER_H_
#define DATA_LOADING_LIDARLOADER_H_

#include "../pipeline/Module.h"

namespace data_loading {
using namespace pipeline;

class LidarLoader: public Module {
public:
	LidarLoader(std::string const & folderpath, std::string const & timestamp_file);
	virtual ~LidarLoader();

	void setBoundingBoxLimits(float x, float y, float z);
	void setFilesRange(size_t min, size_t max);

	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();

private:
	std::vector<Timestamp> _timestamps;
	std::vector<std::string> _filenames;
	int _progress;
	float _limit_x, _limit_y, _limit_z;
	CameraRig* _current_rig;
	Scene * _scene;
};

} /* namespace data_loading */

#endif /* DATA_LOADING_LIDARLOADER_H_ */
