/*
 * IMULoader.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef DATA_LOADING_IMULOADER_H_
#define DATA_LOADING_IMULOADER_H_

#include "../pipeline/Module.h"

namespace data_loading {
using namespace pipeline;

class IMULoader: public Module {
public:
	IMULoader(std::string const & folderpath, std::string const & timestamp_filename);
	virtual ~IMULoader();

	void setFilesRange(size_t min, size_t max);

	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();

private:
	std::vector<IMU> _imu;
	int _progress;
    int _reg_id;
	Scene* _scene;
	CameraRig * _current_rig;
};

} /* namespace data_loading */

#endif /* DATA_LOADING_IMULOADER_H_ */
