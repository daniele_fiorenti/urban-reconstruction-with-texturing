/*
 * LidarLoader.cpp
 *
 *  Created on: Jun 17, 2016
 *      Author: Daniele
 */

#include "PictureLoader.h"

#include "../utility/helper_functions.h"
#include "../utility/info_printer.h"

#include <boost/filesystem.hpp>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

/*
 * Questo modulo popola l'istanza di Camera della current_rig
 */

namespace data_loading {

PictureLoader::PictureLoader(const std::string& folderpath, const std::string& timestamp_file) : Module("PictureLoader", true),
    _progress(0),
    _current_rig(nullptr),
    _scene(nullptr),
    _calibration(Eigen::Matrix4d::Identity())
{
    // Check whether the folderpath passed to the constructor is valid
    namespace fs = boost::filesystem;
    fs::path work_dir(folderpath);
    fs::directory_iterator null_iter;
    if(!fs::exists(work_dir) || !fs::is_directory(work_dir)){
        IERROR("LidarLoader", " Unable to open velodyne directory: " + folderpath);
        //PROFILER_STOP_TIMER(TIMER_DATASET_LOAD);
        return;
    }

    // Save all pictures (.png files) into vector _filenames
    for(fs::directory_iterator dir_iter(work_dir); dir_iter != null_iter; ++dir_iter){
        if(fs::is_regular_file(dir_iter->status()) && dir_iter->path().extension().string() == ".png"){
            _filenames.push_back(dir_iter->path().string());
        }
    }

    // Alphabetic sorting of _filenames
    std::sort(_filenames.begin(), _filenames.end());

    // Leggo il file dei timestamp e li converto con la funzione apposita
    loadTimestampsFromFile(_timestamps, timestamp_file);

    std::cout << "Module " << name() << ": finished loading, vector size = " << _filenames.size() << "\n";
}

PictureLoader::~PictureLoader() { }

bool PictureLoader::process(CameraRig* rig, int iteration_index) {
    //std::cout << name() << ": Received rig_address = " << rig << "\n";
    _progress = iteration_index; //Uso una variabile _progress per sapere a che indice sono
    _current_rig = rig; // !!!
    if(hasFinished()){
        return true;
    }
    if(!rig){ //Guardo se la cameraRig è null
        _current_rig = &_scene->newCameraRig(); //Se non lo è la salvo in current_rig, mi serve per tirare fuori il risultato
    }

    // Carico le png
    cv::Mat &image = _current_rig->getCamera().image.picture;

    image = cv::imread(_filenames[_progress], CV_LOAD_IMAGE_COLOR);
    if(! image.data )                              // Check for invalid input
    {
        std::cout <<  "Could not open or find the image" << std::endl ;
        _scene->removeLastRig(); // Se l'immagine non può essere caricata la tolgo
        return false; // E dico che è andata male
    }
    _current_rig->getCamera().timestamp = _timestamps[_progress]; // Altrimenti aggiungo il timestamp del Lidar nella CameraRig

    // _current_rig->getCamera().pose = _calibration;

    // Mostro l'immagine caricata per 2 secondi
    // cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE ); // Create a window for display.
    // cv::imshow( "Display window", image );                    // Show our image inside it.
    // cv::waitKey(2000);                                        // Wait for 2 seconds in the window


    // std::cout << "=== CALIBRATION === :\n" << _calibration << std::endl;
    return true;
}

CameraRig* PictureLoader::getOutputCameraRig() { // Ritorno il current rig calcolato nella process
    return _current_rig;
}

void PictureLoader::setScene(Scene* scene) {
    _scene = scene;
}

void PictureLoader::setFilesRange(size_t min, size_t max) { // Possiamo dare il range di file da leggere
    size_t min_ = 0, max_ = 0;
    if(min > max){
        min_ = std::max(max, min_);
        max_ = std::min(min, _filenames.size());
    }else{
        min_ = std::max(min, min_);
        max_ = std::min(max, _filenames.size());
    }
    std::vector<std::string> new_vector(_filenames.begin() + min_, _filenames.begin() + max_);
    _filenames.swap(new_vector);
    std::vector<Timestamp> new_timestamps(_timestamps.begin() + min_, _timestamps.begin() + max_);
    _timestamps.swap(new_timestamps);
}

// Il modulo PictureLoader ha finito di eseguire solo quando ho finito di processare tutti i file.
inline bool PictureLoader::hasFinished() {
    return !(_progress < _filenames.size());
}

} /* namespace data_loading */
