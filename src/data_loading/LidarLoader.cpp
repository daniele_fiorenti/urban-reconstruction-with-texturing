/*
 * LidarLoader.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#include "LidarLoader.h"

#include "../utility/helper_functions.h"
#include "../utility/info_printer.h"

#include <boost/filesystem.hpp>
#include <fstream>

namespace data_loading {

// Constructor
// LidarLoader inherit from Module, name="LidarLoader", is_asynch=true
LidarLoader::LidarLoader(const std::string& folderpath, const std::string& timestamp_file) : Module("LidarLoader", true),
      _progress(0),
      _current_rig(nullptr),
      _scene(nullptr),
      // x,y e z represents a bounding box, infinity is default value
      _limit_x(std::numeric_limits<float>::infinity()),
	  _limit_y(std::numeric_limits<float>::infinity()),
      _limit_z(std::numeric_limits<float>::infinity())
{
    // Check whether the folderpath passed to the constructor is valid
	namespace fs = boost::filesystem;
    fs::path work_dir(folderpath);
	fs::directory_iterator null_iter;
	if(!fs::exists(work_dir) || !fs::is_directory(work_dir)){
		IERROR("LidarLoader", " Unable to open velodyne directory: " + folderpath);
		//PROFILER_STOP_TIMER(TIMER_DATASET_LOAD);
		return;
	}

    // Save all pointclouds names (.bin files) into vector _filenames
	for(fs::directory_iterator dir_iter(work_dir); dir_iter != null_iter; ++dir_iter){
		if(fs::is_regular_file(dir_iter->status()) && dir_iter->path().extension().string() == ".bin"){
			_filenames.push_back(dir_iter->path().string());
		}
	}

    // Alphabetic sorting of _filenames
	std::sort(_filenames.begin(), _filenames.end());

    // Leggo il file dei timestamp e li converto con la funzione apposita
	loadTimestampsFromFile(_timestamps, timestamp_file);

	std::cout << "Module " << name() << ": finished loading, vector size = " << _filenames.size() << "\n";
}
LidarLoader::~LidarLoader() { }

// Faccio l'override del metodo process
bool LidarLoader::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
    _progress = iteration_index; //Uso una variabile _progress per sapere a che indice sono
    _current_rig = rig; // !!!
	if(hasFinished()){
		return true;
	}
    if(!rig){ //Guardo se la cameraRig è null
        _current_rig = &_scene->newCameraRig(); //Se non lo è la salvo in current_rig, mi serve per tirare fuori il risultato
	}
    float dummy; //dummy serve perché l'intensità non mi serve ma solo x,y,z
	Cloud &new_cloud = _current_rig->getLidar().cloud;
	Cloud::pointType const point;
    std::fstream input(_filenames[_progress], std::ios::in | std::ios::binary); //Qui leggo i file .bin del lidar
	while(input.good() && !input.eof()){
		input.read((char *) &point.x, 3*sizeof(float));
		input.read((char *) &dummy, sizeof(float));
        if(    (std::abs(point.x) < _limit_x) // Aggiungo solo se sta nei limiti
			&& (std::abs(point.y) < _limit_y)
			&& (std::abs(point.z) < _limit_z))
        {	new_cloud += point;  } // è stato implementato l'operatore += per aggiungere un punto
	}
	input.close();
    if(new_cloud.size() == 0){ // La nuvola ha punti?
        _scene->removeLastRig(); // Se non ne ha la tolgo
        return false; // E dico che è andata male
	}
    _current_rig->getLidar().timestamp = _timestamps[_progress]; // Altrimenti aggiungo il timestamp del Lidar nella CameraRig
//    _current_rig->getLidar().pose = _calibration;

    // std::cout << "=== LIDAR CALIBRATION ===\n" << _calibration << std::endl;
	return true;
}

CameraRig* LidarLoader::getOutputCameraRig() { // Ritorno il current rig calcolato nella process
	return _current_rig;
}

void LidarLoader::setScene(Scene* scene) {
	_scene = scene;
}

void LidarLoader::setBoundingBoxLimits(float x, float y, float z) {
	_limit_x = std::max(x, std::numeric_limits<float>::epsilon());
	_limit_y = std::max(y, std::numeric_limits<float>::epsilon());
	_limit_z = std::max(z, std::numeric_limits<float>::epsilon());
}

void LidarLoader::setFilesRange(size_t min, size_t max) { // Possiamo dare il range di file da leggere
	size_t min_ = 0, max_ = 0;
	if(min > max){
		min_ = std::max(max, min_);
		max_ = std::min(min, _filenames.size());
	}else{
		min_ = std::max(min, min_);
		max_ = std::min(max, _filenames.size());
	}
	std::vector<std::string> new_vector(_filenames.begin() + min_, _filenames.begin() + max_);
	_filenames.swap(new_vector);
	std::vector<Timestamp> new_timestamps(_timestamps.begin() + min_, _timestamps.begin() + max_);
	_timestamps.swap(new_timestamps);
}

// Il modulo LidarLoader ha finito di eseguire solo quando ho finito di processare tutti i file.
inline bool LidarLoader::hasFinished() {
	return !(_progress < _filenames.size());
}

} /* namespace data_loading */
