/*
 * LidarLoader.h
 *
 *  Created on: Jun 17, 2016
 *      Author: Daniele
 */

#ifndef DATA_LOADING_PICTURELOADER_H
#define DATA_LOADING_PICTURELOADER_H

#include "../pipeline/Module.h"

namespace data_loading {
using namespace pipeline;

class PictureLoader: public Module {
public:
    PictureLoader(std::string const & folderpath, std::string const & timestamp_file);
    virtual ~PictureLoader();

    void setFilesRange(size_t min, size_t max);

    void setScene(Scene * scene);
    bool process(CameraRig * rig, int iteration_index);
    CameraRig * getOutputCameraRig();
    bool hasFinished();

private:
    std::vector<Timestamp> _timestamps;
    std::vector<std::string> _filenames;
    Eigen::Matrix4d _calibration;

    int _progress;
    CameraRig* _current_rig;
    Scene * _scene;
};

} /* namespace data_loading */

#endif // DATA_LOADING_PICTURELOADER_H
