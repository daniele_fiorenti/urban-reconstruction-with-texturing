/*
 * IMULoader.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#include "IMULoader.h"

#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>

#include "../utility/helper_functions.h"

namespace data_loading {

IMULoader::IMULoader(const std::string& folderpath, const std::string& timestamp_filename)
    : Module("IMULoader", false), _progress(0), _reg_id(0), _current_rig(nullptr), _scene(nullptr) { //Passo false perché è leggero

    namespace fs = boost::filesystem;
	fs::path work_dir(folderpath);
	fs::directory_iterator null_iter;
	if(!fs::exists(work_dir) || !fs::is_directory(work_dir)){
        //IERROR("KittiUtility:IMUread", "IMU Read: Unable to open directory: " + folderpath); //Questo IERROR è un sistema di logging che è implementato
		return;
	}
	std::fstream input;
	std::vector<std::string> filenames;
	for(fs::directory_iterator dir_iter(work_dir); dir_iter != null_iter; ++dir_iter){
		if(fs::is_regular_file(dir_iter->status()) && dir_iter->path().extension().string() == ".txt"){
			filenames.push_back(dir_iter->path().string());
		}
	}
	std::sort(filenames.begin(), filenames.end());

	std::string line;

	for(int i = 0; i < filenames.size(); i++){

		input.open(filenames[i].c_str(), std::ios::in);

		if(!input.good()){
			IERROR("IMULoader", "Could not read file: " + filenames[i]);
			input.close();
			continue;
		}
		input.seekg(0, std::ios::beg);
		std::getline(input, line);
		_imu.push_back(IMU(line));
		input.close();
	}

	std::vector<Timestamp> times;
	if(!loadTimestampsFromFile(times, timestamp_filename)){
		IERROR("IMULoader", "error retrieving time stamps file");
		//throw std::invalid_argument("error reading timestamps file");
	}
	int i = 0;
	for(auto& imu: _imu){
		imu.timestamp = times[i++];
	}

	std::cout << "Module " << name() << ": finished loading, vector size = " << _imu.size() << "\n";
}

IMULoader::~IMULoader() {
}

bool IMULoader::process(CameraRig* rig, int iteration_index) {
	//std::cout << name() << ": Received rig_address = " << rig << "\n";
	_progress = iteration_index;
	_current_rig = rig;

	if(hasFinished()){
		return true;
	}

	if(!rig){
		_current_rig = &_scene->newCameraRig();
	}

	_current_rig->setImu(_imu[_progress]);
    _current_rig->setID(_reg_id);
    _reg_id++;
	return true;
}

CameraRig* IMULoader::getOutputCameraRig() {
	return _current_rig;
}

inline void IMULoader::setScene(Scene* scene) {
	_scene = scene;
}

inline bool IMULoader::hasFinished() {
	return !(_progress < _imu.size());
}

void IMULoader::setFilesRange(size_t min, size_t max) {
	size_t min_ = 0, max_ = 0;
	if(min > max){
		min_ = std::max(max, min_);
		max_ = std::min(min, _imu.size());
	}else{
		min_ = std::max(min, min_);
		max_ = std::min(max, _imu.size());
	}

    _reg_id = min;

	std::vector<IMU> new_vector(_imu.begin() + min_, _imu.begin() + max_);
	_imu.swap(new_vector);
}

} /* namespace data_loading */



