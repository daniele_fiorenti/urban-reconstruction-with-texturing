/*
 * CalibrationLoader.cpp
 *
 *  Created on: Jun 30, 2016
 *      Author: Daniele
 */

#include "CalibrationLoader.h"
#include "../pipeline/CameraRig.h"
#include <fstream>
#include <boost/filesystem.hpp>

namespace data_loading {

CalibrationLoader::CalibrationLoader() {}

void CalibrationLoader::loadCalibrationVeloToCam(const std::string calibration_file){
    std::fstream input(calibration_file.c_str(), std::ios::in);
    if(!input.good() || !boost::filesystem::is_regular_file(calibration_file)){
        IERROR("CalibrationLoader", "Calibration Read: Could not read file: " + calibration_file);
        input.close();
        return;
    }
    input.seekg(0, std::ios::beg);
    std::string line;
    std::stringstream ss;

    std::string dummy;

    Eigen::Matrix4d mat;
    //cout << "---------- Calibration reading: .... ";

    //Skip the first line is the calibration date, so skip it
    std::getline(input, line);

    //The second line is the rotation matrix... Parse it
    std::getline(input, line);
    ss << line;
    //Drop the "R:" from the beginning of the line
    ss >> dummy;
    //Parse the doubles..
    for(int i = 0; i < mat.rows()-1; i++){
        for(int j = 0; j < mat.cols()-1; j++){
            ss >> mat(i,j);
        }
    }
    //The third line should be the translation vector, starts with T:
    std::getline(input, line);
    ss.clear();
    ss << line;
    ss >> dummy;
    ss >> mat(0,3);
    ss >> mat(1,3);
    ss >> mat(2,3);

    mat.row(3) << 0.0f, 0.0f, 0.0f, 1.0f;

    pipeline::CameraRig::getCalibration().calibration_velo_to_cam = mat;

    input.close();
}

void CalibrationLoader::loadCalibrationImuToVelo(const std::string calibration_file){

    std::fstream input(calibration_file.c_str(), std::ios::in);
    if(!input.good() || !boost::filesystem::is_regular_file(calibration_file)){
        IERROR("CalibrationLoader", "Calibration Read: Could not read file: " + calibration_file);
        input.close();
        return;
    }
    input.seekg(0, std::ios::beg);
    std::string line;
    std::stringstream ss;

    std::string dummy;

    Eigen::Matrix4d mat;
    //cout << "---------- Calibration reading: .... ";

    //Skip the first line is the calibration date, so skip it
    std::getline(input, line);

    //The second line is the rotation matrix... Parse it
    std::getline(input, line);
    ss << line;
    //Drop the "R:" from the beginning of the line
    ss >> dummy;
    //Parse the doubles..
    for(int i = 0; i < mat.rows()-1; i++){
        for(int j = 0; j < mat.cols()-1; j++){
            ss >> mat(i,j);
        }
    }
    //The third line should be the translation vector, starts with T:
    std::getline(input, line);
    ss.clear();
    ss << line;
    ss >> dummy;
    ss >> mat(0,3);
    ss >> mat(1,3);
    ss >> mat(2,3);

    mat.row(3) << 0.0f, 0.0f, 0.0f, 1.0f;

    pipeline::CameraRig::getCalibration().calibration_imu_to_velo = mat;
    input.close();
}

void CalibrationLoader::loadCalibrationCamToRect(const std::string calibration_file){

    std::fstream input(calibration_file.c_str(), std::ios::in);
    if(!input.good() || !boost::filesystem::is_regular_file(calibration_file)){
        IERROR("CalibrationLoader", "Calibration Read: Could not read file: " + calibration_file);
        input.close();
        return;
    }
    input.seekg(0, std::ios::beg);
    std::string line;
    std::stringstream ss;

    std::string dummy;
    // Read the line
    while(std::getline(input, line, '\n')){
        // If the begin of the line match R_rect_ we save the camera

        if(line.find("R_rect") != std::string::npos){
            Eigen::Matrix4d mat;
            mat.setIdentity();
            ss << line;
            ss >> dummy;
            for(int i = 0; i < mat.rows()-1; i++){
                for(int j = 0; j < mat.cols()-1; j++){
                    ss >> mat(i,j);
                }
            }
            ss.clear();
            pipeline::CameraRig::getCalibration().camToRect.R_rect.push_back(mat);
        }
        else if(line.find("P_rect") != std::string::npos){
            Eigen::Matrix<double, 3, 4> mat;
            mat.setIdentity();
            ss << line;
            ss >> dummy;
            for(int i = 0; i < mat.rows(); i++){
                for(int j = 0; j < mat.cols(); j++){
                    ss >> mat(i,j);
                }
            }
            ss.clear();
            pipeline::CameraRig::getCalibration().camToRect.P_rect.push_back(mat);
        }
    }
    input.close();

    // Fill vector "velo_to_cam_rect" (corresponding to loadCalibration.m in MATLAB)
    Eigen::Matrix4d& R_rect00 = pipeline::CameraRig::getCalibration().camToRect.R_rect[0];
    std::vector<Eigen::Matrix<double, 3, 4>>& P_rect_vector = pipeline::CameraRig::getCalibration().camToRect.P_rect;
    std::vector<Eigen::Matrix4d>& velo_to_cam_rect_vector = pipeline::CameraRig::getCalibration().camToRect.velo_to_cam_rect;
    Eigen::Matrix4d& calib_velo_to_cam = pipeline::CameraRig::getCalibration().calibration_velo_to_cam;
    Eigen::Matrix4d Translation;

    for(int i = 0; i < pipeline::CameraRig::getCalibration().camToRect.R_rect.size(); i++){
        Translation.setIdentity();
        Translation(0,3) = P_rect_vector[i](0,3)/P_rect_vector[i](0,0);
        velo_to_cam_rect_vector.push_back(Translation*R_rect00*calib_velo_to_cam);
//        std::cerr << "velo_to_cam_rect_vector["<<i<<"]\n" << velo_to_cam_rect_vector[i] << std::endl;
    }
}

void CalibrationLoader::computeVeloToImagePlane() {
    std::vector<Eigen::Matrix<double, 3, 4>>& P_rect = pipeline::CameraRig::getCalibration().camToRect.P_rect;
    std::vector<Eigen::Matrix4d>& R_rect = pipeline::CameraRig::getCalibration().camToRect.R_rect;
    std::vector<Eigen::Matrix4d>& calibVeloToCamRect1 = pipeline::CameraRig::getCalibration().camToRect.velo_to_cam_rect;

    std::vector<Eigen::Matrix<double, 3, 4>>& velo_to_image_plane = pipeline::CameraRig::getCalibration().camToRect.velo_to_image_plane;
    for(int i = 0; i < P_rect.size(); i++){
        velo_to_image_plane.push_back(P_rect[i]*R_rect[i]*calibVeloToCamRect1[i]);
    }
}

} /* namespace data_loading */
