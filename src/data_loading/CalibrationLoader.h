/*
 * CalibrationLoader.h
 *
 *  Created on: Jun 30, 2016
 *      Author: Daniele
 *
 * This is NOT a module, loads the calibration files of the KITTI dataset into the static variables of CameraRig.
 */

#ifndef CALIBRATIONLOADER_H
#define CALIBRATIONLOADER_H

#include <string>

namespace data_loading {

class CalibrationLoader
{
public:
    CalibrationLoader();

    void loadCalibrationVeloToCam(const std::string calibration_file);
    void loadCalibrationImuToVelo(const std::string calibration_file);
    void loadCalibrationCamToRect(const std::string calibration_file);
    void computeVeloToImagePlane();
};

} /* namespace data_loading */

#endif // CALIBRATIONLOADER_H
