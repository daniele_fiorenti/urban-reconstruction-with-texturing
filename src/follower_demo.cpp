/*
 * follower_demo.cpp
 *
 *  Created on: Jul 19, 2015
 *      Author: george
 */

#include <iostream>

#include "visualization/CloudVisualizerSimple.h"
#include "visualization/CameraFollower.h"

/*
 * Fa un "video" delle camere nelle varie pose
 */

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
	using namespace visualization;
	/*
	std::cout << "************* TESTING FUTURES **************\n";
	for(int i = 0; i < 10; i++){
		std::cout << "-- launching the future now:\n";
		std::future<int> test_future(std::async([]()->int{
			std::cout << "   -- executing the future..\n";
			return 42;
		}));

		std::cout << "-- entering sleep stage...\n";
		auto duration_2s = std::chrono::duration<double, std::milli>(2000);
		std::this_thread::sleep_for(duration_2s);
		auto test_result = test_future.get();
		std::cout << "-- getting the future result: " << test_result << "\n";
	}
	std::cout << "********************************************\n";
	*/

	if(argc < 3){
		std::cerr << "Usage: follower_demo cameras_file.txt cloud.ply\n";
		return 0;
	}

	std::string cameras_file(argv[1]);
	std::string cloud_file(argv[2]);

	CloudVisualizerSimple visualizer;
	visualizer.startRendering();
	std::cout << "Visualizer started..\n";

	CameraFollower follower;
	follower.setVisualizer(&visualizer);
	std::cout << "Visualizer set..\n";

	follower.loadCameras(cameras_file);
	std::cout << "Cameras loaded..\n";

	follower.loadPointCloud(cloud_file);
	std::cout << "Cloud loaded..\n";

	std::cout << "Press [Enter] when ready to start the path following...";
	std::cin.get();

	follower.followPath(true, 0.5f);

	visualizer.waitStop();
}


