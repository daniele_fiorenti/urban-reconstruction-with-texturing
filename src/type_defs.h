/*
 * type_defs.h
 *
 *  Created on: Jun 17, 2015
 *      Author: george
 */

#ifndef TYPE_DEFS_H_
#define TYPE_DEFS_H_

#include <pcl/common/common_headers.h>
#include <pcl/filters/filter.h>

typedef pcl::PointXYZ pointType;
typedef pcl::PointCloud<pointType> pclCloud;

#endif /* TYPE_DEFS_H_ */
