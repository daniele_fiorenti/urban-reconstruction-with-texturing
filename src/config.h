#ifndef CONFIG
#define CONFIG

#define PROJ_DIR "/home/University/Thesis/Point-Cloud/qt-creator/point-cloud-filtering/"

/******** PARSING ********************/
#define SAVE_TO_PCD
#define SAVE_TO_PLY
#define SAVE_FOLDER		std::string("output/")

/******** REGISTRATION ***************/
#define REG_IMU_INTERPOLATE

#define REG_MAX_CORRESPONDENCE 		5.0
#define REG_MAX_ITERATIONS			40
#define REG_TRANSFORMATION_EPSILON	0.0005

#define REG_NDT_RESOLUTION			1.0
#define REG_NDT_STEP_SIZE			0.1

#define REG_DEMO_SUBITERATIONS		1
#define REG_DEMO_FRAME_DURATION 	10

//#define REG_IMU_INTERPOLATE

/**********************  PROFILER  ***********************/
#define PROFILING_ON
/*---- Timers ------*/
#define TIMER_REGISTRATION_COMPUTATION	"Registration computation time"
#define TIMER_REGISTRATION_PREPARATION	"Registration preparation time"
#define TIMER_DATASET_LOAD				"Loading time"
#define TIMER_PARSING					"Parsing time"
#define TIMER_ALL_EXECUTION_TIME		"Total execution time"


/********************** INFO_PRINTER *********************/
#define INFO_PRINTER_ON
#define INFO_PRINTER_DEBUG_ON
/*---- Topics -----*/
#define TOPIC_TRIP_INFO				"Trip Info"
#define TOPIC_REGISTRATION			"Registration Stats"
#define TOPIC_CLOUDS				"Point Clouds"

#endif // CONFIG

