/*
 * planar_segmentation.cpp
 *
 *  Created on: Jun 25, 2015
 *      Author: george
 */

#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/search/kdtree.h>
#include "../utility/cloud_file_opener.h"

#include "../visualization/CloudVisualizerSimple.h"

int
main (int argc, char** argv)
{
	double distance_th = 0.01;
	Eigen::Vector3f axis = Eigen::Vector3f::Zero();
	double eps_angle = 0;
	double min_radius = 0;
	double max_radius = 0;
	double samples_max_dist = 0;

	if(argc < 2){
		std::cout << "Wrong number of arguments: should be: \n" <<
				"planar_segmentation cloud_file.extension [distance_th] "
				<< " [axis.x axis.y axis.z] [eps_angle] [min_radius max_radius] [samples_max_dist]" << std::endl;
		return (-1);
	}
	if(argc >= 3){ // Distance threshold parameters
		distance_th = atof(argv[2]);
	}
	if(argc >= 6){ // Axis parameters
		axis[0] = atof(argv[3]);
		axis[1] = atof(argv[4]);
		axis[2] = atof(argv[5]);
	}
	if(argc <= 7){ // Epsilon angle
		eps_angle = atof(argv[6]);
	}
	if(argc >= 9){ // Min max radius
		min_radius = atof(argv[7]);
		max_radius = atof(argv[8]);
	}
	if(argc >= 10){ // Samples max dist
		samples_max_dist = atof(argv[9]);
	}
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	if ( !cloudIO::open(std::string(argv[1]), *cloud))
	{
		std::cout << "Cloud reading failed." << std::endl;
		return (-1);
	}

	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	// Optional
	seg.setOptimizeCoefficients (true);
	// Mandatory
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);

	// Setup all parameters
	seg.setDistanceThreshold (distance_th);
	if(axis != Eigen::Vector3f::Zero()){
		seg.setAxis(axis);
	}
	if(eps_angle != 0){
		seg.setEpsAngle(eps_angle);
	}
	if(min_radius > 0 && max_radius > min_radius){
		seg.setRadiusLimits(min_radius, max_radius);
	}
	if(samples_max_dist > 0){
		boost::shared_ptr<pcl::search::KdTree<pcl::PointXYZ> > tree_search;
		tree_search = boost::shared_ptr<pcl::search::KdTree<pcl::PointXYZ> >(new pcl::search::KdTree<pcl::PointXYZ> ());
		seg.setSamplesMaxDist(samples_max_dist, tree_search);
	}

	seg.setInputCloud (cloud);
	seg.segment (*inliers, *coefficients);

	if (inliers->indices.size () == 0)
	{
		PCL_ERROR ("Could not estimate a planar model for the given dataset.");
		return (-1);
	}

	std::cerr << "Model coefficients: " << coefficients->values[0] << " "
			<< coefficients->values[1] << " "
			<< coefficients->values[2] << " "
			<< coefficients->values[3] << std::endl;

	std::cerr << "Model inliers: " << inliers->indices.size () << std::endl;

	pcl::PointCloud<pcl::PointXYZ>::Ptr planes_cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr rest_cloud(new pcl::PointCloud<pcl::PointXYZ>);

	std::sort(inliers->indices.begin(), inliers->indices.end());
	int index_count = 0;
	for(int i = 0; i < cloud->size(); i++){
		auto& point = cloud->points[i];
		if(i == inliers->indices[index_count]){
			planes_cloud->push_back(point);
			index_count++;
		}else{
			rest_cloud->push_back(point);
		}
	}

	visualization::CloudVisualizerSimple visualizer;
	visualizer.startRendering();
	visualizer.addPointCloud(rest_cloud, "rest_cloud");
	visualizer.addPointCloud(planes_cloud, "plane_cloud", visualization::Color::RED);
	visualizer.waitStop();

	return (0);
}

