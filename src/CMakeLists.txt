cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(urban_reconst_texturing)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/../cmake_modules/")
find_package(PCL 1.7 REQUIRED)
find_package(Boost COMPONENTS filesystem system regex REQUIRED)
find_package(Eigen3 3.2.0 REQUIRED)
find_package(OpenCV 3.1.0 REQUIRED)
find_program(CCACHE_PROGRAM ccache)
find_package(OpenMP)
#find_package(Nicp REQUIRED)

include_directories(${PCL_INCLUDE_DIRS} ${EIGEN3_INCLUDE_DIR} ${OpenCV_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})
add_definitions(-DEIGEN_DONT_ALIGN)

#Set ccache for faster compilation-------------------------------------
if(CCACHE_PROGRAM)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
endif()

# Set c++11
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pipe -march=native")

# If no Build Type is set we set DEBUG --------------------------------
if( NOT CMAKE_BUILD_TYPE )
  set( CMAKE_BUILD_TYPE Debug CACHE STRING
       "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
       FORCE )
endif()
MESSAGE( STATUS "CMAKE_BUILD_TYPE: " ${CMAKE_BUILD_TYPE} )
#----------------------------------------------------------------------
# Else Use the command line Build Type --------------------------------
if(CMAKE_BUILD_TYPE STREQUAL "Release")
    MESSAGE( STATUS "Compiling Release" )
    SET(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -msse4.2")
#    SET(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -O3 -msse4.2")
    if (OPENMP_FOUND)
        MESSAGE( STATUS "Compiling with OpenMP" )
#        set (CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${OpenMP_C_FLAGS}")
        set (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${OpenMP_CXX_FLAGS}")
    endif()
    message("CMAKE_CXX_FLAGS_RELEASE is ${CMAKE_CXX_FLAGS_RELEASE}")
endif()
if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    MESSAGE( STATUS "Compiling Debug" )
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Og")
    # GProf flags -----------------------------------------------------
#    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -pg")
#    set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -pg")
#    set(CMAKE_SHARED_LINKER_FLAGS_DEBUG "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} -pg")
    #------------------------------------------------------------------
    if (OPENMP_FOUND)
        MESSAGE( STATUS "Compiling with OpenMP" )
        set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} ${OpenMP_CXX_FLAGS}")
    endif()
    message("CMAKE_CXX_FLAGS_DEBUG is ${CMAKE_CXX_FLAGS_DEBUG}")
endif()
# GProf is similar to Release but with GProf flags enabled --------------
if(CMAKE_BUILD_TYPE STREQUAL "GProf")
    MESSAGE( STATUS "Compiling GProf" )
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -msse4.2")
#    SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3 -msse4.2")
    # GProf flags -----------------------------------------------------
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pg")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pg")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pg")
    #------------------------------------------------------------------
    if (OPENMP_FOUND)
        MESSAGE( STATUS "Compiling with OpenMP" )
#        set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
        set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    endif()
    message("CMAKE_CXX_FLAGS is ${CMAKE_CXX_FLAGS}")
endif()
#----------------------------------------------------------------------

file(GLOB_RECURSE pclFiltering_SOURCE
                    filters/*.cpp
                    filters/*.h
                    filters/*.hpp
                    utility/*.cpp
                    utility/*.h
                    utility/*.hpp
                    registration/*.h
                    registration/*.hpp
                    filtering_main.cpp
                    )
add_executable (filtering ${pclFiltering_SOURCE})
target_link_libraries (filtering ${PCL_LIBRARIES} ${Boost_LIBRARIES}  ${OpenCV_LIBS})

file(GLOB_RECURSE urban_reconstruction_SOURCE
                    cloud_cleaning/*.cpp
                    cloud_cleaning/*.h
                    cloud_processing/*.cpp
                    cloud_processing/*.h
                    data_loading/*.cpp
                    data_loading/*.h
                    filters/*.cpp
                    filters/*.h
                    filters/*.hpp
                    pipeline/*.cpp
                    pipeline/*.h
                    utility/*.cpp
                    utility/*.h
                    utility/*.hpp
                    visualization/*.cpp
                    visualization/*.h
                    reconstruction_main.cpp
                    )
add_executable (reconstruction ${urban_reconstruction_SOURCE})
target_link_libraries (reconstruction ${PCL_LIBRARIES} ${Boost_LIBRARIES}  ${OpenCV_LIBS})

file(GLOB_RECURSE cd_eval_SOURCE
                    cloud_cleaning/*.cpp
                    cloud_cleaning/*.h
                    cloud_processing/*.cpp
                    cloud_processing/*.h
                    data_loading/*.cpp
                    data_loading/*.h
                    filters/*.cpp
                    filters/*.h
                    filters/*.hpp
                    pipeline/*.cpp
                    pipeline/*.h
                    utility/*.cpp
                    utility/*.h
                    utility/*.hpp
                    visualization/*.cpp
                    visualization/*.h
                    change_detection_evaluation.cpp
                    )
add_executable (eval_change_detection ${cd_eval_SOURCE})
target_link_libraries (eval_change_detection ${PCL_LIBRARIES} ${Boost_LIBRARIES}  ${OpenCV_LIBS})

file(GLOB_RECURSE poses_SOURCE
                    cloud_cleaning/*.cpp
                    cloud_cleaning/*.h
                    cloud_processing/*.cpp
                    cloud_processing/*.h
                    data_loading/*.cpp
                    data_loading/*.h
                    filters/*.cpp
                    filters/*.h
                    filters/*.hpp
                    pipeline/*.cpp
                    pipeline/*.h
                    utility/*.cpp
                    utility/*.h
                    utility/*.hpp
                    visualization/*.cpp
                    visualization/*.h
                    prepare_poses.cpp
                    )
add_executable (prepare_poses ${poses_SOURCE})
target_link_libraries (prepare_poses ${PCL_LIBRARIES} ${Boost_LIBRARIES}  ${OpenCV_LIBS})

file(GLOB_RECURSE path_following_SOURCE
                    utility/*.cpp
                    utility/*.h
                    utility/*.hpp
                    visualization/*.cpp
                    visualization/*.h
                    follower_demo.cpp
                    )
add_executable (follower_demo ${path_following_SOURCE})
target_link_libraries (follower_demo ${PCL_LIBRARIES} ${Boost_LIBRARIES}  ${OpenCV_LIBS})

file(GLOB_RECURSE mask_extraction_SOURCE
                    cloud_cleaning/*.cpp
                    cloud_cleaning/*.h
                    cloud_processing/*.cpp
                    cloud_processing/*.h
                    data_loading/*.cpp
                    data_loading/*.h
                    filters/*.cpp
                    filters/*.h
                    filters/*.hpp
                    mask_extraction/*.cpp
                    mask_extraction/*.h
                    pipeline/*.cpp
                    pipeline/*.h
                    utility/*.cpp
                    utility/*.h
                    utility/*.hpp
                    visualization/*.cpp
                    visualization/*.h
                    mask_extraction.cpp
                    )
add_executable (mask_extraction ${mask_extraction_SOURCE})
target_link_libraries (mask_extraction ${PCL_LIBRARIES} ${Boost_LIBRARIES}  ${OpenCV_LIBS})

#--------------------------------------------
# TOOLS PROGRAMS
#--------------------------------------------

#add_executable (region_growing_segmentation tools/region_growing_segmentation.cpp)
#target_link_libraries (region_growing_segmentation ${PCL_LIBRARIES} ${Boost_LIBRARIES})

#file(GLOB_RECURSE planar_segmentation_SOURCE
#		  utility/*.cpp
#		  visualization/*.cpp
#		  tools/planar_segmentation.cpp
#                  )
#add_executable (planar_segmentation ${planar_segmentation_SOURCE})
#target_link_libraries (planar_segmentation ${PCL_LIBRARIES} ${Boost_LIBRARIES})
