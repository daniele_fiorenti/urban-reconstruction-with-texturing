/*
 * MovingObjectsRemoval.cpp
 *
 *  Created on: Jul 1, 2015
 *      Author: george
 */

#include "MovingObjectsRemoval.h"

#include <pcl/octree/octree_pointcloud_voxelcentroid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/registration/registration.h>
#include <pcl/filters/grid_minimum.h>

#include <map>

#include "../filters/FloorRemoval.h"

//#define SIMPLE_POINT_CHECK

/*
 * È una primissima versione di un change detector, si può ignorare!!!
 */
namespace cloud_cleaning {

MovingObjectsRemoval::MovingObjectsRemoval(
		size_t buffer_size,
		float voxel_size,
		int min_neighbors,
		size_t min_buffer_in_buffer,
		visualization::CloudVisualizerSimple * vis)

	: Module("ObjectRemoval", true),
	  _finished(false),
	  _voxel_size(voxel_size),
	  _min_neighbors(min_neighbors - 1),
	  _max_buffer_size(2*buffer_size + 1),
	  _min_buffer_size(std::min(min_buffer_in_buffer, buffer_size)),
	  _viewer(vis),
	  _search_radius(1.45f*voxel_size/2.0f),
	  _critical_distance(5.0f)
{
	_filter.addFilter(new filtering::FloorRemoval(voxel_size, voxel_size));
	_filter.addVoxelGrid(voxel_size, voxel_size, voxel_size);
}

MovingObjectsRemoval::~MovingObjectsRemoval() { /* Do nothing for now */ }

void MovingObjectsRemoval::setScene(Scene* scene) { /* Do nothing for now */ }

bool MovingObjectsRemoval::process(CameraRig* rig, int iteration_index) {
	std::cout << "MovingObjectsRemoval: iteration " << std::to_string(iteration_index) << "\n";

	// If not finished do the standard work
	if(!_finished){
		// If input is null then return false...
		if(!rig){
			std::cout << "MM >> Received null rig\n";
			return false;
		}

		// Create a copy of input cloud to avoid side effects
		Cloud::pclCloud::Ptr new_cloud(new Cloud::pclCloud());
		pcl::transformPointCloud(rig->getLidar().cloud.cloud(), *new_cloud, rig->getPose());

		// Spin the buffer or add new octree
		std::shared_ptr<octree_wrapper> new_octree;
		if(_trees_buffer.size() < _max_buffer_size){
			new_octree = std::shared_ptr<octree_wrapper>(new octree_wrapper(_voxel_size));
		}else{
			new_octree = _trees_buffer.front();
			_trees_buffer.erase(_trees_buffer.begin());
		}

		// Setup the octree and its origin
		new_octree->octree.setInputCloud(new_cloud);
		//new_octree->octree.addPointsFromInputCloud();
		new_octree->origin = rig->getPose().cast<float>().block<3,1>(0,3);
		_trees_buffer.push_back(new_octree);

		// Add the rig to the queue
		// but skip the first one (almost useless)
		if(_trees_buffer.size() > _min_buffer_size){
			_rigs.push(rig);
		}
	}
	// Otherwise work with remaining rigs in queue
	else {
		// Remove octrees, two at a time.
		_trees_buffer.erase(_trees_buffer.begin());
		_trees_buffer.erase(_trees_buffer.begin());

		// Empty the _rigs queue if it has only one element
		if(_rigs.size() <= _min_buffer_size){
			std::queue<CameraRig*> empty_queue;
			std::swap(_rigs, empty_queue);
			_trees_buffer.clear();
			return true;
		}
	}
	// Start the removal algorithm if the buffer has odd size (for symmetry)...
	if(_rigs.size() > _min_buffer_size && (_trees_buffer.size() % 2)){
		removeMovingObjects();
		return true;
	}
	return false;
}

void MovingObjectsRemoval::removeMovingObjects() {
	auto &current_pose = _rigs.front()->getPose();
	auto &current_cloud = _rigs.front()->getLidar().cloud;
	Cloud temp_cloud;
	Cloud::indices_ptr indices(new std::vector<int>());
	std::vector<float> squared_distances;

	// Make a copy of original cloud to be voxelized
	temp_cloud = current_cloud;
	temp_cloud.filter(_filter);

	// Transform the temp_cloud to its pose
	temp_cloud.transform(current_pose);

	// Downsample temp_cloud
	//temp_cloud.filter(_voxel_grid);

	// Transform also the current cloud to its pose
	current_cloud.transform(current_pose);

	// Prepare an Octree for current cloud
	pcl::octree::OctreePointCloudSearch<Cloud::pointType> octree_current(0.1);
	octree_current.setResolution(0.1);
	octree_current.setInputCloud(current_cloud.cloud_shared_ptr());
	octree_current.addPointsFromInputCloud();

	// Some useful variables
	int half_way = _rigs.size() - 1;
	std::map<int, int> all_indices;
	float energy_value;

#ifndef SIMPLE_POINT_CHECK
	Eigen::Vector3f point_v;			// vector with absolute position of point
	Eigen::Vector3f sensor_origin = current_pose.cast<float>().block<3,1>(0,3);
	Eigen::Vector3f origin_to_point;	// vector A
	Eigen::Vector3f sensor_to_point;	// vector B
	std::vector<Eigen::Vector3f> origin_to_sensors; // Pre-computed C vectors
	std::vector<float> origin_to_sensors_norms;		// Norms of above vectors
	float origin_to_point_norm;			// keeps the norm of origin_to_point vector
	float ratio_t;						// A.C / |C|
	float ratio_p;						// A.B / |A|
#endif
	// Prepare a clean vector of octrees
	std::vector<std::shared_ptr<octree_wrapper> > trees;
	for(int i = 0; i < _trees_buffer.size(); i++){
		if(i == half_way){ continue; }
		if(_trees_buffer[i]->octree.getInputCloud()){
			trees.push_back(_trees_buffer[i]);
			//std::cout << "captured: " << i << "\n" << _trees_buffer[i].sensor_origin << "\n";
		}
	}

#ifndef SIMPLE_POINT_CHECK
	// Prepare a vector of distances and norms
	for(auto& tree: trees){
		origin_to_sensors.push_back(tree->origin - sensor_origin);
		origin_to_sensors_norms.push_back(origin_to_sensors.back().norm());
	}
#endif
	// Visualize all the clouds involved in computation
	if(_viewer){
		visualizePreComputationClouds(temp_cloud, 1000);
	}

	// Check for every point... [Kind of very slow...]
	int point_i = -1;
	int sensor_i;
	for(auto& point: temp_cloud.cloud().points){
		point_i++;
		sensor_i = 0;

		energy_value = 0;
		indices->clear();
#ifndef SIMPLE_POINT_CHECK
		point_v << point.x, point.y, point.z;
		origin_to_point = point_v - sensor_origin;
		origin_to_point_norm = origin_to_point.norm();

		if(origin_to_point_norm < _critical_distance){
#endif
			for(auto& tree: trees){
				if(tree->octree.radiusSearch(point, _voxel_size, *indices, squared_distances) > _min_neighbors){
					energy_value += 0.75f;
				}else{
					energy_value -= 1.0f;
				}
			}
#ifndef SIMPLE_POINT_CHECK
		}
		else{
			// Search for every previous cloud if point exists
			for(auto& tree: trees){
				sensor_to_point = point_v - tree->origin;
				ratio_t = abs(origin_to_point.dot(origin_to_sensors[sensor_i])) / origin_to_sensors_norms[sensor_i];
				ratio_p = abs(origin_to_point.dot(sensor_to_point)) / origin_to_point_norm;
				//std::cout << "===>  ratio_t = " << ratio_t << "  ratio_p = " << ratio_p << "\n";
				if(tree->octree.radiusSearch(point, _search_radius, *indices, squared_distances) > _min_neighbors){
					energy_value += ratio_p / ratio_t;
				}else{
					energy_value -= ratio_t / ratio_p;
				}

				++sensor_i;
			}
		}
#endif
		//std::cout << "===> energy_level = " << energy_value << "\n";
		// If energy value < 0 then we most probably have a moving object
		// and we need to remove it
		if(energy_value < 0 || std::isnan(energy_value)){
			octree_current.radiusSearch(point, _search_radius*1.3f, *indices, squared_distances);
			for(auto index: *indices){
				all_indices[index]++;
			}
		}
	}
	indices->clear();
	temp_cloud.clear();
	for(auto& index: all_indices){
		indices->push_back(index.first);
		temp_cloud += current_cloud[index.first];
	}

	current_cloud.removePoints(indices);

	if(_viewer){
		visualizePostComputationClouds(temp_cloud, current_cloud, 1000);
	}

	std::cout << "Object Removal: removed indices " << std::to_string(all_indices.size()) << "\n";
	std::cout << "Object Removal: points after cleaning = " << std::to_string(current_cloud.size()) << "\n";
}

CameraRig* MovingObjectsRemoval::getOutputCameraRig() {
	if(_rigs.empty()) return nullptr;
	auto return_rig = _rigs.front();
	_rigs.pop();
	return return_rig;
}

bool MovingObjectsRemoval::hasFinished() {
	return _finished && _rigs.empty();
}

void MovingObjectsRemoval::finalize() {
	_finished = true;
}

void MovingObjectsRemoval::visualizePreComputationClouds(
		const Cloud & sample_cloud, unsigned int pause_ms) {
	auto duration = std::chrono::duration<double, std::milli>(pause_ms);

	std::string previous = "previous";
	std::string next = "next";
	_viewer->clear();
	for(int i = 0; i < _rigs.size() - 1; i++){
		std::string name = previous + std::to_string(i);
		if(_trees_buffer[i]->octree.getInputCloud()){
			_viewer->addPointCloud(_trees_buffer[i]->octree.getInputCloud(), name, visualization::Color::GREEN);
		}
	}
	for(int i = _rigs.size(); i < _trees_buffer.size(); i++){
		std::string name = next + std::to_string(i);
		if(_trees_buffer[i]->octree.getInputCloud()){
			_viewer->addPointCloud(_trees_buffer[i]->octree.getInputCloud(), name, visualization::Color::YELLOW);
		}
	}

	std::this_thread::sleep_for(duration);

	_viewer->addPointCloud(sample_cloud, "sample_cloud", visualization::Color::CYAN);

	for(int i = 0; i < _rigs.size() - 1; i++){
		std::string name = previous + std::to_string(i);
		if(_trees_buffer[i]->octree.getInputCloud()){
			_viewer->removePointCloud(name);
		}
	}
	for(int i = _rigs.size(); i < _trees_buffer.size(); i++){
		std::string name = next + std::to_string(i);
		if(_trees_buffer[i]->octree.getInputCloud()){
			_viewer->removePointCloud(name);
		}
	}
	//std::this_thread::sleep_for(duration);
}

void MovingObjectsRemoval::visualizePostComputationClouds(const Cloud & rem_cloud,
		const Cloud & cur_cloud,
		unsigned int pause_ms) {
	auto duration = std::chrono::duration<double, std::milli>(pause_ms);

	_viewer->addPointCloud(cur_cloud, "current_cloud");
	_viewer->addPointCloud(rem_cloud, "indices_to_remove", visualization::Color::RED);
	_viewer->removePointCloud("sample_cloud");
	std::this_thread::sleep_for(duration);

	_viewer->removePointCloud("indices_to_remove");
	std::this_thread::sleep_for(duration);
}

void MovingObjectsRemoval::setMaxPreceedingClouds(size_t nr_clouds) {
	_max_buffer_size = nr_clouds * 2 + 1;
}

void MovingObjectsRemoval::setMinPreceedingClouds(size_t nr_clouds) {
	_min_buffer_size = std::min(_max_buffer_size / 2, nr_clouds);
}

void MovingObjectsRemoval::setSearchVoxelSize(float voxel_size) {
	_voxel_size = voxel_size;
	_filter.clear();
	_filter.addFilter(new filtering::FloorRemoval(voxel_size, voxel_size));
	_filter.addVoxelGrid(voxel_size, voxel_size, voxel_size);
}

void MovingObjectsRemoval::setSearchRadius(float search_radius) {
	_search_radius = search_radius;
}

size_t MovingObjectsRemoval::getMaxPreceedingClouds() {
	// Since trees buffer has max buffer size,
	// the return value is half that size
	return _max_buffer_size / 2;
}

size_t MovingObjectsRemoval::getMinPreceedingClouds() {
	return _min_buffer_size;
}

float MovingObjectsRemoval::getSearchVoxelSize() {
	return _voxel_size;
}

float MovingObjectsRemoval::getSearchRadius() {
	return _search_radius;
}

void MovingObjectsRemoval::setCriticalDistance(float critical_distance) {
	_critical_distance = std::abs(critical_distance);
}

float MovingObjectsRemoval::getCriticalDistance() {
	return _critical_distance;
}

void MovingObjectsRemoval::enableVisualization(
		visualization::CloudVisualizerSimple* visualizer) {
	_viewer = visualizer;
}

void MovingObjectsRemoval::disableVisualization() {
	std::string previous = "previous";
	std::string next = "next";
	for(int i = 0; i < _rigs.size() - 1; i++){
		std::string name = previous + std::to_string(i);
		if(_trees_buffer[i]->octree.getInputCloud()){
			_viewer->removePointCloud(name);
		}
	}
	for(int i = _rigs.size(); i < _trees_buffer.size(); i++){
		std::string name = next + std::to_string(i);
		if(_trees_buffer[i]->octree.getInputCloud()){
			_viewer->removePointCloud(name);
		}
	}
	_viewer->removePointCloud("current_cloud");
	_viewer->removePointCloud("sample_cloud");
	_viewer->removePointCloud("indices_to_remove");

	_viewer = nullptr;
}

} /* namespace cloud_cleaning */
