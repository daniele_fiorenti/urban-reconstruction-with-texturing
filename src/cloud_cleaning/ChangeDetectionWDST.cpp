/*
 * ChangeDetectionWDST.cpp
 *
 *  Created on: Jul 3, 2015
 *      Author: george
 */

#include "ChangeDetectionWDST.h"

#include <vector>
#include <set>

#include <pcl/octree/octree_pointcloud.h>
#include <pcl/octree/octree_impl.h>
#include <pcl/io/ply_io.h>

#include "../utility/mini_profiler.h"
#include "../utility/helper_functions.h"

#include "../filters/MultiFilter.h"
#include "../filters/FloorRemoval.h"
#include "../filters/PlaneSegmentation.h"

#include <set>

#define PI 3.14159265358979
#define _2PI 2.0*3.14159265358979

#define OMP_CHUNK_SIZE	1000

#define CATCH_NAN(x) \
	if(std::isnan(x)){ \
		std::cout << "Caught "#x" as NaN\n";\
		std::cin.get(); }\
	//else if(x == 0){ \
		std::cout << "Caught "#x" to be ZERO";\
		std::cin.get(); }\

namespace cloud_cleaning {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DST FRAME ==========================================================================================================
DSTframe::DSTframe()
    : _traj_norm(0.0), _shift(0.0), _debug(false), _viewer(nullptr), _rig_id(-1)
	,_points_computed(0), _avg_neighbors(0), _minmax_dist(1)
{ }

void DSTframe::setTrajectoryVector(const Eigen::Vector3f& traj) {
	_traj_vector = traj;
	_traj_normal = _traj_vector.normalized();
	_traj_norm = _traj_vector.norm();
}

void DSTframe::setCameraRig(CameraRig& rig) {
	// Save the rig id
	_rig_id = rig.getID();

	// Save the bounding box
	_bbox = RigBox(rig);

	// For visualization purposes
	_cloud = rig.getCloud();

	// Save the sensor origin
	_origin = rig.getPose().cast<float>().block<3,1>(0,3);

	// Copy the point cloud and filter it
	Cloud::pclCloud::Ptr input_cloud;
	if(_filter.empty()){
		input_cloud = rig.getCloud().cloud_shared_ptr();
	} else {
		_filter.filter(rig.getCloud().cloud_shared_ptr(), input_cloud);
	}

	auto& work_cloud = *input_cloud;
	// Resize the points
	// This data contains points transformed in local reference frame
	_points.resize(work_cloud.size());

	// Transform the cloud into spherical coordinates cloud
	pcl::PointCloud<pcl::PointXY>::Ptr tree_cloud(new pcl::PointCloud<pcl::PointXY>());
	tree_cloud->points.resize(work_cloud.size());

	double min_d = std::numeric_limits<double>::max();
	double max_d = 0;

	int point_i = 0;
	double x, y, z;

	#pragma omp parallel for \
	default(shared) \
	private(point_i, x, y, z)\
	schedule(dynamic, OMP_CHUNK_SIZE)
	// Transform each point into a spherical coordinates point
	//for(auto& point: input_cloud->points){
	for(point_i = 0; point_i < _points.size(); point_i++){
		auto& point = work_cloud[point_i];
		auto& p_i = _points[point_i];
		auto& p =  tree_cloud->points[point_i];

		// Move the point to local reference frame
		p_i.vec[0] = x = point.x - _origin[0];
		p_i.vec[1] = y = point.y - _origin[1];
		p_i.vec[2] = z = point.z - _origin[2];
		p_i.norm = p_i.vec.norm();

		// From now on x,y will become phi and theta
		// Phi calculation
		p.x = atan2(y,x);

		// Theta calculation
		p.y = atan2(sqrt(x*x + y*y),z);
		//p.y = acos(z/sqrt(x*x + y*y + z*z));

		if(p_i.norm < min_d){
			min_d = p_i.norm;
		}else if(p_i.norm > max_d){
			max_d = p_i.norm;
		}

		// Done transforming
	}

	// Set the spherical point representation
	// Add the newly created cloud to main kd-tree
	_kdtree.setInputCloud(tree_cloud);

	// Compute min max distance for weight normalization
	_minmax_dist = max_d - min_d;
}

void DSTframe::setFilter(const MultiFilter& filter) {
	_filter = filter;
}

void DSTframe::setParameters(DSTparameter parameter) {
	// Save the parameters
	_param = parameter;

	// Prepare the gaussian function
	_gaussian.precompute(_param.sigma_step);
	//std::cout << "Gaussian beams: " << _gaussian.beams << "\n";

	// Perform the convolution of error functions ===================================
	// Needed to set the shift and find the convoluted gaussian mean
	double max_occupation = 0;
	double max_emptiness = 0;
	// Shortcut variables
	auto& b_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;
	// Do the convolution
	for(double R = -3.0*_param.sigma_f;
            R <= 3.0*(_param.sigma_f + _param.lambda_thick);
			R += _param.sigma_step * _param.sigma_f){

		double occupation = 0;
		double emptyness = 0;
		double r;
		// sum up the inner loop
		for(int i = -beams; i <= beams; i++){
			r = R + _param.sigma_step * i * _param.sigma_f;
			if(r >= 0){
				occupation += b_values[i + beams] * exp(-0.5 * r * r / _param.sigma_f2);
			}else{
				emptyness += b_values[i + beams];
			}
		}

		// add the new occupation value
		_occupancy_gaussian.push_back(occupation);

		// add the new emptyness value
		_emptiness_gaussian.push_back(emptyness);

		// check for the maximum
		if(occupation > max_occupation){
			// set the shift value
			max_occupation = occupation;
			_shift = R;
		}
		if(emptyness > max_emptiness){
			max_emptiness = emptyness;
		}
	} // End of convolution

	// Normalize the occupation values back to [0-1]
	for(auto& value: _occupancy_gaussian){
		value = value / max_occupation;
	}

	// Normalize the emptiness values back to [0-1]
	for(auto& value: _emptiness_gaussian){
		value = value / max_emptiness;
	}

	//std::cout << "Gaussian sums: EMPTY + OCCUPIED:\n";
	// Normalize wrt to both emptiness and occupancy
	for(int i = 0; i < _occupancy_gaussian.size(); i++){
		double sum = _emptiness_gaussian[i] + _occupancy_gaussian[i];
		if(sum > 1.0){
			_emptiness_gaussian[i] /= sum;
			_occupancy_gaussian[i] /= sum;
		}
	}

	// Computing the cos_theta gaussian
	double theta_max = 0;
	for(double i = 0.0; i <= 3.0*_param.sigma_theta; i+= _param.sigma_step*_param.sigma_theta){
		double theta = exp(-0.5 * (i*i) / _param.sigma_theta2);
		_cos_theta_gaussian.push_back(theta);
		if(theta > theta_max){ theta_max = theta; }
//		std::cout << "\t" << i << ": " << theta << "\n";
	}
	for(auto& theta: _cos_theta_gaussian){
		theta /= theta_max;
	}

	std::cout << "----------- _shift = " << _shift << std::endl;
	// Done for the preparations...
}

MassConsistency DSTframe::computeConsistency(const Cloud::pointType& location) {
	_points_computed++;

	MassConsistency final_occupancy;
	final_occupancy = computeConsistencySimple(location);

	// For debug purposes...
	_avg_occupancy.emp += final_occupancy.emp;
	_avg_occupancy.occ += final_occupancy.occ;
	_avg_occupancy.unk += final_occupancy.unk;

	// Done.. return the final occupancy
	return final_occupancy;
}

// Searches for neighbors of input point ================================================
bool DSTframe::findNeighbors(const Eigen::Vector3f& search_point,
                             double search_radius, std::vector<LocalPoint>& out_neighbors,
                             int k_neighbors) {

	// Prepare the search point
	// Convert it to spherical coordinate system
	pcl::PointXY s_p;
	float p_x = search_point[0];
	float p_y = search_point[1];
	float p_z = search_point[2];
	s_p.x = atan2(p_y, p_x);
	s_p.y = atan2(sqrt(p_x*p_x + p_y*p_y),p_z);

	// Additional data
	std::vector<int> indices;
	std::vector<float> sqr_distances;

	// If there are points in search radius...
//	if(_kdtree.nearestKSearch(s_p, k_neighbors, indices, sqr_distances) > 0){
	if(_kdtree.radiusSearch(s_p, search_radius, indices, sqr_distances) > 0){
		int min_size = indices.size() > k_neighbors? k_neighbors: indices.size();
		out_neighbors.clear();
		out_neighbors.resize(min_size);

		// Retrieve the neighbors from cloud
		for(int n_i = 0; n_i < min_size; n_i++){
			out_neighbors[n_i] = _points[indices[n_i]];
		}
		// Set positive outcome...
		return true;
	}
	// No neighbors were found...
	return false;
}

// Searches for neighbors of input point and sort them based on norm =====================
bool DSTframe::findNeighborsSorted(const Eigen::Vector3f& search_point,
                                   double search_radius, std::vector<LocalPoint>& out_neighbors,
                                   int k_neighbors) {

	// Prepare the search point
	// Convert it to spherical coordinate system
	pcl::PointXY s_p;
	float p_x = search_point[0];
	float p_y = search_point[1];
	float p_z = search_point[2];
	s_p.x = atan2(p_y, p_x);
	s_p.y = atan2(sqrt(p_x*p_x + p_y*p_y),p_z);

	// Additional data
	std::vector<int> indices;
	std::vector<float> sqr_distances;

	// If there are points in search radius...
//	if(_kdtree.nearestKSearch(s_p, k_neighbors, indices, sqr_distances) > 0){
	if(_kdtree.nearestKSearch(s_p, k_neighbors, indices, sqr_distances) > 0){
		double sqr_d = search_radius * search_radius;
		out_neighbors.clear();

		// Retrieve the closest k neighbors from cloud
		int n_i = 0;
		while(sqr_d > sqr_distances[n_i] && n_i < indices.size()){
			out_neighbors.push_back(_points[indices[n_i]]);
			n_i++;
		}

		// Set positive outcome...
		return true;
	}
	// No neighbors were found...
	return false;
}


void DSTframe::computeRemovedPointsVisibility(RemovedPointsContainer& rem_points) {

	// A for loop for OpenMP
	int point_i = 0;
	#pragma omp parallel for \
						default(shared) \
						private(point_i) \
						schedule(dynamic, OMP_CHUNK_SIZE)
	for(point_i = 0; point_i < rem_points.size(); point_i++){

		// Get the removed OP and transform into local reference frame
		auto OP = rem_points.removed_points[point_i].point - _origin;

		// Since X is in the direction of the trajectory then
		// make a check for positive X... if negative, skip it
		if(OP[0] < 0){
			continue;
		}

		double OP_norm = OP.norm();

		// Find neighboring rays
		std::vector<LocalPoint> neighbors;

		if(!findNeighbors(OP, _param.seen_search_radius, neighbors)){
			// No neighbors were found... Move to next point
//			continue;
		}
		bool seen = true;
		// See for each neighbor if seen threshold was surpassed
		for(auto& OQ: neighbors){
			if(OP_norm - OQ.norm > _param.seen_threshold){
				seen = false;
				break;
			}
		}
		// If seen then add this view to point visibility
		if(seen){
			#pragma omp critical
			{
				rem_points.removed_points[point_i].view_idx.push_back(_rig_id);
			}
		}
	}

}

void DSTframe::computeConsistenciesMedian(const Cloud& cloud,
                                          std::vector<MassConsistency>& consistencies) {

	// The vector containing all consistencies
	//std::vector<MassConsistency> consistencies;
	#pragma omp critical
	{
		if(consistencies.size() != cloud.size()){
			consistencies.resize(cloud.size());
		}
	}
	auto& b_n_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;

	// Transform each point to local reference frame
	// and calculate its consistency map in this frame
	int point_i;

	// Some variable declarations
	MassConsistency new_mass;

#pragma omp parallel for \
		default(shared) \
		private(point_i) \
		schedule(dynamic, OMP_CHUNK_SIZE)
	for(point_i = 0; point_i < cloud.size(); point_i++){
		auto& P = cloud[point_i];

		// Check if in bounding box, if not then skip this point
		if(_bbox.isOutsideBBox3D(P)){
			//			new_mass.emp = 0.5;
			//			new_mass.occ = 0.5;
			//			new_mass.unk = 0;
			//			//			new_mass.roundTo(rounded_value);
			//
			//			#pragma omp critical
			//			{
			//				consistencies[point_i].fuse(new_mass);
			//			}
			continue;
		}

		// Get the point in local reference frame
		Eigen::Vector3f OP;
		OP[0] = P.x - _origin[0];
		OP[1] = P.y - _origin[1];
		OP[2] = P.z - _origin[2];
		double OP_norm = OP.norm();

		// Rounded value in case it is needed
		double rounded_value = 0.8 - 0.6*(OP_norm/_minmax_dist);

		// Find neighboring rays
		std::vector<LocalPoint> neighbors;

		// Search kd-tree using radius search
		if(!findNeighbors(OP, _param.search_radius, neighbors, _param.neighbors)){
			new_mass.emp = 0.6;
			new_mass.occ = 0.4;
			new_mass.unk = 0;
			//			new_mass.roundTo(rounded_value);

			#pragma omp critical
			{
				consistencies[point_i].fuse(new_mass);
			}
			continue;
		}

		// Compute consistency over all neighboring points
		double cons_emp;	// empty consistency
		double cons_occ;	// occupied consistency
		double cons_angle;	// angular consistency
		//double cons_normal; // normal consistency -> higher weight for close to point cameras

		// Shortcut for angular consistency
		double cos_theta_min = cos(3.0*_param.sigma_theta);
		double cos_theta_step = (1.0 - cos_theta_min)/_cos_theta_gaussian.size();

		// Sort the neighbors based on their length
//		std::sort(neighbors.begin(), neighbors.end());
		std::nth_element(neighbors.begin(), neighbors.begin() + neighbors.size()/2, neighbors.end());

		// And take the median...
		auto& OQ = neighbors[neighbors.size()/2];

		// Compute ray distance: r = OP2_norm - OQ_norm
		double ray_distance = OP_norm - OQ.vec.dot(OP.normalized());
		//			double ray_distance = OP_norm - OQ.norm;

		// Compute cos(theta) for angular consistency
		double cos_theta = OQ.vec.dot(OP)/(OQ.norm*OP_norm);

		// Compute consistencies ===============================.
		// Reset consistencies
		cons_emp = cons_occ = 0.0;

        int offset = static_cast<int>((ray_distance + 3.0*_param.sigma_f + _shift)/(_param.sigma_step*_param.sigma_f));
		// Compute occupancy and emptiness consistency
		if(offset < 0){
			cons_occ = 0.0;
			cons_emp = 1.0;
		}
		else if(offset < _occupancy_gaussian.size()){
			cons_occ = _occupancy_gaussian[offset];
			cons_emp = _emptiness_gaussian[offset];
		}else{
			cons_emp = 0.0;
			cons_occ = 0.0;
		}

		// Compute angular consistency
		if(cos_theta < cos_theta_min){
			cons_angle = 0;
		}else{
			int offset = static_cast<int>((1.0 - cos_theta) / cos_theta_step);
			cons_angle = _cos_theta_gaussian[offset];
		}

		// Compute masses: e1 = empty, o1 = occupied, u1 = unknown
		// Weight more occupy (do not multiply by angle gaussian)
		new_mass.occ = cons_occ;	// * cons_angle;
		new_mass.emp = cons_emp * cons_angle;
		new_mass.unk = 1.0 - new_mass.occ - new_mass.emp;
		//new_mass.rebalance();

		#pragma omp critical
		{
			// Fuse it to consistencies
			consistencies[point_i].fuse(new_mass);
		}
	} // End of each point loop
}

// SIMPLIFIED ALGORITHM =======================================================================
MassConsistency DSTframe::computeConsistencySimple(const Cloud::pointType& location) {
	MassConsistency new_mass;
	if(_bbox.isOutsideBBox(location)){
//		new_mass.emp = 0.6;
//		new_mass.occ = 0.4;
//		new_mass.unk = 0;

		return new_mass;
	}

	// Some useful shortcuts
	MassConsistency final_mass;
	auto& b_n_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;

	const double max_r_value = 0.8;
	const double delta_r_value = 0.2;

	// Transform location to local reference frame
	// also save it for later use...
	Eigen::Vector3f OP;
	OP[0] = location.x - _origin[0];
	OP[1] = location.y - _origin[1];
	OP[2] = location.z - _origin[2];

	auto OP_normalized = OP.normalized();
	double OP_norm = OP.norm();

	// Find neighboring rays
	std::vector<LocalPoint> neighbors;

	// Search kd-tree using radius search
	if(!findNeighbors(OP, _param.search_radius, neighbors, _param.neighbors)){
		new_mass.emp = 0.65;
		new_mass.occ = 0.35;
		new_mass.unk = 0;

		return new_mass;
	}

	double rounded_value = max_r_value - delta_r_value*(OP_norm/_minmax_dist);

	// Neighboring Points calculation ==========================
	// Compute consistency over all neighboring points
	double cons_emp;	// empty consistency
	double cons_occ;	// occupied consistency
	double cons_angle;	// angular consistency

	// Used to set final_mass
	bool first_pass = true;

	// Shortcut for angular consistency
	double cos_theta_min = cos(3.0*_param.sigma_theta);
	double cos_theta_step = (1.0 - cos_theta_min)/_cos_theta_gaussian.size();

	// Begin the neighborhood loop
	for(auto& OQ: neighbors){
		// Compute ray distance: r = OP2_norm - OQ_norm
		double ray_distance = OP_norm - OQ.vec.dot(OP_normalized);

		// Compute cos(theta) for angular consistency
		double cos_theta = OQ.vec.dot(OP)/(OQ.norm*OP_norm);

		// Compute consistencies ===============================.
		// Reset consistencies
		cons_emp = cons_occ = 0.0;

		int offset = static_cast<int>(
				(ray_distance + 3.0*_param.sigma_f + _shift)/(_param.sigma_step*_param.sigma_f));
		// Compute occupancy and emptiness consistency
		if(offset < 0){
			cons_occ = 0.0;
			cons_emp = 1.0;
		}
		else if(offset < _occupancy_gaussian.size()){
			cons_occ = _occupancy_gaussian[offset];
			cons_emp = _emptiness_gaussian[offset];
		}else{
			cons_emp = 0.0;
			cons_occ = 0.0;
		}

		// Compute angular consistency
		if(cos_theta < cos_theta_min){
			cons_angle = 0;
		}else{
			int offset = static_cast<int>((1.0 - cos_theta) / cos_theta_step);
			cons_angle = _cos_theta_gaussian[offset];
		}

		// Compute masses: e1 = empty, o1 = occupied, u1 = unknown
		// Weight more occupy (do not multiply by angle gaussian)
		new_mass.occ = cons_occ;// * cons_angle;
		new_mass.emp = cons_emp * cons_angle;
		new_mass.unk = 1.0 - new_mass.occ - new_mass.emp;
		//new_mass.rebalance();

		if(first_pass){
			final_mass = new_mass;
			first_pass = false;
		}else{
			final_mass.fuse(new_mass);
		}

	} // End of neighborhood loop

	final_mass.roundTo(rounded_value);

	return final_mass;
}

void DSTframe::computeConsistencies(const Cloud& original_cloud, std::vector<MassConsistency>& consistencies) {

	// The vector containing all consistencies
	//std::vector<MassConsistency> consistencies;
	#pragma omp critical
	{
		if(consistencies.size() != original_cloud.size()){
			consistencies.resize(original_cloud.size());
		}
	}
	auto& b_n_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;

	// Transform each point to local reference frame
	// and calculate its consistency map in this frame
	int point_i;

	// Some variable declarations
	MassConsistency new_mass, final_mass;
	const double max_r_value = 0.8;
	const double delta_r_value = 0.2;

	#pragma omp parallel for \
	default(shared) \
	private(point_i) \
	schedule(dynamic, OMP_CHUNK_SIZE)
	for(point_i = 0; point_i < original_cloud.size(); point_i++){
		auto& P = original_cloud[point_i];

		// Check if in bounding box, if not then skip this point
		if(_bbox.isOutsideBBox3D(P)){
//			new_mass.emp = 0.5;
//			new_mass.occ = 0.5;
//			new_mass.unk = 0;
//			//			new_mass.roundTo(rounded_value);
//
//			#pragma omp critical
//			{
//				consistencies[point_i].fuse(new_mass);
//			}
			continue;
		}

		// Get the point in local reference frame
		Eigen::Vector3f OP;
		OP[0] = P.x - _origin[0];
		OP[1] = P.y - _origin[1];
		OP[2] = P.z - _origin[2];
		double OP_norm = OP.norm();

		// Rounded value in case it is needed
		double rounded_value = max_r_value - delta_r_value*(OP_norm/_minmax_dist);

		// Find neighboring rays
		std::vector<LocalPoint> neighbors;

		// Search kd-tree using radius search
		if(!findNeighbors(OP, _param.search_radius, neighbors, _param.neighbors)){
			new_mass.emp = 0.6;
			new_mass.occ = 0.4;
			new_mass.unk = 0;
//			new_mass.roundTo(rounded_value);

			#pragma omp critical
			{
				consistencies[point_i].fuse(new_mass);
			}
			continue;
		}

		// Neighboring Points calculation ==========================
		// First some shortcuts
		final_mass = MassConsistency();

		// Compute consistency over all neighboring points
		double cons_emp;	// empty consistency
		double cons_occ;	// occupied consistency
		double cons_angle;	// angular consistency
		//double cons_normal; // normal consistency -> higher weight for close to point cameras

		// Used to set final_occupancy
		bool first_pass = true;

		// Shortcut for angular consistency
		double cos_theta_min = cos(3.0*_param.sigma_theta);
		double cos_theta_step = (1.0 - cos_theta_min)/_cos_theta_gaussian.size();

		// Begin the neighborhood loop
		for(auto& OQ: neighbors){
			// Compute ray distance: r = OP2_norm - OQ_norm
			double ray_distance = OP_norm - OQ.vec.dot(OP.normalized());
//			double ray_distance = OP_norm - OQ.norm;

			// Compute cos(theta) for angular consistency
			double cos_theta = OQ.vec.dot(OP)/(OQ.norm*OP_norm);

			// Compute consistencies ===============================.
			// Reset consistencies
			cons_emp = cons_occ = 0.0;

			int offset = static_cast<int>(
									(ray_distance + 3.0*_param.sigma_f + _shift)/(_param.sigma_step*_param.sigma_f));
			// Compute occupancy and emptiness consistency
			if(offset < 0){
				cons_occ = 0.0;
				cons_emp = 1.0;
			}
			else if(offset < _occupancy_gaussian.size()){
				cons_occ = _occupancy_gaussian[offset];
				cons_emp = _emptiness_gaussian[offset];
			}else{
				cons_emp = 0.0;
				cons_occ = 0.0;
			}

			// Compute angular consistency
			if(cos_theta < cos_theta_min){
				cons_angle = 0;
			}else{
				int offset = static_cast<int>((1.0 - cos_theta) / cos_theta_step);
				cons_angle = _cos_theta_gaussian[offset];
			}

			// Compute masses: e1 = empty, o1 = occupied, u1 = unknown
			// Weight more occupy (do not multiply by angle gaussian)
			new_mass.occ = cons_occ;// * cons_angle;
			new_mass.emp = cons_emp * cons_angle;
			new_mass.unk = 1.0 - new_mass.occ - new_mass.emp;
			//new_mass.rebalance();

			if(first_pass){
				final_mass = new_mass;
				first_pass = false;
			}else{
//				if(final_mass.emp > 0.2 && new_mass.occ > 0.2){
//					for(int i = 0; i < 6; i++){
//						final_mass.fuse(new_mass);
//					}
//				}
//				else{
					final_mass.fuse(new_mass);
//				}
			}

			//final_mass.rebalance();
		} // End of neighborhood loop

		// Add the cons_normal effect
		//final_mass.rebalance();
		final_mass.roundTo(rounded_value);
//		final_mass.roundTo(0.8);

		#pragma omp critical
		{
			// Fuse it to consistencies
			consistencies[point_i].fuse(final_mass);
		}
	} // End of each point loop
}


void DSTframe::computeConsistenciesNewRule(const Cloud& original_cloud, std::vector<MassConsistency>& consistencies) {

	// The vector containing all consistencies
	//std::vector<MassConsistency> consistencies;
	#pragma omp critical
	{
		if(consistencies.size() != original_cloud.size()){
			consistencies.resize(original_cloud.size());
		}
	}
	auto& b_n_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;

	// Transform each point to local reference frame
	// and calculate its consistency map in this frame
	int point_i;

	// Some variable declarations
	MassConsistency new_mass, final_mass;
	const double max_r_value = 0.8;
	const double delta_r_value = 0.2;

	#pragma omp parallel for \
	default(shared) \
	private(point_i) \
	schedule(dynamic, OMP_CHUNK_SIZE)
	for(point_i = 0; point_i < original_cloud.size(); point_i++){
		auto& P = original_cloud[point_i];

		// Check if in bounding box, if not then skip this point
		if(_bbox.isOutsideBBox3D(P)){
//			new_mass.emp = 0.5;
//			new_mass.occ = 0.5;
//			new_mass.unk = 0;
//			//			new_mass.roundTo(rounded_value);
//
//			#pragma omp critical
//			{
//				consistencies[point_i].fuse(new_mass);
//			}
			continue;
		}

		// Get the point in local reference frame
		Eigen::Vector3f OP;
		OP[0] = P.x - _origin[0];
		OP[1] = P.y - _origin[1];
		OP[2] = P.z - _origin[2];
		double OP_norm = OP.norm();

		// Rounded value in case it is needed
		double rounded_value = max_r_value - delta_r_value*(OP_norm/_minmax_dist);

		// Find neighboring rays
		std::vector<LocalPoint> neighbors;

		// Search kd-tree using radius search
		if(!findNeighbors(OP, _param.search_radius, neighbors, _param.neighbors)){
//			new_mass.emp = 0.6;
//			new_mass.occ = 0.4;
//			new_mass.unk = 0;
////			new_mass.roundTo(rounded_value);
//
//			#pragma omp critical
//			{
//				consistencies[point_i].fuse(new_mass);
//			}
			continue;
		}

		// Neighboring Points calculation ==========================
		// First some shortcuts
		final_mass = MassConsistency();

		// Compute consistency over all neighboring points
		double cons_emp;	// empty consistency
		double cons_occ;	// occupied consistency
		double cons_angle;	// angular consistency
		//double cons_normal; // normal consistency -> higher weight for close to point cameras

		// Used to set final_occupancy
		bool first_pass = true;

		// Shortcut for angular consistency
		double cos_theta_min = cos(3.0*_param.sigma_theta);
		double cos_theta_step = (1.0 - cos_theta_min)/_cos_theta_gaussian.size();

		// Begin the neighborhood loop
		for(auto& OQ: neighbors){
			// Compute ray distance: r = OP2_norm - OQ_norm
			double ray_distance = OP_norm - OQ.vec.dot(OP.normalized());
//			double ray_distance = OP_norm - OQ.norm;

			// Compute cos(theta) for angular consistency
			double cos_theta = OQ.vec.dot(OP)/(OQ.norm*OP_norm);

			// Compute consistencies ===============================.
			// Reset consistencies
			cons_emp = cons_occ = 0.0;

			int offset = static_cast<int>(
									(ray_distance + 3.0*_param.sigma_f + _shift)/(_param.sigma_step*_param.sigma_f));
			// Compute occupancy and emptiness consistency
			if(offset < 0){
				cons_occ = 0.0;
				cons_emp = 1.0;
			}
			else if(offset < _occupancy_gaussian.size()){
				cons_occ = _occupancy_gaussian[offset];
				cons_emp = _emptiness_gaussian[offset];
			}else{
				cons_emp = 0.0;
				cons_occ = 0.0;
			}

			// Compute angular consistency
			if(cos_theta < cos_theta_min){
				cons_angle = 0;
			}else{
				int offset = static_cast<int>((1.0 - cos_theta) / cos_theta_step);
				cons_angle = _cos_theta_gaussian[offset];
			}

			// Compute masses: e1 = empty, o1 = occupied, u1 = unknown
			// Weight more occupy (do not multiply by angle gaussian)
			new_mass.occ = cons_occ;// * cons_angle;
			new_mass.emp = cons_emp * cons_angle;
			new_mass.unk = 1.0 - new_mass.occ - new_mass.emp;
			//new_mass.rebalance();

			if(first_pass){
				final_mass = new_mass;
				first_pass = false;
			}else{
//				if(final_mass.emp > 0.2 && new_mass.occ > 0.2){
//					for(int i = 0; i < 6; i++){
//						final_mass.fuse(new_mass);
//					}
//				}
//				else{
//					final_mass.fuse(new_mass);
//				}

				// New fuse rule-----------------------------------------------
				// Empty...
//				double prod = (1 - new_mass.emp)*(1 - final_mass.emp);
//				double emp = (1 - prod)/(1 + prod);
//				// Occupied...
//				prod = (1 - new_mass.occ)*(1 - final_mass.occ);
//				double occ = (1 - prod)/(1 + prod);
//				// Unknown
//				prod = (1 - new_mass.unk)*(1 - final_mass.unk);
//				double unk = (1 - prod)/(1 + prod);
//
//				// normalize.....
//				double summ = emp + occ + unk;
//				final_mass.emp = emp/summ;
//				final_mass.occ = occ/summ;
//				final_mass.unk = 1 - final_mass.emp - final_mass.occ;
				// --------------------------------------------------------------
				final_mass += new_mass;
			}

			//final_mass.rebalance();
		} // End of neighborhood loop

		// Add the cons_normal effect
		//final_mass.rebalance();
//		final_mass.roundTo(rounded_value);
//		final_mass.roundTo(0.8);

		final_mass.emp /= neighbors.size();
		final_mass.occ /= neighbors.size();
		final_mass.unk /= neighbors.size();

		#pragma omp critical
		{
			// Fuse it to consistencies ------------------------------
//			auto& mass = consistencies[point_i];
//			// New fuse rule
//			// Empty...
//			double prod = (1 - final_mass.emp)*(1 - mass.emp);
//			double emp = (1 - prod)/(1 + prod);
//			// Occupied...
//			prod = (1 - final_mass.occ)*(1 - mass.occ);
//			double occ = (1 - prod)/(1 + prod);
//			// Unknown
//			prod = (1 - final_mass.unk)*(1 - mass.unk);
//			double unk = (1 - prod)/(1 + prod);
//
//			// normalize.....
//			double summ = emp + occ + unk;
//			mass.emp = emp/summ;
//			mass.occ = occ/summ;
//			mass.unk = 1 - mass.emp - mass.occ;
			// --------------------------------------------------------
			consistencies[point_i].fuse(final_mass);
		}
	} // End of each point loop
}

void DSTframe::enableDebug(bool enable) {
	_debug = enable;
}

void DSTframe::printDebug() {
	double points = static_cast<double>(_points_computed);
	_avg_neighbors = _avg_neighbors / points;
	_avg_occupancy.emp /= points;
	_avg_occupancy.occ /= points;
	_avg_occupancy.unk /= points;
	std::cout << "Frame >> avg_neighbors = " << _avg_neighbors
			<< "\n avg_occupancy(e,o,u): (" << _avg_occupancy.emp
			<< ", " << _avg_occupancy.occ << ", " << _avg_occupancy.unk << ")\n";
	// Reset the values...
	_avg_neighbors = 0.0;
	_avg_occupancy = MassConsistency();
	_points_computed = 0;
}

void DSTframe::setVisualizer(visualization::CloudVisualizerSimple* visualizer) {
	_viewer = visualizer;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change Detection WDST
// ctor
ChangeDetectionWDST::ChangeDetectionWDST(): Module("ObjectRemovalWDST", true),
		_viewer(nullptr),
		_finished(false),
		_min_buffer_size(0),
		_max_buffer_size(1),
		_skip_frames(2),
		_debug(false),
		_floor_separation_ON(false),
		_post_process_ON(true),
		_frames_delay(4),
		_voxelized_algorithm_ON(false),
		_voxel_size(0.2),
		_swap_removed_points_ON(true),
		_save_removed_points_ON(true),
        _rem_points_filename("../output/rem_points_out.txt"),
        _rem_points_ofstream(std::make_shared<std::ofstream>("../output/rem_points_out.txt"))
        //_rem_points_ofstream("../output/rem_points_out.txt")
{
    // Set up the default parameters...
	_param.search_radius 	= 0.3 * PI/180.0; // In rad
	_param.neighbors		= 16;

	_param.lambda_t 		= 0.05;
	_param.lambda_theta 	= _param.search_radius / 3;
	_param.lambda_thick 	= 0.05;

	_param.sigma_r 			= 0.15;
	_param.sigma_m 			= 0.05;
	_param.sigma_step 		= 0.2;

	_param.sigma_theta	= _param.lambda_theta;

	// Removed points visibility
	_param.seen_search_radius = 0.3*PI/180.0;
	_param.seen_threshold = 0.3;

	// Compute the squared values of parameters
	_param.precompute();

	// Nullify the last rigs...
	_last_rigs[0] = _last_rigs[1] = nullptr;

	// Register profiler topic
	PROFILER_INIT_TIMER(name(), "Dempster-Shafer change detection omp time");
}

// dtor
ChangeDetectionWDST::~ChangeDetectionWDST() { /* Nothing for now */ } // Provoca crash alla fine del programma!

void ChangeDetectionWDST::setScene(Scene* scene) { /* No need for that */ }

bool ChangeDetectionWDST::process(CameraRig* rig, int iteration_index) {
     std::cout << name() << ": iteration " << std::to_string(iteration_index) << "\n";


	 auto time_start = std::chrono::system_clock::now();

	 // If not finished do the standard work
	 if(!_finished){
		 // If input is null then return false...
		 if(!rig){
             std::cout << name() << " >> Received null rig\n";
			 return false;
		 }

		 // Check if _last_rig was set
		 if(!_last_rigs[0]){
			 _last_rigs[0] = _last_rigs[1] = rig;
		 }

		 // Add a new frame... or update existing one
		 std::shared_ptr<DSTframe> new_frame;
		 if(_frames.size() < _max_buffer_size){
			 new_frame = std::shared_ptr<DSTframe>(new DSTframe());
			 // TODO: consider to dynamically change parameters
			 new_frame->setParameters(_param);
			 // Debugging and visualization...
			 new_frame->enableDebug(_debug);
			 new_frame->setVisualizer(_viewer);
		 }else{
			 new_frame = _frames.front();
			 _frames.erase(_frames.begin());
		 }

		 // Setup the new frame and its origin
		 new_frame->setCameraRig(*rig);
		 //		new_frame->setFilter(_filter);

		 // Calculate the trajectory vector.. O[i+1] - O[i-1]
		 Eigen::Vector3f traj = rig->getPose().cast<float>().block<3,1>(0,3)
									- _last_rigs[1]->getPose().cast<float>().block<3,1>(0,3);
		 new_frame->setTrajectoryVector(traj);
		 _frames.push_back(new_frame);

		 // If visualization is enabled... Add the clouds
		 if(_viewer){
			 // Keep the same size as the buffer has
			 if(_viz_clouds.size() >= _max_buffer_size){
				 _viz_clouds.erase(_viz_clouds.begin());
			 }
			 // Add the last cloud to visualization buffer
			 _viz_clouds.push_back(rig->getCloud());
		 }

		 // Separate the floor if it was requested...
		 if(_floor_separation_ON){
			 ///////////////////////////////////////////////////////////////////////////////
			 // Remove floor from the cloud ===============================================
			 if(rig->createCloud("floor")){
				 _floor_removal->setInputCloud(rig->getCloud().cloud_shared_ptr());
				 _floor_removal->filter(rig->getCloud().cloud(), rig->getCloud("floor").cloud());
			 }
			 ///////////////////////////////////////////////////////////////////////////////
		 }

		 // Add the rig to the queue
		 // but skip the first one (almost useless)
		 if(_frames.size() > _min_buffer_size){
			 _rigs.push(rig);
		 }

		 // "Rotate" the last rigs
		 _last_rigs[1] = _last_rigs[0];
		 _last_rigs[0] = rig;

		 //		std::cout << "----------- DST: buffer.size = " << _frames.size() << "\n";
		 //		std::cout << "----------- DST:   rigs.size = " << _rigs.size() << "\n";
	 }
	 // Otherwise work with remaining rigs in queue
	 else {
		 // Remove frames, two at a time.
		 _frames.erase(_frames.begin());
		 _frames.erase(_frames.begin());

		 // If visualization is enabled.. Remove the clouds
		 if(_viewer){
			 _viz_clouds.erase(_viz_clouds.begin());
			 _viz_clouds.erase(_viz_clouds.begin());
		 }

		 // Empty the _rigs queue if it has only min_buffer_size elements
		 if(_rigs.size() <= _min_buffer_size){
			 std::queue<CameraRig*> empty_queue;
			 std::swap(_rigs, empty_queue);
			 _frames.clear();
			 return true;
		 }
	 }
	 // Start the removal algorithm if the buffer has odd size (for symmetry)...
	 if(_rigs.size() > _min_buffer_size && (_frames.size() % 2)){
		 // Execute the appropriate algorithm
		 if(_voxelized_algorithm_ON){
			 //			removeMovingObjectsVoxelized();
			 removeMovingObjectsOctree();
		 }else{
			 removeMovingObjects();
		 }
		 auto time_diff = std::chrono::system_clock::now() - time_start;
		 double nano = time_diff.count();

		 _overall_time.aggregate(nano);
		 return true;
	 }
	 auto time_diff = std::chrono::system_clock::now() - time_start;
	 double nano = time_diff.count();

	 _overall_time.aggregate(nano);
	 return false;
}

void ChangeDetectionWDST::removeMovingObjects() {
	// Prepare Scene 2, all frames except current one and skipped ones
	int half_way = _rigs.size() + _frames.size()%_skip_frames;
	std::vector<std::shared_ptr<DSTframe> > scene2;
	for(int i = 0; i < _rigs.size() - 1; i+=_skip_frames){
		scene2.push_back(_frames[i]);
	}
	for(int i = half_way; i < _frames.size(); i++){
		scene2.push_back(_frames[i]);
	}

	// Indices vector...
	//std::vector<int> indices_to_remove;
	Cloud::indices_ptr indices_to_remove(new std::vector<int>());

	// For visualization only..
	Cloud viz_cloud;

	// Some useful shortcuts...
	auto& current_cloud = _rigs.front()->getCloud();

	// Visualize the precomputed clouds
	if(_viewer){
		visualizePreComputationClouds(_viz_clouds[_rigs.size()-1], 1000);
	}

	// Only for debug
	std::string info("DST >> analyzing cloud... ");

	auto time_start = std::chrono::system_clock::now();
	PROFILER_START_TIMER(name());
	// For statistics...
	MassConsistency avg_occ;

	std::vector<MassConsistency> points_consistencies;
	int frame_i;
//	#pragma omp parallel for default(shared) private(frame_i)
	for(frame_i = 0; frame_i < scene2.size(); frame_i++){
		scene2[frame_i]->computeConsistencies(current_cloud, points_consistencies);
//		scene2[frame_i]->computeConsistenciesNewRule(current_cloud, points_consistencies);
//		scene2[frame_i]->computeConsistenciesMedian(current_cloud, points_consistencies);
	}

	// Variables to keep track of removed points
    RemovedPointsContainer &rem_points = _rigs.front()->getCamera().removed_points_container;
	rem_points.rig_id = _rigs.front()->getID();
	rem_points.rig_pose = _rigs.front()->getPose();

	// Search for points to be removed...
	// If post process was requested, do it
	if(_post_process_ON){
        postCheckRemovedPoints(current_cloud, viz_cloud, *indices_to_remove, points_consistencies, rem_points);
	}
	// Otherwise process the raw removed points
	else{
		// Start the loop
		for(int i = 0; i < points_consistencies.size(); i++){
			auto& mass = points_consistencies[i];
			if(mass.emp > mass.occ && mass.emp > mass.unk){
				auto& p = current_cloud[i];

				// If empty prevails then mark this point as to be removed
				indices_to_remove->push_back(i);

				///////////////////////////////////////////////////////////
				// Save the removed points
				rem_points.removed_points.push_back(RemovedPoint(p.x, p.y, p.z, rem_points.rig_id));
				_removed_points += p;
				//////////////////////////////////////////////////////////

				// Only for statistics...
				avg_occ.emp += mass.emp;
				avg_occ.occ += mass.occ;
				avg_occ.unk += mass.unk;

				if(_viewer){ viz_cloud += p; }
			}
		}
	}

	PROFILER_STOP_TIMER(name());
	auto time_diff = std::chrono::system_clock::now() - time_start;
	double nano = time_diff.count();
	std::cout << info << " done; removed points = " << indices_to_remove->size() << "\n";
	std::cout << "Computation time: " << nano/1e6 << " millis\n";
	std::cout << "Avg removed consistency (e,o,u): " << avg_occ.emp/indices_to_remove->size() << ", "
			<< avg_occ.occ/indices_to_remove->size() << ", "
			<< avg_occ.unk/indices_to_remove->size() << "\n";

	// For debug purposes...
	std::cout << "DST DEBUG: total points = " << current_cloud.size() << "\n";
	//for(auto& frame: scene2){
		//frame->printDebug();
	//}

	// Remove the marked points from cloud
	current_cloud.removePoints(indices_to_remove);

	if(_save_removed_points_ON){        
		///////////////////////////////////////////////////////////////////////////////////
		// Get the views for every removed point
		// First limit the amount of frames to be checked...
		auto last_frame_to_check = scene2.size() - _rigs.size()/2;
		#pragma omp parallel for default(shared) private(frame_i)
		for(frame_i = 0; frame_i <last_frame_to_check; frame_i++){
			scene2[frame_i]->computeRemovedPointsVisibility(rem_points);
		}
		// Filter out points with only one view...
		RemovedPointsContainer filtered_rem_points;
		for(auto& p: rem_points.removed_points){
			if(p.view_idx.size() > 1){
				filtered_rem_points.removed_points.push_back(p);
			}
		}
		filtered_rem_points.rig_id = rem_points.rig_id;
		filtered_rem_points.rig_pose = rem_points.rig_pose;
		///////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////
		// WRITE REMOVED POINTS
        createFolderFromFilename(_rem_points_filename);

		// <rig_id> <num_points>
        (*_rem_points_ofstream) << filtered_rem_points.rig_id << " " << filtered_rem_points.size() << "\n";
		// All points with their respected view id
		for(auto& p: filtered_rem_points.removed_points){
			// <point.x> <point.y> <point.z>
            (*_rem_points_ofstream) << p.point[0] << " " << p.point[1] << " " << p.point[2];
			// <num_views>
            (*_rem_points_ofstream) << " " << p.view_idx.size();
			for(auto& view_id: p.view_idx){
				// <view_id>
                (*_rem_points_ofstream) << " " << view_id;
			}
            (*_rem_points_ofstream) << "\n";
		}
		///////////////////////////////////////////////////////////////////////////////////
	}

	// Add the floor if it was separated...
	if(_floor_separation_ON){
		///////////////////////////////////////////////////////////////////////////////////
		// Put the floor back
		current_cloud += _rigs.front()->getCloud("floor");
		_rigs.front()->clearCloud("floor");
		//////////////////////////////////////////////////////////////////////////////////
	}

	// Visualize...
	if(_viewer){
		visualizePostComputationClouds(viz_cloud, current_cloud, 2000);
	}

	// Change the previously saved cloud with the new cleaned one
	if(_swap_removed_points_ON){
		_frames[_rigs.size()-1]->setCameraRig(*(_rigs.front()));
		if(_viewer){
			_viz_clouds[_rigs.size()-1] = current_cloud;
		}
	}
}

void ChangeDetectionWDST::removeMovingObjectsVoxelized() {
	// Convenience shortcuts
	auto& current_cloud = _rigs.front()->getCloud();

	// Used to understand if occupancy was updated
	bool first_try;

	int half_way = _rigs.size() - 1;

	// Prepare Scene 1, all past frames
	std::vector<std::shared_ptr<DSTframe> > scene1;
	for(int i = 0; i < _frames.size()/2; i+=_skip_frames){
		if(i == half_way) continue;
		scene1.push_back(_frames[i]);
	}

	// Prepare Scene 2, all future frames
	std::vector<std::shared_ptr<DSTframe> > scene2;
	for(int i = _frames.size()/2 + 1; i < _frames.size(); i+=_skip_frames){
		if(i == half_way) continue;
		scene2.push_back(_frames[i]);
	}

	// Prepare the voxelized and floor-less cloud...
	Cloud sample;
	MultiFilter filter;
	filter.addFloorRemoval(0.08, 0.08);
	filter.addVoxelGrid(0.1, 0.1, 0.1);
	filter.filter(current_cloud.cloud_shared_ptr(), sample.cloud_shared_ptr());

	// Add current cloud to a kd-tree
	pcl::KdTreeFLANN<Cloud::pointType> main_tree;
	main_tree.setInputCloud(current_cloud.cloud_shared_ptr());

	// Indices vector...
	Cloud::indices_ptr indices_to_remove(new std::vector<int>());
	std::set<int> all_indices_to_remove;

	// For visualization only..
	Cloud rem_cloud;
	Cloud mass_emp_cloud;
	Cloud mass_occ_cloud;
	Cloud mass_unc_cloud;

	// Visualize the precomputed clouds
	if(_viewer){
		visualizePreComputationClouds(sample, 1000);
	}

	// Only for debug
	std::string info("DST >> analyzing cloud... ");

	auto time_start = std::chrono::system_clock::now();
	PROFILER_START_TIMER(name());

	//#define POINTS_FIRST

	// Main change detection loop =============================================================
	std::vector<MassConsistency> scene1_consistencies;
	std::vector<MassConsistency> scene2_consistencies;

	int frame_i;
	#pragma omp parallel for default(shared) private(frame_i)
	for(frame_i = 0; frame_i < scene1.size(); frame_i++){
		scene1[frame_i]->computeConsistencies(sample, scene1_consistencies);
	}

	#pragma omp parallel for default(shared) private(frame_i)
	for(frame_i = 0; frame_i < scene2.size(); frame_i++){
		scene2[frame_i]->computeConsistencies(sample, scene2_consistencies);
	}

	int point_i;
	#pragma omp parallel for default(shared) private(point_i)
	for(point_i = 0; point_i < scene1_consistencies.size(); point_i++){
		auto mass1 = scene1_consistencies[point_i];
		auto& mass2 = scene2_consistencies[point_i];

		mass1.rebalance();
		mass2.rebalance();

		mass1.fuse(mass2);
		mass1.rebalance();

		double conf = mass1.emp;
		double cons = mass1.occ;
		double unc = mass1.unk;

		if(conf > cons && conf > unc){
            #pragma omp critical
			{
				// If conflicting prevails then mark this point as to be removed
				indices_to_remove->push_back(point_i);
			}
		}

		if(_viewer){
			#pragma omp critical
			{
				if(conf > cons && conf > unc){
					mass_emp_cloud += sample[point_i];
				}
				else if(cons > conf && cons > unc){
					mass_occ_cloud += sample[point_i];
				}
				else{
					mass_unc_cloud += sample[point_i];
				}
			}
		}
	}

	// Search the neighborhood of removed point in un-voxelized cloud
	// First create the needed variables...
	Cloud::indices_ptr indices(new std::vector<int>());
	std::vector<float> squared_distances;
	float search_radius = 0.14f;
	for(auto& index: *indices_to_remove){
		if(main_tree.radiusSearch(sample[index], search_radius, *indices, squared_distances)){
			for(auto& i: *indices){
				all_indices_to_remove.insert(i);
			}
		}
	}

	// Now remove the selected points from cloud
	indices_to_remove->clear();
	if(_viewer){
		for(auto& rem_point_index: all_indices_to_remove){
			indices_to_remove->push_back(rem_point_index);
			rem_cloud += current_cloud[rem_point_index];
		}
	}
	else{
		for(auto& rem_point: all_indices_to_remove){
			indices_to_remove->push_back(rem_point);
		}
	}

	PROFILER_STOP_TIMER(name());
	auto time_diff = std::chrono::system_clock::now() - time_start;
	double nano = time_diff.count();

	std::cout << info << " done; removed points = " << indices_to_remove->size() << "\n";
	std::cout << "Computation time: " << nano/1e6 << " millis\n";

	// For debug purposes...
	std::cout << "DST DEBUG: total points = " << current_cloud.size() << "\n";
	//for(auto& frame: scene2){
	//frame->printDebug();
	//}

	// For visualization purposes...
	if(_viewer){
		_viewer->clear();
		_viewer->addPointCloud(mass_occ_cloud, "occupied", visualization::Color::GREEN);
		_viewer->addPointCloud(mass_emp_cloud, "empty", visualization::Color::RED);
		_viewer->addPointCloud(mass_unc_cloud, "unknown", visualization::Color::BLUE);

        //std::this_thread::sleep_for(std::chrono::duration<double,std::milli>(2000));
		_viewer->clear();
	}

	// Remove the marked points from cloud
	current_cloud.removePoints(indices_to_remove);

	// Visualize...
	if(_viewer){
		visualizePostComputationClouds(rem_cloud, current_cloud, 2000);
	}
}

void ChangeDetectionWDST::removeMovingObjectsOctree() {
	using namespace pcl::octree;

	// Prepare Scene 2, all frames except current one and skipped ones
	int half_way = _rigs.size() + _frames.size()%_skip_frames;
	std::vector<std::shared_ptr<DSTframe> > scene2;
	for(int i = 0; i < _rigs.size() - 1; i+=_skip_frames){
		scene2.push_back(_frames[i]);
	}
	for(int i = half_way; i < _frames.size(); i++){
		scene2.push_back(_frames[i]);
	}

	// Indices vector...
	//std::vector<int> indices_to_remove;
	Cloud::indices_ptr indices_to_remove(new std::vector<int>());
	std::set<int> rem_indices;

	// For visualization only..
	Cloud viz_cloud;

	// Some useful shortcuts...
	auto& current_cloud = _rigs.front()->getCloud();

	// Visualize the precomputed clouds
	if(_viewer){
		visualizePreComputationClouds(_viz_clouds[_rigs.size()-1], 1000);
	}

	//*****************************************
	// Applying the octree
	OctreePointCloudPointVector<Cloud::pointType> octree(_voxel_size);
	octree.setInputCloud(current_cloud.cloud_shared_ptr());
	octree.addPointsFromInputCloud();

//	std::cout << ">>>>>>> Octree leaves: " << octree.getLeafCount() << "\n";
//	std::cin.get();

	std::vector<OctreeContainerPointIndices*> leafs;
	octree.serializeLeafs(leafs);

	std::cout << ">>>>>>> Serialized leaves: " << leafs.size() << "\n";
//		std::cin.get();

	// Create the leaf iterator
	OctreePointCloudPointVector<Cloud::pointType>::LeafNodeIterator it(&octree);
	std::vector<int> indices;

	// Only for debug
	std::string info("DST >> analyzing cloud... ");

	auto time_start = std::chrono::system_clock::now();
//	PROFILER_START_TIMER(name());

	int leaf_i = 0;
	// iterate over leaf nodes and look for change points
	#pragma omp parallel for default(shared) private(leaf_i)
	for(leaf_i = 0; leaf_i < leafs.size(); leaf_i++){
//		OctreePointCloud<Cloud::pointType>::LeafNode * leaf = static_cast<OctreePointCloud<Cloud::pointType>::LeafNode *>(*it);
//		leaf->getContainer().getPointIndices(indices);
		auto& indices = leafs[leaf_i]->getPointIndicesVector();

//		std::cout << ">>>> Doint Leaf " << leaf_i << " with " << indices.size() << " indices\n";

		// first select random idx
		// prepare the random vector
		std::vector<int> rand_idx;
		int balance_divider = _split_factor*2;
		// the indices vector could be shorter than split factor
		if(indices.size() > balance_divider){
			for(int i = 0; i < indices.size() - _split_factor + 1; i += _split_factor){
				rand_idx.push_back(i + (rand() % _split_factor));
			}
		}
		// if it is shorter, then just copy the indices to random indices
		else{
			balance_divider = 1;
			rand_idx.resize(indices.size());
			for(int i = 0; i < indices.size(); i++){
				rand_idx[i] = i;
			}
		}

		// start the consistency check
		int bad_count = 0;
		int max_bad_count = indices.size()/balance_divider;

		// start the random points consistency check
		for(int i = 0; i < rand_idx.size(); i++){
			auto& point = current_cloud[indices[rand_idx[i]]];

			// prepare consistency
			MassConsistency cons;

			// let it check for all frames
			for(auto& frame: scene2){
				cons.fuse(frame->computeConsistency(point));
			}

			// see if empty, and adjust counters
			if(cons.emp > cons.occ && cons.emp > cons.unk){
				bad_count++;
			}

			// if bad count or good count is over half then quit
			if(bad_count >= max_bad_count || (i - bad_count) >= max_bad_count){
				break;
			}
		}

		// if bad points are a lot, set the whole voxel to be removed
		if(bad_count >= max_bad_count){
			#pragma omp critical
			{
				// here we have a potential index out of bound
				rem_indices.insert(indices.begin(), indices.end());
			}
		}
	}
	// For statistics...
	MassConsistency avg_occ;

	int frame_i;

	// Variables to keep track of removed points
    RemovedPointsContainer &rem_points = _rigs.front()->getCamera().removed_points_container;
	rem_points.rig_id = _rigs.front()->getID();
	rem_points.rig_pose = _rigs.front()->getPose();

	// Search for points to be removed...
	// If post process was requested, do it
		// Start the loop
	for(auto& point_i: rem_indices){
		auto& p = current_cloud[point_i];

		// If empty prevails then mark this point as to be removed
		indices_to_remove->push_back(point_i);

		///////////////////////////////////////////////////////////
		// Save the removed points
		rem_points.removed_points.push_back(RemovedPoint(p.x, p.y, p.z, rem_points.rig_id));
		_removed_points += p;
		//////////////////////////////////////////////////////////

		if(_viewer){ viz_cloud += p; }
	}

//	PROFILER_STOP_TIMER(name());
	auto time_diff = std::chrono::system_clock::now() - time_start;
	double nano = time_diff.count();

	_comp_time.aggregate(nano);

	std::cout << info << " done; removed points = " << indices_to_remove->size() << "\n";
	std::cout << "Computation time: " << nano/1e6 << " millis\n";

	// For debug purposes...
	std::cout << "DST DEBUG: total points = " << current_cloud.size() << "\n";
	//for(auto& frame: scene2){
		//frame->printDebug();
	//}

    // if  bin_folder was set to a value calling enableSavingPointsBinaryFile
    if(!_bin_folder.empty()){
        createFolder(_bin_folder);

		///////////////////////////////////////////////////////////////////////////////////////
		// Save the points in a binary file
		std::fstream _bin_file (_bin_folder + "000" + std::to_string(_rigs.front()->getID()) + ".bin",
				std::ios::out | std::ios::binary);
		for(int i = 0; i < indices_to_remove->size(); ++i){
			//Some calculations to fill a[]
			auto& p = current_cloud[indices_to_remove->operator [](i)];
			_bin_file.write((char*)&p.x, sizeof(float)*3);
		}
		_bin_file.close();
    }

	// Remove the marked points from cloud
	current_cloud.removePoints(indices_to_remove);

	if(_save_removed_points_ON){
		///////////////////////////////////////////////////////////////////////////////////
		// Get the views for every removed point
		// First limit the amount of frames to be checked...
		auto last_frame_to_check = scene2.size() - _rigs.size()/2;
		#pragma omp parallel for default(shared) private(frame_i)
		for(frame_i = 0; frame_i <last_frame_to_check; frame_i++){
			scene2[frame_i]->computeRemovedPointsVisibility(rem_points);
		}
		// Filter out points with only one view...
		RemovedPointsContainer filtered_rem_points;
		for(auto& p: rem_points.removed_points){
			if(p.view_idx.size() > 1){
				filtered_rem_points.removed_points.push_back(p);
			}
		}
		filtered_rem_points.rig_id = rem_points.rig_id;
		filtered_rem_points.rig_pose = rem_points.rig_pose;
		///////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////
		// WRITE REMOVED POINTS
		// <rig_id> <num_points>
        (*_rem_points_ofstream) << filtered_rem_points.rig_id << " " << filtered_rem_points.size() << "\n";
		// All points with their respected view id
		for(auto& p: filtered_rem_points.removed_points){
			// <point.x> <point.y> <point.z>
            (*_rem_points_ofstream) << p.point[0] << " " << p.point[1] << " " << p.point[2];
			// <num_views>
            (*_rem_points_ofstream) << " " << p.view_idx.size();
			for(auto& view_id: p.view_idx){
				// <view_id>
                (*_rem_points_ofstream) << " " << view_id;
			}
            (*_rem_points_ofstream) << "\n";
		}
		///////////////////////////////////////////////////////////////////////////////////
	}

	// Add the floor if it was separated...
	if(_floor_separation_ON){
		///////////////////////////////////////////////////////////////////////////////////
		// Put the floor back
//		visualizePostComputationClouds(current_cloud, current_cloud, 2000);
//		visualizePostComputationClouds(_rigs.front()->getCloud("floor"), _rigs.front()->getCloud("floor"), 2000);
		current_cloud += _rigs.front()->getCloud("floor");
		_rigs.front()->clearCloud("floor");
//		visualizePostComputationClouds(current_cloud, current_cloud, 2000);
		//////////////////////////////////////////////////////////////////////////////////
	}

	// Visualize...
	if(_viewer){
		visualizePostComputationClouds(viz_cloud, current_cloud, 2000);
	}

	// Change the previously saved cloud with the new cleaned one
	if(_swap_removed_points_ON){
		_frames[_rigs.size()-1]->setCameraRig(*(_rigs.front()));
		if(_viewer){
			_viz_clouds[_rigs.size()-1] = current_cloud;
		}
	}
}

void ChangeDetectionWDST::postCheckRemovedPoints(Cloud& cloud, Cloud &viz_cloud,
                                                 std::vector<int> &indices_to_remove,
                                                 std::vector<MassConsistency>& consistencies,
                                                 RemovedPointsContainer& rem_container) {

	// Mandatory variables for kd-tree search
	std::vector<int> indices;
	std::vector<float> sqr_distances;

	// Do the size checks
	if(_pp_clouds.size() >= _frames_delay){
		// Add a new tree
		_pp_frames.push_back(pcl::KdTreeFLANN<Cloud::pointType>());
		_pp_frames.back().setInputCloud(_pp_clouds[0].cloud_shared_ptr());

		// See if size too big and remove the first tree
		if(_pp_frames.size() >= _rigs.size() - _frames_delay){
			_pp_frames.erase(_pp_frames.begin());
		}

		// Remove the first cloud in line
		_pp_clouds.erase(_pp_clouds.begin());
	}

	// Add a new empty cloud for this frame removed points
	_pp_clouds.push_back(Cloud());

	// Aux data...
	Cloud &new_cloud = _pp_clouds.back();
	bool skip_point;
	auto search_radius = _voxel_size;
	int min_points = 1;

	// Start the loop
	for(int i = 0; i < consistencies.size(); i++){
		auto& mass = consistencies[i];
		if(mass.emp > mass.occ && mass.emp > mass.unk){
			// Get the point from the cloud
			auto& p = cloud[i];

			// Add it to the removed points cloud for future comparisons
			new_cloud += p;

			// Reset the skip point flag
			skip_point = false;

			// Search if there are neighbors for the point
			for(auto& frame: _pp_frames){
				if(frame.radiusSearch(p, search_radius, indices, sqr_distances) >= min_points){
					skip_point = true;
					break;
				}
			}

			// If yes then this point is a false positive
			if(skip_point){ continue; }

			// If empty prevails then mark this point as to be removed
			indices_to_remove.push_back(i);

			///////////////////////////////////////////////////////////
			// Save the removed points
			rem_container.removed_points.push_back(RemovedPoint(p.x, p.y, p.z, rem_container.rig_id));
			_removed_points += p;
			//////////////////////////////////////////////////////////

			if(_viewer){ viz_cloud += p; }
		}
	}
}

CameraRig* ChangeDetectionWDST::getOutputCameraRig() {
	if(_rigs.empty()) return nullptr;
	auto return_rig = _rigs.front();
	_rigs.pop();
	return return_rig;
}

bool ChangeDetectionWDST::hasFinished() {
	if(_finished && _rigs.empty()){
//		std::ofstream points_out("../output/rem_points.txt");
//		points_out << _removed_points.size() << "\n";
//		for(auto& point: _removed_points.cloud().points){
//			points_out << point.x << " " << point.y << " " << point.z << "\n";
//		}
//		points_out.close();
        (*_rem_points_ofstream).close();
		std::cout << "[DST Change Detection]: Removed Points: " << _removed_points.size() << "\n";

        // We don't need this for now
//        if(_save_removed_points_ON){
//            pcl::io::savePLYFile(_rem_points_filename, _removed_points.cloud(), true);
//        }

		if(!_info_file.empty()){
            createFolderFromFilename(_info_file);

			std::ofstream info_file;
			info_file.open(_info_file, std::ios::out | std::ios::app);

			info_file << _param.sigma_r << "\t" << _param.search_radius << "\t"
					<< _comp_time.min << "\t" << _comp_time.max << "\t"
					<< _comp_time.avgMillis() << "\t" << _comp_time.sum / 1000000.0 << "\t"
					<< _overall_time.min << "\t" << _overall_time.max << "\t"
					<< _overall_time.avgMillis() << "\t" << _overall_time.sum / 1000000.0 << "\n";

			info_file.close();
			_info_file.clear();
		}

		if(_viewer){
			_viewer->clear();
			_viewer->setTitle("All Removed Points");
			_viewer->addPointCloud(_removed_points, "removed_points", visualization::Color::RED);
            //std::cin.get(); // Il commento evita che reconstruction_main.cpp si blocchi
		}
		return true;
	}
//	return _finished && _rigs.empty();
	return false;
}

void ChangeDetectionWDST::finalize() {
	_finished = true;
}

void ChangeDetectionWDST::setParameters(const DSTparameter& params) {
	_param = params;
	_param.precompute();
}

void ChangeDetectionWDST::setMaxPreceedingClouds(size_t nr_clouds) {
	_max_buffer_size = 2*nr_clouds + 1;
}

void ChangeDetectionWDST::setMinPreceedingClouds(size_t nr_clouds) {
	_min_buffer_size = nr_clouds;
}

DSTparameter& ChangeDetectionWDST::getParameters() {
	return _param;
}

size_t ChangeDetectionWDST::getMaxPreceedingClouds() {
	return _max_buffer_size;
}

size_t ChangeDetectionWDST::getMinPreceedingClouds() {
	return _min_buffer_size/2;
}

void ChangeDetectionWDST::enableVisualization(
		visualization::CloudVisualizerSimple* visualizer) {
	_viewer = visualizer;

	for(auto& frame: _frames){
		frame->setVisualizer(visualizer);
	}
}

void ChangeDetectionWDST::disableVisualization() {
	std::string previous = "previous";
	std::string next = "next";
	for(int i = 0; i < _rigs.size() - 1 && i < _viz_clouds.size(); i++){
		std::string name = previous + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
	for(int i = _rigs.size(); i < _viz_clouds.size(); i++){
		std::string name = next + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
	_viewer->removePointCloud("current_cloud");
	_viewer->removePointCloud("sample_cloud");
	_viewer->removePointCloud("indices_to_remove");

	_viewer = nullptr;
	_viewer = nullptr;
	_viz_clouds.clear();
}

void ChangeDetectionWDST::visualizePreComputationClouds(
		const Cloud& sample_cloud, unsigned int pause_ms) {
	auto duration = std::chrono::duration<double, std::milli>(pause_ms);

	std::string previous = "previous";
	std::string next = "next";
	int second_half_start = _rigs.size() + _viz_clouds.size()%_skip_frames;
	_viewer->clear();
	_viewer->setTitle("Change Detection And Removal", visualization::Color::GREY);
	for(int i = 0; i < _rigs.size() - 1 && i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = previous + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->addPointCloud(_viz_clouds[i], name, visualization::Color::GREEN);
		}
	}
	for(int i = second_half_start; i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = next + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->addPointCloud(_viz_clouds[i], name, visualization::Color::YELLOW);
		}
	}

    //std::this_thread::sleep_for(duration);

	_viewer->addPointCloud(sample_cloud, "sample_cloud", visualization::Color::CYAN);

	for(int i = 0; i < _rigs.size() - 1 && i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = previous + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
	for(int i = second_half_start; i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = next + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
}

void ChangeDetectionWDST::setSkippedFrames(size_t skipped_frames) {
	_skip_frames = skipped_frames;
}

size_t ChangeDetectionWDST::getSkippedFrames() {
	return _skip_frames;
}


void ChangeDetectionWDST::visualizePostComputationClouds(
		const Cloud& removing_indices_cloud, const Cloud& current_cloud,
		unsigned int pause_ms) {
	auto duration = std::chrono::duration<double, std::milli>(pause_ms);

	_viewer->addPointCloud(current_cloud, "current_cloud");
	_viewer->addPointCloud(removing_indices_cloud, "indices_to_remove", visualization::Color::RED);
	_viewer->removePointCloud("sample_cloud");
    //std::this_thread::sleep_for(duration);

	_viewer->removePointCloud("indices_to_remove");
    //std::this_thread::sleep_for(duration);
}

void ChangeDetectionWDST::enableDebug(bool enable) {
	_debug = enable;
	for(auto& frame: _frames){
		frame->enableDebug(enable);
	}
}

void ChangeDetectionWDST::saveRemovedPoints(RemovedPointsContainer& container) {
	// TODO: Still thinking if implementation needed...
}

void ChangeDetectionWDST::enableFloorSeparation(bool enable, double res,
		double sensor_height, int min_points, double max_slope) {
	_floor_separation_ON = enable;
	if(enable){
		_floor_removal = std::shared_ptr<filtering::FloorRemoval>
						 (new filtering::FloorRemoval(res, res, min_points, -sensor_height));
		_floor_removal->setMaxSlopeDifference(max_slope);
	}
}

void ChangeDetectionWDST::enablePostProcess(bool enable, int frames_delay) {
	_post_process_ON = enable;
	_frames_delay = frames_delay;
}

void ChangeDetectionWDST::enableVoxelizedAlgorithm(bool enable,
		double voxel_size, int split_factor) {
	_voxelized_algorithm_ON = enable;
	if(enable){
		_filter.clear();
		_filter.addVoxelGrid(voxel_size, voxel_size, voxel_size);
		_voxel_size = voxel_size;
		_split_factor = split_factor;
	}
}

void ChangeDetectionWDST::enableDynamicRemovedPointsSwapping(bool enable) {
	_swap_removed_points_ON = enable;
}

void ChangeDetectionWDST::enableInformationStoring(bool enable, std::string const &filename){
	if(enable){
		_info_file = filename;
	}
}
void ChangeDetectionWDST::enableSavingPointsBinaryFile(bool enable, std::string const &folder){
	if(enable){
		_bin_folder = folder;
	}
}

void ChangeDetectionWDST::enableSavingRemovedPoints(bool enable, const std::string& filename) {
	_save_removed_points_ON = enable;
	if(enable){
		_rem_points_filename = filename;

        std::shared_ptr<std::ofstream> removed_points_ofstream(std::make_shared<std::ofstream>(filename));
        _rem_points_ofstream = removed_points_ofstream;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Spherical Point Representation ======================================================================
SphericalPointRepresentation2D::SphericalPointRepresentation2D()
: trivial_(false), nr_dimensions_(2){ }

void SphericalPointRepresentation2D::copyToFloatArray(
		const PointType& p, float* out) const {
	//std::cout << "PointRepresentation: out = " << out << "\n";
	out = new float[2];
	out[0] = p.x;
	out[1] = p.y;
}

bool SphericalPointRepresentation2D::isValid(
		const PointType& p) const {
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////

} /* namespace cloud_cleaning */
