/*
 * ChangeDetectionOriginal.h
 *
 *  Created on: Jul 3, 2015
 *      Author: george
 */

#ifndef CLOUD_CLEANING_CHANGEDETECTIONORIGINAL_H_
#define CLOUD_CLEANING_CHANGEDETECTIONORIGINAL_H_

#include <pcl/point_representation.h>
#include <pcl/kdtree/kdtree_flann.h>
#include "../pipeline/Cloud.h"
#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"
#include "../filters/FloorRemoval.h"
#include "../visualization/CloudVisualizerSimple.h"

#include "ChangeDetectionWDST.h"

#include <vector>
#include <queue>

#define USE_RADIAL_KDTREE
//#define USE_SECOND_RADIAL_KDTREE
//#define OVERSIMPLIFIED_ALGORITHM
//#define DST_NO_UNCERTAINTIES

namespace cloud_cleaning {
using namespace pipeline;

class DSTframeX{
public:
	DSTframeX();
	void setTrajectoryVector(Eigen::Vector3f const & traj);
	void setParameters(DSTparameter parameter);
	void setCameraRig(CameraRig & rig);
	void setFilter(MultiFilter const & filter);
	MassConsistency computeConsistency(Cloud::pointType const & location);
	void computeConsistencies(Cloud const & original_cloud, std::vector<MassConsistency>& consistencies);
	void computeConsistenciesNewRule(Cloud const & original_cloud, std::vector<MassConsistency>& consistencies);
	void computeConsistenciesMedian(Cloud const & cloud, std::vector<MassConsistency>& consistencies);
	void computeRemovedPointsVisibility(RemovedPointsContainer & rem_points);

	void enableDebug(bool enable);
	void printDebug();
	void setVisualizer(visualization::CloudVisualizerSimple * visualizer);

private:
	// Use 2 interleaved kdtrees to avoid angular discontinuing searches
	pcl::KdTreeFLANN<pcl::PointXY> _kdtree;
	MultiFilter _filter;
	// Keep the input cloud to retrieve the points
	std::vector<LocalPoint> _points;
	Eigen::Vector3f _traj_vector;
	Eigen::Vector3f _traj_normal;
	Eigen::Vector3f _origin;
	double _traj_norm;

	// Data for images subtraction
	int _rig_id;

	// Improving the precision
	double _minmax_dist;
	RigBox _bbox;

	// For visualization Purposes
	Cloud _cloud;

	// convolution variables
	double _shift;
	std::vector<double> _occupancy_gaussian;
	std::vector<double> _emptiness_gaussian;
	std::vector<double> _cos_theta_gaussian;
	DSTparameter _param;
	Gaussian _gaussian;

	// Various algorithms
	bool findNeighbors(Eigen::Vector3f const & search_point,
			double search_radius, std::vector<LocalPoint> &out_neighbors, int k_neighbors = 50);
	bool findNeighborsSorted(Eigen::Vector3f const & search_point,
				double search_radius, std::vector<LocalPoint> &out_neighbors, int k_neighbors = 50);
	MassConsistency computeConsistencySimple(Cloud::pointType const & location);

	// Debug related ==========================================
	bool _debug;
	visualization::CloudVisualizerSimple * _viewer;

	double _avg_neighbors;
	MassConsistency _avg_occupancy;
	int _points_computed;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change detection based on Weighted Dempster-Shafer Theory
class ChangeDetectionOriginal: public pipeline::Module {
public:
	ChangeDetectionOriginal();
	virtual ~ChangeDetectionOriginal();

	// Module related ====================================================================
	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();

	// Parameters related ================================================================
	void setParameters(DSTparameter const & params);
	void setMaxPreceedingClouds(size_t nr_clouds);
	void setMinPreceedingClouds(size_t nr_clouds);
	void setSkippedFrames(size_t skipped_frames);

	DSTparameter& getParameters();
	size_t getMaxPreceedingClouds();
	size_t getMinPreceedingClouds();
	size_t getSkippedFrames();

	// Algorithm related ====================================================================
	void enableFloorSeparation(bool enable);
	void enablePostProcess(bool enable, int frames_delay = 4);
	void enableVoxelizedAlgorithm(bool enable, double voxel_size = 0.1);
	void enableDynamicRemovedPointsSwapping(bool enable);
	void enableSavingRemovedPoints(bool enable, std::string const &filename = "../output/rem_points_out.txt");
	void enableInformationStoring(bool enable, std::string const &filename = "../output/orig/orig_info.txt");
	void enableSavingPointsBinaryFile(bool enable, std::string const &filename = "../output/orig/rem_points/");

	// Visualization related =============================================================
	void enableVisualization(visualization::CloudVisualizerSimple * visualizer);
	void disableVisualization();

	// Debug related =====================================================================
	void enableDebug(bool enable);

	// WDST Related ======================================================================

private:
	// Parameters
	DSTparameter _param;

	// Needed to compute the trajectory...
	CameraRig* _last_rigs[2];

	// Module related
	bool _finished;

	// Frames buffer related
	std::vector<std::shared_ptr<DSTframeX> > _frames;
	std::queue<CameraRig*> _rigs;
	size_t _max_buffer_size;
	size_t _min_buffer_size;
	int _skip_frames;

	// All removed points
	Cloud _removed_points;

	// Floor separation related
	std::shared_ptr<filtering::FloorRemoval> _floor_removal;

	// Post process related data
	std::vector<pcl::KdTreeFLANN<Cloud::pointType>> _pp_frames;
	std::vector<Cloud> _pp_clouds;
	int _frames_delay;

	// Flags
	bool _floor_separation_ON;
	bool _post_process_ON;
	bool _voxelized_algorithm_ON;
	bool _swap_removed_points_ON;
	bool _save_removed_points_ON;

	// Voxelized algorithm related
	MultiFilter _filter;
	double _voxel_size;

	// Save removed points related
    std::shared_ptr<std::ofstream> _rem_points_ofstream;
	std::string _rem_points_filename;

	// Commodity methods
	void removeMovingObjects();
	void removeMovingObjectsVoxelized();
	void removeMovingObjectsOctree();

	void postCheckRemovedPoints(Cloud &cloud, Cloud &viz_cloud,
			std::vector<int> &indices_to_remove,
			std::vector<MassConsistency> &consistencies,
			RemovedPointsContainer &rem_container);

	void saveRemovedPoints(RemovedPointsContainer & container);

	// Information Related ==============================================================
	std::string _info_file;
	std::string _bin_folder;

	TimeInfo _comp_time;
	TimeInfo _overall_time;

	// Debug related =====================================================================
	bool _debug;

	// Visualization related =============================================================
	visualization::CloudVisualizerSimple * _viewer;
	std::vector<Cloud> _viz_clouds;

	//Visualization methods...
	void visualizePreComputationClouds(const Cloud & sample_cloud, unsigned int pause_ms);
	void visualizePostComputationClouds(const Cloud & removing_indices_cloud, const Cloud & current_cloud, unsigned int pause_ms);
};

} /* namespace cloud_cleaning */

#endif /* CLOUD_CLEANING_CHANGEDETECTIONWDST_H_ */
