/*
 * ChangeDetectionOriginal.cpp
 *
 *  Created on: Jul 3, 2015
 *      Author: george
 */

#include "ChangeDetectionOriginal.h"

#include <vector>
#include <set>

#include <pcl/octree/octree_pointcloud.h>
#include <pcl/octree/octree_impl.h>

#include "../utility/mini_profiler.h"

#include "../filters/MultiFilter.h"
#include "../filters/FloorRemoval.h"
#include "../filters/PlaneSegmentation.h"

#include "../utility/helper_functions.h"

#include <set>

#define PI 3.14159265358979
#define _2PI 2.0*3.14159265358979

#define OMP_CHUNK_SIZE	1000

#define CATCH_NAN(x) \
	if(std::isnan(x)){ \
		std::cout << "Caught "#x" as NaN\n";\
		std::cin.get(); }\
	else if(std::isinf(x)){ \
		std::cout << "Caught "#x" to be INF";\
		std::cin.get(); }\

/*
 * Questo è il ChangeDetection originale preso dal paper
 */

namespace cloud_cleaning {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DST FRAME ==========================================================================================================
DSTframeX::DSTframeX()
: _traj_norm(0.0), _shift(0.0), _debug(false), _viewer(nullptr), _rig_id(-1)
	,_points_computed(0), _avg_neighbors(0), _minmax_dist(1)
{ }

void DSTframeX::setTrajectoryVector(const Eigen::Vector3f& traj) {
	_traj_vector = traj;
	_traj_normal = _traj_vector.normalized();
	_traj_norm = _traj_vector.norm();
}

void DSTframeX::setCameraRig(CameraRig& rig) {
	// Save the rig id
	_rig_id = rig.getID();

	// Save the bounding box
	_bbox = RigBox(rig);

	// For visualization purposes
	_cloud = rig.getCloud();

	// Save the sensor origin
	_origin = rig.getPose().cast<float>().block<3,1>(0,3);

	// Copy the point cloud and filter it
	Cloud::pclCloud::Ptr input_cloud;
	if(_filter.empty()){
		input_cloud = rig.getCloud().cloud_shared_ptr();
	} else {
		_filter.filter(rig.getCloud().cloud_shared_ptr(), input_cloud);
	}

	auto& work_cloud = *input_cloud;
	// Resize the points
	// This data contains points transformed in local reference frame
	_points.resize(work_cloud.size());

	// Transform the cloud into spherical coordinates cloud
	pcl::PointCloud<pcl::PointXY>::Ptr tree_cloud(new pcl::PointCloud<pcl::PointXY>());
	tree_cloud->points.resize(work_cloud.size());

	double min_d = std::numeric_limits<double>::max();
	double max_d = 0;

	int point_i = 0;
	double x, y, z;

	#pragma omp parallel for \
	default(shared) \
	private(point_i, x, y, z)\
	schedule(dynamic, OMP_CHUNK_SIZE)
	// Transform each point into a spherical coordinates point
	//for(auto& point: input_cloud->points){
	for(point_i = 0; point_i < _points.size(); point_i++){
		auto& point = work_cloud[point_i];
		auto& p_i = _points[point_i];
		auto& p =  tree_cloud->points[point_i];

		// Move the point to local reference frame
		p_i.vec[0] = x = point.x - _origin[0];
		p_i.vec[1] = y = point.y - _origin[1];
		p_i.vec[2] = z = point.z - _origin[2];
		p_i.norm = p_i.vec.norm();

		// From now on x,y will become phi and theta
		// Phi calculation
		p.x = atan2(y,x);

		// Theta calculation
		p.y = atan2(sqrt(x*x + y*y),z);
		//p.y = acos(z/sqrt(x*x + y*y + z*z));

		if(p_i.norm < min_d){
			min_d = p_i.norm;
		}else if(p_i.norm > max_d){
			max_d = p_i.norm;
		}

		// Done transforming
	}

	// Set the spherical point representation
	// Add the newly created cloud to main kd-tree
	_kdtree.setInputCloud(tree_cloud);

	// Compute min max distance for weight normalization
	_minmax_dist = max_d - min_d;
}

void DSTframeX::setFilter(const MultiFilter& filter) {
	_filter = filter;
}

void DSTframeX::setParameters(DSTparameter parameter) {
	// Save the parameters
	_param = parameter;

	// Prepare the gaussian function
	_gaussian.precompute(_param.sigma_step);
	//std::cout << "Gaussian beams: " << _gaussian.beams << "\n";

	// Perform the convolution of error functions ===================================
	// Needed to set the shift and find the convoluted gaussian mean
	double max_occupation = 0;
	double max_emptiness = 0;
	// Shortcut variables
	auto& b_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;
	// Do the convolution
	for(double R = -3.0*_param.sigma_f;
			R <= 3.0*(_param.sigma_f + _param.lambda_thick);
			R += _param.sigma_step * _param.sigma_f){

		double occupation = 0;
		double emptyness = 0;
		double r;
		// sum up the inner loop
		for(int i = -beams; i <= beams; i++){
			r = R + _param.sigma_step * i * _param.sigma_f;
			if(r >= 0){
				occupation += b_values[i + beams] * exp(-0.5 * r * r / _param.sigma_f2);
			}else{
				emptyness += b_values[i + beams];
			}
		}

		// add the new occupation value
		_occupancy_gaussian.push_back(occupation);

		// add the new emptyness value
		_emptiness_gaussian.push_back(emptyness);

		// check for the maximum
		if(occupation > max_occupation){
			// set the shift value
			max_occupation = occupation;
			_shift = R;
		}
		if(emptyness > max_emptiness){
			max_emptiness = emptyness;
		}
	} // End of convolution

	// Normalize the occupation values back to [0-1]
	for(auto& value: _occupancy_gaussian){
		value = value / max_occupation;
	}

	// Normalize the emptiness values back to [0-1]
	for(auto& value: _emptiness_gaussian){
		value = value / max_emptiness;
	}

	//std::cout << "Gaussian sums: EMPTY + OCCUPIED:\n";
	// Normalize wrt to both emptiness and occupancy
	for(int i = 0; i < _occupancy_gaussian.size(); i++){
		double sum = _emptiness_gaussian[i] + _occupancy_gaussian[i];
		if(sum > 1.0){
			_emptiness_gaussian[i] /= sum;
			_occupancy_gaussian[i] /= sum;
		}
	}

	// Computing the cos_theta gaussian
	double theta_max = 0;
	for(double i = 0.0; i <= 3.0*_param.sigma_theta; i+= _param.sigma_step*_param.sigma_theta){
		double theta = exp(-0.5 * (i*i) / _param.sigma_theta2);
		_cos_theta_gaussian.push_back(theta);
		if(theta > theta_max){ theta_max = theta; }
//		std::cout << "\t" << i << ": " << theta << "\n";
	}
	for(auto& theta: _cos_theta_gaussian){
		theta /= theta_max;
	}

	std::cout << "----------- _shift = " << _shift << std::endl;
	// Done for the preparations...
}

MassConsistency DSTframeX::computeConsistency(const Cloud::pointType& location) {
	_points_computed++;

	MassConsistency final_occupancy;
	final_occupancy = computeConsistencySimple(location);

	// For debug purposes...
	_avg_occupancy.emp += final_occupancy.emp;
	_avg_occupancy.occ += final_occupancy.occ;
	_avg_occupancy.unk += final_occupancy.unk;

	// Done.. return the final occupancy
	return final_occupancy;
}

// Searches for neighbors of input point ================================================
bool DSTframeX::findNeighbors(const Eigen::Vector3f& search_point,
		double search_radius, std::vector<LocalPoint>& out_neighbors, int k_neighbors) {

	// Prepare the search point
	// Convert it to spherical coordinate system
	pcl::PointXY s_p;
	float p_x = search_point[0];
	float p_y = search_point[1];
	float p_z = search_point[2];
	s_p.x = atan2(p_y, p_x);
	s_p.y = atan2(sqrt(p_x*p_x + p_y*p_y),p_z);

	// Additional data
	std::vector<int> indices;
	std::vector<float> sqr_distances;

	// If there are points in search radius...
//	if(_kdtree.nearestKSearch(s_p, k_neighbors, indices, sqr_distances) > 0){
	if(_kdtree.radiusSearch(s_p, search_radius, indices, sqr_distances) > 0){
		int min_size = indices.size() > k_neighbors? k_neighbors: indices.size();
		out_neighbors.clear();
		out_neighbors.resize(min_size);

		// Retrieve the neighbors from cloud
		for(int n_i = 0; n_i < min_size; n_i++){
			out_neighbors[n_i] = _points[indices[n_i]];
		}
		// Set positive outcome...
		return true;
	}
	// No neighbors were found...
	return false;
}

void DSTframeX::computeRemovedPointsVisibility(
		RemovedPointsContainer& rem_points) {

	// A for loop for OpenMP
	int point_i = 0;
	#pragma omp parallel for \
						default(shared) \
						private(point_i) \
						schedule(dynamic, OMP_CHUNK_SIZE)
	for(point_i = 0; point_i < rem_points.size(); point_i++){

		// Get the removed OP and transform into local reference frame
		auto OP = rem_points.removed_points[point_i].point - _origin;

		// Since X is in the direction of the trajectory then
		// make a check for positive X... if negative, skip it
		if(OP[0] < 0){
			continue;
		}

		double OP_norm = OP.norm();

		// Find neighboring rays
		std::vector<LocalPoint> neighbors;

		if(!findNeighbors(OP, _param.seen_search_radius, neighbors)){
			// No neighbors were found... Move to next point
//			continue;
		}
		bool seen = true;
		// See for each neighbor if seen threshold was surpassed
		for(auto& OQ: neighbors){
			if(OP_norm - OQ.norm > _param.seen_threshold){
				seen = false;
				break;
			}
		}
		// If seen then add this view to point visibility
		if(seen){
			#pragma omp critical
			{
				rem_points.removed_points[point_i].view_idx.push_back(_rig_id);
			}
		}
	}

}


// SIMPLIFIED ALGORITHM =======================================================================
MassConsistency DSTframeX::computeConsistencySimple(const Cloud::pointType& location) {
	if(_bbox.isOutsideBBox(location)){
		return MassConsistency();
	}

	// Some useful shortcuts
	MassConsistency final_mass, new_mass;
	auto& b_n_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;

	const double max_r_value = 0.8;
	const double delta_r_value = 0.2;

	// Transform location to local reference frame
	// also save it for later use...
	Eigen::Vector3f OP;
	OP[0] = location.x - _origin[0];
	OP[1] = location.y - _origin[1];
	OP[2] = location.z - _origin[2];

	auto OP_normalized = OP.normalized();
	double OP_norm = OP.norm();

	// Find neighboring rays
	std::vector<LocalPoint> neighbors;

	// Search kd-tree using radius search
	if(!findNeighbors(OP, _param.search_radius, neighbors, _param.neighbors)){
//		new_mass.emp = 0.6;
//		new_mass.occ = 0.4;
//		new_mass.unk = 0;

		return new_mass;
	}

	double rounded_value = max_r_value - delta_r_value*(OP_norm/_minmax_dist);

	// Neighboring Points calculation ==========================
	// Compute consistency over all neighboring points
	double cons_emp;	// empty consistency
	double cons_occ;	// occupied consistency
	double cons_angle;	// angular consistency

	// Used to set final_mass
	bool first_pass = true;

	// Shortcut for angular consistency
	double cos_theta_min = cos(3.0*_param.sigma_theta);
	double cos_theta_step = (1.0 - cos_theta_min)/_cos_theta_gaussian.size();

	// Begin the neighborhood loop
	for(auto& OQ: neighbors){
		// Compute ray distance: r = OP2_norm - OQ_norm
		double ray_distance = OP_norm - OQ.vec.dot(OP_normalized);

		// Compute cos(theta) for angular consistency
		double cos_theta = OQ.vec.dot(OP)/(OQ.norm*OP_norm);

		// Compute consistencies ===============================.
		// Reset consistencies
		cons_emp = cons_occ = 0.0;

		int offset = static_cast<int>(
				(ray_distance + 3.0*_param.sigma_f + _shift)/(_param.sigma_step*_param.sigma_f));
		// Compute occupancy and emptiness consistency
		if(offset < 0){
			cons_occ = 0.0;
			cons_emp = 1.0;
		}
		else if(offset < _occupancy_gaussian.size()){
			cons_occ = _occupancy_gaussian[offset];
			cons_emp = _emptiness_gaussian[offset];
		}else{
			cons_emp = 0.0;
			cons_occ = 0.0;
		}

		// Compute angular consistency
		if(cos_theta < cos_theta_min){
			cons_angle = 0;
		}else{
			int offset = static_cast<int>((1.0 - cos_theta) / cos_theta_step);
			cons_angle = _cos_theta_gaussian[offset];
		}

		// Compute masses: e1 = empty, o1 = occupied, u1 = unknown
		// Weight more occupy (do not multiply by angle gaussian)
		new_mass.occ = cons_occ;// * cons_angle;
		new_mass.emp = cons_emp * cons_angle;
		new_mass.unk = 1.0 - new_mass.occ - new_mass.emp;
		//new_mass.rebalance();

		if(first_pass){
			final_mass = new_mass;
			first_pass = false;
		}else{
			final_mass.fuse(new_mass);
		}

	} // End of neighborhood loop

	final_mass.roundTo(rounded_value);

	return final_mass;
}

void DSTframeX::computeConsistencies(
		const Cloud& original_cloud, std::vector<MassConsistency>& consistencies) {

	// The vector containing all consistencies
	//std::vector<MassConsistency> consistencies;
	#pragma omp critical
	{
		if(consistencies.size() != original_cloud.size()){
			consistencies.resize(original_cloud.size());
		}
	}
	auto& b_n_values = _gaussian.beam_values_normalized;
	auto& beams = _gaussian.beams;

	// Transform each point to local reference frame
	// and calculate its consistency map in this frame
	int point_i;

	// Some variable declarations
	MassConsistency new_mass, final_mass;

	#pragma omp parallel for \
	default(shared) \
	private(point_i) \
	schedule(dynamic, OMP_CHUNK_SIZE)
	for(point_i = 0; point_i < original_cloud.size(); point_i++){
		auto& P = original_cloud[point_i];

		// Check if in bounding box, if not then skip this point
		if(_bbox.isOutsideBBox3D(P)){
//			new_mass.emp = 0.5;
//			new_mass.occ = 0.5;
//			new_mass.unk = 0;
//			//			new_mass.roundTo(rounded_value);
//
//			#pragma omp critical
//			{
//				consistencies[point_i].fuse(new_mass);
//			}
			continue;
		}

		// Get the point in local reference frame
		Eigen::Vector3f OP;
		OP[0] = P.x - _origin[0];
		OP[1] = P.y - _origin[1];
		OP[2] = P.z - _origin[2];
		double OP_norm = OP.norm();

		// Find neighboring rays
		std::vector<LocalPoint> neighbors;

		// Search kd-tree using radius search
		if(!findNeighbors(OP, _param.search_radius, neighbors)){
			continue;
		}

		// Neighboring Points calculation ==========================
		// First some shortcuts
		final_mass = MassConsistency();

		// Compute consistency over all neighboring points
		double cons_emp;	// empty consistency
		double cons_occ;	// occupied consistency
		double cons_angle;	// angular consistency
		double cons_traj;	// trajectory consistency
		//double cons_normal; // normal consistency -> higher weight for close to point cameras

		// Used to set final_occupancy
		bool first_pass = true;

		// Shortcut for angular consistency
		double cos_theta_min = cos(3.0*_param.sigma_theta);
		double cos_theta_step = (1.0 - cos_theta_min)/_cos_theta_gaussian.size();

		double t_length = OP.dot(_traj_normal);
		Eigen::Vector3f OP1 = OP - t_length*_traj_normal;
		double OP1_norm = OP1.norm();

		double lambda_t2 = (_traj_norm*_traj_norm)/4;
		double t_distance = OP.dot(_traj_vector);
		double t;
		cons_traj = 0;
		for(int i = -beams; i <= beams; i++){
			t = t_distance + _param.sigma_step * i * _param.sigma_r;
			double Gr = b_n_values[i + beams];
			double Gt = exp(-0.5*(t*t)/lambda_t2);
			cons_traj += Gr*Gt;
		}

		// Begin the neighborhood loop
		for(auto& OQ: neighbors){
			// Compute ray distance: r = OP2_norm - OQ_norm
			double ray_distance = OP_norm - OQ.vec.dot(OP.normalized());
//			double ray_distance = OP_norm - OQ.norm;

			// Compute cos(theta) for angular consistency
			double cos_theta = OQ.vec.dot(OP)/(OQ.norm*OP_norm);

			// Compute consistencies ===============================.
			// Reset consistencies
			cons_emp = cons_occ = 0.0;

			int offset = static_cast<int>(
									(ray_distance + 3.0*_param.sigma_f + _shift)/(_param.sigma_step*_param.sigma_f));
			// Compute occupancy and emptiness consistency
			if(offset < 0){
				cons_occ = 0.0;
				cons_emp = 1.0;
			}
			else if(offset < _occupancy_gaussian.size()){
				cons_occ = _occupancy_gaussian[offset];
				cons_emp = _emptiness_gaussian[offset];
			}else{
				cons_emp = 0.0;
				cons_occ = 0.0;
			}

//			double cos_theta = OQ.vec.dot(OP1)/(OQ.norm * OP1_norm);
			double theta;
			if(cos_theta >= 1.0 || cos_theta <=-1.0){
				theta = 0.0;
			}else{
				theta = acos(cos_theta);
			}
			double sigma_theta = _param.sigma_r / OQ.norm;
			// Compute angular_consistency and empty consistency
			cons_angle = 0;
			cons_emp = 0;
			for(int i = -beams; i <= beams; i++){
				double Gr = b_n_values[i + beams];
				double a = theta + _param.sigma_step * i * sigma_theta;
				double Ga = exp(-0.5 * (a*a)/_param.lambda_theta2);
				cons_angle += Gr * Ga;
				double r = ray_distance + _param.sigma_step * i * _param.sigma_f + _shift;
				double Ge = (r < 0)? 1.0 : 0.0;
				cons_emp += Gr * Ge;
			}
//			CATCH_NAN(cons_angle);
//			CATCH_NAN(cons_traj);
//			CATCH_NAN(cons_occ);
//			CATCH_NAN(cons_emp);
			// Compute angular consistency
//			if(cos_theta < cos_theta_min){
//				cons_angle = 0;
//			}else{
//				int offset = static_cast<int>((1.0 - cos_theta) / cos_theta_step);
//				cons_angle = _cos_theta_gaussian[offset];
//			}

			// Compute masses: e1 = empty, o1 = occupied, u1 = unknown
			// Weight more occupy (do not multiply by angle gaussian)
			new_mass.occ = cons_occ * cons_angle;// * cons_traj;
			new_mass.emp = cons_emp * cons_angle;// * cons_traj;
			new_mass.unk = 1.0 - new_mass.occ - new_mass.emp;
			new_mass.rebalance();

//			CATCH_NAN(new_mass.occ);
//			CATCH_NAN(new_mass.emp);
//			CATCH_NAN(new_mass.unk);

			if(first_pass){
				final_mass = new_mass;
				first_pass = false;
			}else{
				if(final_mass.emp > 0.2 && new_mass.occ > 0.2){
					for(int i = 0; i < 6; i++){
						final_mass.fuse(new_mass);
					}
				}
				else{
					final_mass.fuse(new_mass);
				}
			}


			//-------------------------------------------------------------------
//			double OQ_norm = OQ.norm();
//			// Compute OP2: OP projected onto OQ
//			Eigen::Vector3f OP2 = OQ.dot(OP)/OQ.dot(OQ) * OQ;
//			double OP2_norm = OP2.norm();
//
//			// Compute ray distance: r = OP2_norm - OQ_norm
//			double ray_distance = OP2_norm - OQ_norm;
//
//			// Compute theta angle...
//			double cos_theta = OQ.dot(OP1)/(OQ_norm * OP1_norm);
//			double theta;
//			if(cos_theta >= 1.0 || cos_theta <=-1.0){
//				theta = 0.0;
//			}else{
//				theta = acos(cos_theta);
//			}
//			if(OQ_norm * OP1_norm == 0){
//				std::cout << "OQ_norm * OP1_norm = ZERO!!!\n";
//			}
//
//			// Compute consistencies ===============================.
//			// Reset consistencies
//			cons_a = cons_emp = cons_occ = 0.0;
//			// Compute sigma_theta;
//			// TODO: See if sigma_r2 or sigma_r for sigma_theta...
//			double sigma_theta = _param.sigma_r / OQ_norm;
//			// Compute angular_consistency and empty consistency
//			for(int i = -beams; i <= beams; i++){
//				double Gr = b_n_values[i + beams];
//				double a = theta + _param.sigma_step * i * sigma_theta;
//				double Ga = exp(-0.5 * (a*a)/_param.lambda_theta2);
//				cons_angle += Gr * Ga;
//				double r = ray_distance + _param.sigma_step * i * _param.sigma_f + _shift;
//				double Ge = (r < 0)? 1.0 : 0.0;
//				cons_emp += Gr * Ge;
//			}
//
//			// Compute occupancy consistency
//			if(ray_distance >= -3.0*_param.sigma_f && ray_distance <= 3.0*(_param.sigma_f + _param.lambda_thick)){
//				int offset = static_cast<int>(
//						(ray_distance + 3.0*_param.sigma_f + _shift)/(_param.sigma_step*_param.sigma_f));
//				cons_occ = _occupancy_gaussian[offset];
//				CATCH_NAN(cons_occ)
//			}
//
//			// Compute masses: e1 = empty, o1 = occupied, u1 = unknown
//			double o1 = cons_a * cons_t * cons_occ;
//			double e1 = cons_a * cons_t * cons_emp;
//			if(e1 + o1 > 1.0){
//				o1 = o1 / (e1 + o1);
//				e1 = e1 / (e1 + o1);
//				//std::cout << " e1 + o1 surpassing 1\n";
//			}
//			double u1 = 1.0 - e1 - o1;
//
//		}
			//-------------------------------------------------------------------
			//final_mass.rebalance();
		} // End of neighborhood loop

//		CATCH_NAN(final_mass.occ);
//		CATCH_NAN(final_mass.emp);
//		CATCH_NAN(final_mass.unk);

		final_mass.rebalance();

		#pragma omp critical
		{
			// Fuse it to consistencies
			consistencies[point_i].fuse(final_mass);
		}
	} // End of each point loop
}


void DSTframeX::enableDebug(bool enable) {
	_debug = enable;
}

void DSTframeX::printDebug() {
	double points = static_cast<double>(_points_computed);
	_avg_neighbors = _avg_neighbors / points;
	_avg_occupancy.emp /= points;
	_avg_occupancy.occ /= points;
	_avg_occupancy.unk /= points;
	std::cout << "Frame >> avg_neighbors = " << _avg_neighbors
			<< "\n avg_occupancy(e,o,u): (" << _avg_occupancy.emp
			<< ", " << _avg_occupancy.occ << ", " << _avg_occupancy.unk << ")\n";
	// Reset the values...
	_avg_neighbors = 0.0;
	_avg_occupancy = MassConsistency();
	_points_computed = 0;
}

void DSTframeX::setVisualizer(visualization::CloudVisualizerSimple* visualizer) {
	_viewer = visualizer;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change Detection Original
ChangeDetectionOriginal::ChangeDetectionOriginal(): Module("ObjectRemovalOriginal", true),
		_viewer(nullptr),
		_finished(false),
		_min_buffer_size(0),
		_max_buffer_size(1),
		_skip_frames(2),
		_debug(false),
		_floor_separation_ON(false),
		_post_process_ON(true),
		_frames_delay(4),
		_voxelized_algorithm_ON(false),
		_voxel_size(0.2),
		_swap_removed_points_ON(true),
		_save_removed_points_ON(true),
		_rem_points_filename("../output/rem_points_out.txt"),
        _rem_points_ofstream(std::make_shared<std::ofstream>("../output/rem_points_out.txt"))
{
	// Set up the default parameters...
	_param.search_radius 	= 0.3 * PI/180.0; // In rad
	_param.neighbors		= 16;

	_param.lambda_t 		= 0.05;
	_param.lambda_theta 	= _param.search_radius;
	_param.lambda_thick 	= 0.05;

	_param.sigma_r 			= 0.15;
	_param.sigma_m 			= 0.05;
	_param.sigma_step 		= 0.2;

	_param.sigma_theta	= _param.lambda_theta;

	// Removed points visibility
	_param.seen_search_radius = 0.3*PI/180.0;
	_param.seen_threshold = 0.3;

	// Compute the squared values of parameters
	_param.precompute();

	// Nullify the last rigs...
	_last_rigs[0] = _last_rigs[1] = nullptr;

	// Register profiler topic
	PROFILER_INIT_TIMER(name(), "Dempster-Shafer change detection omp time");
}

ChangeDetectionOriginal::~ChangeDetectionOriginal() { /* Nothing for now */ }

void ChangeDetectionOriginal::setScene(Scene* scene) { /* No need for that */ }

bool ChangeDetectionOriginal::process(CameraRig* rig, int iteration_index) {
	 std::cout << name() << ": iteration " << std::to_string(iteration_index) << "\n";


	 auto time_start = std::chrono::system_clock::now();

	// If not finished do the standard work
	if(!_finished){
		// If input is null then return false...
		if(!rig){
			std::cout << "MM >> Received null rig\n";
			return false;
		}

		// Add a new frame... or update existing one
		std::shared_ptr<DSTframeX> new_frame;
		if(_frames.size() < _max_buffer_size){
			new_frame = std::shared_ptr<DSTframeX>(new DSTframeX());
			// TODO: consider to dynamically change parameters
			new_frame->setParameters(_param);
			// Debugging and visualization...
			new_frame->enableDebug(_debug);
			new_frame->setVisualizer(_viewer);
		}else{
			new_frame = _frames.front();
			_frames.erase(_frames.begin());
		}

		// Setup the new frame and its origin
		new_frame->setCameraRig(*rig);
		new_frame->setFilter(_filter);

		// Calculate the trajectory vector.. O[i+1] - O[i-1]
		// Check if _last_rig was set
		if(!_last_rigs[0]){
			_last_rigs[0] = _last_rigs[1] = rig;
			new_frame->setTrajectoryVector(Eigen::Vector3f(1, 0, 0));
		}
		else{
			Eigen::Vector3f traj = rig->getPose().cast<float>().block<3,1>(0,3)
									- _last_rigs[1]->getPose().cast<float>().block<3,1>(0,3);
			new_frame->setTrajectoryVector(traj);
		}
		_frames.push_back(new_frame);

		// If visualization is enabled... Add the clouds
		if(_viewer){
			// Keep the same size as the buffer has
			if(_viz_clouds.size() >= _max_buffer_size){
				_viz_clouds.erase(_viz_clouds.begin());
			}
			// Add the last cloud to visualization buffer
			_viz_clouds.push_back(rig->getCloud());
		}

		// Separate the floor if it was requested...
		if(_floor_separation_ON){
			///////////////////////////////////////////////////////////////////////////////
			// Remove floor from the cloud ===============================================
			if(rig->createCloud("floor")){
				_floor_removal->setInputCloud(rig->getCloud().cloud_shared_ptr());
				_floor_removal->filter(rig->getCloud().cloud(), rig->getCloud("floor").cloud());
			}
			///////////////////////////////////////////////////////////////////////////////
		}

		// Add the rig to the queue
		// but skip the first one (almost useless)
		if(_frames.size() > _min_buffer_size){
			_rigs.push(rig);
		}

		// "Rotate" the last rigs
		_last_rigs[1] = _last_rigs[0];
		_last_rigs[0] = rig;

//		std::cout << "----------- DST: buffer.size = " << _frames.size() << "\n";
//		std::cout << "----------- DST:   rigs.size = " << _rigs.size() << "\n";
	}
	// Otherwise work with remaining rigs in queue
	else {
		// Remove frames, two at a time.
		_frames.erase(_frames.begin());
		_frames.erase(_frames.begin());

		// If visualization is enabled.. Remove the clouds
		if(_viewer){
			_viz_clouds.erase(_viz_clouds.begin());
			_viz_clouds.erase(_viz_clouds.begin());
		}

		// Empty the _rigs queue if it has only min_buffer_size elements
		if(_rigs.size() <= _min_buffer_size){
			std::queue<CameraRig*> empty_queue;
			std::swap(_rigs, empty_queue);
			_frames.clear();
			return true;
		}
	}
	// Start the removal algorithm if the buffer has odd size (for symmetry)...
	if(_rigs.size() > _min_buffer_size && (_frames.size() % 2)){
		// Execute the appropriate algorithm
		removeMovingObjects();

		auto time_diff = std::chrono::system_clock::now() - time_start;
		double nano = time_diff.count();

		_overall_time.aggregate(nano);

		return true;
	}

	auto time_diff = std::chrono::system_clock::now() - time_start;
	double nano = time_diff.count();

	_overall_time.aggregate(nano);

	return false;
}

void ChangeDetectionOriginal::removeMovingObjects() {
	// Prepare Scene 2, all frames except current one and skipped ones
	int half_way = _rigs.size() + _frames.size()%_skip_frames;
	std::vector<std::shared_ptr<DSTframeX> > scene2;
	for(int i = 0; i < _rigs.size() - 1; i+=_skip_frames){
		scene2.push_back(_frames[i]);
	}
	for(int i = half_way; i < _frames.size(); i++){
		scene2.push_back(_frames[i]);
	}

	// Indices vector...
	//std::vector<int> indices_to_remove;
	Cloud::indices_ptr indices_to_remove(new std::vector<int>());

	// For visualization only..
	Cloud viz_cloud;

	// Some useful shortcuts...
	auto& current_cloud = _rigs.front()->getCloud();

	// Visualize the precomputed clouds
	if(_viewer){
		visualizePreComputationClouds(_viz_clouds[_rigs.size()-1], 1000);
	}

	// Only for debug
	std::string info("DST >> analyzing cloud... ");

	auto time_start = std::chrono::system_clock::now();
	PROFILER_START_TIMER(name());
	// For statistics...
	MassConsistency avg_occ;

	std::vector<MassConsistency> points_consistencies;
	int frame_i;
	#pragma omp parallel for default(shared) private(frame_i)
	for(frame_i = 0; frame_i < scene2.size(); frame_i++){
		scene2[frame_i]->computeConsistencies(current_cloud, points_consistencies);
//		scene2[frame_i]->computeConsistenciesNewRule(current_cloud, points_consistencies);
//		scene2[frame_i]->computeConsistenciesMedian(current_cloud, points_consistencies);
	}

	// Variables to keep track of removed points
	RemovedPointsContainer rem_points;
	rem_points.rig_id = _rigs.front()->getID();
	rem_points.rig_pose = _rigs.front()->getPose();

	// Search for points to be removed...
	// If post process was requested, do it
	if(_post_process_ON){
		postCheckRemovedPoints(current_cloud, viz_cloud,
				*indices_to_remove, points_consistencies, rem_points);
	}
	// Otherwise process the raw removed points
	else{
		// Start the loop
		for(int i = 0; i < points_consistencies.size(); i++){
			auto& mass = points_consistencies[i];
			if(mass.emp > mass.occ && mass.emp > mass.unk){
				auto& p = current_cloud[i];

				// If empty prevails then mark this point as to be removed
				indices_to_remove->push_back(i);

				///////////////////////////////////////////////////////////
				// Save the removed points
				rem_points.removed_points.push_back(RemovedPoint(p.x, p.y, p.z, rem_points.rig_id));
				_removed_points += p;
				//////////////////////////////////////////////////////////

				// Only for statistics...
				avg_occ.emp += mass.emp;
				avg_occ.occ += mass.occ;
				avg_occ.unk += mass.unk;

				if(_viewer){ viz_cloud += p; }
			}
		}
	}

	PROFILER_STOP_TIMER(name());
	auto time_diff = std::chrono::system_clock::now() - time_start;
	double nano = time_diff.count();

	_comp_time.aggregate(nano);

	std::cout << info << " done; removed points = " << indices_to_remove->size() << "\n";
	std::cout << "Computation time: " << nano/1e6 << " millis\n";

	// For debug purposes...
	std::cout << "DST DEBUG: total points = " << current_cloud.size() << "\n";
	//for(auto& frame: scene2){
		//frame->printDebug();
	//}

	if(!_bin_folder.empty()){
        createFolder(_bin_folder);

		///////////////////////////////////////////////////////////////////////////////////////
		// Save the points in a binary file
		std::fstream _bin_file (_bin_folder + "000" + std::to_string(_rigs.front()->getID()) + ".bin",
				std::ios::out | std::ios::binary);
		for(int i = 0; i < indices_to_remove->size(); ++i){
			//Some calculations to fill a[]
			auto& p = current_cloud[indices_to_remove->operator [](i)];
			_bin_file.write((char*)&p.x, sizeof(float)*3);
		}
		_bin_file.close();
	}
	// Remove the marked points from cloud
	current_cloud.removePoints(indices_to_remove);

	if(_save_removed_points_ON){
		///////////////////////////////////////////////////////////////////////////////////
		// Get the views for every removed point
		// First limit the amount of frames to be checked...
		auto last_frame_to_check = scene2.size() - _rigs.size()/2;
		#pragma omp parallel for default(shared) private(frame_i)
		for(frame_i = 0; frame_i <last_frame_to_check; frame_i++){
			scene2[frame_i]->computeRemovedPointsVisibility(rem_points);
		}
		// Filter out points with only one view...
		RemovedPointsContainer filtered_rem_points;
		for(auto& p: rem_points.removed_points){
			if(p.view_idx.size() > 1){
				filtered_rem_points.removed_points.push_back(p);
			}
		}
		filtered_rem_points.rig_id = rem_points.rig_id;
		filtered_rem_points.rig_pose = rem_points.rig_pose;
		///////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////
		// WRITE REMOVED POINTS
		// <rig_id> <num_points>
        (*_rem_points_ofstream) << filtered_rem_points.rig_id << " " << filtered_rem_points.size() << "\n";
		// All points with their respected view id
		for(auto& p: filtered_rem_points.removed_points){
			// <point.x> <point.y> <point.z>
            (*_rem_points_ofstream) << p.point[0] << " " << p.point[1] << " " << p.point[2];
			// <num_views>
            (*_rem_points_ofstream) << " " << p.view_idx.size();
			for(auto& view_id: p.view_idx){
				// <view_id>
                (*_rem_points_ofstream) << " " << view_id;
			}
            (*_rem_points_ofstream) << "\n";
		}
		///////////////////////////////////////////////////////////////////////////////////
	}

	// Add the floor if it was separated...
	if(_floor_separation_ON){
		///////////////////////////////////////////////////////////////////////////////////
		// Put the floor back
		current_cloud += _rigs.front()->getCloud("floor");
		_rigs.front()->clearCloud("floor");
		//////////////////////////////////////////////////////////////////////////////////
	}

	// Visualize...
	if(_viewer){
		visualizePostComputationClouds(viz_cloud, current_cloud, 2000);
	}

	// Change the previously saved cloud with the new cleaned one
	if(_swap_removed_points_ON){
		_frames[_rigs.size()-1]->setCameraRig(*(_rigs.front()));
		if(_viewer){
			_viz_clouds[_rigs.size()-1] = current_cloud;
		}
	}
}


void ChangeDetectionOriginal::postCheckRemovedPoints(Cloud& cloud, Cloud &viz_cloud,
		std::vector<int> &indices_to_remove,
		std::vector<MassConsistency>& consistencies,
		RemovedPointsContainer& rem_container) {

	// Mandatory variables for kd-tree search
	std::vector<int> indices;
	std::vector<float> sqr_distances;

	// Do the size checks
	if(_pp_clouds.size() >= _frames_delay){
		// Add a new tree
		_pp_frames.push_back(pcl::KdTreeFLANN<Cloud::pointType>());
		_pp_frames.back().setInputCloud(_pp_clouds[0].cloud_shared_ptr());

		// See if size too big and remove the first tree
		if(_pp_frames.size() >= _rigs.size() - _frames_delay){
			_pp_frames.erase(_pp_frames.begin());
		}

		// Remove the first cloud in line
		_pp_clouds.erase(_pp_clouds.begin());
	}

	// Add a new empty cloud for this frame removed points
	_pp_clouds.push_back(Cloud());

	// Aux data...
	Cloud &new_cloud = _pp_clouds.back();
	bool skip_point;
	auto search_radius = _voxel_size;
	int min_points = 1;

	// Start the loop
	for(int i = 0; i < consistencies.size(); i++){
		auto& mass = consistencies[i];
		if(mass.emp > mass.occ && mass.emp > mass.unk){
			// Get the point from the cloud
			auto& p = cloud[i];

			// Add it to the removed points cloud for future comparisons
			new_cloud += p;

			// Reset the skip point flag
			skip_point = false;

			// Search if there are neighbors for the point
			for(auto& frame: _pp_frames){
				if(frame.radiusSearch(p, search_radius, indices, sqr_distances) >= min_points){
					skip_point = true;
					break;
				}
			}

			// If yes then this point is a false positive
			if(skip_point){ continue; }

			// If empty prevails then mark this point as to be removed
			indices_to_remove.push_back(i);

			///////////////////////////////////////////////////////////
			// Save the removed points
			rem_container.removed_points.push_back(RemovedPoint(p.x, p.y, p.z, rem_container.rig_id));
			_removed_points += p;
			//////////////////////////////////////////////////////////

			if(_viewer){ viz_cloud += p; }
		}
	}
}

CameraRig* ChangeDetectionOriginal::getOutputCameraRig() {
	if(_rigs.empty()) return nullptr;
	auto return_rig = _rigs.front();
	_rigs.pop();
	return return_rig;
}

bool ChangeDetectionOriginal::hasFinished() {
	if(_finished && _rigs.empty()){
//		std::ofstream points_out("../output/rem_points.txt");
//		points_out << _removed_points.size() << "\n";
//		for(auto& point: _removed_points.cloud().points){
//			points_out << point.x << " " << point.y << " " << point.z << "\n";
//		}
//		points_out.close();
        _rem_points_ofstream->close();
		std::cout << "[DST Change Detection]: Removed Points: " << _removed_points.size() << "\n";

		if(!_info_file.empty()){
            createFolderFromFilename(_info_file);
			std::ofstream info_file;
			info_file.open(_info_file, std::ios::out | std::ios::app);

			info_file << _param.sigma_r << "\t" << _param.search_radius << "\t"
					<< _comp_time.min << "\t" << _comp_time.max << "\t"
					<< _comp_time.avgMillis() << "\t" << _comp_time.sum / 1000000.0 << "\t"
					<< _overall_time.min << "\t" << _overall_time.max << "\t"
					<< _overall_time.avgMillis() << "\t" << _overall_time.sum / 1000000.0 << "\n";

			info_file.close();
			_info_file.clear();
		}

		if(_viewer){
			_viewer->clear();
			_viewer->setTitle("All Removed Points");
			_viewer->addPointCloud(_removed_points, "removed_points", visualization::Color::RED);
//			std::cin.get();
		}
		return true;
	}
//	return _finished && _rigs.empty();
	return false;
}

void ChangeDetectionOriginal::finalize() {
	_finished = true;
}

void ChangeDetectionOriginal::setParameters(const DSTparameter& params) {
	_param = params;
	_param.precompute();
}

void ChangeDetectionOriginal::setMaxPreceedingClouds(size_t nr_clouds) {
	_max_buffer_size = 2*nr_clouds + 1;
}

void ChangeDetectionOriginal::setMinPreceedingClouds(size_t nr_clouds) {
	_min_buffer_size = nr_clouds;
}

DSTparameter& ChangeDetectionOriginal::getParameters() {
	return _param;
}

size_t ChangeDetectionOriginal::getMaxPreceedingClouds() {
	return _max_buffer_size;
}

size_t ChangeDetectionOriginal::getMinPreceedingClouds() {
	return _min_buffer_size/2;
}

void ChangeDetectionOriginal::enableVisualization(
		visualization::CloudVisualizerSimple* visualizer) {
	_viewer = visualizer;

	for(auto& frame: _frames){
		frame->setVisualizer(visualizer);
	}
}

void ChangeDetectionOriginal::disableVisualization() {
	std::string previous = "previous";
	std::string next = "next";
	for(int i = 0; i < _rigs.size() - 1 && i < _viz_clouds.size(); i++){
		std::string name = previous + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
	for(int i = _rigs.size(); i < _viz_clouds.size(); i++){
		std::string name = next + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
	_viewer->removePointCloud("current_cloud");
	_viewer->removePointCloud("sample_cloud");
	_viewer->removePointCloud("indices_to_remove");

	_viewer = nullptr;
	_viewer = nullptr;
	_viz_clouds.clear();
}

void ChangeDetectionOriginal::visualizePreComputationClouds(
		const Cloud& sample_cloud, unsigned int pause_ms) {
	auto duration = std::chrono::duration<double, std::milli>(pause_ms);

	std::string previous = "previous";
	std::string next = "next";
	int second_half_start = _rigs.size() + _viz_clouds.size()%_skip_frames;
	_viewer->clear();
	_viewer->setTitle("Vallet Change Detection", visualization::Color::GREY);
	for(int i = 0; i < _rigs.size() - 1 && i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = previous + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->addPointCloud(_viz_clouds[i], name, visualization::Color::GREEN);
		}
	}
	for(int i = second_half_start; i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = next + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->addPointCloud(_viz_clouds[i], name, visualization::Color::YELLOW);
		}
	}

	std::this_thread::sleep_for(duration);

	_viewer->addPointCloud(sample_cloud, "sample_cloud", visualization::Color::CYAN);

	for(int i = 0; i < _rigs.size() - 1 && i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = previous + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
	for(int i = second_half_start; i < _viz_clouds.size(); i+=_skip_frames){
		std::string name = next + std::to_string(i);
		if(_viz_clouds[i].cloud_shared_ptr()){
			_viewer->removePointCloud(name);
		}
	}
}

void ChangeDetectionOriginal::setSkippedFrames(size_t skipped_frames) {
	_skip_frames = skipped_frames;
}

size_t ChangeDetectionOriginal::getSkippedFrames() {
	return _skip_frames;
}


void ChangeDetectionOriginal::visualizePostComputationClouds(
		const Cloud& removing_indices_cloud, const Cloud& current_cloud,
		unsigned int pause_ms) {
	auto duration = std::chrono::duration<double, std::milli>(pause_ms);

	_viewer->addPointCloud(current_cloud, "current_cloud");
	_viewer->addPointCloud(removing_indices_cloud, "indices_to_remove", visualization::Color::RED);
	_viewer->removePointCloud("sample_cloud");
	std::this_thread::sleep_for(duration);

	_viewer->removePointCloud("indices_to_remove");
	std::this_thread::sleep_for(duration);
}

void ChangeDetectionOriginal::enableDebug(bool enable) {
	_debug = enable;
	for(auto& frame: _frames){
		frame->enableDebug(enable);
	}
}

void ChangeDetectionOriginal::saveRemovedPoints(RemovedPointsContainer& container) {
	// TODO: Still thinking if implementation needed...
}

void ChangeDetectionOriginal::enableFloorSeparation(bool enable) {
	_floor_separation_ON = enable;
	if(enable){
		_floor_removal = std::shared_ptr<filtering::FloorRemoval>
						 (new filtering::FloorRemoval(0.4, 0.4, 3, -1.5));
		_floor_removal->setMaxSlopeDifference(0.22);
	}
}

void ChangeDetectionOriginal::enablePostProcess(bool enable, int frames_delay) {
	_post_process_ON = enable;
	_frames_delay = frames_delay;
}

void ChangeDetectionOriginal::enableVoxelizedAlgorithm(bool enable,
		double voxel_size) {
	_voxelized_algorithm_ON = enable;
	if(enable){
		_filter.clear();
		_filter.addVoxelGrid(voxel_size, voxel_size, voxel_size);
		_voxel_size = voxel_size;
	}
}

void ChangeDetectionOriginal::enableDynamicRemovedPointsSwapping(bool enable) {
	_swap_removed_points_ON = enable;
}

void ChangeDetectionOriginal::enableSavingRemovedPoints(bool enable,
		const std::string& filename) {
	_save_removed_points_ON = enable;
	if(enable){
        createFolderFromFilename(filename);
		_rem_points_filename = filename;

        std::shared_ptr<std::ofstream> removed_points_ofstream(std::make_shared<std::ofstream>(filename));
        _rem_points_ofstream = removed_points_ofstream;
	}
}

void ChangeDetectionOriginal::enableInformationStoring(bool enable, std::string const &filename){
	if(enable){
		_info_file = filename;
	}
}
void ChangeDetectionOriginal::enableSavingPointsBinaryFile(bool enable, std::string const &folder){
	if(enable){
		_bin_folder = folder;
	}
}

} /* namespace cloud_cleaning */
