/*
 * MovingObjectsRemoval.h
 *
 *  Created on: Jul 1, 2015
 *      Author: george
 */

#ifndef CLOUD_CLEANING_MOVINGOBJECTSREMOVAL_H_
#define CLOUD_CLEANING_MOVINGOBJECTSREMOVAL_H_

#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"
#include "../visualization/CloudVisualizerSimple.h"

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/octree/octree.h>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/voxel_grid.h>
#include <queue>

namespace cloud_cleaning {
using namespace pipeline;

class MovingObjectsRemoval: public pipeline::Module {
public:
	MovingObjectsRemoval(size_t buffer_size,
			float voxel_size,
			int min_neighbour_points = 10,
			size_t min_trees_in_buffer = 3,
			visualization::CloudVisualizerSimple * vis = nullptr);
	virtual ~MovingObjectsRemoval();

	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();

	void setMaxPreceedingClouds(size_t nr_clouds);
	void setMinPreceedingClouds(size_t nr_clouds);
	void setSearchVoxelSize(float voxel_size);
	void setSearchRadius(float search_radius);
	void setCriticalDistance(float critical_distance);

	size_t getMaxPreceedingClouds();
	size_t getMinPreceedingClouds();
	float getSearchVoxelSize();
	float getSearchRadius();
	float getCriticalDistance();

	void enableVisualization(visualization::CloudVisualizerSimple * visualizer);
	void disableVisualization();

private:
	struct octree_wrapper{
		pcl::KdTreeFLANN<Cloud::pointType> octree;
		Eigen::Vector3f origin;
		octree_wrapper(float resolution): octree(resolution){ }
	};

	std::queue<CameraRig*> _rigs;
	std::vector<std::shared_ptr<octree_wrapper> > _trees_buffer;
	MultiFilter _filter;
	bool _finished;

	size_t _max_buffer_size;
	size_t _min_buffer_size;
	float _voxel_size;
	float _search_radius;
	float _critical_distance;
	int _min_neighbors;

	visualization::CloudVisualizerSimple * _viewer;

	void removeMovingObjects();

	//Visualization methods...
	void visualizePreComputationClouds(const Cloud & sample_cloud, unsigned int pause_ms);
	void visualizePostComputationClouds(const Cloud & removing_indices_cloud, const Cloud & current_cloud, unsigned int pause_ms);
};
} /* namespace cloud_cleaning */

#endif /* CLOUD_CLEANING_MOVINGOBJECTSREMOVAL_H_ */
