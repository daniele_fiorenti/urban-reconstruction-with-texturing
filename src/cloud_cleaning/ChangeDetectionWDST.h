/*
 * ChangeDetectionWDST.h
 *
 *  Created on: Jul 3, 2015
 *      Author: george
 */

#ifndef CLOUD_CLEANING_CHANGEDETECTIONWDST_H_
#define CLOUD_CLEANING_CHANGEDETECTIONWDST_H_

#include <pcl/point_representation.h>
#include <pcl/kdtree/kdtree_flann.h>
#include "../pipeline/Cloud.h"
#include "../pipeline/Module.h"
#include "../filters/MultiFilter.h"
#include "../filters/FloorRemoval.h"
#include "../visualization/CloudVisualizerSimple.h"

#include <vector>
#include <queue>

#define USE_RADIAL_KDTREE
//#define USE_SECOND_RADIAL_KDTREE
//#define OVERSIMPLIFIED_ALGORITHM
//#define DST_NO_UNCERTAINTIES

namespace cloud_cleaning {
using namespace pipeline;

double constexpr clamp(double x, double min, double max){
	return (x > max? max : (x < min? min : x));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
struct LocalPoint{
	Eigen::Vector3f vec;
	float norm;

	LocalPoint():norm(0){ }
	LocalPoint(Eigen::Vector3f const & point): vec(point), norm(point.norm()){ }
	LocalPoint(float x, float y, float z, Eigen::Vector3f origin = Eigen::Vector3f::Zero()){
		vec[0] = x - origin[0];
		vec[1] = y - origin[1];
		vec[2] = z - origin[2];
		norm = vec.norm();
	}

	bool operator<(const LocalPoint &other) const{
		return norm < other.norm;
	}
};

// In seconds
struct TimeInfo{
	double min;
	double max;
	double sum;
	int n;

	TimeInfo():min(std::numeric_limits<double>::max()), max(0), sum(0), n(0){ }

	void aggregate(double nano_s){
		auto millis = nano_s/1e6;
		if(millis < min){ min = millis; }
		if(millis > max){ max = millis; }
		sum += nano_s;
		n++;
	}

	double avgMillis(){
		return (sum/n)/1000000.0;
	}

	double avgNanos(){
		return sum/n;
	}
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// WDST Algorithm objects
struct MassConsistency{
	double emp;
	double occ;
	double unk;

	MassConsistency(): emp(0.0), occ(0.0), unk(1.0) { }
	MassConsistency(double empty, double occupied):
		emp(empty), occ(occupied), unk(1.0 - empty - occupied) { }

	inline void rebalance(){
		emp = clamp(emp, 0.0, 1.0);
		occ = clamp(occ, 0.0, 1.0);
		// Not sure it is needed.. but
		// ensure empty + occupied + unknown = 1.0
		if(emp + occ > 1.0){
			double total = emp + occ;
			emp = emp / total;
			occ = occ / total;
		}
		unk = 1.0 - emp - occ;
	}

	inline void roundTo(double value_1, double value_2 = 0){
		if(emp > occ && emp > unk){
			emp = value_1;
			occ = value_2;
		}
		else if(occ > emp && occ > unk){
			emp = value_2;
			occ = value_1;
		}else{
			emp = value_2;
			occ = value_2;
		}
		unk = 1.0 - emp - occ;
	}

	inline void fuse(MassConsistency const & other){
//		emp = clamp(emp, 0, 1);
//		occ = clamp(occ, 0, 1);

		double K = 1.0 - emp * other.occ - occ * other.emp;
		// There is certainly a conflict, so set max uncertainty
		if(K < 1e-5){
			emp = 0.0;
			occ = 0.0;
			unk = 1.0;
		}else{
			double e = emp*other.emp + emp*other.unk + unk*other.emp;
			double o = occ*other.occ + occ*other.unk + unk*other.occ;
			double u = unk*other.unk;

			emp = e/K;
			occ = o/K;
			unk = u/K;

			double sum = emp + occ + unk;
			if(sum > 1){
//				std::cout << "** GOT OVER 1.0: e = " << emp << " o = " << occ << " u = " << unk << " s = " << sum << "\n";
				emp /= sum;
				occ /= sum;
				unk /= sum;
			}

//			unk = 1.0 - emp - occ;
		}
	}

	inline MassConsistency& operator+=(MassConsistency const & other){
		emp += other.emp;
		occ += other.occ;
		unk += other.unk;
		return *this;
	}
};

struct Gaussian{
	std::vector<double> beam_values;
	std::vector<double> beam_values_normalized;
	double beams_sum;
	double sigma_step;
	int beams;

	void precompute(double step){
		sigma_step = step;
		beams_sum = 0;
		beams = static_cast<int>(3.0 / step);

		double sigma_step2 = step * step;
		double beam_value;
		for(int i = -beams; i <= beams; i++){
			beam_value = exp(-0.5 * sigma_step2 * i * i);
			beams_sum += beam_value;
			beam_values.push_back(beam_value);
		}

		for(int i = 0; i <= 2*beams; i++){
			beam_values_normalized.push_back(beam_values[i]/beams_sum);
		}
	}
};

struct DSTparameter{
	double sigma_r;
	double sigma_m;
	double sigma_f;
	double sigma_theta;
	double sigma_step;

	double lambda_t;
	double lambda_theta;
	double lambda_thick;

	double sigma_r2;
	double sigma_m2;
	double sigma_f2;
	double sigma_theta2;
	double sigma_step2;

	double lambda_t2;
	double lambda_theta2;
	double lambda_thick2;

	int neighbors;

	// Removed points visibility related
	double seen_search_radius;
	double seen_threshold;

	// Temporary
	double search_radius;

	inline void precompute(){
		sigma_r2 = sigma_r * sigma_r;
		sigma_m2 = sigma_m * sigma_m;
		sigma_f2 = sigma_m2 + sigma_r2;
		sigma_f = sqrt(sigma_f2);
		sigma_step2 = sigma_step * sigma_step;
		sigma_theta2 = sigma_theta * sigma_theta;

		lambda_t2 = lambda_t * lambda_t;
		lambda_theta2 = lambda_theta * lambda_theta;
		lambda_thick2 = lambda_thick * lambda_thick2;
	}
};

class DSTframe{
public:
	DSTframe();
	void setTrajectoryVector(Eigen::Vector3f const & traj);
	void setParameters(DSTparameter parameter);
	void setCameraRig(CameraRig & rig);
	void setFilter(MultiFilter const & filter);
	MassConsistency computeConsistency(Cloud::pointType const & location);
	void computeConsistencies(Cloud const & original_cloud, std::vector<MassConsistency>& consistencies);
	void computeConsistenciesNewRule(Cloud const & original_cloud, std::vector<MassConsistency>& consistencies);
	void computeConsistenciesMedian(Cloud const & cloud, std::vector<MassConsistency>& consistencies);
	void computeRemovedPointsVisibility(RemovedPointsContainer & rem_points);

	void enableDebug(bool enable);
	void printDebug();
	void setVisualizer(visualization::CloudVisualizerSimple * visualizer);

private:
	// Use 2 interleaved kdtrees to avoid angular discontinuing searches
	pcl::KdTreeFLANN<pcl::PointXY> _kdtree;
	MultiFilter _filter;
	// Keep the input cloud to retrieve the points
	std::vector<LocalPoint> _points;
	Eigen::Vector3f _traj_vector;
	Eigen::Vector3f _traj_normal;
	Eigen::Vector3f _origin;
	double _traj_norm;

	// Data for images subtraction
	int _rig_id;

	// Improving the precision
	double _minmax_dist;
	RigBox _bbox;

	// For visualization Purposes
	Cloud _cloud;

	// convolution variables
	double _shift;
	std::vector<double> _occupancy_gaussian;
	std::vector<double> _emptiness_gaussian;
	std::vector<double> _cos_theta_gaussian;
	DSTparameter _param;
	Gaussian _gaussian;

	// Various algorithms
	bool findNeighbors(Eigen::Vector3f const & search_point,
			double search_radius, std::vector<LocalPoint> &out_neighbors, int k_neighbors = 50);
	bool findNeighborsSorted(Eigen::Vector3f const & search_point,
				double search_radius, std::vector<LocalPoint> &out_neighbors, int k_neighbors = 50);
	MassConsistency computeConsistencySimple(Cloud::pointType const & location);

	// Debug related ==========================================
	bool _debug;
	visualization::CloudVisualizerSimple * _viewer;

	double _avg_neighbors;
	MassConsistency _avg_occupancy;
	int _points_computed;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// Change detection based on Weighted Dempster-Shafer Theory
class ChangeDetectionWDST: public pipeline::Module {
public:
	ChangeDetectionWDST();
	virtual ~ChangeDetectionWDST();

	// Module related ====================================================================
	void setScene(Scene * scene);
	bool process(CameraRig * rig, int iteration_index);
	CameraRig * getOutputCameraRig();
	bool hasFinished();
	void finalize();

	// Parameters related ================================================================
	void setParameters(DSTparameter const & params);
	void setMaxPreceedingClouds(size_t nr_clouds);
	void setMinPreceedingClouds(size_t nr_clouds);
	void setSkippedFrames(size_t skipped_frames);

	DSTparameter& getParameters();
	size_t getMaxPreceedingClouds();
	size_t getMinPreceedingClouds();
	size_t getSkippedFrames();

	// Algorithm related ====================================================================
	void enableFloorSeparation(bool enable, double resolution = 0.4,
							   double sensor_height = 1.5, int min_points = 3, double max_slope = 0.22);
	void enablePostProcess(bool enable, int frames_delay = 4);
	void enableVoxelizedAlgorithm(bool enable, double voxel_size = 0.1, int split_factor = 3);
	void enableDynamicRemovedPointsSwapping(bool enable);
	void enableSavingRemovedPoints(bool enable, std::string const &filename = "../output/rem_points_out.txt");
	void enableInformationStoring(bool enable, std::string const &filename = "../output/wdst/wdst_info.txt");
    void enableSavingPointsBinaryFile(bool enable, std::string const &filename = "../output/wdst/rem_points/");

	// Visualization related =============================================================
	void enableVisualization(visualization::CloudVisualizerSimple * visualizer);
	void disableVisualization();

	// Debug related =====================================================================
	void enableDebug(bool enable);

	// WDST Related ======================================================================

private:
	// Parameters
	DSTparameter _param;

	// Needed to compute the trajectory...
	CameraRig* _last_rigs[2];

	// Module related
	bool _finished;

	// Frames buffer related
	std::vector<std::shared_ptr<DSTframe> > _frames;
	std::queue<CameraRig*> _rigs;
	size_t _max_buffer_size;
	size_t _min_buffer_size;
	int _skip_frames;

	// All removed points
	Cloud _removed_points;

	// Floor separation related
	std::shared_ptr<filtering::FloorRemoval> _floor_removal;

	// Post process related data
	std::vector<pcl::KdTreeFLANN<Cloud::pointType>> _pp_frames;
	std::vector<Cloud> _pp_clouds;
	int _frames_delay;

	// Flags
	bool _floor_separation_ON;
	bool _post_process_ON;
	bool _voxelized_algorithm_ON;
	bool _swap_removed_points_ON;
	bool _save_removed_points_ON;

	// Voxelized algorithm related
	MultiFilter _filter;
	double _voxel_size;
	int _split_factor;

	// Save removed points related
    std::shared_ptr<std::ofstream> _rem_points_ofstream;
	std::string _rem_points_filename;

	// Commodity methods
	void removeMovingObjects();
	void removeMovingObjectsVoxelized();
	void removeMovingObjectsOctree();

    void postCheckRemovedPoints(Cloud &cloud, Cloud &viz_cloud,
                                std::vector<int> &indices_to_remove,
                                std::vector<MassConsistency> &consistencies,
                                RemovedPointsContainer &rem_container);

	void saveRemovedPoints(RemovedPointsContainer & container);

	// Information Related ==============================================================
	std::string _info_file;
	std::string _bin_folder;

	TimeInfo _comp_time;
	TimeInfo _overall_time;

	// Debug related =====================================================================
	bool _debug;

	// Visualization related =============================================================
	visualization::CloudVisualizerSimple * _viewer;
	std::vector<Cloud> _viz_clouds;

	//Visualization methods...
	void visualizePreComputationClouds(const Cloud & sample_cloud, unsigned int pause_ms);
	void visualizePostComputationClouds(const Cloud & removing_indices_cloud, const Cloud & current_cloud, unsigned int pause_ms);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Point representation for KD-Tree: 2D based on spherical coordinates (theta and phi)
class SphericalPointRepresentation2D: public pcl::PointRepresentation<Cloud::pointType>{
public:
	typedef pipeline::Cloud::pointType PointType;
	SphericalPointRepresentation2D();
	virtual void copyToFloatArray (const PointType &p, float *out) const;
	virtual bool isValid (const PointType &p) const;

protected:
	int nr_dimensions_;
	bool trivial_;
};

/*
 * // KD-Tree related
class RadialKDTree{
public:
	RadialKDTree();
	void setCloud(Cloud const & cloud, Eigen::Vector3f center);
	// This method returns cartesian XYZ points which form rays close to requested location
	int raysSearch(Cloud::pointType & location, double radius, std::vector<Cloud::pointType> neighbor_points);
	// This method return spherical XYZ points (x - phi, y - theta, z - radius) whose rays are close
	int radialSearch(Cloud::pointType & location, double radius, std::vector<Cloud::pointType> neighbor_rays);
private:
	// Point representation for KD-Tree: 2D based on spherical coordinates (theta and phi)
	class SphericalPointRepresentation2D: public pcl::PointRepresentation<Cloud::pointType>{
	public:
		typedef pipeline::Cloud::pointType PointType;
		SphericalPointRepresentation2D();
		virtual void copyToFloatArray (const PointType &p, float *out) const;
		virtual bool isValid (const PointType &p) const;

	protected:
		int nr_dimensions_;
		bool trivial_;
	};

	pcl::KdTreeFLANN<Cloud::pointType> _kdtree_main;
	pcl::KdTreeFLANN<Cloud::pointType> _kdtree_shifted;
};
*/

} /* namespace cloud_cleaning */

#endif /* CLOUD_CLEANING_CHANGEDETECTIONWDST_H_ */
