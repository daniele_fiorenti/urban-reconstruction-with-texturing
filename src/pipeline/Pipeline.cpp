/*
 * Pipeline.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#include "Pipeline.h"
#include "../utility/info_printer.h"

/*
 * Nella Pipeline i moduli vengono raggruppati e lanciati.
 */

namespace pipeline {

// Constructor
Pipeline::Pipeline() {
}

// Destructor
Pipeline::~Pipeline() {
}

// Function called to start the pipeline, checks in which mode to start the pipeline and begins execution
void Pipeline::init(option_ option) {
	if(option == Pipeline::PARALLEL_AUTO){
		parallelAutoExecute();
	}
	else if(option == Pipeline::SEQUENTIAL){
		sequentialExecute();
	}
	else if(option == Pipeline::PARALLEL){
		parallelExecute();
	}
	if(_callback){
		_callback();
	}
}

// Add module to vector _modules and set his Scene to the one of the pipeline.
void Pipeline::addModule(Module* module_) {
	module_->setScene(&_scene);
	_modules.push_back(module(module_));
}

void Pipeline::addModule(module & module_) {
	module_->setScene(&_scene);
	_modules.push_back(module_);
}

void Pipeline::addModule(module&& module_) {
	module_->setScene(&_scene);
	_modules.push_back(module_);
}

// Setter for _callback function
void Pipeline::setFinishCallback(std::function<void()> function) {
	_callback = function;
}

// Getter for Scene _scene
inline Scene& Pipeline::getScene() {
	return _scene;
}

// Getter for vector _modules
std::vector<module>& Pipeline::getModules() {
	return _modules;
}

// Execute in sequential mode
void Pipeline::sequentialExecute() {
	IPRINT("Pipeline", "Executing in sequential mode");
	auto next_module_to_finish = _modules.begin();
	CameraRig* last_result = nullptr;
	int iteration_index = 0;

    // Until last element of vector _modules hasn't finish...
    while(!_modules.back()->hasFinished()){
        // Check whether next_module_to_finish has finished and next_module_to_finish is not the last module,
		while(next_module_to_finish->get()->hasFinished() && next_module_to_finish != _modules.end() - 1){
			next_module_to_finish++;
            // finalize() is called only when all the previous modules have performed hasFinished() function
            next_module_to_finish->get()->finalize();
		}
		last_result = nullptr;
        // If next_module_to_finish has not finish we tell him to process
        for(auto module_iter = next_module_to_finish; module_iter != _modules.end(); module_iter++){
            //std::cout << "_modules.size(): " << _modules.size() << std::endl;
            //std::cout << "module_iter: " << std::distance(_modules.begin(), module_iter) << std::endl;
            // For each module start processing
			if(!(*module_iter)->process(last_result, iteration_index)){
				break;
			}
			last_result = (*module_iter)->getOutputCameraRig();
		}
        //std::cout << "Pipeline: Iteration Index " << iteration_index << std::endl;
		iteration_index++;
	}
}

// Execute in parallel mode
void Pipeline::parallelExecute() {
	IPRINT("Pipeline", "Executing in parallel mode");
	bool more_modules_are_asynch = false;
	int next_stage_index = 0;
	_stages.push_back(ProcessingStage(next_stage_index++));
	auto current_stage = &_stages.back();
	for(auto& module: _modules){
		if(module->isAsynch()){
			if(more_modules_are_asynch){
				_stages.push_back(ProcessingStage(next_stage_index++));
				current_stage = &_stages.back();
			}
            current_stage->addModule(module);
			more_modules_are_asynch = true;
		}
		else{
			current_stage->addModule(module);
		}
	}

	// Connect all the stages together
	current_stage = &_stages.front();
	for(int i = 1; i < _stages.size(); i++){
		_stages[i].connectToProcessingUnit(current_stage);
		current_stage = &_stages[i];
	}

	// Start all stages in execution mode
	for(auto& stage: _stages){
		stage.execute();
	}

	ProcessingStage& last_stage = _stages.back();
	while(!last_stage.hasFinished()){
		last_stage.getResult();
		last_stage.execute();
	}
}

// Execute in Parallel auto mode
void Pipeline::parallelAutoExecute() {
	IPRINT("Pipeline", "Executing in parallel auto mode");
	bool more_modules_are_asynch = false;
	int next_stage_index = 0;
	_pipeline_stages.push_back(PipelineStage(next_stage_index++));
	auto current_stage = &_pipeline_stages.back();
	for(auto& module: _modules){
		if(module->isAsynch()){
			if(more_modules_are_asynch){
				_pipeline_stages.push_back(PipelineStage(next_stage_index++));
				current_stage = &_pipeline_stages.back();
			}
			current_stage->addModule(module);
			more_modules_are_asynch = true;
		}
		else{
			current_stage->addModule(module);
		}
	}

	// Connect all the stages together
	current_stage = &_pipeline_stages.front();
	for(int i = 1; i < _pipeline_stages.size(); i++){
		_pipeline_stages[i].connectToProcessingUnit(current_stage);
		current_stage = &_pipeline_stages[i];
	}

	// Start all stages in execution mode
	for(auto& stage: _pipeline_stages){
		stage.start();
	}

	PipelineStage& last_stage = _pipeline_stages.back();
    //*** All'interno di questo while viene fatta tutta la computazione ***
	while(!last_stage.hasFinished()){
		last_stage.getResult();
	}
	last_stage.stop();
}

} /* namespace pipeline */

