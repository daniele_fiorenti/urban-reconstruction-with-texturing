/*
 * Camera.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef PIPELINE_CAMERARIG_H_
#define PIPELINE_CAMERARIG_H_

#include "data_types.h"
/*
 * CameraRig contiene:
 * _pose = Posizione e rotazione globale rispetto alla prima pointcloud
 * _relative_pose = posizione e rotazione rispetto all'ultima pointcloud
 * RigBox = è un parallelepipedo rispetto al quale origin è il centro
 */
namespace pipeline {

class CameraRig {
public:
    // Constructor
    CameraRig(): _pose(Eigen::Matrix4d::Identity()), _id(-1), _relative_pose(Eigen::Matrix4d::Identity()) { }

    // Destructor
	virtual ~CameraRig() { }

	// Getters...
	Camera& getCamera();
	IMU& getImu();
	Lidar& getLidar();
	Eigen::Matrix4d& getPose();
	Eigen::Matrix4d& getRelativePose();
	Cloud & getCloud();
	Cloud & getCloud(std::string const & id);
	void clearCloud(std::string const & id);
	bool isCloudCreated(std::string const & id);
    int getID();
    static Calibration& getCalibration();

	// Setters...
    void setID(int id);
    void setImu(IMU& imu);

	/**
	 *  @brief Create a new special cloud for current camera rig rig.
	 *  @param  id  String to identify the cloud to be created
	 *  @return  true if the cloud has been created, false if it
	 *  		 already exists
	 *
	 *  This function takes an id and tries to create a new special cloud,
	 *  usually used to exchange special clouds between pipeline modules,
	 *  or to store additional clouds to this camera rig. If the cloud
	 *  already exists, then it returns false, otherwise the cloud is created
	 *  with given id and returns true
	 */
	bool createCloud(std::string const & id);

private:
	int _id;
    Camera _camera; // _camera contains cloud, pose, image and timestamp
    Lidar _lidar; // _lidar contains cloud, pose and timestamp
	IMU _imu;
	Eigen::Matrix4d _pose;
	Eigen::Matrix4d _relative_pose;
    std::map<std::string, Cloud> _special_clouds;
    static Calibration _calibration;
};

struct RigBox{
    Eigen::Vector4f min_p;
    Eigen::Vector4f max_p;
    Eigen::Vector4f origin;

    RigBox(){ }
    RigBox(CameraRig &rig){
        pcl::getMinMax3D(rig.getCloud().cloud(), min_p, max_p);
        origin = rig.getPose().cast<float>().block<4,1>(0,3);
    }

    RigBox(const Cloud &cloud, const Eigen::Vector4f &orig){
        pcl::getMinMax3D(cloud.cloud(), min_p, max_p);
        origin = orig;
    }

    RigBox(const Cloud &cloud){
        pcl::getMinMax3D(cloud.cloud(), min_p, max_p);
        origin = (min_p + max_p) / 2;
    }

    inline bool isOutsideBBox(Cloud::pointType const & point){
        return point.x < min_p[0] || point.x > max_p[0]
                                                     || point.y < min_p[1] || point.y > max_p[1];
    }

    inline bool isOutsideBBox3D(Cloud::pointType const & point){
        return point.x < min_p[0] || point.x > max_p[0]
            || point.y < min_p[1] || point.y > max_p[1]
            || point.z < min_p[2] || point.z > max_p[2];
    }
};

} /* namespace pipeline */

#endif /* PIPELINE_CAMERARIG_H_ */
