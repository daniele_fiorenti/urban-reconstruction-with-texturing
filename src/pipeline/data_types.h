/*
 * data_types.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef PIPELINE_DATA_TYPES_H_
#define PIPELINE_DATA_TYPES_H_

#include <sstream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>

#include <Eigen/Core>

#include "Cloud.h"

#include "../utility/info_printer.h"

#include <opencv2/core/core.hpp>

namespace pipeline{

struct Timestamp{
		unsigned int h;
		unsigned int m;
		float s;

		Timestamp(std::string const & s_time){
			std::vector<std::string> elems;
			boost::split(elems, s_time, boost::is_any_of(": "), boost::token_compress_on);

			//IDEBUG("Timestamp", "Captured: " + elems[0] + " " + elems[1] + " " + elems[2] + " " + elems[3]);
			assert(elems.size() > 3 && "Timestamp Error: Unable to parse line");
			h = std::stoi(elems[1]);
			m = std::stoi(elems[2]);
			s = std::stof(elems[3]);
			//std::cout << "------------------\n";
		}

		Timestamp(): h(0), m(0), s(0.0f) { }

		Timestamp(Timestamp const &other): h(other.h), m(other.m), s(other.s) { }

		Timestamp(unsigned int h, unsigned int m, float s){
			this->s = std::fabs(s);
			this->m = m % 60 + int(this->s)/3600;;
			this->h = h + m/60 + int(this->s)/3600;
			this->s = std::max(s, 60.0f);
		}

		bool operator>(const Timestamp& other) const{
			return ((h > other.h)
					|| ((h == other.h) && (m > other.m))
					|| ((h == other.h) && (m == other.m) && (s > other.s)));
		}

		bool operator<(const Timestamp& other) const{
			return ((h < other.h)
					|| ((h == other.h) && (m < other.m))
					|| ((h == other.h) && (m == other.m) && (s < other.s)));
		}

		bool operator==(const Timestamp& other) const{
			return (h == other.h) && (m == other.m) && (s == other.s);
		}

		float operator-(const Timestamp& other) const{
			return (float)(h - other.h)*3600.0f
					+ (float)(m - other.m)*60.0f
					+ (s - other.s);
		}

		bool operator()(){
			return h > 0 && m > 0 && s > 0.0f;
		}
};

struct RemovedPoint{
    Eigen::Vector3f point;
    std::vector<int> view_idx;

    RemovedPoint(double x, double y, double z): point(x,y,z){ }
    RemovedPoint(double x, double y, double z, int view_id): point(x,y,z){
        view_idx.push_back(view_id);
    }
};

struct RemovedPointsContainer{
    int rig_id;
    Eigen::Matrix4d rig_pose;
    std::vector<RemovedPoint> removed_points;

    int size(){
        return removed_points.size();
    }
};

struct Lidar{
	Cloud cloud;
	Eigen::Matrix4d pose;
	Timestamp timestamp;
};

struct Image{
    cv::Mat picture;
    cv::Mat depth_map;
};

struct Camera{
    Cloud points; // Questa nuvola dovrebbe essere generata dall'immagine della fotocamera
	Image image;
	Eigen::Matrix4d pose;
	Timestamp timestamp;
    RemovedPointsContainer removed_points_container;
};

struct IMU{
	double lat; 	//	latitude of the oxts-unit (deg)
	double lon;		//	longitude of the oxts-unit (deg)
	double alt;		//	altitude of the oxts-unit (m)
	double roll;	//	roll angle (rad),    0 = level, positive = left side up,      range: -pi   .. +pi
	double pitch;	//	pitch angle (rad),   0 = level, positive = front down,        range: -pi/2 .. +pi/2
	double yaw;		//  heading (rad),       0 = east,  positive = counter clockwise, range: -pi   .. +pi

	Timestamp timestamp;

	IMU(std::string const & s_imu){
		lat = lon = alt = roll = pitch = yaw = 0;
		std::stringstream ss;
		ss << s_imu;
		ss >> lat >> lon >> alt >> roll >> pitch >> yaw;
	}

	IMU() : lat(0), lon(0), alt(0), roll(0), pitch(0), yaw(0) { }
};

struct CamToRect{
    std::vector<Eigen::Vector2i> S;
    std::vector<Eigen::Matrix3d> K;
    std::vector<Eigen::Matrix<double, 5, 1>> D;
    std::vector<Eigen::Matrix3d> R;
    std::vector<Eigen::Vector3d> T;
    std::vector<Eigen::Vector2i> S_rect;
    std::vector<Eigen::Matrix4d> R_rect;
    std::vector<Eigen::Matrix<double, 3, 4>> P_rect;

    // velodyne -> rectified camera coordinates
    std::vector<Eigen::Matrix4d> velo_to_cam_rect;

    std::vector<Eigen::Matrix<double, 3, 4>> velo_to_image_plane;
};

struct Calibration{
    Eigen::Matrix4d calibration_velo_to_cam;
    Eigen::Matrix4d calibration_imu_to_velo;

    CamToRect camToRect;
    Calibration(){
        calibration_velo_to_cam.setIdentity();
        calibration_imu_to_velo.setIdentity();
    }
};

}

#endif /* PIPELINE_DATA_TYPES_H_ */
