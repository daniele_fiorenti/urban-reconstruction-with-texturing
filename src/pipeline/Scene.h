/*
 * Scene.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef PIPELINE_SCENE_H_
#define PIPELINE_SCENE_H_

#include "CameraRig.h"
#include <mutex>

namespace pipeline {

class Scene {
public:
	Scene();
	size_t size() const;
	bool empty() const;
	CameraRig& newCameraRig();
	CameraRig& getLastCameraRig();
	CameraRig& getCameraRig(size_t index);
	CameraRig& operator[](size_t index);
	void removeLastRig();

	~Scene();
private:
	std::vector<CameraRig*> _rigs;
	std::mutex _data_mutex;
	CameraRig* _last_rig;
};

} /* namespace pipeline */

#endif /* PIPELINE_SCENE_H_ */
