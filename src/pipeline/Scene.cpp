/*
 * Scene.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#include "Scene.h"

/*
 * È un array di cloud sincronizzato contenente tutte le cloud.
 * Le nuvole vengono aggiunte alla Scene ma quest'ultima non è mai usata.
 */

namespace pipeline {

// Constructor
Scene::Scene():_last_rig(nullptr) { }

// Destructor
Scene::~Scene() {
	_data_mutex.lock();
	for(auto rig: _rigs){
		delete rig;
	}
	_data_mutex.unlock();
}

// Restutisce il numero di CameraRig contenute nella Scene
size_t Scene::size() const {
	return _rigs.size();
}

// Aggiunge una CameraRig "vuota" alla Scene
CameraRig& Scene::newCameraRig() {
	CameraRig * new_rig = new CameraRig();
	_data_mutex.lock();
	_rigs.push_back(new_rig);
	_last_rig = new_rig;
	_data_mutex.unlock();
	return *new_rig;
}

// Restituisce l'ultima CameraRig aggiunta alla Scene
CameraRig& Scene::getLastCameraRig() {
	return *_last_rig;
}

// Restituisce la CameraRig in posizione index
CameraRig& Scene::getCameraRig(size_t index) {
	_data_mutex.lock();
	CameraRig* retCameraRig = _rigs[index];
	_data_mutex.unlock();
	return *retCameraRig;
}

// Guarda se il vettore _rigs è vuoto
bool Scene::empty() const {
	return _rigs.empty();
}

// Rimuove l'ultimo oggetto da _rigs
void Scene::removeLastRig() {
	_data_mutex.lock();
	auto last = _rigs.end() - 1;
	_rigs.erase(last);
	delete _last_rig;
	_last_rig = _rigs.back();
	_data_mutex.unlock();
}

// Permette di accedere al vettore _rigs tramite operatore []
CameraRig& Scene::operator [](size_t index) {
	_data_mutex.lock();
	CameraRig* retCameraRig = _rigs[index];
	_data_mutex.unlock();
	return *retCameraRig;
}

} /* namespace pipeline */
