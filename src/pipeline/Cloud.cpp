/*
 * Cloud.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#include "Cloud.h"
#include "../filters/MultiFilter.h"

#include <pcl/registration/registration.h>
#include <pcl/filters/extract_indices.h>

/*
 * Oggetto che contiene le cloud di pcl, sono stati implementati gli operatori per comodità
 */

static bool constexpr use_shadow_copy(){
	return true;
}

namespace pipeline {

Cloud::Cloud(): _cloud_pointer(new pclCloud()){
	if(use_shadow_copy()){
		_cloud_shadow_copy = pclCloud::Ptr(new pclCloud());
    } //_cloud_shadow_copy è una copia della cloud, serve per i filtri
}

Cloud::Cloud(pclCloud const & pcl_cloud) : Cloud() {
	*_cloud_pointer = pcl_cloud;
}

Cloud::Cloud(double points[], size_t size): Cloud() {
	pclCloud &cloud = *_cloud_pointer;
	pointType point;
	for(int i = 0; i < size; i+=3){
		point.x = points[i];
		point.y = points[i+1];
		point.z = points[i+2];
		cloud.push_back(point);
	}
}

void Cloud::filter(pcl::Filter<pointType>& filter) {
	if(use_shadow_copy()){
		filter.setInputCloud(_cloud_pointer);
		filter.filter(*_cloud_shadow_copy);
        _cloud_pointer.swap(_cloud_shadow_copy); // Scambio i puntatori!
	}else{
		boost::shared_ptr<pclCloud> temp_cloud(new pclCloud());
		*temp_cloud = *_cloud_pointer;
		filter.setInputCloud(temp_cloud);
		filter.filter(*_cloud_pointer);
	}
}

void Cloud::filter(MultiFilter& filter) {
	if(use_shadow_copy()){
		filter.filterFast(_cloud_pointer, _cloud_shadow_copy);
		_cloud_pointer.swap(_cloud_shadow_copy);
	}else{
		boost::shared_ptr<pclCloud> temp_cloud(new pclCloud());
		*temp_cloud = *_cloud_pointer;
		filter.filterFast(temp_cloud, _cloud_pointer);
	}
}

Cloud::Cloud(pclCloud&& pcl_cloud): _cloud_pointer(&pcl_cloud) { }

void Cloud::transform(Eigen::Isometry3f const & transform) {
	pcl::transformPointCloud(*_cloud_pointer, *_cloud_shadow_copy, transform);
	_cloud_pointer.swap(_cloud_shadow_copy);
}

void Cloud::transform(const Eigen::Matrix4d& matrix) {
	pcl::transformPointCloud(*_cloud_pointer, *_cloud_shadow_copy, matrix);
	_cloud_pointer.swap(_cloud_shadow_copy);
}

void Cloud::sort() {
    std::sort(_cloud_pointer->points.begin(), _cloud_pointer->points.end(), comparePoint);

//    for (auto i: _cloud_pointer->points){
//        std::cout << i << std::endl;
//    }
}

bool Cloud::comparePoint(const pcl::PointXYZ& p1, const pcl::PointXYZ& p2) {
    if (p1.x < p2.x) {
        return true;
    }else{
        return false;
    }
}

void Cloud::clone(Cloud& copy) {
    pcl::copyPointCloud<pcl::PointXYZ>(*_cloud_pointer, *(copy._cloud_pointer));
}

void Cloud::clear() {
	_cloud_pointer->clear();
}

Cloud& Cloud::operator +=(const Cloud& other) {
	*_cloud_pointer += other.cloud();
	return *this;
}

Cloud& Cloud::operator =(const Cloud& other) {
	*_cloud_pointer = other.cloud();
	return *this;
}

Cloud Cloud::operator +(const Cloud& other) const{
	pclCloud new_cloud = *_cloud_pointer + other.cloud();
	return Cloud(new_cloud);
}

void Cloud::setPointCloud(const pclCloud& cloud) {
	*_cloud_pointer = cloud;
}

Cloud& Cloud::operator +=(const pclCloud& pcl_cloud) {
	*_cloud_pointer += pcl_cloud;
	return *this;
}

Cloud& Cloud::operator +=(pointType const & point) {
	_cloud_pointer->push_back(point);
	return *this;
}

Cloud& Cloud::operator =(const pclCloud& pcl_cloud) {
	*_cloud_pointer = pcl_cloud;
	return *this;
}

void Cloud::removePoints(std::vector<int>& indices_to_remove) {
	std::sort(indices_to_remove.begin(), indices_to_remove.end());
	int indices_progress = 0;
	int i;
	_cloud_shadow_copy->clear();
	for(i = 0; i < _cloud_pointer->size() && indices_progress < indices_to_remove.size(); i++){
		if(i != indices_to_remove[indices_progress]){
			_cloud_shadow_copy->push_back((*_cloud_pointer)[i]);
		}else{
			indices_progress++;
		}
	}
	for(; i < _cloud_pointer->size(); i++){
		_cloud_shadow_copy->push_back((*_cloud_pointer)[i]);
	}
	_cloud_pointer.swap(_cloud_shadow_copy);
}

Cloud& Cloud::operator -(std::vector<int>& indices_to_remove) {
	removePoints(indices_to_remove);
	return *this;
}

void Cloud::removePoints(indices_ptr& indices_to_remove) {
	pcl::ExtractIndices<pointType> eifilter(true);
	eifilter.setInputCloud(_cloud_pointer);
	eifilter.setIndices(indices_to_remove);
	eifilter.setNegative(true);
	eifilter.filter(*_cloud_shadow_copy);
	_cloud_pointer.swap(_cloud_shadow_copy);
}

Cloud& Cloud::operator -(indices_ptr& indices_to_remove) {
	removePoints(indices_to_remove);
	return *this;
}

Cloud::pointType& Cloud::operator [](size_t n) const{
	return (*_cloud_pointer)[n];
}

} /* namespace pipeline */
