/*
 * Worker.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#include "ProcessingStage.h"

namespace pipeline {

ProcessingStage::ProcessingStage(int index)
	: _input_stage(nullptr),
	  _iteration(0),
	  _index(index),
	  _finishing(false),
	  _has_finished(false),
	  _future_is_created(false)
{ }

ProcessingStage::ProcessingStage(const ProcessingStage& other)
	: _input_stage(other._input_stage),
	  _iteration(other._iteration),
	  _index(other._index),
	  _finishing(other._finishing),
	  _has_finished(other._has_finished),
	  _future_is_created(false),
	  _modules(std::move(other._modules))
{ }

void ProcessingStage::addModule(module const & module_) {
	std::cout << "Stage " << _index << ": adding module " << module_->name() << "\n";
	_modules.push_back(module_);
}

void ProcessingStage::connectToProcessingUnit(ProcessingStage* in_stage) {
	std::cout << "Stage " << _index << ": connecting to stage " << in_stage->_index
			<< " with modules = " << in_stage->_modules.size() << "\n";
	_input_stage = in_stage;
}

void ProcessingStage::execute() {
	_future_is_created = true;

	CameraRig* last_result = nullptr;
	if(_input_stage && !(_input_stage->hasFinished())){
		last_result = _input_stage->getResult();
	}else if(!_input_stage || _input_stage->hasFinished()){
		_finishing = true;
	}

	if(_modules.empty()){
		std::cout << "Stage " << _index << ": has finished.. \n";
		return;
	}

	//_has_finished = true;

	//std::cout << "Stage " << _index << ": executing iteration: " << _iteration << "\n";
	_future = std::async(&ProcessingStage::executeOnThread, this, last_result);
	if(!_finishing){
		_input_stage->execute();
	}
}

CameraRig* ProcessingStage::getResult() {
	return _future.get();
}

CameraRig* ProcessingStage::executeOnThread(CameraRig* last_result) {
	if(_finishing){
		(*_modules.begin())->finalize();
		clearFinishedModules();
	}
	auto last_result_ = last_result;
	for(auto module: _modules){
		//std::cout << "Stage " << _index << ": executing module " << module->name() << " ... sending pointer = " << last_result_ << "\n";
		if(!module->process(last_result_, _iteration)){ break; }
		last_result_ = module->getOutputCameraRig();
	}
	_iteration++;
	return last_result_;
}

void ProcessingStage::clearFinishedModules() {
	// Check if there are finished modules and remove them
	auto module_iterator = _modules.begin();
	auto module_last = _modules.end() - 1;
	while((module_iterator != (_modules.end() - 1)) && (*module_iterator)->hasFinished()){
		//std::cout << "Stage " << _index << ": removing module " << (*module_iterator)->name() << "\n";
		module_iterator = _modules.erase(module_iterator);
		//std::cout << "Stage " << _index << ": finalizing module " << (*_modules.begin())->name() << " ... ";
		(*module_iterator)->finalize();
		//std::cout << "DONE\n";
	}
	if(_modules.size() == 1 && _modules.back()->hasFinished()){
		//std::cout << "Stage " << _index << ": removing last modules \n";
		_has_finished = true;
		_modules.clear();
	}
}

bool ProcessingStage::hasFinished() {
	return _has_finished;
}

size_t ProcessingStage::size() {
	return _modules.size();
}

std::string ProcessingStage::debug() {
	std::string debug_info = "[Debug Stage " + std::to_string(_index) + "]\n - Modules:\n ( )";
	if(_modules.empty()){ return debug_info + " -> !! NO MODULES\n"; }
	for(auto& module: _modules){
		debug_info += " -> [ " + module->name() + " ]";
		if(module->isAsynch()){
			debug_info += "$";
		}
	}
	return debug_info + "\n";
}

} /* namespace pipeline */
