#include "CameraRig.h"

namespace pipeline {

Calibration CameraRig::_calibration;

Calibration& CameraRig::getCalibration(){
    return _calibration;
}

Camera& CameraRig::getCamera() {
    return _camera;
}

IMU& CameraRig::getImu() {
    return _imu;
}

Lidar& CameraRig::getLidar() {
    return _lidar;
}

Eigen::Matrix4d& CameraRig::getPose() {
    return _pose;
}

Eigen::Matrix4d& CameraRig::getRelativePose() {
    return _relative_pose;
}

// Getter for a cloud in std::map _special_clouds
Cloud& CameraRig::getCloud(const std::string& id) {
    return _special_clouds[id];
}

bool CameraRig::isCloudCreated(const std::string& id) {
    return _special_clouds.find(id) != _special_clouds.end();
}

void CameraRig::setImu(IMU& imu) {
    _imu = imu;
}

Cloud& CameraRig::getCloud() {
    return _lidar.cloud;
}

void CameraRig::setID(int id) {
    if(_id < 0){
        _id = id;
    }
}

int CameraRig::getID() {
    return _id;
}

void CameraRig::clearCloud(const std::string& id) {
    _special_clouds.erase(id);
//	_special_clouds.erase(_special_clouds.find(id));
}

bool CameraRig::createCloud(const std::string& id) {
    // Return false if cloud already exists...
    if(isCloudCreated(id)){ return false; }
    // Else create a new cloud with given id and return true...
    _special_clouds[id] = Cloud();
    return true;
}

} /* namespace pipeline */
