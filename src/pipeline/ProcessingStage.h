/*
 * Worker.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef PIPELINE_PROCESSINGSTAGE_H_
#define PIPELINE_PROCESSINGSTAGE_H_

#include <future>
#include <vector>
#include <mutex>
#include <condition_variable>

#include "Module.h"

namespace pipeline {

class ProcessingStage {
public:
	ProcessingStage(int index);
	ProcessingStage(ProcessingStage const & other);
	void addModule(module const & module);
	void connectToProcessingUnit(ProcessingStage * in_stage);
	void execute();
	bool hasFinished();
	size_t size();
	CameraRig* getResult();

	std::string debug();

private:
	std::future<CameraRig*> _future;
	std::vector<module> _modules;
	ProcessingStage* _input_stage;

	std::mutex _future_creation_lock;
	std::condition_variable _future_cond_var;
	bool _future_is_created;

	int _iteration;
	int _index;

	bool _finishing;
	bool _has_finished;

	void clearFinishedModules();
	CameraRig* executeOnThread(CameraRig * last_result);
};

} /* namespace pipeline */

#endif /* PIPELINE_PROCESSINGSTAGE_H_ */
