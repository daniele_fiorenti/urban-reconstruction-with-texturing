/*
 * Cloud.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef PIPELINE_CLOUD_H_
#define PIPELINE_CLOUD_H_

#include <pcl/common/common_headers.h>
#include <pcl/filters/filter.h>

class MultiFilter;

namespace pipeline {

class Cloud {
public:
	typedef pcl::PointXYZ pointType;
	typedef pcl::PointCloud<pointType> pclCloud;
	typedef boost::shared_ptr<std::vector<int> > indices_ptr;

	Cloud();
	Cloud(Cloud const & source);
	Cloud(Cloud && source);
	Cloud(pclCloud const &pcl_cloud);
	Cloud(pclCloud &&pcl_cloud);
	Cloud(double points[], size_t size);

	void filter(pcl::Filter<pointType> &filter);
	void filter(MultiFilter &filter);
	void transform(Eigen::Isometry3f const &transform);
	void transform(Eigen::Matrix4d const &matrix);
	void removePoints(std::vector<int>& indices_to_remove);
	void removePoints(indices_ptr & indices_to_remove);
	void swap(Cloud &other);
    void clear();

    void sort();
    void clone(Cloud& copy);

	void setPointCloud(pclCloud const &cloud);
	pclCloud & cloud() const;
	pclCloud::Ptr const & cloud_shared_ptr() const;
	size_t size() const;

	Cloud & operator+=(Cloud const & other);
	Cloud & operator+=(pclCloud const & pcl_cloud);
	Cloud & operator+=(pointType const & point);
	Cloud & operator=(Cloud const & other);
	Cloud & operator=(pclCloud const & pcl_cloud);
	Cloud operator+(Cloud const & other) const;
	Cloud & operator-(std::vector<int>& indices_to_remove);
	Cloud & operator-(indices_ptr & indices_to_remove);
	pointType & operator[](size_t n) const;

private:
    static bool comparePoint(const pcl::PointXYZ& p1, const pcl::PointXYZ& p2);

private:
	//pclCloud _cloud;
	pclCloud::Ptr _cloud_pointer;
	pclCloud::Ptr _cloud_shadow_copy;
};


inline Cloud::pclCloud& Cloud::cloud() const {
	return *_cloud_pointer;
}

inline Cloud::pclCloud::Ptr const & Cloud::cloud_shared_ptr() const{
	return _cloud_pointer;
}

// Copy constructor
inline Cloud::Cloud(const Cloud& source): Cloud(){
	(*_cloud_pointer) = source.cloud();
}

// Move constructor
inline Cloud::Cloud(Cloud&& source): _cloud_pointer(source._cloud_pointer)
{ }

inline void Cloud::swap(Cloud& other) {
	_cloud_pointer.swap(other._cloud_pointer);
}

inline size_t Cloud::size() const {
	return _cloud_pointer->size();
}

} /* namespace pipeline */

#endif /* PIPELINE_CLOUD_H_ */
