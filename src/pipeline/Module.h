/*
 * Module.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef PIPELINE_MODULE_H_
#define PIPELINE_MODULE_H_

#include "Scene.h"
#include "CameraRig.h"

#include <future>

/*
 * Classe astratta Module (in quanto ha almeno una funzione astratta)
 * Gli oggetti che ereditano da Module vengono aggiunti all'oggetto Pipeline.
 * Da questa classe derivano:
 * ChangeDetectionOriginal
 * ChangeDetectionWDST
 * MovingObjectsRemoval
 * Registration
 * CloudTransformer
 * PosesSaver
 * PlainCloudSaver
 * PoseCalculator
 * CloudFilter
 * CloudAccumulator
 * CloudMultiFilter
 * GICPRegistration
 * PosesLoader
 * IMULoader
 * LidarLoader
 */

namespace pipeline {
class Module;
typedef std::shared_ptr<Module> module;

class Module {
public:
    Module(std::string const & name, bool is_asynch); // is_asynch è true se il modulo è pesante (e quindi gli serve un thread tutto suo)

    virtual void setScene(Scene * scene) = 0; // Scene è un oggetto che contiene tutte le nuvole (array sincronizzato, viene passato da un modulo all'altro ma non è stato usato!)
    virtual bool process(CameraRig * rig, int iteration_index) = 0; // È l'algoritmo PRINCIPALE del metodo, il bool dice se è andato a buon fine o no
    virtual bool hasFinished() = 0; // Controlla se il modulo ha completato la finalize()
    virtual CameraRig * getOutputCameraRig() = 0; // Ritorna il valore dopo la process
    virtual void finalize(){ } // Dice al modulo che tutti i moduli precedenti hanno finito

	virtual std::string const & name() const final;
	virtual bool isAsynch() const final;
	virtual ~Module(){ }
private:
	bool _is_asynch;
	std::string _name;
};

inline Module::Module(std::string const & name, bool is_asynch)
	: _name(name), _is_asynch(is_asynch) { }

inline std::string const & Module::name() const {
	return _name;
}

inline bool Module::isAsynch() const {
	return _is_asynch;
}


} /* namespace pipeline */

#endif /* PIPELINE_MODULE_H_ */
