/*
 * PipelineStage.cpp
 *
 *  Created on: Jun 17, 2015
 *      Author: george
 */

#include "PipelineStage.h"

/*
 * Serve per l'esecuzione parallela della Pipeline.
 */

namespace pipeline {

PipelineStage::PipelineStage(int index)
	  : _input_stage(nullptr),
		_iteration(0),
		_index(index),
		_finishing(false),
		_has_finished(false),
		_result_rig(nullptr){ }

PipelineStage::PipelineStage(const PipelineStage& other)
	 : _input_stage(nullptr),
	   _iteration(other._iteration),
	   _index(other._index),
	   _finishing(false),
	   _has_finished(false),
	   _result_rig(nullptr),
	   _modules(other._modules){ }

PipelineStage::~PipelineStage() { }

void PipelineStage::addModule(const module& module_) {
	//std::cout << "Stage " << _index << ": adding module " << module_->name() << "\n";
	_modules.push_back(module_);
}

void PipelineStage::connectToProcessingUnit(PipelineStage* in_stage) {
	//std::cout << "Stage " << _index << ": connecting to stage " << in_stage->_index
	//	<< " with modules = " << in_stage->_modules.size() << "\n";
	_input_stage = in_stage;
}

void PipelineStage::start() {
	_thread = std::thread(&PipelineStage::runOnThread, this);
}

void PipelineStage::stop() {
	_thread.join();
}

void PipelineStage::runOnThread() {
	bool is_first_in_pipeline = _input_stage == nullptr;
	auto next_module_to_finish = _modules.begin();
	int iteration_index = 0;
    while(!_modules.empty()){ //
		_result_rig = nullptr;

        if(is_first_in_pipeline || _input_stage->hasFinished()){ // se il primo è nella pipeline oppure la pipeline precedente ha finito allora
            next_module_to_finish->get()->finalize(); //il prossimo modulo che deve finire può finire
            while(next_module_to_finish->get()->hasFinished() && next_module_to_finish != _modules.end() - 1){ //Se il prossimo modulo da finire ha finito continua ad iterare i moduli
                next_module_to_finish = _modules.erase(next_module_to_finish); //
				next_module_to_finish->get()->finalize();
			}
			if(_modules.size() == 1 && _modules.back()->hasFinished()){
				{	// Notify the next stage to get the result
                    // Mettendo questo blocco di codice crea l'oggetto e lo distrugge appena esce da questo blocco di codice quindi abbiamo il rilascio!
                    std::lock_guard<std::mutex> locker(_mutex); //Serve per la programmazione parallela
					_finishing = true;
                    _cond_variable.notify_one(); //Notifica alla pipelineStage successiva che ho finito
				}
				break;
			}
		}
		else{
            _result_rig = _input_stage->getResult(); //Prendo il risultato dalla PipelinStage precedente
		}
        for(auto& module_iter : _modules){ //Itera per tutti i moduli nella pipeline stage
            if(!module_iter->process(_result_rig, iteration_index)){ //Lancio la computazione del modulo, iteration_index è l'iterazione nella pipeline (il numero della nuvola nella pipeline), serve solo per alcuni moduli
				break;
			}
            _result_rig = module_iter->getOutputCameraRig(); //Tiro fuori il risultato
		}
        iteration_index++; // Aumento l'indice, viene fatto a livello di PipelineStage!

		{	// Notify the next stage to get the result
            std::lock_guard<std::mutex> locker(_mutex);
            _results.push(_result_rig); // Altro pezzo di codice critico, metto nella pipeline stage successiva i risultati
            _cond_variable.notify_one(); // Notifica alla pipelineStage successiva che ho aggiunto un risultato nuovo, (NB: è la pipelineStage successiva che capisce se ho finito oppure ho aggiunto un risultato)
		}
	}
    if(!is_first_in_pipeline){ // Se non è prima nella pipeline allora fermo la pipelineStage precedente
        _input_stage->stop(); // Questo perché se la pipelineStage attuale ha finito sicuramente ha finito anche quella prima, quindi dobbiamo fermare totalmente la pipeline stage precedente
	}
}

bool PipelineStage::hasFinished() {
	return _has_finished;
}

size_t PipelineStage::size() {
	return _modules.size();
}

// Questo metodo si mette in ascolto per vedere se ci sono risultati
CameraRig* PipelineStage::getResult() {
	std::unique_lock<std::mutex> locker(_mutex);
	while(_results.empty() && !_finishing){
		_cond_variable.wait(locker);
	}
	_has_finished = _results.empty() && _finishing;
	CameraRig * result = nullptr;
	if(!_has_finished){
		result = _results.front();
		_results.pop();
	}
	return result;
}

std::string PipelineStage::debug() {
	std::string debug_info = "[Debug Stage " + std::to_string(_index) + "]\n - Modules:\n ( )";
	if(_modules.empty()){ return debug_info + " -> !! NO MODULES\n"; }
	for(auto& module: _modules){
		debug_info += " -> [ " + module->name() + " ]";
		if(module->isAsynch()){
			debug_info += "$";
		}
	}
	return debug_info + "\n";
}

} /* namespace pipeline */
