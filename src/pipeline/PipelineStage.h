/*
 * PipelineStage.h
 *
 *  Created on: Jun 17, 2015
 *      Author: george
 */

#ifndef PIPELINE_PIPELINESTAGE_H_
#define PIPELINE_PIPELINESTAGE_H_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <queue>

#include "Module.h"

namespace pipeline {

class PipelineStage {

public:
	PipelineStage(int index);
	PipelineStage(PipelineStage const & other);
	virtual ~PipelineStage();

	void addModule(module const & module);
	void connectToProcessingUnit(PipelineStage * in_stage);
	void start();
	void stop();
	bool hasFinished();
	size_t size();
	CameraRig* getResult();

	std::string debug();

private:
	std::vector<module> _modules;
	std::queue<CameraRig*> _results;
	PipelineStage* _input_stage;

	CameraRig * _result_rig;
	std::thread _thread;
	std::mutex _mutex;
	std::condition_variable _cond_variable;
	bool _has_finished;

	int _iteration;
	int _index;

	bool _finishing;

	void runOnThread();
};

} /* namespace pipeline */

#endif /* PIPELINE_PIPELINESTAGE_H_ */
