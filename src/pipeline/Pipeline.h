/*
 * Pipeline.h
 *
 *  Created on: Jun 9, 2015
 *      Author: george
 */

#ifndef PIPELINE_PIPELINE_H_
#define PIPELINE_PIPELINE_H_

#include "ProcessingStage.h"
#include "PipelineStage.h"
#include "Scene.h"

namespace pipeline {

class Pipeline {
public:
	enum option_{
		PARALLEL,
		PARALLEL_AUTO,
		SEQUENTIAL
	};
	Pipeline();
	virtual ~Pipeline();

	void init(option_ option = PARALLEL_AUTO);
	void addModule(Module* module_);
	void addModule(module& module_);
	void addModule(module&& module);
	void setFinishCallback(std::function<void()> function);

	Scene & getScene();
	std::vector<module>& getModules();

private:
	std::vector<ProcessingStage> _stages;
	std::vector<PipelineStage> _pipeline_stages;
	std::vector<module> _modules;
	std::function<void()> _callback;
	Scene _scene;

	void sequentialExecute();
	void parallelExecute();
	void parallelAutoExecute();
};

} /* namespace pipeline */

#endif /* PIPELINE_PIPELINE_H_ */
