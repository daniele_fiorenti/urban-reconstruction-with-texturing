/*
 * kitti_utility.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: george
 */
#ifndef UTILITY_KITTI_UTILITY_HPP
#define UTILITY_KITTI_UTILITY_HPP

#include "kitti_utility.h"

template<typename PointT>
inline void KittiUtility::addPoints(std::fstream& input,
		boost::shared_ptr<pcl::PointCloud<PointT> > points, int index) {

}

template <>
inline void KittiUtility::addPoints<pcl::PointXYZ>(std::fstream& input,
		boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> > points, int index){

	float dummy;
	while(input.good() && !input.eof()){
		pcl::PointXYZ point;
		input.read((char *) &point.x, 3*sizeof(float));
		input.read((char *) &dummy, sizeof(float));
		points->push_back(point);
	}
}

template <>
inline void KittiUtility::addPoints<pcl::PointXYZI>(std::fstream& input,
		boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI> > points, int index){

	while(input.good() && !input.eof()){
		pcl::PointXYZI point;
		input.read((char *) &point.x, 3*sizeof(float));
		input.read((char *) &point.intensity, sizeof(float));
		if(index > -1){
			point.intensity = double(index)/200.0;
		}
		points->push_back(point);
	}
}

template <>
inline void KittiUtility::addPoints<pcl::PointXYZRGB>(std::fstream& input,
		boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB> > points, int index){

	while(input.good() && !input.eof()){
		pcl::PointXYZRGB point;
		input.read((char *) &point.x, 3*sizeof(float));
		input.read((char *) &point.rgb, sizeof(float));
		points->push_back(point);
	}
}

// |     |    |   |  | ||                         || |  |   |    |     |
// |     |    |   |  | ||  ADD OTHER POINTS HERE  || |  |   |    |     |
// |     |    |   |  | ||  						  || |  |   |    |     |
///----------------------\                       /----------------------\
//________________________\                     /________________________\

template<typename PointT>
inline typename boost::shared_ptr<pcl::PointCloud<PointT> > KittiUtility::loadFromFile(const std::string &pathname) {
	std::fstream input(pathname.c_str(), std::ios::in | std::ios::binary);
	if(!input.good() || !boost::filesystem::is_regular_file(pathname)){
		IERROR("KittiUtility:LoadFile", "Could not read file: " + pathname);
		input.close();
		return boost::shared_ptr<pcl::PointCloud<PointT> >();
	}
	input.seekg(0, std::ios::beg);

	boost::shared_ptr<pcl::PointCloud<PointT> > points(new pcl::PointCloud<PointT>());

	addPoints<PointT>(input, points);

	input.close();

	boost::shared_ptr<pcl::PointCloud<PointT> > ret_points(points);
	return ret_points;
}

template<typename PointT>
inline typename boost::shared_ptr<pcl::PointCloud<PointT> > KittiUtility::loadFromFolder(
		const std::string& pathname, int min, int max) {

	//Since max has to be included into the interval, then we increment it by one
	max++;

	PROFILER_INIT_TIMER(TIMER_DATASET_LOAD, "Time needed to load all point clouds");
	PROFILER_START_TIMER(TIMER_DATASET_LOAD);
	namespace fs = boost::filesystem;
	fs::path work_dir(pathname);
	fs::directory_iterator null_iter;
	if(!fs::exists(work_dir) || !fs::is_directory(work_dir)){
		IERROR("KittiUtility:LoadFolder", "Unable to open directory:  " + pathname);
		PROFILER_STOP_TIMER(TIMER_DATASET_LOAD);
		return boost::shared_ptr<pcl::PointCloud<PointT> >();
	}

	IPRINT("KittiUtility:LoadFolder", "Loading folder: " + pathname);

	boost::shared_ptr<pcl::PointCloud<PointT> > points(new pcl::PointCloud<PointT>());
	std::fstream input;

	std::vector<std::string> filenames;
	for(fs::directory_iterator dir_iter(work_dir); dir_iter != null_iter; ++dir_iter){
		if(fs::is_regular_file(dir_iter->status()) && dir_iter->path().extension().string() == ".bin"){
			filenames.push_back(dir_iter->path().string());
		}
	}

	std::sort(filenames.begin(), filenames.end());


	min = min < 0? 0 : min;
	max = max < filenames.size() && max > min ? max : filenames.size();
	min = min > max? 0 : min;

	for(int i = min; i < max; i++){
		input.open(filenames[i].c_str(), std::ios::in | std::ios::binary);

		if(!input.good()){
			IERROR("KittiUtility:LoadFolder", "Could not read file: " + filenames[i]);
			input.close();
			continue;
		}
		input.seekg(0, std::ios::beg);

		IPRINT("Loaded file", filenames[i]);

		boost::shared_ptr<pcl::PointCloud<PointT> > _points(new pcl::PointCloud<PointT>());

		addPoints<PointT>(input, _points);

		*points += *_points;
		input.close();
	}

	PROFILER_STOP_TIMER(TIMER_DATASET_LOAD);
	return points;
}

template<typename PointT>
inline typename boost::shared_ptr<pcl::PointCloud<PointT> > KittiUtility::loadSynchronized(
		const std::string& folderpath, boost::shared_ptr<Registrator<PointT> > registrator, int min, int max) {
	return loadSynchronized<PointT>(folderpath + "/velodyne_points", folderpath + "/oxts",
			folderpath + "/calibration/calib_imu_to_velo.txt", registrator, min, max);
}

template<typename PointT>
inline typename boost::shared_ptr<pcl::PointCloud<PointT> > KittiUtility::loadSynchronized(
		const std::string& velodyne_folder, const std::string& imu_folder,
		const std::string& calibration_file,
		boost::shared_ptr<Registrator<PointT> > registrator, int min, int max) {

	//Since max has to be included into the interval, then we increment it by one
	max++;

	PROFILER_INIT_TIMER(TIMER_DATASET_LOAD, "Time needed to load all point clouds");
	PROFILER_START_TIMER(TIMER_DATASET_LOAD);
	namespace fs = boost::filesystem;
	fs::path work_dir(velodyne_folder + "/data/");
	fs::directory_iterator null_iter;
	if(!fs::exists(work_dir) || !fs::is_directory(work_dir)){
		IERROR("KittiUtility:LoadSync", " Unable to open velodyne directory: " + velodyne_folder);
		PROFILER_STOP_TIMER(TIMER_DATASET_LOAD);
		return boost::shared_ptr<pcl::PointCloud<PointT> >();
	}

	readIMU<PointT>(imu_folder + "/data/", *registrator, min, max);
	readTimestamps<PointT>(imu_folder + "/timestamps.txt", *registrator, min, max);
	readCalibration<PointT>(calibration_file, *registrator);

	readLidarTimestamps<PointT>(velodyne_folder, *registrator, min, max);

	std::fstream input;

	IPRINT("KittiUtility:LoadSync", "---------- Velodyne reading:");

	int n = 0;
	std::vector<std::string> filenames;
	for(fs::directory_iterator dir_iter(work_dir); dir_iter != null_iter; ++dir_iter){
		if(fs::is_regular_file(dir_iter->status()) && dir_iter->path().extension().string() == ".bin"){
			filenames.push_back(dir_iter->path().string());
		}
	}

	std::sort(filenames.begin(), filenames.end());

	registrator->prepare();

	min = min < 0? 0 : min;
	max = max < filenames.size() && max > min ? max : filenames.size();
	min = min > max? 0 : min;

	for(int i = min; i < max; i++){
		input.open(filenames[i].c_str(), std::ios::in | std::ios::binary);

		if(!input.good()){
			IERROR("KittiUtility:LoadSync", "Could not read file: " + filenames[i]);
			input.close();
			continue;
		}
		input.seekg(0, std::ios::beg);

		IPRINT("Loaded file", filenames[i]);

		boost::shared_ptr<pcl::PointCloud<PointT> > points(new pcl::PointCloud<PointT>());

		addPoints<PointT>(input, points);

		PROFILER_STOP_TIMER(TIMER_DATASET_LOAD);
		registrator->addPointCloud(points, n++);
		PROFILER_START_TIMER(TIMER_DATASET_LOAD);
		input.close();
	}

	registrator->finalize();

	PROFILER_STOP_TIMER(TIMER_DATASET_LOAD);
	return registrator->getFinalCloud();
}

template <typename PointT>
void KittiUtility::readIMU(const std::string& folderpath, Registrator<PointT> &reg, int min, int max) {
	namespace fs = boost::filesystem;
	fs::path work_dir(folderpath);
	fs::directory_iterator null_iter;
	if(!fs::exists(work_dir) || !fs::is_directory(work_dir)){
		IERROR("KittiUtility:IMUread", "IMU Read: Unable to open directory: " + folderpath);
		return;
	}
	//std::cerr << "Loading folder: " << folderpath << std::endl;
	std::fstream input;
	//cout << "---------- IMU reading files:" << endl;
	std::vector<std::string> filenames;
	for(fs::directory_iterator dir_iter(work_dir); dir_iter != null_iter; ++dir_iter){
		if(fs::is_regular_file(dir_iter->status()) && dir_iter->path().extension().string() == ".txt"){
			filenames.push_back(dir_iter->path().string());
		}
	}
	std::sort(filenames.begin(), filenames.end());

	min = min < 0? 0 : min;
	max = max < filenames.size() && max > min ? max : filenames.size();
	min = min > max? 0 : min;

	for(int i=min; i < max; i++){

		input.open(filenames[i].c_str(), std::ios::in);

		if(!input.good()){
			IERROR("KittiUtility:IMUread", "Could not read file: " + filenames[i]);
			input.close();
			continue;
		}
		input.seekg(0, std::ios::beg);
		//cout << filenames[i] << endl;
		std::string line;
		std::getline(input, line);
		reg.parseLineIMU(line);
		input.close();
	}
}

template <typename PointT>
void KittiUtility::readTimestamps(const std::string& filepath, Registrator<PointT> &reg, int min, int max) {
	std::fstream input(filepath.c_str(), std::ios::in);
	if(!input.good() || !boost::filesystem::is_regular_file(filepath)){
		IERROR("KittiUtility:TimeRead", "Could not read file: " + filepath);
		input.close();
		return;
	}
	input.seekg(0, std::ios::beg);
	std::string line;
	//cout << "---------- Timestamps reading: .... ";

	min = min > max? 0 : min;
	bool b_max = max > min;
	int c_min = -1;

	while(input.good() && !input.eof()){
		std::getline(input, line);
		c_min++;
		if(c_min < min){ continue; }
		else if(c_min >= max && b_max){ break; }
		reg.parseLineTimestamp(line);
	}

	//cout  << "OK" << endl;
	input.close();
}

template<typename PointT>
inline void KittiUtility::readLidarTimestamps(const std::string& filepath,
		Registrator<PointT>& reg, int min, int max) {
	std::string s_times_start = filepath + "/timestamps_start.txt";
	std::string s_times_end = filepath + "/timestamps_end.txt";
	std::string s_times_sample = filepath + "/timestamps.txt";

	std::fstream input_start(s_times_start.c_str(), std::ios::in);
	std::fstream input_end(s_times_end.c_str(), std::ios::in);
	std::fstream input(s_times_sample.c_str(), std::ios::in);
	if(!input.good() || !boost::filesystem::is_regular_file(s_times_sample)
		|| !input_start.good() || !boost::filesystem::is_regular_file(s_times_start)
		|| !input_end.good() || !boost::filesystem::is_regular_file(s_times_end)){
		IERROR("KittiUtility:LidarTimeRead","Lidar Timestamps Read: Could not read file(s) in: " + filepath
				  + "\n\t3 files needed: timestamps.txt, timestamps_start.txt and timestamps_end.txt");
		input.close();
		return;
	}
	input.seekg(0, std::ios::beg);
	std::string line;
	std::string line_start;
	std::string line_end;
	//cout << "---------- Velodyne Timestamps reading: .... ";

	min = min > max? 0 : min;
	bool b_max = max > min;
	int c_min = -1;

	while(input.good() && !input.eof()){
		std::getline(input, line);
		std::getline(input_start, line_start);
		std::getline(input_end, line_end);
		c_min++;
		if(c_min < min){ continue; }
		else if(c_min >= max && b_max){ break; }
		reg.parseLineLidarTimestamps(line_start, line_end, line);
	}

	//cout  << "OK" << endl;
	input.close();
}

template <typename PointT>
void KittiUtility::readCalibration(const std::string& filepath, Registrator<PointT> &reg) {
	std::fstream input(filepath.c_str(), std::ios::in);
	if(!input.good() || !boost::filesystem::is_regular_file(filepath)){
		IERROR("KittiUtility:CalibrationRead", "Calibration Read: Could not read file: " + filepath);
		input.close();
		return;
	}
	input.seekg(0, std::ios::beg);
	std::string line;
	std::stringstream ss;

	std::string dummy;

	Eigen::Matrix4f mat = Eigen::Matrix4f::Identity();

	//cout << "---------- Calibration reading: .... ";

	//Skip the first line is the calibration date, so skip it
	std::getline(input, line);

	//The second line is the rotation matrix... Parse it
	std::getline(input, line);
	ss << line;
	//Drop the "R:" from the beginning of the line
	ss >> dummy;
	//Parse the doubles..
	ss >> mat(0,0);
	ss >> mat(0,1);
	ss >> mat(0,2);
	ss >> mat(1,0);
	ss >> mat(1,1);
	ss >> mat(1,2);
	ss >> mat(2,0);
	ss >> mat(2,1);
	ss >> mat(2,2);

	//The third line should be the translation vector, starts with T:
	std::getline(input, line);
	ss.clear();
	ss << line;
	ss >> dummy;
	ss >> mat(0,3);
	ss >> mat(1,3);
	ss >> mat(2,3);

	//Send it to the registrator
	reg.setIMUtoLidarCalibration(mat);

	//cout << "OK" << endl;
	//cout << mat << endl;
	input.close();
}

#endif
