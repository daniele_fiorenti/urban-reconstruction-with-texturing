/*
 * info.h
 *
 *  Created on: May 10, 2015
 *      Author: george
 */

#ifndef UTILITY_INFO_PRINTER_H_
#define UTILITY_INFO_PRINTER_H_

#include <iostream>
#include <vector>
#include <map>
#include <ctime>
#include <fstream>
#include <sstream>

#include "../config.h"

#ifdef INFO_PRINTER_DEBUG_ON
#define IDEBUG(name, text) Info::debug(name, text)
#else
#define IDEBUG(name, text)
#endif
#define IWARN(name, text) Info::warn(name, text)
#define IPRINT(name, text) Info::info(name, text)
#define IERROR(name, text) Info::error(name, text)
#define IARCHIVE(name, text) Info::archive(name, text)
#define ISHOW_ARCHIVE Info::showArchive()
#define ISAVE_TO_FILE(file, text) Info::printToFile(file, text)

//Questo serve per stampare le info di base delle cloud
#define IARCHIVE_CLOUD(name, cloud)\
	IARCHIVE(TOPIC_CLOUDS, name + std::string(": ") + boost::to_string(cloud->points.size()) + std::string(" points"))

//using namespace std;

class Info {
public:
	static void debug(std::string name, std::string s);
	static void warn(std::string name, std::string s);
	static void info(std::string name, std::string s);
	static void error(std::string name, std::string s);
	static std::string archive(std::string name, std::string info);
	static std::string showArchive();
	static void printToFile(std::string filename, std::string text);
private:
	static std::map<std::string, std::vector<std::string> > info_archive;
};

#endif /* UTILITY_INFO_PRINTER_H_ */
