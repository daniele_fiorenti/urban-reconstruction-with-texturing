/*
 * kitti_utility.h
 *
 *  Created on: Apr 24, 2015
 *      Author: george
 */

#ifndef UTILITY_KITTI_UTILITY_H_
#define UTILITY_KITTI_UTILITY_H_

#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>

#include <pcl/common/common_headers.h>
#include <iostream>
#include <fstream>

#include "../filters/chain_filter.h"
#include "../registration/kitti_registrator.hpp"
#include "mini_profiler.h"
#include "info_printer.h"

/*
 * Fa lettura e scrittura dei file per kitti
 */

class KittiUtility {
public:
	template <typename PointT>
	static typename boost::shared_ptr<pcl::PointCloud<PointT> > loadFromFile(const std::string &pathname);

	template <typename PointT>
	static typename boost::shared_ptr<pcl::PointCloud<PointT> > loadFromFolder(const std::string &pathname,
																			int min = 0, int max = -2);

	template <typename PointT>
	static typename boost::shared_ptr<pcl::PointCloud<PointT> > loadSynchronized(const std::string &folderpath,
			boost::shared_ptr<Registrator<PointT> > registrator
			= boost::shared_ptr<Registrator<PointT> >(new KittiRegistrator<PointT>()), int min = 0, int max = -2);

	template <typename PointT>
	static typename boost::shared_ptr<pcl::PointCloud<PointT> > loadSynchronized(const std::string &velodyne_folder,
			const std::string &imu_folder, const std::string &calibration_filepath,
			boost::shared_ptr<Registrator<PointT> > registrator
			= boost::shared_ptr<Registrator<PointT> >(new KittiRegistrator<PointT>()), int min = 0, int max = -2);

private:
	template <typename PointT>
	static void addPoints(std::fstream& input, boost::shared_ptr<pcl::PointCloud<PointT> > points, int index = -2);

	template <typename PointT>
	static void readIMU(const std::string& folderpath, Registrator<PointT>& reg, int min = 0, int max = -2);

	template <typename PointT>
	static void readTimestamps(const std::string& filepath, Registrator<PointT>& reg, int min = 0, int max = -2);

	template <typename PointT>
	static void readLidarTimestamps(const std::string& filepath, Registrator<PointT>& reg, int min = 0, int max = -2);

	template <typename PointT>
	static void readCalibration(const std::string& filepath, Registrator<PointT>& reg);
};

#endif /* UTILITY_KITTI_UTILITY_H_ */
