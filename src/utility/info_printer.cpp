/*
 * info.cpp
 *
 *  Created on: May 10, 2015
 *      Author: george
 */

#include "info_printer.h"

std::map<std::string, std::vector<std::string> > Info::info_archive;

void Info::debug(std::string name, std::string s) {
	std::cout << "[DEBUG]:[" << name << "]: " << s << "\n";
}

void Info::warn(std::string name, std::string s) {
	std::cout << "[WARNING]:[" << name << "]: " << s << "\n";
}

void Info::info(std::string name, std::string s) {
	std::cout << "[" << name << "]: " << s << "\n";
}

std::string Info::archive(std::string name, std::string info) {
	info_archive[name].push_back(info);
	return info;
}

void Info::error(std::string name, std::string s) {
	std::cerr << "[ERROR]:[" << name << "]: " << s << "\n";
}

std::string Info::showArchive() {
	std::stringstream ss;
	ss << "\n..:: Late Messages: " << std::endl;
	for (std::map<std::string, std::vector<std::string> >::iterator it = info_archive.begin(); it != info_archive.end(); ++it)
	{
		ss << "   ├─  " << it->first << ": \n";
		for(int i=0; i<it->second.size(); i++){
			ss << "   ╷   - " << it->second[i] << "\n";
		}
		ss << "   ╷\n";
	}
	ss << "   └──────────────────────────\n";
	std::cout << ss.str();
	info_archive.clear();
	return ss.str();
}

void Info::printToFile(std::string filename, std::string text) {
	std::ofstream file;
	file.open("results.txt", std::ios::app);
	time_t t = time(0);
	struct tm * now = localtime( & t );
	file << "------------------ " << (now->tm_year + 1900) << '-' << (now->tm_mon + 1) << '-' <<  now->tm_mday
			<< ' ' << now->tm_hour << ':' << now->tm_min << ':' << now->tm_sec << "  ------------------\n";
	file << text << "\n";
	file.close();
}
