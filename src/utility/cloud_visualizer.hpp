#ifndef UTILITY_CLOUD_VISUALIZER_HPP
#define UTILITY_CLOUD_VISUALIZER_HPP

#include "cloud_visualizer.h"

#define MAX_FONT_SIZE 50
#define MIN_FONT_SIZE 10
#define CLAMP(num, min, max)  (num > max ? max : (num < min ? min : num))
#define ABS(x) (x < 0.0 ? -x : x)

inline CloudVisualizer::CloudVisualizer(int width, int height, const std::string & name){
	// Initialize the shared_ptr holding the visualizer
	viewer = boost::shared_ptr<pcl::visualization::PCLVisualizer>(new pcl::visualization::PCLVisualizer (name));
	// Set the initial camera position...
	viewer->setCameraPosition(5.0, -40.0, 20.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0);
	viewer->setSize(width, height);

	// Set the default background color to black, point size and font size
	bg_color[0] = bg_color[1] = bg_color[2] = 0.0f;
	point_size = 1;
	text_size = 22;
	window_width = width;
	window_height = height;
	running = false;
	demo_running = false;

	// Initialize viewports management defaults
	nr_viewports = 0;
}

inline CloudVisualizer::~CloudVisualizer(){

}

template<typename PointT>
inline void CloudVisualizer::showIterationStep(
		boost::shared_ptr<pcl::PointCloud<PointT> > source,
		boost::shared_ptr<pcl::PointCloud<PointT> > target,
		boost::shared_ptr<pcl::PointCloud<PointT> > all,
		unsigned int spin_time) {

	//If already running do not allow showing iterations
	if(running){ return; }

	pcl::visualization::PointCloudColorHandlerCustom<PointT> tgt_h (target, 0, 128, 255);
	pcl::visualization::PointCloudColorHandlerCustom<PointT> src_h (source, 255, 0, 0);
	pcl::visualization::PointCloudColorHandlerCustom<PointT> all_h (all, 255, 255, 255);

	demo_mutex.lock();
	if(!viewer->updatePointCloud<PointT>(target, tgt_h, "target")){
		viewer->addPointCloud (target, tgt_h, "target");
	}
	if(!viewer->updatePointCloud<PointT>(source, src_h, "source")){
		viewer->addPointCloud (source, src_h, "source");
	}
	if(!viewer->updatePointCloud<PointT>(all, all_h, "all")){
		viewer->addPointCloud (all, all_h, "all");
	}

	if(!demo_running){
		demo_running = true;
		demo_mutex.unlock();
		worker = boost::thread(&CloudVisualizer::runDemoOnThread, this, spin_time);
	}else{
		demo_mutex.unlock();
	}
}

template<typename PointT>
inline void CloudVisualizer::showIterationStep(
		boost::shared_ptr<pcl::PointCloud<PointT> > source,
		unsigned int spin_time) {

	//If already running do not allow showing iterations
	if(running){ return; }

	pcl::visualization::PointCloudColorHandlerCustom<PointT> src_h (source, 255, 0, 0);

	demo_mutex.lock();
	if(!viewer->updatePointCloud<PointT>(source, src_h, "source")){
		viewer->addPointCloud (source, src_h, "source");
	}

	if(!demo_running){
		demo_running = true;
		demo_mutex.unlock();
		worker = boost::thread(&CloudVisualizer::runDemoOnThread, this, spin_time);
	}else{
		demo_mutex.unlock();
	}
}

template<>
inline void CloudVisualizer::showIterationStep<pcl::PointNormal>(
		boost::shared_ptr<pcl::PointCloud<pcl::PointNormal> > source,
		unsigned int spin_time) {

	//If already running do not allow showing iterations
	if(running){ return; }

	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal> src_h (source, 255, 0, 0);

	demo_mutex.lock();
	if(!viewer->updatePointCloud<pcl::PointNormal>(source, src_h, "source")){
		viewer->addPointCloud (source, src_h, "source");
	}

	if(!demo_running){
		demo_running = true;
		demo_mutex.unlock();
		worker = boost::thread(&CloudVisualizer::runDemoOnThread, this, spin_time);
	}else{
		demo_mutex.unlock();
	}
}

inline void CloudVisualizer::runDemoOnThread(unsigned int spin_time) {
	demo_mutex.lock();
	while(demo_running){
		if(!viewer->wasStopped()){
			viewer->spinOnce(spin_time);
			demo_mutex.unlock();
			boost::this_thread::sleep (boost::posix_time::microseconds (100 * spin_time));
			demo_mutex.lock();
		}else{
			exit(0);
		}
	}
	demo_mutex.unlock();
}

template <typename PointT>
int CloudVisualizer::addPointCloud(boost::shared_ptr<pcl::PointCloud<PointT> > cloud,
    		const std::string &viewportName, const std::string &details) {

	assert(nr_viewports < VIS_MAX_VIEWPORTS && !running);

	viewports[nr_viewports] = view_ptr(new Viewport<PointT>(-1, cloud, viewportName, details));
	nr_viewports++;
	return nr_viewports;
}


template <typename PointT>
bool CloudVisualizer::updatePointCloud(unsigned int v_id, boost::shared_ptr<pcl::PointCloud<PointT> > cloud,
    		const std::string &viewportName, const std::string &details) {

	assert(!running);

	if(v_id > nr_viewports - 1){
		return false;
	}

	viewports[v_id - 1] = view_ptr(new Viewport<PointT>(-1, cloud, viewportName, details));
	return true;
}


inline void CloudVisualizer::render(unsigned int us_delay, unsigned int spin_time){
	if(!running){
		running = true;

		// Check if there were viewports defined
		assert(nr_viewports > 0);
		demo_mutex.lock();
		if(demo_running){
			demo_running = false;
			demo_mutex.unlock();
			worker.join();
			demo_running = true;
		}else{
			demo_mutex.unlock();
		}
		// Start the thread
		worker = boost::thread(&CloudVisualizer::runOnThread, this, us_delay, spin_time);
	}
	else{
		IERROR("CloudVisualizer", "[::render(...)] called before stopping the visualization");
	}
}

inline void CloudVisualizer::operator()(){
	runOnThread(100000, 100);
}

inline int CloudVisualizer::getNrViewports() {
	return nr_viewports;
}

inline void CloudVisualizer::runOnThread(unsigned int us_delay, unsigned int spin_time){
	if(demo_running){
		//Stop the current representation until the user presses 'q'
		std::cout << "Press Q to show the final result..." << std::endl;
		viewer->spin();
		viewer->resetStoppedFlag();

		//Remove any iterations demo point clouds...
		viewer->removePointCloud ("target");
		viewer->removePointCloud ("source");
		viewer->removePointCloud ("all");

		demo_running = false;
	}
	viewer->addCoordinateSystem (1.0);
	//viewer->initCameraParameters ();

	if(nr_viewports < 4){
		for(int i=0; i < nr_viewports; i++){
			viewer->createViewPort(i/(double)nr_viewports, 0.0, (i+1)/(double)nr_viewports, 1.0, viewports[i]->id);
		}
	}
	else{
		viewer->createViewPort(0.0, 0.5, 0.5, 1.0, viewports[0]->id);
		viewer->createViewPort(0.5, 0.5, 1.0, 1.0, viewports[1]->id);
		viewer->createViewPort(0.0, 0.0, 0.5, 0.5, viewports[2]->id);
		viewer->createViewPort(0.5, 0.0, 1.0, 0.5, viewports[3]->id);
	}

	for(int i = 0; i < nr_viewports; i++){
		viewports[i]->addToViewer(viewer, bg_color, point_size, text_size);
	}

	//viewer->setCameraPosition(5.0, 0.0, 50.0, 5.0, 0.0, -5.0, 0.0, -1.0, 0.0);

	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (spin_time);
		boost::this_thread::sleep (boost::posix_time::microseconds (us_delay));
	}
}

inline void CloudVisualizer::stop(){
	if(running){
		worker.interrupt();
		worker.join();
		running = false;

		//Reset all viewports
		nr_viewports = 0;
	}
}

inline void CloudVisualizer::waitClose(){
	if(running){
		worker.join();
		running = false;

		//Reset all viewports
		nr_viewports = 0;
	}
}

inline void CloudVisualizer::setBackgroundColor(double red, double green, double blue){
	bg_color[0] = CLAMP(red, 0.0, 1.0);
	bg_color[1] = CLAMP(green, 0.0, 1.0);
	bg_color[2] = CLAMP(blue, 0.0, 1.0);
}

inline void CloudVisualizer::setPointSize(double point_s){
	point_size = CLAMP(point_s, 1, 10) ;
}

inline void CloudVisualizer::setTextSize(int font_size){
	text_size = CLAMP(font_size, MIN_FONT_SIZE, MAX_FONT_SIZE);
}

inline void CloudVisualizer::renameViewport(unsigned int id, const std::string &name){
	assert(id - 1 < nr_viewports);
	if(name.length() > 0){
		viewer->updateText(name, 10, 10, text_size, 1.0, 1.0, 1.0, viewports[id - 1]->title);
		viewports[id - 1]->title = name;
	}
}

inline void CloudVisualizer::addDetail(unsigned int viewport_id,
		const std::string& detail) {
	viewports[viewport_id - 1]->details.push_back(detail);
}

inline void CloudVisualizer::IViewport::addToViewer(
		const boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, double b_color[], double p_size, int t_size) {

	int line_start = 40;
	int line_height = (t_size*3)/4;
	int line_bs = 20;

	std::string s_id = boost::to_string(id);

	viewer->setBackgroundColor(b_color[0], b_color[1], b_color[2], id);
	viewer->addText(s_id, 5, 10, 60,
			ABS(b_color[0] - 0.8), ABS(b_color[1] - 0.8), ABS(b_color[2] - 0.8), title + title + s_id, id);

	for(unsigned int i = 0; i < details.size(); i++){
		viewer->addText(details[i], line_start, line_height * (details.size() - i - 1) + line_bs,
				(t_size*2)/3, ABS(b_color[0] - 0.6), ABS(b_color[1] - 0.6), ABS(b_color[2] - 0.6),
				title +"_d_" + boost::to_string(i), id);
	}
	viewer->addText(title, line_start, line_height * (details.size() + 1) + line_bs + line_bs/3, t_size,
			ABS(b_color[0] - 0.4), ABS(b_color[1] - 0.4), ABS(b_color[2] - 0.4), title + "_t_" + s_id, id);
	viewer->addText(description, line_start, line_height * details.size() + line_bs, (t_size * 3)/4,
			ABS(b_color[0] - 0.5), ABS(b_color[1] - 0.5), ABS(b_color[2] - 0.5),
			title + "_D_" + s_id + ":" + description, id);
	viewer->addText("Points: " + boost::to_string(points),
			line_start, 5, 14, ABS(b_color[0] - 0.7), ABS(b_color[1] - 0.7), ABS(b_color[2] - 0.4),
			"Points: " + title + "_p_" + s_id, id);

	addCloudToViewer(viewer);

	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, p_size, pc_name);
}

inline void CloudVisualizer::IViewport::addCloudToViewer(
		const boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer) {

}

template<typename PointT>
inline void CloudVisualizer::Viewport<PointT>::addCloudToViewer(
		const boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer) {

	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ>
	single_color(cloud, 225, 225, 255);
	viewer->addPointCloud<PointT> (cloud, single_color, pc_name, id);
}

template<>
inline void CloudVisualizer::Viewport<pcl::PointXYZI>::addCloudToViewer(
		const boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer) {

	pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZI> color_handler (cloud, "intensity");
	viewer->addPointCloud<pcl::PointXYZI> (cloud, color_handler, pc_name, id);
}

template<>
inline void CloudVisualizer::Viewport<pcl::PointXYZRGB>::addCloudToViewer(
		const boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer) {

	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
	viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, pc_name, id);
}

template<typename PointT>
inline typename boost::shared_ptr<pcl::PointCloud<PointT> > CloudVisualizer::getCloudPtr(
		unsigned int viewport_id) {
	assert(viewport_id - 1 < nr_viewports);
	return boost::static_pointer_cast<Viewport<PointT> >(viewports[viewport_id - 1])->getCloud();
}

#endif
