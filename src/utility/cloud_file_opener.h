/*
 * cloud_file_opener.h
 *
 *  Created on: Jun 25, 2015
 *      Author: george
 */

#ifndef UTILITY_CLOUD_FILE_OPENER_H_
#define UTILITY_CLOUD_FILE_OPENER_H_

#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>
#include <boost/filesystem.hpp>
#include <string>

/*
 * È la vecchia classe che apre i file tipo kitti
 */

namespace cloudIO{

template<typename PointT>
static void loadPoints(std::fstream& input, pcl::PointCloud<PointT> &out_cloud);

template<typename PointT>
static bool openBIN(std::string const &filename, pcl::PointCloud<PointT> &out_cloud){
	std::fstream input(filename.c_str(), std::ios::in | std::ios::binary);
	if(!input.good()){
		std::cerr << "cloudIO::open(...): Corrupt file: " << filename << std::endl;
		input.close();
		return false;
	}
	input.seekg(0, std::ios::beg);

	loadPoints<PointT>(input, out_cloud);
	input.close();
	return true;
}


template<typename PointT>
bool open(std::string const &filename, pcl::PointCloud<PointT> &out_cloud){
	if(!boost::filesystem::exists(filename)){
		std::cerr << "cloudIO::open(...): file " << filename << " does NOT exist" << std::endl;
		return false;
	}
	auto file_extension = boost::filesystem::extension(filename);
	if(file_extension == ".bin"){
		return openBIN(filename, out_cloud);
	}
	else if(file_extension == ".pcd"){
		return pcl::io::loadPCDFile<PointT> (filename, out_cloud) != -1;
	}
	// Add other extensions...
	std::cerr << "cloudIO::open(...): file " << filename << " has UNKNOWN extension" << std::endl;
	return false;
}


/////////////////  SPECIALIZED IMPLEMENTATIONS ////////////////////////////////

template<>
inline void loadPoints<pcl::PointXYZ>(std::fstream& input,
		pcl::PointCloud<pcl::PointXYZ>& out_cloud) {

	float dummy;
	while(input.good() && !input.eof()){
		pcl::PointXYZ point;
		input.read((char *) &point.x, 3*sizeof(float));
		input.read((char *) &dummy, sizeof(float));
		out_cloud.push_back(point);
	}
}

template<>
inline void loadPoints<pcl::PointXYZI>(std::fstream& input,
		pcl::PointCloud<pcl::PointXYZI>& out_cloud) {

	while(input.good() && !input.eof()){
		pcl::PointXYZI point;
		input.read((char *) &point.x, 3*sizeof(float));
		input.read((char *) &point.intensity, sizeof(float));
		out_cloud.push_back(point);
	}
}

template<>
inline void loadPoints<pcl::PointXYZRGB>(std::fstream& input,
		pcl::PointCloud<pcl::PointXYZRGB>& out_cloud) {

	while(input.good() && !input.eof()){
		pcl::PointXYZRGB point;
		input.read((char *) &point.x, 3*sizeof(float));
		input.read((char *) &point.rgb, sizeof(float));
		out_cloud.push_back(point);
	}
}

};  /* namespace cloudIO */
#endif /* UTILITY_CLOUD_FILE_OPENER_H_ */
