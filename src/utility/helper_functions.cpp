/*
 * helper_functions.cpp
 *
 *  Created on: Jun 4, 2015
 *      Author: george
 */

#include "helper_functions.h"

#include "info_printer.h"

#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>

static double constexpr earth_ecuator_radius = 6378137.0;
static double constexpr earth_polar_radius = 6356752.3;
static double constexpr earth_ecuator_sqr_radius = earth_ecuator_radius * earth_ecuator_radius;
static double constexpr earth_polar_sqr_radius = earth_polar_radius * earth_polar_radius;

static double const earth_radius = 6378137.0;

static inline double getEarthRadius(double lat){
	using namespace std;
	double acos2 = earth_ecuator_radius * cos(lat) * earth_ecuator_radius * cos(lat);
	double bsin2 = earth_polar_radius * sin(lat) * earth_polar_radius * sin(lat);
	return sqrt((acos2 * earth_ecuator_sqr_radius + bsin2 * earth_polar_sqr_radius) / (acos2 + bsin2));
}

double lerp(double a, double b, double x){
	return a + x*(b-a);
}

double vector_length(double x, double y, double z){
	return std::sqrt(x*x + y*y + z*z);
}

double angle_lerp(double start, double end, double coeff) {
	double a_norm = start < 0.0 ? math_2pi + start : start;
	double b_norm = end < 0.0 ? math_2pi + end : end;
	double min_a, max_a;
	if(a_norm < b_norm){
		min_a = a_norm;
		max_a = b_norm;
	}
	else{
		min_a = b_norm;
		max_a = a_norm;
	}
	double min_angle = max_a - min_a;
	if(min_angle > math_pi){ min_angle = math_2pi - min_angle;}
	return min_a + min_angle*coeff;
}

void fastPoseMatrixComposition(Eigen::Matrix4d& matrix, double lat, double lon,
		double alt, double roll, double pitch, double yaw, double scale) {
	double cosZ = std::cos(yaw);
	double sinZ = std::sin(yaw);
	double cosY = std::cos(pitch);
	double sinY = std::sin(pitch);
	double cosX = std::cos(roll);
	double sinX = std::sin(roll);

//	matrix(0,0) = cosZ*cosY;
//	matrix(0,1) = cosZ*sinY*sinX - sinZ*cosX;
//	matrix(0,2) = cosZ*sinY*cosX + sinZ*sinX;
//
//	matrix(1,0) = sinZ*cosY;
//	matrix(1,1) = sinZ*sinY*sinX + cosZ*cosX;
//	matrix(1,2) = sinZ*sinY*cosX - cosZ*sinX;
//
//	matrix(2,0) = -sinY;
//	matrix(2,1) = cosY*sinX;
//	matrix(2,2) = cosY*cosX;

//	rx = oxts{i}(4); % roll
//	ry = oxts{i}(5); % pitch
//	rz = oxts{i}(6); % heading
//	Rx = [1 0 0; 0 cos(rx) -sin(rx); 0 sin(rx) cos(rx)]; % base => nav  (level oxts => rotated oxts)
//	Ry = [cos(ry) 0 sin(ry); 0 1 0; -sin(ry) 0 cos(ry)]; % base => nav  (level oxts => rotated oxts)
//	Rz = [cos(rz) -sin(rz) 0; sin(rz) cos(rz) 0; 0 0 1]; % base => nav  (level oxts => rotated oxts)

	Eigen::Matrix4d Rx, Ry, Rz;
	Rx << 	1, 		0, 		0,		0,
			0,		cosX,	-sinX,	0,
			0,		sinX,	cosX,	0,
			0,		0,		0,		1;

	Ry << 	cosY,	0,		sinY,	0,
			0,		1,		0,		0,
			-sinY,	0,		cosY,	0,
			0,		0,		0,		1;

	Rz <<	cosZ,	-sinZ,	0,		0,
			sinZ,	cosZ,	0,		0,
			0,		0,		1,		0,
			0,		0,		0,		1;

	matrix = Rz*Ry*Rx;

//	er = 6378137;
//	mx = scale * lon * pi * er / 180;
//	my = scale * er * log( tan((90+lat) * pi / 360) );

//	matrix(0,3) = scale * lon * earth_radius * math_deg_to_rad;
//	matrix(1,3) = scale * earth_radius * std::log( tan((90.0 + lat) * math_deg_to_rad * 0.5));
	matrix(0,3) = scale * lon * earth_radius * math_pi / 180.0;
	matrix(1,3) = scale * earth_radius * log( tan((90.0 + lat) * math_pi / 360.0));
	matrix(2,3) = alt;

	matrix.row(3) << 0.0f, 0.0f, 0.0f, 1.0f;
}

bool loadTimestampsFromFile(std::vector<pipeline::Timestamp>& timestamps,
		const std::string& filename) {

	std::fstream input;
	input.open(filename.c_str(), std::ios::in);
		if(!input.good() || !boost::filesystem::is_regular_file(filename)){
			IERROR("HelperFunction:TimeRead", "Could not read file: " + filename);
			input.close();
			return false;
		}
		input.seekg(0, std::ios::beg);
		std::string line;

		timestamps.clear();
		std::getline(input, line);
		while(input.good() && !input.eof()){
			//IDEBUG("HelperFunction:TimeRead", "Line read: " + line);
			timestamps.push_back(pipeline::Timestamp(line));
			std::getline(input, line);
		}
		input.close();
		return true;
}
/*
void interpolatePoses(const std::vector<PoseTime>& in_poses1,
		const std::vector<PoseTime>& in_poses2,
		std::vector<PoseTime>& out_poses) {

}

void interpolatePoses(const std::vector<PoseTime>& in_poses,
		std::vector<PoseTime>& in_out_poses) {

}

*/

double stdDevMatrix(cv::Mat matrix){

    std::vector<double> matrix_flat;
    if (matrix.isContinuous()) {
      matrix_flat.assign(matrix.datastart, matrix.dataend);
    } else {
      for (int i = 0; i < matrix.rows; ++i) {
        matrix_flat.insert(matrix_flat.end(), matrix.ptr<uchar>(i), matrix.ptr<uchar>(i)+matrix.cols);
      }
    }

    double sum = std::accumulate(matrix_flat.begin(), matrix_flat.end(), 0.0);
    double mean = sum / matrix_flat.size();

    std::vector<double> diff(matrix_flat.size());
    std::transform(matrix_flat.begin(), matrix_flat.end(), diff.begin(), [mean](double x) { return x - mean; });
//        std::transform(feat_s_flat.begin(), feat_s_flat.end(), diff.begin(), std::bind2nd(std::minus<double>(), mean));
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / (matrix_flat.size()-1));

    return stdev;
}

bool createFolder(const std::string &foldername){
    const char* foldername_c_str = foldername.c_str();
    boost::filesystem::path dir(foldername_c_str);
    if(boost::filesystem::create_directories(dir))
    {
        std::cerr << "Directory Created: " << foldername << std::endl;
        return true;
    }
    return false;
}

bool createFolderFromFilename(const std::string &filename){
    std::string folder = filename.substr(0, filename.find_last_of("/"));
    return createFolder(folder);
}

size_t countFiles(const std::string &foldername){
    size_t count = 0;
    for(boost::filesystem::directory_iterator it(foldername); it != boost::filesystem::directory_iterator(); ++it)
        ++count;
    return count;
}

cv::Mat PoinXYZToMat(pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr) {
    cv::Mat OpenCVPointCloud(3, point_cloud_ptr->points.size(), CV_64FC1);
    for(size_t i=0; i < point_cloud_ptr->points.size();i++){
        OpenCVPointCloud.at<double>(0,i) = point_cloud_ptr->points.at(i).x;
        OpenCVPointCloud.at<double>(1,i) = point_cloud_ptr->points.at(i).y;
        OpenCVPointCloud.at<double>(2,i) = point_cloud_ptr->points.at(i).z;
    }

    return OpenCVPointCloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr MatToPoinXYZ(cv::Mat OpencVPointCloud) {
    /*
             *  Function: Get from a Mat to pcl pointcloud datatype
             *  In: cv::Mat
             *  Out: pcl::PointCloud
             */

    //char pr=100, pg=100, pb=100;
    pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);//(new pcl::pointcloud<pcl::pointXYZ>);

    for(int i=0;i<OpencVPointCloud.cols;i++)
    {
        //std::cout<<i<<endl;

        pcl::PointXYZ point;
        point.x = OpencVPointCloud.at<float>(0,i);
        point.y = OpencVPointCloud.at<float>(1,i);
        point.z = OpencVPointCloud.at<float>(2,i);

        // when color needs to be added:
        //uint32_t rgb = (static_cast<uint32_t>(pr) << 16 | static_cast<uint32_t>(pg) << 8 | static_cast<uint32_t>(pb));
        //point.rgb = *reinterpret_cast<float*>(&rgb);

        point_cloud_ptr -> points.push_back(point);


    }
    point_cloud_ptr->width = (int)point_cloud_ptr->points.size();
    point_cloud_ptr->height = 1;

    return point_cloud_ptr;
}
