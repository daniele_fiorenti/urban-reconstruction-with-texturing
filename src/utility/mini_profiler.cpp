/*
 * mini_profiler.cpp
 *
 *  Created on: May 14, 2015
 *      Author: george
 */

/*
 * Serve a tirare fuori dei tempi (molto approssimati)
 */

#include "mini_profiler.h"

#ifdef PROFILING_ON

std::map<std::string, MiniProfiler::timer*> MiniProfiler::timers;
std::map<std::string, std::vector<MiniProfiler::timer*> > MiniProfiler::archived_timers;
boost::chrono::high_resolution_clock::time_point MiniProfiler::fast_sample;

void MiniProfiler::initTimer(const std::string& name,
		const std::string& description) {
	if(timers.find(name) == timers.end()){
		timer * t = new timer();
		t->name = name;
		t->desc = description;
		timers[name] = t;
	}else{
		timers.erase(name);
		timer * t = new timer();
		t->name = name;
		t->desc = description;
		timers[name] = t;
	}
	archived_timers[name].push_back(timers[name]);
}

void MiniProfiler::startTimer(const std::string& name) {
	timers[name]->time_sample = boost::chrono::high_resolution_clock::now();
}

void MiniProfiler::stopTimer(const std::string& name) {
	fast_sample = boost::chrono::high_resolution_clock::now();
	timers[name]->all_time += fast_sample - timers[name]->time_sample;
	timers[name]->count++;
	timers[name]->time_sample = boost::chrono::high_resolution_clock::now();
}

std::string MiniProfiler::showStatistics() {
	double time;
	double total_time = 0.0;
	std::stringstream ss;
	ss << "\n..:: TIMING STATISTICS" << std::endl;
	for (std::map<std::string, std::vector<timer*> >::iterator it = archived_timers.begin(); it != archived_timers.end(); ++it)
	{
		ss << "   ├─  " << it->first << ": " << std::endl;
		for(int i=0; i<it->second.size(); i++){
			if(it->second[i]->count == 0){ continue; }
			time = (double)it->second[i]->all_time.count()*1e-9;
			total_time += time;
			ss.precision(5);
			ss << "   ╷   └─ " << it->second[i]->desc << std::endl
					  << "   ╷      ├─  Total time:         " << time << " s\n"
					  << "   ╷      └─  Avg Iteration time: " << time/(double)(it->second[i]->count) << " s\n";
			free(it->second[i]);
		}
		ss << "   ╷\n";
	}
	ss << "   └─  Total time: " << total_time  << " s\n";
	timers.clear();
	archived_timers.clear();
	std::cout << ss.str();
	return ss.str();
}

#endif
