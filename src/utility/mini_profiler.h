/*
 * mini_profiler.h
 *
 *  Created on: May 14, 2015
 *      Author: george
 */

#ifndef UTILITY_MINI_PROFILER_H_
#define UTILITY_MINI_PROFILER_H_

#include <iostream>
#include <boost/thread/thread.hpp>
#include <map>
#include "../config.h"

#ifdef PROFILING_ON
#define PROFILER_INIT_TIMER(name, description) \
	MiniProfiler::initTimer(name, description)
#define PROFILER_START_TIMER(name)\
	MiniProfiler::startTimer(name)
#define PROFILER_STOP_TIMER(name)\
	MiniProfiler::stopTimer(name)
#define PROFILER_SHOW_STATISTICS\
	MiniProfiler::showStatistics()

class MiniProfiler {
public:
	static void initTimer(const std::string &name, const std::string &description = "");
	static void startTimer(const std::string &name);
	static void stopTimer(const std::string &name);
	static std::string showStatistics();
private:
	typedef struct TimeNode{
		boost::chrono::high_resolution_clock::time_point time_sample;
		boost::chrono::duration<long int, boost::nano> all_time;
		int count;
		std::string desc;
		std::string name;

		TimeNode(){
			count = 0;
		}
	} timer;
	static std::map<std::string, timer*> timers;
	static std::map<std::string, std::vector<timer*> > archived_timers;
	static boost::chrono::high_resolution_clock::time_point fast_sample;
};

#else
#define PROFILER_INIT_TIMER(name, description)
#define PROFILER_START_TIMER(name)
#define PROFILER_STOP_TIMER(name)
#define PROFILER_SHOW_STATISTICS
#endif

#endif /* UTILITY_MINI_PROFILER_H_ */
