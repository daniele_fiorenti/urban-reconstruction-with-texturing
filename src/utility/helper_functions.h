/*
 * helper_functions.h
 *
 *  Created on: Jun 4, 2015
 *      Author: george
 */

#ifndef REGISTRATION_HELPER_FUNCTIONS_H_
#define REGISTRATION_HELPER_FUNCTIONS_H_

#include "../pipeline/data_types.h"

double const math_deg_to_rad = 3.14159265358979323846/180.0;
double const math_pi = 3.14159265358979323846;
double const math_2pi = 2.0 * math_pi;
double const math_pi_2 = 0.5 * math_pi;

/*
void interpolatePoses(std::vector<PoseTime> const & in_poses1,
					  std::vector<PoseTime> const & in_poses2,
					  std::vector<PoseTime> & out_poses);

void interpolatePoses(std::vector<PoseTime> const & in_poses,
					  std::vector<PoseTime> & in_out_poses);

*/

double lerp(double a, double b, double x);

double angle_lerp(double start, double end, double coeff);

double vector_length(double x, double y, double z);

void fastPoseMatrixComposition(Eigen::Matrix4d& matrix, double lat, double lon, double alt,
							double roll, double pitch, double yaw, double scale = 1.0);

bool loadTimestampsFromFile(std::vector<pipeline::Timestamp>& timestamps, std::string const & filename);

double stdDevMatrix(cv::Mat matrix);
bool createFolder(const std::string & foldername);
bool createFolderFromFilename(const std::string &filename);
size_t countFiles(const std::string &foldername);
cv::Mat PoinXYZToMat(pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr);
pcl::PointCloud<pcl::PointXYZ>::Ptr MatToPoinXYZ(cv::Mat OpencVPointCloud);

#endif /* REGISTRATION_HELPER_FUNCTIONS_H_ */
