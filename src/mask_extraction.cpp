/*
 * mask_extraction.cpp
 *
 *  Created on: June 17, 2016
 *      Author: Daniele
 */

#include <iostream>

#include <pcl/io/ply_io.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>

#include "cloud_cleaning/MovingObjectsRemoval.h"
#include "cloud_cleaning/ChangeDetectionWDST.h"
#include "cloud_cleaning/ChangeDetectionOriginal.h"
#include "filters/MultiFilter.h"

#include "pipeline/Pipeline.h"

#include "data_loading/CalibrationLoader.h"
#include "data_loading/LidarLoader.h"
#include "data_loading/IMULoader.h"
#include "data_loading/PictureLoader.h"
#include "cloud_processing/CloudTransformer.h"
#include "cloud_processing/PoseCalculator.h"
#include "cloud_processing/Registration.h"
#include "cloud_processing/GICPRegistration.h"
#include "cloud_processing/CloudAccumulator.h"
#include "cloud_processing/CloudFilter.h"
#include "cloud_processing/CloudMultiFilter.h"
#include "cloud_processing/PosesSaver.h"
#include "cloud_processing/PosesLoader.h"
#include "cloud_processing/PlainCloudSaver.h"

#include "mask_extraction/MaskExtraction.h"
#include "mask_extraction/DepthMapExtraction.h"

#include "visualization/CloudVisualizerSimple.h"

#include "utility/mini_profiler.h"
#include "utility/helper_functions.h"

#define PI 3.14159265358979

// --------------
// -----Main-----
// --------------
int
main (int argc, char** argv)
{
    using namespace pipeline;
    using namespace data_loading;
    using namespace cloud_processing;
    using namespace cloud_cleaning;
    using namespace visualization;

    PROFILER_INIT_TIMER("Main", "Total execution time");
    PROFILER_START_TIMER("Main");
    std::cout << "Setting up the modules...\n";

    std::string dataset_number = "0091";
    std::string workspace = "/mnt/WinPartition/Users/danie/workspace";
    std::string dataset = workspace + "/dataset_" + dataset_number;
    int start_position = 0;
    int end_position = countFiles(dataset+"/sync/image_00/data")-1;

    float fuse_voxel_radius = 0.1f;

    switch(argc){
    case(2):
        dataset_number = std::string(argv[1]);
        dataset = workspace + "/dataset_" + dataset_number;
        end_position = countFiles(dataset+"/sync/image_00/data")-1;
        break;
    case(3):
        start_position 	= atoi(argv[1]);
        end_position	= atoi(argv[2]);
        break;
    case(4):
        dataset_number = std::string(argv[1]);
        dataset = workspace + "/dataset_" + dataset_number;
        start_position 	= atoi(argv[2]);
        end_position	= atoi(argv[3]);
        break;
    }
    std::string output_folder = "/mnt/WinPartition/Users/danie/Dropbox/Polimi/Thesis/dataset_output/";
    std::string output_path = output_folder+dataset_number+"/output"+"_"+std::to_string(start_position)+"-"+std::to_string(end_position);

    /// TODO: FOR VIDEO: VERY GOOD RESULTS IN CLUTTERED ENVIRONMENT FOR _0091: [100, 155] sigma_r = 0.15, sigma_theta = 0.1
    /// TODO: FOR VIDEO: car and pedestrian example in _0104: [0, 60] sigma_r = 15, sigma_theta = 0.1;
    /// TODO: Look for ground points in _0091: there is a barrier --> possible problem...

    data_loading::CalibrationLoader calib_loader;
    calib_loader.loadCalibrationVeloToCam(workspace + "/calibration/calib_velo_to_cam.txt");
    calib_loader.loadCalibrationImuToVelo(workspace + "/calibration/calib_imu_to_velo.txt");
    calib_loader.loadCalibrationCamToRect(workspace + "/calibration/calib_cam_to_cam.txt");
    calib_loader.computeVeloToImagePlane();

//    PictureLoader *picture_loader = new PictureLoader(dataset+"/sync/image_02/data",
//                                                      dataset+"/sync/image_02/timestamps.txt");

    PictureLoader picture_loader = PictureLoader(dataset+"/sync/image_02/data", dataset+"/sync/image_02/timestamps.txt");
    picture_loader.setFilesRange(start_position, end_position);

    LidarLoader lidar_loader(dataset + "/sync/velodyne_points/data", dataset + "/sync/velodyne_points/timestamps.txt");
//    lidar_loader->setBoundingBoxLimits(15.0f, 20.0f, 2.25f);
    lidar_loader.setFilesRange(start_position, end_position);

    IMULoader imu_loader(dataset + "/sync/oxts/data", dataset + "/sync/oxts/timestamps.txt");
    imu_loader.setFilesRange(start_position, end_position);

    CloudVisualizerSimple visualizer;

    MultiFilter output_final_filter;
    output_final_filter.addVoxelGrid(0.2, 0.2, 0.2, 1, true);

    MultiFilter registration_filter;
    registration_filter.addVoxelGrid(0.075, 0.075, 0.075);

    MultiFilter post_registration_filter;
    post_registration_filter.addCropBox(50.0f, 50.0f, 4.2f);

    CloudMultiFilter cloud_multifilter(post_registration_filter);

    CloudAccumulator cloud_accum;
    cloud_accum.setOutputFilter(output_final_filter);
    cloud_accum.enableDynamicFusion(true, fuse_voxel_radius);
    cloud_accum.enableDiscriminatoryDownsampling(true);
    cloud_accum.registerBundlerFileSaving(output_path + "/bundler/points.out",
                                           output_path + "/bundler/cams_out.txt",
                                           output_path + "/bundler/lidars_out.txt");
    cloud_accum.enableVisualization(&visualizer);

    GICPRegistration registration;
    registration.setVisualizer(&visualizer);
    registration.setMaxIterations(50);
    registration.setMinTransfThreshold(1e-3);
    registration.setMaxCorrispondenceDistance(4.0);

    MovingObjectsRemoval object_removal(5, 0.2f, 2);
    object_removal.setCriticalDistance(4.5f);
    object_removal.setMinPreceedingClouds(3);

    ChangeDetectionWDST change_detector;
    change_detector.enableVisualization(&visualizer);
    change_detector.setSkippedFrames(1);
    change_detector.setMaxPreceedingClouds(10);
    change_detector.setMinPreceedingClouds(10);
    change_detector.enablePostProcess(false, 6);
    change_detector.enableFloorSeparation(true);
    change_detector.enableDynamicRemovedPointsSwapping(true);
    change_detector.enableVoxelizedAlgorithm(true, 0.3, 3);

    change_detector.enableInformationStoring(true, output_path+"/wdst/wdst_info.txt");
    change_detector.enableSavingPointsBinaryFile(true, output_path+"/wdst/rem_points/");
    change_detector.enableSavingRemovedPoints(true, output_path+"/wdst/rem_points_out.txt");
    //change_detector.enableDebug(true);

    double p_theta = PI/180.0;
    change_detector.getParameters().sigma_r = 0.15;
    change_detector.getParameters().sigma_theta = 0.1*p_theta;
    change_detector.getParameters().precompute();

    DepthMapExtraction depth_map_extractor;
    depth_map_extractor.enableSavingDepthImages(true, output_path+"/depths_cpp");

    MaskExtraction mask_extractor;
    mask_extractor.enableSavingMaskImages(true, output_path+"/exp_masks_cpp");
    mask_extractor.enableSavingDiffImages(true, output_path+"/exp_masks_cpp_diff");
    mask_extractor.enablePatchValidation(true, 0.05);
    mask_extractor.enableDepthValidation(true);

    Eigen::Vector3f cam_pos = { 10, 40, 20};
    Eigen::Vector3f view_pos = { 10, 0, 1};

    visualizer.startRendering();
    visualizer.showCoordinateAxes(true);
    visualizer.setCameraPose(cam_pos, view_pos);

    PoseCalculator pose_calculator(false);
    PosesSaver poses_saver(output_path + "/all_lidar_poses.txt");
    CloudTransformer cloud_transformer;
    PlainCloudSaver plain_cloud_saver(output_path+"/plain_clouds/");

    std::cout << "Setting up the pipeline...\n";
    Pipeline pipeLine;
    pipeLine.addModule(&picture_loader);
    pipeLine.addModule(&lidar_loader);
    pipeLine.addModule(&imu_loader);
    pipeLine.addModule(&pose_calculator);
    pipeLine.addModule(&registration);
    pipeLine.addModule(&poses_saver);
//    pipeLine.addModule(&cloud_multifilter);
    pipeLine.addModule(&cloud_transformer);
    pipeLine.addModule(&depth_map_extractor);
    pipeLine.addModule(&change_detector);
    pipeLine.addModule(&mask_extractor);
    pipeLine.addModule(&cloud_accum);
    pipeLine.addModule(&plain_cloud_saver);

    std::cout << "Starting the pipeline...\n";

    pipeLine.init(Pipeline::PARALLEL_AUTO);
//    pipeLine.init(Pipeline::SEQUENTIAL);

    Cloud& final_cloud = cloud_accum.getFinalCloud();

    PROFILER_STOP_TIMER("Main");
    PROFILER_SHOW_STATISTICS;
    IDEBUG("Main", "Received final cloud with " + std::to_string(final_cloud.size()) + " points");

    pcl::io::savePLYFile(output_path+"/final_cloud.ply", final_cloud.cloud(), true);
    IDEBUG("Main", "Saving the bundler file");
    IDEBUG("Main", "Waiting for visualizer to end");
    visualizer.waitStop();


}
