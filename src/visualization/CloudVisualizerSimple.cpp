/*
 * CloudVisualizer.cpp
 *
 *  Created on: Jun 14, 2015
 *      Author: george
 */

#include "CloudVisualizerSimple.h"

#include "../utility/cloud_visualizer.h"
#include "../utility/info_printer.h"

/*
 * Fa la stessa cosa di cloud_visualizer ma questo è più ottimizzato. Viene usato nei moduli
 */

namespace visualization {

static constexpr int duration_millis = 20;

CloudVisualizerSimple::CloudVisualizerSimple(
		unsigned int window_width, unsigned int window_height)
    : _viewer(nullptr), _new_function_to_execute(false), _running(false),
	  _window_width(window_width), _window_height(window_height),
	  _lines_of_text(1)
{	}

CloudVisualizerSimple::~CloudVisualizerSimple() {
	stopRendering();
}

void CloudVisualizerSimple::addPointCloud(const Cloud& cloud,
		const std::string& name, Color::_color_enum ce) {
	addPointCloud(cloud.cloud_shared_ptr(), name, ce);
}

void CloudVisualizerSimple::addPointCloud(const cloud_simple& cloud,
		const std::string& name, Color::_color_enum ce) {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&, ce, name](){
		auto& colour = ColourPalette::pickColourInt(ce);
		pcl_color_handler color_h (cloud, colour.r, colour.g, colour.b);
		if(!_viewer->updatePointCloud<Cloud::pointType>(cloud, color_h, name)){
			_viewer->addPointCloud(cloud, color_h, name);
			_lines_of_text++;
		}
		auto& fcolor = ColourPalette::pickColor(ce);
		_viewer->removeShape(name + "text");
		_viewer->addText("points: " + std::to_string(cloud->size()),
				10, 12 * _lines_of_text, 12,
				fcolor.r, fcolor.g, fcolor.b, name + "text");
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::addPointCloud(const cloud_const_simple& cloud,
		const std::string& name, Color::_color_enum ce) {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&, ce, name](){
		auto& colour = ColourPalette::pickColourInt(ce);
		pcl_color_handler color_h (cloud, colour.r, colour.g, colour.b);
		if(!_viewer->updatePointCloud<Cloud::pointType>(cloud, color_h, name)){
			_viewer->addPointCloud(cloud, color_h, name);
			_lines_of_text++;
		}
		auto& fcolor = ColourPalette::pickColor(ce);
		_viewer->removeShape(name + "text");
		_viewer->addText("points: " + std::to_string(cloud->size()),
				10, 12 * _lines_of_text, 12,
				fcolor.r, fcolor.g, fcolor.b, name + "text");
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::addPointCloud(
		const cloud_with_normals& cloud, const std::string& name,
		Color::_color_enum ce) {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&, ce, name](){
		auto& colour = ColourPalette::pickColourInt(ce);
		pcl_n_color_handler color_h (cloud, colour.r, colour.g, colour.b);
		if(!_viewer->updatePointCloud<pcl::PointNormal>(cloud, color_h, name)){
			_viewer->addPointCloud(cloud, color_h, name);
			_lines_of_text++;
		}
		auto& fcolor = ColourPalette::pickColor(ce);
		_viewer->removeShape(name + "text");
		_viewer->addText("points: " + std::to_string(cloud->size()),
				10, 12 * _lines_of_text, 12,
				fcolor.r, fcolor.g, fcolor.b, name + "text");
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::removePointCloud(
		const std::string& name) {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&, name](){
			_viewer->removePointCloud(name);
			_viewer->removeShape(name + "text");
			_lines_of_text = _lines_of_text > 1? _lines_of_text - 1 : 1;
		}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::setTitle(const std::string& title,
		Color::_color_enum color) {
	auto& colour = ColourPalette::pickColor(color);
	int size = 26;
	int x = _window_width/2 - title.size()*size/2;
	int y = _window_height - size*2;
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->removeShape("title");
		_viewer->addText(title, x, y, size, colour.r, colour.g, colour.b, "title");
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::clearTitle() {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->removeShape("title");
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

std::string CloudVisualizerSimple::addText(const std::string& text, int x,
		int y, int size, Color::_color_enum color) {
	std::string id = text + std::to_string(x) + std::to_string(y) + std::to_string(size);
	auto& colour = ColourPalette::pickColor(color);
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->addText(text, x, y, size, colour.r, colour.g, colour.b, id);
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
	return id;
}

void CloudVisualizerSimple::removeText(const std::string& id) {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->removeShape(id);
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::setBackgroundColor(Color::_color_enum color) {
	auto& color_f = ColourPalette::pickColor(color);
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->setBackgroundColor(color_f.r, color_f.g, color_f.b);
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::setCameraPose(const Eigen::Vector3f& position,
		const Eigen::Vector3f& viewpoint) {
	double p_x = position[0];
	double p_y = position[1];
	double p_z = position[2];
	double v_x = viewpoint[0];
	double v_y = viewpoint[1];
	double v_z = viewpoint[2];
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->setCameraPosition(p_x, p_y, p_z, v_x, v_y, v_z, 0, 0, 1);
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::setCameraParameters(
		const Eigen::Matrix3f& intrinsics, const Eigen::Matrix4f& extrinsics) {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->setCameraParameters(intrinsics, extrinsics);
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::clear() {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->removeAllPointClouds();
		_viewer->removeAllShapes();
		_lines_of_text = 1;
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::startRendering() {
	if(_running){
		IERROR("PointCloudVisualizer", "Unable to start rendering, already rendering");
		return;
	}
	_rendering_thread = std::thread([&](){
		//IDEBUG("Visualizer", "Setting up the pcl visualizer");
		_viewer = new pcl::visualization::PCLVisualizer();
		_viewer->setSize(_window_width, _window_height);
		_viewer->addCoordinateSystem (3.0);
		_viewer->setBackgroundColor(0.1f, 0.1f, 0.1f);
		_viewer->setCameraPosition(5.0, -40.0, 20.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0);

		//IDEBUG("Visualizer", "Starting rendering loop");
		auto duration_ = std::chrono::duration<double, std::milli>(duration_millis);
		while(!_viewer->wasStopped()){
			_viewer->spinOnce(duration_millis);
			std::this_thread::sleep_for(duration_);
			if(_new_function_to_execute){
				std::lock_guard<std::mutex> locker(_mutex);
				while(!_functions.empty()){
					_functions.front()();
					_functions.pop();
				}
				_new_function_to_execute = false;
				_cond_var.notify_all();
			}
		}
		//delete _viewer;
	});
	_running = true;
}

void CloudVisualizerSimple::stopRendering() {
	if(_running){
		_mutex.lock();
		_functions.push(delegate([&](){
			_viewer->close();
		}));
		_new_function_to_execute = true;
		_mutex.unlock();
		_rendering_thread.join();
		_running = false;
	}
}

void CloudVisualizerSimple::showCoordinateAxes(bool enable) {
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		if(enable){
			_viewer->addCoordinateSystem(3.0);
		}else{
			_viewer->removeCoordinateSystem();
		}
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::addMarker(const Cloud::pointType& center,
		const std::string& name, double radius, Color::_color_enum color) {
	std::string id = name + "marker";
	auto c = ColourPalette::pickColor(color);
	std::unique_lock<std::mutex> locker(_mutex);
	_functions.push(delegate([&](){
		_viewer->addSphere(center, radius, c.r, c.g, c.b, id);
	}));
	_new_function_to_execute = true;
	while(_new_function_to_execute){
		_cond_var.wait(locker);
	}
}

void CloudVisualizerSimple::removeMarker(const std::string& name) {
	removeText(name+"marker");
}

void CloudVisualizerSimple::waitStop() {
	IDEBUG("Visualizer", "Waiting termination...");
	if(_running){
		//IDEBUG("Visualizer", "Joining...");
		_rendering_thread.join();
		_running = false;
	}
	IDEBUG("Visualizer", "Finished...");
    exit(EXIT_SUCCESS);
}

};/* namespace visualization */
