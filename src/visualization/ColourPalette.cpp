/*
 * ColourPalette.cpp
 *
 *  Created on: Jun 14, 2015
 *      Author: george
 */

#include "ColourPalette.h"

namespace visualization {

const Color::color ColourPalette::_colours[] =
	{	Color::BLACKf, Color::WHITEf, Color::GREYf, Color::REDf, Color::GREENf, Color::BLUEf, Color::YELLOWf,
		Color::CYANf, Color::PURPLEf, Color::ORANGEf, Color::VIOLETf, Color::EMERALDf };

const Color::color_int ColourPalette::_colours_int[] =
	{	Color::BLACKi, Color::WHITEi, Color::GREYi, Color::REDi, Color::GREENi, Color::BLUEi, Color::YELLOWi,
		Color::CYANi, Color::PURPLEi, Color::ORANGEi, Color::VIOLETi, Color::EMERALDi };

const Color::color& ColourPalette::pickColor(
		Color::_color_enum color_) {
	return _colours[color_];
}

const Color::color_int& ColourPalette::pickColourInt(
		Color::_color_enum color_) {
	return _colours_int[color_];
}

} /* namespace visualization */

