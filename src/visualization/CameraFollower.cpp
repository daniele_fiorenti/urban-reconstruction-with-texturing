/*
 * CameraFollower.cpp
 *
 *  Created on: Jul 19, 2015
 *      Author: george
 */

#include "CameraFollower.h"
#include <pcl/io/ply_io.h>

/*
 * Interpola le posizioni. Serve per vedere la nuvola dall'altro
 */

namespace visualization {

CameraFollower::CameraFollower()
: _fps(10), _cam_height(40.0f), _viewer(nullptr)
{ }

CameraFollower::~CameraFollower() { /* Nothing for now */ }

void CameraFollower::setVisualizer(CloudVisualizerSimple* visualizer) {
	_viewer = visualizer;
}

void CameraFollower::loadCameras(const std::string& cameras_file) {
	std::fstream input(cameras_file.c_str(), std::ios::in);
	if(!input.good() || !boost::filesystem::is_regular_file(cameras_file)){
		std::cerr << "Cameras File Read: Could not read file: " + cameras_file << std::endl;
		input.close();
		return;
	}
	input.seekg(0, std::ios::beg);
	std::string line;
	std::stringstream ss;

	int cameras;

	//First line is the number of cameras...
	std::getline(input, line);
	ss << line;
	ss >> cameras;
	_cam_positions.resize(cameras);

	std::cout << "Loading cameras: " << cameras << "\n";
	float x, y, z;
	//Subsequent lines are camera positions
	for(int i = 0; i < cameras; i++){
		auto& cam = _cam_positions[i];
		std::getline(input, line);
		std::stringstream ss;
		ss << line;
		ss >> x;
		ss >> y;
		ss >> z;
		cam[0] = x; cam[1] = y; cam[2] = z;
		std::cout << cam << std::endl;
	}
}

void CameraFollower::loadPointCloud(const std::string& cloud_ply_file) {
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
	std::cout << "Trying to load cloud from " << cloud_ply_file << "\n";

	pcl::io::loadPLYFile<pcl::PointXYZ> (cloud_ply_file, *cloud);
	std::cout << "Cloud file loaded..\n";

	if(_viewer){
		_viewer->addPointCloud(cloud, "to_follow");
	}else{
		std::cerr << "Could not load cloud, please set the visualizer first!!\n";
	}
}



void CameraFollower::followPath(bool interpolated, float interpolation_step) {
	if(interpolated){
		// Do the interpolated path
		interpolation_step = std::min(std::max(interpolation_step, 0.0f), 1.0f);
		float new_fps = _fps / interpolation_step;
		auto frame_duration = std::chrono::duration<float, std::milli>(1000/new_fps);
		for(int i = 0; i < _cam_positions.size()-2; i++){
			for(float step = 0; step < 1.0f; step += interpolation_step){
				auto cam = interpolate(_cam_positions[i], _cam_positions[i+1], step);
				auto view_p = interpolate(_cam_positions[i+1], _cam_positions[i+2], step);
				cam[2] = _cam_height;
				_viewer->setCameraPose(cam, view_p);
				std::this_thread::sleep_for(frame_duration);
			}
		}
		for(float step = 0; step < 1.0f; step += interpolation_step){
			auto cam = interpolate(_cam_positions[_cam_positions.size()-2], _cam_positions.back(), interpolation_step);
			auto view_p = _cam_positions.back();
			cam[2] = _cam_height;
			_viewer->setCameraPose(cam, view_p);
			std::this_thread::sleep_for(frame_duration);
		}
	}else{
		// Do the simple path
		auto frame_duration = std::chrono::duration<float, std::milli>(1000/_fps);
		for(int i = 0; i < _cam_positions.size()-1; i++){
			auto cam = _cam_positions[i];
			cam[2] = _cam_height;
			_viewer->setCameraPose(cam, _cam_positions[i+1]);
			std::this_thread::sleep_for(frame_duration);
		}
		auto cam = _cam_positions.back();
		cam[2] = _cam_height;
		_viewer->setCameraPose(cam, _cam_positions.back());
	}
}

void CameraFollower::setCameraHeight(float height) {
	_cam_height = height;
}

void CameraFollower::setFrameRate(int fps) {
	_fps = fps;
}

Eigen::Vector3f CameraFollower::interpolate(const Eigen::Vector3f& a,
		const Eigen::Vector3f& b, float step) {
	return a + (b - a)*step;
}

} /* namespace visualization */
