/*
 * CloudVisualizer.h
 *
 *  Created on: Jun 14, 2015
 *      Author: george
 */

#ifndef VISUALIZATION_CLOUDVISUALIZERSIMPLE_H_
#define VISUALIZATION_CLOUDVISUALIZERSIMPLE_H_

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <mutex>
#include <thread>
#include <queue>
#include <string>
#include <condition_variable>
#include <functional>

#include "../pipeline/Cloud.h"
#include "ColourPalette.h"

namespace visualization {
using namespace pipeline;

class CloudVisualizerSimple {
public:
	typedef pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals;
	typedef pcl::PointCloud<Cloud::pointType>::Ptr cloud_simple;
	typedef pcl::PointCloud<Cloud::pointType>::ConstPtr cloud_const_simple;

	CloudVisualizerSimple(unsigned int window_width = 1280, unsigned int window_height = 768);
	CloudVisualizerSimple(const CloudVisualizerSimple& other) = delete;
	~CloudVisualizerSimple();
	
	void addPointCloud(const Cloud& cloud, std::string const &name, Color::_color_enum color = Color::WHITE);
	void addPointCloud(const cloud_simple& cloud, std::string const &name, Color::_color_enum color = Color::WHITE);
	void addPointCloud(const cloud_const_simple& cloud, std::string const &name, Color::_color_enum color = Color::WHITE);
	void addPointCloud(const cloud_with_normals& cloud, std::string const &name, Color::_color_enum color = Color::WHITE);
	void removePointCloud(std::string const &name);

	std::string addText(std::string const & text, int x, int y, int size, Color::_color_enum color = Color::WHITE);
	void removeText(std::string const & id);

	void setTitle(std::string const & title, Color::_color_enum color = Color::WHITE);
	void clearTitle();

	void setBackgroundColor(Color::_color_enum color);
	void setCameraPose(Eigen::Vector3f const &position, Eigen::Vector3f const &viewpoint);
	void setCameraParameters(Eigen::Matrix3f const &intrinsics, Eigen::Matrix4f const &extrinsics);

	void addMarker(const Cloud::pointType& center, std::string const &name,
			double radius = 0.1, Color::_color_enum color = Color::WHITE);
	void removeMarker(std::string const &name);

	void showCoordinateAxes(bool enable);

	void clear();

	void startRendering();
	void stopRendering();
	void waitStop();

private:
	typedef std::function<void()> delegate;
	typedef pcl::visualization::PointCloudColorHandlerCustom<Cloud::pointType> pcl_color_handler;
	typedef pcl::visualization::PointCloudColorHandlerCustom<pcl::PointNormal> pcl_n_color_handler;

	pcl::visualization::PCLVisualizer* _viewer;
	std::queue<delegate> _functions;
	std::mutex _mutex;
	std::condition_variable _cond_var;
	std::thread _rendering_thread;
	bool _new_function_to_execute;
	bool _running;

	// Text management related
	int _lines_of_text;

	unsigned int _window_width, _window_height;
};

} /* namespace visualization */

#endif /* VISUALIZATION_CLOUDVISUALIZERSIMPLE_H_ */
