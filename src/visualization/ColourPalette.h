/*
 * ColourPalette.h
 *
 *  Created on: Jun 14, 2015
 *      Author: george
 */

#ifndef VISUALIZATION_COLOURPALETTE_H_
#define VISUALIZATION_COLOURPALETTE_H_

namespace visualization {

class Color{
private:
	static constexpr float alpha_f = 1.0f;
	static constexpr float half_f = 0.5f;
	static constexpr float full_f = 1.0f;
	static constexpr float zero_f = 0.0f;

	static constexpr unsigned int alpha_i = 255;
	static constexpr unsigned int half_i = 128;
	static constexpr unsigned int full_i = 255;
	static constexpr unsigned int zero_i = 0;

public:
	enum _color_enum {
		BLACK = 0, WHITE = 1, GREY = 2,
		RED = 3, GREEN = 4, BLUE = 5,
		YELLOW = 6, CYAN = 7, PURPLE = 8,
		ORANGE = 9, VIOLET = 10, EMERALD = 11
	};

	struct color_int{
		unsigned int r;
		unsigned int g;
		unsigned int b;
		unsigned int a;
	};

	struct color{
		float r;
		float g;
		float b;
		float a;
	};

	static constexpr color BLACKf 		= {zero_f, zero_f, zero_f, alpha_f };
	static constexpr color WHITEf		= {full_f, full_f, full_f, alpha_f };
	static constexpr color GREYf		= {half_f, half_f, half_f, alpha_f };
	static constexpr color REDf			= {full_f, zero_f, zero_f, alpha_f };
	static constexpr color GREENf		= {zero_f, full_f, zero_f, alpha_f };
	static constexpr color BLUEf		= {zero_f, zero_f, full_f, alpha_f };
	static constexpr color YELLOWf		= {full_f, full_f, zero_f, alpha_f };
	static constexpr color CYANf		= {zero_f, full_f, full_f, alpha_f };
	static constexpr color PURPLEf		= {full_f, zero_f, full_f, alpha_f };
	static constexpr color ORANGEf		= {full_f, half_f, zero_f, alpha_f };
	static constexpr color VIOLETf		= {half_f, zero_f, full_f, alpha_f };
	static constexpr color EMERALDf		= {zero_f, full_f, half_f, alpha_f };

	static constexpr color_int BLACKi	= {	zero_i,	zero_i, zero_i };
	static constexpr color_int WHITEi	= {	full_i, full_i, full_i };
	static constexpr color_int GREYi	= { half_i, half_i, half_i };
	static constexpr color_int REDi		= { full_i, zero_i, zero_i };
	static constexpr color_int GREENi	= { zero_i, full_i, zero_i };
	static constexpr color_int BLUEi	= { zero_i, zero_i, full_i };
	static constexpr color_int YELLOWi	= { full_i, full_i, zero_i };
	static constexpr color_int CYANi	= { zero_i, full_i, full_i };
	static constexpr color_int PURPLEi	= { full_i, zero_i, full_i };
	static constexpr color_int ORANGEi 	= {full_i, half_i, zero_i, alpha_i};
	static constexpr color_int VIOLETi 	= {half_i, zero_i, full_i, alpha_i};
	static constexpr color_int EMERALDi = {zero_i, full_i, half_i, alpha_i};

	static constexpr float BLACKfloat[4]	= {zero_f, zero_f, zero_f, alpha_f};
	static constexpr float WHITEfloat[4]	= {full_f, full_f, full_f, alpha_f};
	static constexpr float GREYfloat[4]		= {half_f, half_f, half_f, alpha_f};
	static constexpr float REDfloat[4]		= {full_f, zero_f, zero_f, alpha_f};
	static constexpr float GREENfloat[4]	= {zero_f, full_f, zero_f, alpha_f};
	static constexpr float BLUEfloat[4]		= {zero_f, zero_f, full_f, alpha_f};
	static constexpr float YELLOWfloat[4]	= {full_f, full_f, zero_f, alpha_f};
	static constexpr float CYANfloat[4]		= {zero_f, full_f, full_f, alpha_f};
	static constexpr float PURPLEfloat[4]	= {full_f, zero_f, full_f, alpha_f};
	static constexpr float ORANGEfloat[4]	= {full_f, half_f, zero_f, alpha_f};
	static constexpr float VIOLETfloat[4]	= {half_f, zero_f, full_f, alpha_f};
	static constexpr float EMERALDfloat[4]	= {zero_f, full_f, half_f, alpha_f};
};

class ColourPalette {
public:
	static Color::color const & pickColor(Color::_color_enum color_);
	static Color::color_int const & pickColourInt(Color::_color_enum color_);

private:
	static const Color::color _colours[];
	static const Color::color_int _colours_int[];
};

} /* namespace visualization */

#endif /* VISUALIZATION_COLOURPALETTE_H_ */
