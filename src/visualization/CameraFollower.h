/*
 * CameraFollower.h
 *
 *  Created on: Jul 19, 2015
 *      Author: george
 */

#ifndef VISUALIZATION_CAMERAFOLLOWER_H_
#define VISUALIZATION_CAMERAFOLLOWER_H_

#include "CloudVisualizerSimple.h"

namespace visualization {

class CameraFollower {
public:
	CameraFollower();
	virtual ~CameraFollower();

	void setVisualizer(CloudVisualizerSimple * visualizer);
	void loadCameras(std::string const &cameras_file);
	void loadPointCloud(std::string const &cloud_ply_file);
	void followPath(bool interpolated, float interpolation_step = 0.5f);
	void setCameraHeight(float height);
	void setFrameRate(int fps);

private:
	CloudVisualizerSimple * _viewer;
	std::vector<Eigen::Vector3f> _cam_positions;
	float _cam_height;
	int _fps;

	Eigen::Vector3f interpolate(Eigen::Vector3f const & a, Eigen::Vector3f const & b, float step);

};

} /* namespace visualization */

#endif /* VISUALIZATION_CAMERAFOLLOWER_H_ */
