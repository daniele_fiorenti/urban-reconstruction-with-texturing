This code has been used in the papers

A. Romanoni, D. Fiorenti, M. Matteucci. "Mesh-based 3D Textured Urban Mapping", In IEEE/RSJ International Conference on  Intelligent Robots and Systems (IROS), 2017
G. Postica, A. Romanoni, M. Matteucci. "Robust moving objects detection in lidar data exploiting visual cues". In IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS), 2016  (pp. 1093-1098).


If you use this code, please cite our work:

@inproceedings{postica2016robust,
  title={Robust moving objects detection in lidar data exploiting visual cues},
  author={Postica, Gheorghii and Romanoni, Andrea and Matteucci, Matteo},
  booktitle={Intelligent Robots and Systems (IROS), 2016 IEEE/RSJ International Conference on},
  pages={1093--1098},
  year={2016},
  organization={IEEE}
}

@inproceedings{romanoni2015incremental,
  title={Mesh-based 3D Textured Urban Mapping},
  author={Romanoni, Andrea and Fiorenti, Daniele and Matteucci, Matteo},
  booktitle={Intelligent Robots and Systems (IROS), 2015 IEEE/RSJ International Conference on},
  year={2017},
  organization={IEEE}
}
